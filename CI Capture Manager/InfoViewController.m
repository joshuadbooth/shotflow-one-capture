//
//  InfoViewController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "InfoViewController.h"
@import QuartzCore;  // for kCAMediaTimingFunctionEaseInEaseOut
#import "QBSyncManager.h"
#import "QBDataImporter.h"
#import "AppDelegate.h"

@interface InfoViewController ()

@property (strong) NSLayoutConstraint *closingConstraint; // layout constraint applied to this view controller when closed
@property float normalHeight;
@property (weak) IBOutlet NSLayoutConstraint *infoViewHeightConstraint;
@property __block BOOL isOffline;
@end


#pragma mark -
@implementation InfoViewController {
    NSColor *sfBlueColor;
    NSColor *sfGreyColor;
    NSColor *greenColor;
    NSColor *lightGreyColor;
}

NSString *kUpdateStatusNotification = @"kUpdateStatusNotification";

@synthesize normalHeight;


- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
    
}

- (void)awakeFromNib
{
    
    normalHeight = self.view.frame.size.height;
    
    _infoViewHeightConstraint.constant = 0;
    
    
    
    
    sfBlueColor = [AppDelegate sharedInstance].sfBlueColor;
    sfGreyColor = [AppDelegate sharedInstance].sfGreyColor;
    greenColor = [NSColor colorWithSRGBRed:0.0 green:0.867 blue:(0.0196) alpha:1];
    lightGreyColor = [NSColor colorWithSRGBRed:0.7 green:0.7 blue:0.7 alpha:1];

    
}


- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateStatusNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kBeganSyncNotification object:[QBSyncManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFinishedSyncNotification object:[QBSyncManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kErrorSyncingNotification object:[QBSyncManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConnectionOnlineNotification object:[QBSyncManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConnectionOfflineNotification object:[QBSyncManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSyncingDisabledNotification object:[QBSyncManager sharedManager]];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSyncingEnabledNotification object:[QBSyncManager sharedManager]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    
    self.view.wantsLayer = YES;
    self.view.layer.backgroundColor = [sfBlueColor CGColor];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(updateStatus:) name:kUpdateStatusNotification object:nil];
    
    [center addObserver:self selector:@selector(startedSyncing:) name:kBeganSyncNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(finishedSyncing:) name:kFinishedSyncNotification object:[QBSyncManager sharedManager]];
    
    [center addObserver:self selector:@selector(errorSyncing:) name:kErrorSyncingNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(connectionOffline:) name:kConnectionOfflineNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(connectionOnline:) name:kConnectionOnlineNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(syncingDisabled:) name:kSyncingDisabledNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(syncingEnabled:) name:kSyncingEnabledNotification object:[QBSyncManager sharedManager]];
}

#pragma mark -

- (void) updateStatus:(NSNotification*) note {
    if (note.userInfo)
    {
        [self updateListViewStatus:[note.userInfo valueForKey:@"statusText"]];
    }
}

- (void) updateListViewStatus:(NSString *)statusText {
    InfoViewController __weak *weakSelf =  self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.syncStatus setStringValue:statusText];
    });
}

- (void) startedSyncing:(NSNotification *) note {
    if (note.object == [QBSyncManager sharedManager]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.syncProgress setHidden:NO];
            [self.syncProgress startAnimation:self];
            
            [self.syncStatus setStringValue:@"Syncing..."];
            [self.syncButton setHidden:YES];
            [self fadeToBlue];

            
        });
        
    }
}

- (void) finishedSyncing:(NSNotification *) note {
    
    if (note.object == [QBSyncManager sharedManager]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.syncProgress stopAnimation:self];
            [self.syncProgress setHidden:YES];
            [self.syncStatus setStringValue:@"Sync Complete."];
            [_syncButton setHidden:YES];
            [self fadeToGreen];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self fadeToBlue];
                self.syncStatus.stringValue = [NSString stringWithFormat:@"Updated from Server: %@",[[QBSyncManager sharedManager] lastImportDateString]];
            });
            
           
            
            
        });
    }
}

- (void) errorSyncing:(NSNotification *) note {
    if (note.object == [QBSyncManager sharedManager]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.syncProgress setHidden:YES];
            [self.syncProgress stopAnimation:self];
            [self.syncStatus setStringValue:@"Error Syncing."];
            [self.syncButton setHidden:NO];
            [self.syncButton setEnabled:YES];
            [self fadeToRed];
        });
    }
}

- (void) syncingDisabled:(NSNotification *) note {
    if (note.object == [QBSyncManager sharedManager]) {
        self.isOffline = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [self.syncProgress setHidden:YES];
            [self.syncProgress stopAnimation:self];
            [self.syncStatus setStringValue:@"Syncing Disabled."];
            [self.syncButton setHidden:NO];
            [self.syncButton setEnabled:YES];
            [self fadeToRed];
        });
    }
}
- (void) syncingEnabled:(NSNotification *) note {
    if (note.object == [QBSyncManager sharedManager]) {
        self.isOffline = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [self.syncProgress setHidden:YES];
            [self.syncProgress stopAnimation:self];
            [self.syncStatus setStringValue:@"Syncing Enabled."];
            [self.syncButton setHidden:NO];
            [self.syncButton setEnabled:YES];
            [self fadeToGrey];
        });
    }
}

- (void) connectionOffline:(NSNotification *) note {
    if (note.object == [QBSyncManager sharedManager]) {
        self.isOffline = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
            [self.syncProgress setHidden:YES];
            [self.syncProgress stopAnimation:self];
            [self.syncStatus setStringValue:@"Connection Offline."];
            [self.syncButton setHidden:NO];
            [self.syncButton setEnabled:YES];
            [self fadeToRed];
            
            
        });
    }
}

- (void) connectionOnline:(NSNotification *) note {
    
    if (note.object == [QBSyncManager sharedManager]) {
        
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.syncProgress setHidden:YES];
                [self.syncProgress stopAnimation:self];
                
                [self.syncStatus setStringValue:@"Connection Online."];
                
                [self.syncButton setHidden:NO];
                [self.syncButton setEnabled:YES];
                [self fadeToGrey];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.syncStatus setStringValue:[NSString stringWithFormat:@"Last Sync: %@",[[QBSyncManager sharedManager] lastImportDateString]]];
                });
                 _isOffline = NO;
            });
    
    }
}

#pragma mark - background fades

- (void) fadeToRed {
    __block CALayer *layer = self.view.layer;
    dispatch_async(dispatch_get_main_queue(), ^{
        CABasicAnimation *fadeAnim = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
        fadeAnim.toValue = ([NSColor redColor]);
        fadeAnim.duration = .25;
        [self.view.layer addAnimation:fadeAnim forKey:@"backgroundColor"];
        self.view.layer.backgroundColor = [[NSColor redColor] CGColor];
    });
    
}

- (void) fadeToGreen {
    __block CALayer *layer = self.view.layer;
    dispatch_async(dispatch_get_main_queue(), ^{
        CABasicAnimation *fadeAnim = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
        fadeAnim.toValue = sfBlueColor;
        fadeAnim.duration = .25;
        [layer addAnimation:fadeAnim forKey:@"backgroundColor"];
        layer.backgroundColor = [sfBlueColor CGColor];
    });
}

- (void) fadeToBlue {
    __block CALayer *layer = self.view.layer;
    dispatch_async(dispatch_get_main_queue(), ^{
        CABasicAnimation *fadeAnim = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
        
        fadeAnim.toValue = sfBlueColor;
        fadeAnim.duration = .25;
        [layer addAnimation:fadeAnim forKey:@"backgroundColor"];
        
        layer.backgroundColor = [sfBlueColor CGColor];
    });
}

- (void) fadeToGrey {
    __block CALayer *layer = self.view.layer;
    dispatch_async(dispatch_get_main_queue(), ^{
        CABasicAnimation *fadeAnim = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
        
        fadeAnim.toValue = sfGreyColor;
        fadeAnim.duration = .25;
        [layer addAnimation:fadeAnim forKey:@"backgroundColor"];
        
        layer.backgroundColor = [sfGreyColor CGColor];
    });
}



- (IBAction)trySync:(id)sender {
    [[QBDataImporter sharedImporter] setInitialLoad:NO];
    [[QBSyncManager sharedManager] startSyncing];
}
@end
