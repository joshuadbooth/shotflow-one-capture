//
//  ShotTableCellView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/3/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "Shot.h"
#import "SetList.h"
#import "QBDBInfo.h"
#import "AppDelegate.h"
#import "DetailViewWindowController.h"
#import "C1ProController.h"

#import "QBDataExporter.h"

#import "M1TableRowView.h"
#import "M1TableViewCell.h"
#import "RowSelectionButton.h"

//@class Shot, SetList;

@interface ShotTableCellView : M1TableViewCell

@property (strong) IBOutlet NSStackView *iconStackView;

@property (strong) IBOutlet RowSelectionButton *alertButton;
@property (strong) IBOutlet RowSelectionButton *completeButton;
@property (strong) IBOutlet RowSelectionButton *linkedButton;

@property (nonatomic, weak) Shot *shot;

- (IBAction)showInfo:(id)sender;
- (IBAction)markCompleted:(id)sender;


- (void) assignShot:(Shot*)s;
- (void) registerForObservations;
- (void) setupCellView;

@end
