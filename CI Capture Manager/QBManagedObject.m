//
//  QBManagedObject.m
//  QBApiTest2
//
//  Created by Josh Booth on 7/9/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "QBManagedObject.h"
#import <Cocoa/Cocoa.h>

#import "RXMLElement.h"
#import "QBTable.h"
#import "QBField.h"

@implementation QBManagedObject
@dynamic record_id;
@dynamic date_created;
@dynamic date_modified;
@dynamic update_id;
@dynamic name;
@dynamic status;

@dynamic params;
@dynamic previousParams;
@dynamic fieldIDs;
@dynamic online;
@dynamic dictionaryValues;

@dynamic tableName;
@dynamic primaryKeyValue;

@dynamic shouldUpdate;



static NSString * _QBMOContext = @"_QBMOContext";


#pragma mark - Easy Creation - 
- (NSString *) tableName {
    return @"QBMO";
}

+ (NSString *)entityName {
    return @"QBMO";
}


- (id) primaryKeyValue {
    QBTable *parentTable = [QBTable tableNamed:self.tableName inMOC:self.managedObjectContext];
    QBField *primaryKey = parentTable.primaryKey;
    
    return [self.params valueForKey:primaryKey.name];
}


+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context
{
    id obj = [NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                         inManagedObjectContext:context];
    NSError *permIDError = nil;
    [context obtainPermanentIDsForObjects:@[obj] error:&permIDError];
    
    
    
    return obj;
}

#pragma mark - Data Loading -

-(void) awakeFromInsert {
    [super awakeFromInsert];
    
    if (self.params == nil) {
        self.params = [NSMutableDictionary dictionary];
    }
    
    if (self.fieldIDs == nil) {
        self.fieldIDs = [NSMutableDictionary dictionary];
    }
    
}

-(void) awakeFromFetch {
    [super awakeFromFetch];
    
    if (self.params == nil) {
        self.params = [NSMutableDictionary dictionary];
    }
    
    if (self.fieldIDs == nil) {
        self.fieldIDs = [NSMutableDictionary dictionary];
    }
}


-(NSDictionary*) dictionaryValues {
    NSArray *keys = [[[self entity] attributesByName] allKeys];
    return [self dictionaryWithValuesForKeys:keys];
}

- (void) populateParams:(RXMLElement *)record{
    
    self.previousParams = [NSDictionary dictionaryWithDictionary:self.params];
    
    [record iterate:@"f" usingBlock:^(RXMLElement *param) {
        NSString* paramKey = [self.fieldIDs valueForKey:[param attribute:@"id"]];
        RXMLElement* paramValue = param;
        [self setParam:paramValue.text forKey:paramKey];

    }];

}

- (void) updateWithRecordData:(RXMLElement *)record {
    self.record_id = [self recordAsNSNumber:[[self.params valueForKey:@"Record ID#"] intValue]];
    self.date_created = [self convertDateFromImport:[self.params valueForKey:@"Date Created"]];
    self.date_modified = [self convertDateFromImport:[self.params valueForKey:@"Date Modified"]];
    
}


- (NSDictionary *) getPropertiesToPOST {
    return [self getChangedParamsForPOST];
}

- (NSDictionary *) getChangedParamsForPOST {
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    
    NSMutableArray *changedParams = [NSMutableArray arrayWithArray:[self getUpdatedParams]];
    NSArray *illegalFields = [[[QBTable tableNamed:self.tableName] fields_illegal] valueForKeyPath:@"@distinctUnionOfObjects.name"];

    NSLog(@"illegalFields: %@", illegalFields);
    
    [changedParams enumerateObjectsUsingBlock:^(id  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([illegalFields containsObject:key]) return;
        
        if ([self.params objectForKey:key]) {
            id val = [self.params objectForKey:key];
            if (![[self.params objectForKey:key] isEqualTo:@""]){
                [postParams setValue:[self.params valueForKey:key] forKey:[NSString stringWithFormat:@"_fid_%@",[self getFieldIDForField:key]]];
            } else {
                NSLog(@"blank value!");
                [postParams setValue:@"" forKey:[NSString stringWithFormat:@"_fid_%@",[self getFieldIDForField:key]]];
            }
        } else {
            NSLog(@"No value!");
        }
    }];
   
    
    NSLog(@"postParamsWithKeys: %@", changedParams);
    return postParams;
   
}
/*
- (NSDictionary *) getChangedParamsForPostIgnoringFields:(NSArray *)ignoreFields {
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    
    NSMutableArray *changedParams = [NSMutableArray arrayWithArray:[self getUpdatedParams]];
    
    [changedParams enumerateObjectsUsingBlock:^(id  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([self.params objectForKey:key]) {    
            if (![[self.params objectForKey:key] isEqualTo:@""]){
                [postParams setValue:[self.params valueForKey:key] forKey:[NSString stringWithFormat:@"_fid_%@",[self getFieldIDForField:key]]];
            }
        }
    }];
    
    DDLogVerbose(@"postParamsWithKeys: %@", changedParams);
    return postParams;
    
}
*/

- (id) findInManagedObjectContext:(NSManagedObjectContext *)context error:(NSError**)error {
    id obj = [context existingObjectWithID:self.objectID error:error];
    return obj;
}

#pragma mark -

- (NSNumber *) recordAsNSNumber: (NSInteger)num {
    return [NSNumber numberWithInteger:num];
}

#pragma mark - Date Conversion
- (NSString*) convertDateForPOST:(NSDate *)date {
    
    //    DDLogVerbose(@"Date To Convert: %@", date);
    NSString *dateString = nil;
    long newInt = [date timeIntervalSince1970] * 1000;
    dateString = [NSString stringWithFormat:@"%ld", newInt];
    //    DDLogVerbose(@"Converted String: %@", dateString);
    
    return dateString;
}

-(NSString*) prettyDate:(NSDate*) date{

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];

    [format setTimeZone:[NSTimeZone systemTimeZone]];
    [format setDateStyle: NSDateFormatterMediumStyle];
    [format setTimeStyle: NSDateFormatterMediumStyle];
    [format setCalendar:calendar];
    
    return [format stringFromDate:date];
}

- (NSString*) xmpDate:(NSString*)dateString {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    double seconds = dateString.doubleValue;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(seconds/1000)];

    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    [format setTimeZone:[NSTimeZone systemTimeZone]];
    [format setDateStyle: NSDateFormatterShortStyle];
    [format setTimeStyle: NSDateFormatterNoStyle];
    [format setCalendar:calendar];
    [format setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MM/dd/YYYY" options:0 locale:nil]];
   // DDLogVerbose(@"XMPDate: %@", [format stringFromDate:date]);
    return [format stringFromDate:date];
}

- (NSDate*) convertDateFromImport:(NSString*)dateString {
    
    double seconds = dateString.doubleValue;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(seconds/1000)];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
    [format setDateStyle: NSDateFormatterMediumStyle];
    [format setTimeStyle: NSDateFormatterMediumStyle];
    [format setCalendar:calendar];
    
//        DDLogVerbose(@"Converted Date: %@", [format stringFromDate:date]);
    return date;
}

- (NSComparisonResult) compareModDateToRecord:(RXMLElement*) record {
    NSDate *myDate = [self date_modified];
    NSDate *compareDate = [self convertDateFromImport:[[self getCompareParams:record] valueForKey:@"Date Modified"]];
//    DDLogVerbose(@"myDate:      %@", [self prettyDate:myDate]);
//    DDLogVerbose(@"compareDate: %@", [self prettyDate:compareDate]);
    
   return [myDate compare:compareDate];
    
}





- (NSMutableDictionary*) getCompareParams:(RXMLElement*) record {
    NSMutableDictionary *compareParams = [NSMutableDictionary dictionary];
    
    for (RXMLElement* param in [record children:@"f"]) {
        NSString* paramKey = [self.fieldIDs valueForKey:[param attribute:@"id"]];
        RXMLElement* paramValue = param;
        
         [compareParams setValue:paramValue.text forKey:paramKey];
    }
    
    return compareParams;
}


- (NSString *) getFieldIDForField:(NSString*)fieldName {
//    DDLogVerbose(@"self.fieldIDs: %@", self.fieldIDs);
//    DDLogVerbose(@"fieldID: %@ - %@", [[self.fieldIDs allKeysForObject:fieldName] firstObject], fieldName);
    return [[self.fieldIDs allKeysForObject:fieldName] firstObject];
}


-(NSMutableDictionary*) compareToRecord:(RXMLElement *)record {
    NSMutableDictionary *compareParams = [self getCompareParams:record];
    __block NSMutableDictionary *differences = [NSMutableDictionary dictionary];
    
    if ([self.params isEqualToDictionary:compareParams]){
        return nil;
    } else {
        DDLogVerbose(@"Comparing records for %@ with RID: %@", [self class], self.record_id);
        [compareParams enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
            // do something with key and obj
            if (![[self.params valueForKey:key] isEqualTo:[compareParams valueForKey:key]]){
//                DDLogVerbose(@"\n\n");
//                DDLogVerbose(@"key: %@", key);
//                DDLogVerbose(@"local: %@", [self.params valueForKey:key]);
//                DDLogVerbose(@"remote: %@\n\n", [compareParams valueForKey:key]);
                [differences setValue:[compareParams valueForKey:key] forKey:key];
            }
        }];
        // DDLogVerbose(@"total differences: %@", differences);
        return differences;
    }
}

-(NSArray*) getUpdatedParams {
    __block NSMutableDictionary *differences = [NSMutableDictionary dictionary];
    
    //self.previousParams = nil;
    
    NSMutableArray *changedParams = [NSMutableArray array];
    
    if ([self.params isEqualToDictionary:self.previousParams]) {
        NSLog(@"Params & PreviousParams are identical.");
        return nil;
    }
    else if (self.previousParams == nil){
        NSLog(@"No previousParams to compare to. Adding all params & values");
        [self.params enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
            [differences setValue:[self.params valueForKey:key] forKey:key];
        }];
    }
    else {
//        DDLogVerbose(@"Comparing records for %@ with RID: %@", [self class], self.record_id);
        [self.params enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
//            DDLogVerbose(@"\n\n");
//            DDLogVerbose(@"Key: %@", key);
//            DDLogVerbose(@"current: %@", [self.params valueForKey:key]);
//            DDLogVerbose(@"previous: %@", [self.previousParams valueForKey:key]);
            
            // Value for Current Param AND No value for Previous Params
            if (([self.params objectForKey:key]) && (![self.previousParams objectForKey:key])) {
                [differences setValue:@{@"Previous" : [NSNull null],
                                        @"Current" : [self.params valueForKey:key]
                                        }
                               forKey:key];
                
            }
            // No value for Current Params but exists for Previous Params
            else if (([self.previousParams objectForKey:key]) && (![self.params objectForKey:key])) {
                [differences setValue:@{@"Previous" : [self.previousParams valueForKey:key],
                                        @"Current" : [NSNull null]
                                        }
                               forKey:key];
            }
            // If the values are equal, do nothing.
            else if ([[self.params valueForKey:key] isEqualTo:[self.previousParams valueForKey:key]]){
                NSLog(@"paramValues are equal");
            }
            // The values aren't equal, so mark the change.
            else {

                [differences setValue:@{@"Previous": [self.previousParams valueForKey:key],
                                        @"Current": [self.params valueForKey:key]}
                               forKey:key];
            }
            
        }];
    }
    
    // Clean up
    changedParams = [NSMutableArray arrayWithArray:[differences allKeys]];
    
    QBTable *table = [QBTable tableNamed:self.tableName];
    
    NSArray *illegalFields = [table.fields_illegal valueForKeyPath:@"@distinctUnionOfObjects.name"];
    
    NSArray *checkParams = [NSArray arrayWithArray:changedParams];
    
    for (NSString* param in checkParams) {
        if ([illegalFields containsObject:param]) {
            [changedParams removeObject:param];
        }
    }
    
    /*
    if ([changedParams containsObject:@"Record ID#"]) [changedParams removeObject:@"Record ID#"];
    if ([changedParams containsObject:@"Date Modified"]) [changedParams removeObject:@"Date Modified"];
    if ([changedParams containsObject:@"Date Created"]) [changedParams removeObject:@"Date Created"];
    if ([changedParams containsObject:@"Last Modified By"]) [changedParams removeObject:@"Last Modified By"];
    */
    
    
    NSDictionary *returnDict = [differences dictionaryWithValuesForKeys:changedParams];
     NSLog(@"total differences: %@", returnDict);
    return changedParams;
}

- (void) setParam:(id)param forKey:(NSString *)key {
    
     NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:self.params];
  
    [tempParams setValue:param forKey:key];
    
     [self willChangeValueForKey:@"params"];
      self.params = [NSMutableDictionary dictionaryWithDictionary:tempParams];
    //[self.params setValue:param forKey:key];
     [self didChangeValueForKey:@"params"];
      tempParams = nil;
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)theKey {
    
    BOOL automatic = NO;
    if (([theKey isEqualToString:@"params"]) || ([theKey isEqualToString:@"changedParams"])){
        automatic = NO;
    }
    else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    return automatic;
}

@end
