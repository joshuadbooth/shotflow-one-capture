//
//  AppDelegate.m
//  CI Capture Manager
//
//  Created by Josh Booth on 7/9/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "AppDelegate.h"

#import "Objective-Zip.h"


#import "LoginController.h"
#import "SetRegistrationController.h"
#import "ListViewController.h"

#import "DetailViewWindowController.h"




#import "QBDBInfo.h"
#import "QBSyncManager.h"
#import "QBDataImporter.h"
#import "QBDataExporter.h"
#import "DummyObject.h"

#import "BooleanValueTransformer.h"

@interface AppDelegate () <NSURLConnectionDelegate>

@property (weak) IBOutlet NSWindow *window;
@property BOOL ADMIN_MODE;
@end

//static const DDLogLevel ddLogLevel = DDLogLevelVerbose;



@implementation AppDelegate {
    NSString *logfilePath;
    NSString *appDBID;
}
NSString *kSelectionDidChangeNotification = @"kSelectionDidChangeNotification";
NSString *kUpdateShotNotification = @"kUpdateShotNotification";
NSString* kDV_AddShotNotification = @"kDV_AddShotNotification";
NSString* kStatusFilterChangedNotification = @"kStatusFilterChangedNotification";

//@synthesize defaultSorting;
@synthesize sortByRecordID;
@synthesize sortByPrimaryKeyValue;
@synthesize sortByPriority;
@synthesize sortByName;
@synthesize sortByTitle;
@synthesize sortByCreationDate;
@synthesize sortByModDate;

@synthesize selectedSet = _selectedSet;
@synthesize selectedShot= _selectedShot;
@synthesize selectedSetList = _selectedSetList;

@synthesize sfBlueColor;
@synthesize sfGreyColor;

@synthesize turnSyncOffBtn;
@synthesize turnSyncOnBtn;


@synthesize hiddenStatuses;
@synthesize hiddenStatusesForImport;
@synthesize hiddenStatusPredicate;
//@synthesize statusesToHideInUI;

@synthesize uiStatusPredicate;
@synthesize filteredChildrenPredicate;
@synthesize searchFieldPredicate;

//@synthesize outlineView;

@synthesize loginView;
@synthesize setRegistrationView;
@synthesize listView;
@synthesize detailView;
@synthesize ADMIN_MODE;
@synthesize sampleMode;

@synthesize hideCompleted;
@synthesize hideInProgress;
@synthesize hideReady;

#pragma mark -

+ (instancetype) sharedInstance {
    __strong static id _sharedInstance  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self allocForReal] initForReal];
    });
    return _sharedInstance;
}

+ (id) allocForReal { return [super allocWithZone:NULL]; }
+ (id) alloc { return [self sharedInstance]; }
+ (id) allocWithZone:(NSZone *)zone { return [self sharedInstance]; }
- (id) init {  return self; }

- (id) initForReal {
    self = [super init];
    
    sortByRecordID = [[NSSortDescriptor alloc] initWithKey:@"record_id" ascending:YES];
    sortByPrimaryKeyValue = [[NSSortDescriptor alloc] initWithKey:@"primaryKeyValue" ascending:YES];
    sortByName = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    sortByTitle = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    sortByPriority = [[NSSortDescriptor alloc] initWithKey:@"priorityNum" ascending:NO];
    sortByCreationDate = [[NSSortDescriptor alloc] initWithKey:@"date_created" ascending:YES];
    sortByModDate = [[NSSortDescriptor alloc] initWithKey:@"date_modified" ascending:YES];
    
    sfBlueColor = [NSColor colorWithSRGBRed:(CGFloat)(0.0) green:(CGFloat)(108.0/255.0) blue:(CGFloat)(183.0/255.0) alpha:1];
    sfGreyColor = [NSColor colorWithSRGBRed:(CGFloat)(95.0/255.0) green:(CGFloat)(105.0/255.0) blue:(CGFloat)(107.0/255.0) alpha:1];
    
    if (self.statusesToHideInUI == nil) self.statusesToHideInUI = @[];
    
    self.hideCompleted = NO;
    self.hideReady = NO;
    self.hideInProgress = NO;
    
    return self;
}



- (NSColor*) randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    NSColor *color = [NSColor colorWithHue:hue saturation:saturation brightness:brightness alpha:.5];
    return color;
}

- (NSArray *) statusesToHideInUI {
    NSMutableArray *array = [NSMutableArray arrayWithArray:@[@"Awaiting Assignment", @"Assigned", @"Awaiting Samples"]];
    if (self.hideCompleted)
    {
        [array addObject:@"Completed"];
    }
    if (hideInProgress)
    {
        [array addObject:@"In Progress"];
    }
    if (hideReady)
    {
        [array addObject:@"Ready"];
    }
    
    return [NSArray arrayWithArray:array];
}

- (NSPredicate *) uiStatusPredicate {
    return [NSPredicate predicateWithFormat:@"(status in %@)", self.statusesToHideInUI];
    //return self.filteredChildrenPredicate;
}

- (NSCompoundPredicate*) filteredChildrenPredicate
{
    NSPredicate *uiChildStatusPredicate = [NSPredicate predicateWithFormat:
                                           @"NOT(status in %@) OR \
                                           (SUBQUERY(self.children, $child, NOT($child.status IN %@) OR \
                                           (SUBQUERY($child.children, $subChild, ($subChild.status IN %@)).@count < $child.children.@count)).@count > 0) \
                                           ",
                                           self.statusesToHideInUI, self.statusesToHideInUI, self.statusesToHideInUI];
    
    return [NSCompoundPredicate andPredicateWithSubpredicates:@[uiChildStatusPredicate, self.searchFieldPredicate]];
    
    /*
     NSPredicate *uiChildStatusPredicate = [NSPredicate predicateWithFormat:
     @"NOT(status in %@) OR \
     (SUBQUERY(children, $child, NOT($child.status IN %@) OR \
     (NOT $child.children.status IN %@)).@count > 0)",
     self.statusesToHideInUI, self.statusesToHideInUI, self.statusesToHideInUI];
     */
    
    /*
     [NSPredicate predicateWithFormat:
     @"NOT(status in %@) OR \
     (SUBQUERY(self.children, $child, NOT($child.status IN %@) OR \
     (SUBQUERY($child.children, $subChild, ($subChild.status IN %@)).@count < $child.children.@count)).@count > 0) \
     ",
     self.statusesToHideInUI, self.statusesToHideInUI, self.statusesToHideInUI];
     */
}



#pragma mark - Main Functions -

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    

    [DDLog addLogger:[DDASLLogger sharedInstance]];
//    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init];
    fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
    fileLogger.logFileManager.maximumNumberOfLogFiles = 3;
    [DDLog addLogger:fileLogger];
    
    logfilePath = [fileLogger.currentLogFileInfo.filePath stringByDeletingLastPathComponent];
    
    DDLogVerbose(@"");
    DDLogVerbose(@"Application Finished Launching");
    

    NSProcessInfo *pInfo = [NSProcessInfo processInfo];
    NSString *version = [pInfo operatingSystemVersionString];
    
    NSDictionary *cmInfo = [[NSBundle mainBundle] infoDictionary];
    
    DDLogVerbose(@"Capture Module Version: %@ build %@", [cmInfo valueForKey:@"CFBundleShortVersionString"], [cmInfo valueForKey:@"CFBundleVersion"]);
    DDLogVerbose(@"HostName: %@", [pInfo hostName]);
    DDLogVerbose(@"OS Version: %@", version);
    DDLogVerbose(@"SerialNumber: %@", [self getSystemSerialNumber]);
    DDLogVerbose(@"UUID: %@", [self getSystemUUID]);
    DDLogVerbose(@"RAM: %@ MB", @([pInfo physicalMemory] / (int)pow(1024, 2)));
    
    
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"ADMIN_MODE"]) {
        [_adminMenu setHidden:NO];
        ADMIN_MODE = YES;
    }
    else {
        [_adminMenu setHidden:NO];
        ADMIN_MODE = NO;
    }

    
    
    // Insert code here to initialize your application
    
    

    
    
    hiddenStatuses = @[@"Awaiting Assignment", @"Assigned", @"Awaiting Samples", @"Cancelled"];
    hiddenStatusesForImport = @[@"Awaiting Assignment", @"Assigned", @"Awaiting Samples"];
    hiddenStatusPredicate = [NSPredicate predicateWithFormat:@"NOT(status IN %@)", hiddenStatuses];
    _completedPredicate = [NSPredicate predicateWithFormat:@"status LIKE[c] %@", @"Completed"];
    _cancelledPredicate = [NSPredicate predicateWithFormat:@"status LIKE[c] %@", @"Cancelled"];
    
    searchFieldPredicate = [NSPredicate predicateWithValue:YES];
    
    
    [QBSyncManager sharedManager];
   
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    
    [center addObserver:self selector:@selector(toggleSyncMenus:) name: kBeganSyncNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(toggleSyncMenus:) name: kFinishedSyncNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(toggleSyncMenus:) name: kErrorSyncingNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(toggleSyncMenus:) name: kConnectionOfflineNotification object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(toggleSyncMenus:) name: kConnectionOnlineNotification object:[QBSyncManager sharedManager]];
    
    [center addObserver:self selector:@selector(toggleCameraMenu:) name:kCameraOfflineNotification object:nil];
    [center addObserver:self selector:@selector(toggleCameraMenu:) name:kCameraOnlineNotification object:nil];
    
    loginView = [[LoginController alloc] initWithWindowNibName:@"LoginController"];
    [loginView showWindow:self];
    
    detailView = [[DetailViewWindowController alloc] initWithWindowNibName:@"DetailViewWindowController"];
    
    
    [turnSyncOffBtn setEnabled:YES];
    [turnSyncOnBtn setEnabled:NO];
    
    _toggleReadyItemsMenuItem.enabled = YES;
    _toggleCompletedItemsMenuItem.enabled = YES;
    _toggleInProgressItemsMenuItem.enabled = YES;
    
    [self setupToggleMenus];
}



- (void) setupToggleMenus {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if ([prefs objectForKey:@"hideCompleted"])
    {
        hideCompleted = [prefs boolForKey:@"hideCompleted"];
    }
    if ([prefs objectForKey:@"hideInProgress"])
    {
        hideInProgress = [prefs boolForKey:@"hideInProgress"];
    }
    if ([prefs objectForKey:@"hideReady"])
    {
        hideReady = [prefs boolForKey:@"hideReady"];
    }
    
//    BooleanValueTransformer *completedTransformer = [BooleanValueTransformer valueTransformerForName:@"completedTransformer" valueTrue:@"Show Completed Items" valueFalse:@"Hide Completed Items"];
    
//    [self.toggleCompletedItemsMenuItem.title bind:NSValueBinding
//                                         toObject:self
//                                      withKeyPath:@"hideCompleted"
//                                          options:@{NSValueTransformerNameBindingOption : completedTransformer}];
   
     _toggleCompletedItemsMenuItem.title = hideCompleted? @"Show Completed Items" : @"Hide Completed Items";
    _toggleInProgressItemsMenuItem.title = hideInProgress? @"Show In Progress Items" : @"Hide In Progress Items";
    _toggleReadyItemsMenuItem.title = hideReady? @"Show Ready Items" : @"Hide Ready Items";
}


- (void) awakeFromNib {
    [super awakeFromNib];
    [turnSyncOffBtn setEnabled:YES];
    [turnSyncOnBtn setEnabled:NO];
    
    _toggleReadyItemsMenuItem.enabled = YES;
    _toggleCompletedItemsMenuItem.enabled = YES;
    _toggleInProgressItemsMenuItem.enabled = YES;
}

#pragma mark - Status Toggle Menu Items
- (void) updateToggleMenuTitles {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (hideCompleted)
        {
            _toggleCompletedItemsMenuItem.title = @"Show Completed Items";
        }
        else
        {
            _toggleCompletedItemsMenuItem.title = @"Hide Completed Items";
        }
        
        
        if (hideInProgress)
        {
            _toggleInProgressItemsMenuItem.title = @"Show In Progress Items";
        }
        else
        {
            _toggleInProgressItemsMenuItem.title = @"Hide In Progress Items";
        }
        
        
        
        if (hideReady)
        {
            _toggleReadyItemsMenuItem.title = @"Show Ready Items";
        }
        else
        {
            _toggleReadyItemsMenuItem.title = @"Hide Ready Items";
        }
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setBool:hideCompleted forKey:@"hideCompleted"];
        [prefs setBool:hideInProgress forKey:@"hideInProgress"];
        [prefs setBool:hideReady forKey:@"hideReady"];
        [prefs synchronize];
        
        
        [[listView outlineView] reloadData];
        [[[detailView detailsOutlineViewController] outlineView] reloadData];
    });
}


- (IBAction)toggleCompletedItems:(id)sender
{
    //[self willChangeValueForKey:@"hideCompleted"];
    
    if ([_toggleCompletedItemsMenuItem.title containsString:@"Hide"])
    {
        hideCompleted = YES;
    }
    else
    {
        hideCompleted = NO;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kStatusFilterChangedNotification object:self];
    
    
    // save to defaults
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:hideCompleted forKey:@"hideCompleted"];
    [prefs synchronize];
    
    [self updateToggleMenuTitles];
}



- (IBAction)toggleInProgressItems:(id)sender
{
    if ([_toggleInProgressItemsMenuItem.title containsString:@"Hide"])
    {
       
        hideInProgress = YES;
    }
    else
    {
        hideInProgress = NO;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kStatusFilterChangedNotification object:self];
    
    // save to defaults
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:hideInProgress forKey:@"hideInProgress"];
    [prefs synchronize];
    
    [self updateToggleMenuTitles];
}

- (IBAction)toggleReadyItems:(id)sender
{
    if ([_toggleReadyItemsMenuItem.title containsString:@"Hide"])
    {
        hideReady = YES;
    }
    else
    {
        hideReady = NO;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kStatusFilterChangedNotification object:self];
    
    // save to defaults
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:hideReady forKey:@"hideReady"];
    [prefs synchronize];
    
    [self updateToggleMenuTitles];
}

#pragma mark - Selected: Set | SetList | Shot

- (void) setSelectedSet:(Set *)aSet {
    [self willChangeValueForKey:@"selectedSet"];
    _selectedSet = (aSet!=nil) ? [Set existingWithObjectID:aSet.objectID inContext:_mainMOC] : nil;
    [self didChangeValueForKey:@"selectedSet"];
}

- (void) setSelectedSetList:(SetList *)aSetList {
    [self willChangeValueForKey:@"selectedSetList"];
    _selectedSetList = (aSetList!=nil) ? [SetList existingWithObjectID:aSetList.objectID inContext:_mainMOC] : nil;
    [self didChangeValueForKey:@"selectedSetList"];
}

- (void) setSelectedShot:(Shot *)aShot {
    [self willChangeValueForKey:@"selectedShot"];
    _selectedShot = (aShot!=nil) ? [Shot existingWithObjectID:aShot.objectID inContext:_mainMOC] : nil;
    [self didChangeValueForKey:@"selectedShot"];
}



- (BOOL) sampleMode {
    return self.selectedSetList.sampleMode;
}



#pragma mark - System Information

- (NSString *)getSystemUUID {
    io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
    if (!platformExpert)
        return nil;
    CFTypeRef serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,CFSTR(kIOPlatformUUIDKey),kCFAllocatorDefault, 0);
    IOObjectRelease(platformExpert);
    if (!serialNumberAsCFString)
        return nil;
    NSString *returnValue = (__bridge NSString *)(serialNumberAsCFString);
    return returnValue;
}


- (NSString *)getSystemSerialNumber {
    io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,IOServiceMatching("IOPlatformExpertDevice"));
    if (!platformExpert)
        return nil;
    CFTypeRef serialNumberAsCFString = IORegistryEntryCreateCFProperty(platformExpert,CFSTR(kIOPlatformSerialNumberKey),kCFAllocatorDefault, 0);
    IOObjectRelease(platformExpert);
    if (!serialNumberAsCFString)
        return nil;
    
    return (__bridge NSString *)(serialNumberAsCFString);
}

- (IBAction)submitCrashInfo:(id)sender {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *desktopFolder = [fileManager URLForDirectory:NSDesktopDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    [fileManager changeCurrentDirectoryPath:[desktopFolder path]];
    
    NSURL *appSupportFolder = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL *sfLogsFolder = [NSURL fileURLWithPath:logfilePath];
    NSURL *diagnosticReportsFolder = [[sfLogsFolder URLByDeletingLastPathComponent] URLByAppendingPathComponent:@"DiagnosticReports"];
    NSLog(@"currentDirectory: %@", fileManager.currentDirectoryPath);
    NSMutableArray *logFiles = [NSMutableArray array];
    
    NSDate *date = [NSDate date];
    NSString *dateFormat = @"yyyyMMdd_HHmm";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSString *appName = [[NSProcessInfo processInfo] processName];
    
    NSString *zipFileName = [[appName stringByReplacingOccurrencesOfString:@" " withString:@""] stringByAppendingFormat:@"-%@.zip", dateString];
    
    OZZipFile *zipFile= [[OZZipFile alloc] initWithFileName:zipFileName mode:OZZipFileModeCreate legacy32BitMode:YES];
    
    
    // Get last 2 log files in standardLogsFolder
    NSArray *dirContents = [fileManager contentsOfDirectoryAtURL:sfLogsFolder
                                      includingPropertiesForKeys:@[NSURLNameKey, NSURLIsDirectoryKey, NSURLCreationDateKey]
                                                         options:NSDirectoryEnumerationSkipsHiddenFiles
                                                           error:nil];
    
    NSArray *sortedContent = [dirContents sortedArrayUsingComparator:
                              ^(NSURL *file1, NSURL *file2)
                              {
                                  // compare
                                  NSDate *file1Date;
                                  [file1 getResourceValue:&file1Date forKey:NSURLContentModificationDateKey error:nil];
                                  
                                  NSDate *file2Date;
                                  [file2 getResourceValue:&file2Date forKey:NSURLContentModificationDateKey error:nil];
                                  
                                  // Ascending:
                                  return [file1Date compare: file2Date];
                                  // Descending:
                                  //return [file2Date compare: file1Date];
                              }];
    
    
    for (NSURL *lastFile in sortedContent) {
        
        OZZipWriteStream *stream= [zipFile writeFileInZipWithName:[NSString stringWithFormat:@"Logs/%@",lastFile.lastPathComponent]
                                                 compressionLevel:OZZipCompressionLevelBest];
        
        NSData *data = [NSData dataWithContentsOfFile:[lastFile path]];
        
        [stream writeData:data];
        [stream finishedWriting];
    }
    
    // **** Get last crash Crash Reports **** //
    
    dirContents = [fileManager contentsOfDirectoryAtURL:diagnosticReportsFolder
                                      includingPropertiesForKeys:@[NSURLNameKey, NSURLIsDirectoryKey, NSURLCreationDateKey]
                                                         options:NSDirectoryEnumerationSkipsHiddenFiles
                                                           error:nil];
    
    dirContents = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"lastPathComponent contains %@", @"ShotFlow"]];
    
    sortedContent = [dirContents sortedArrayUsingComparator:
                              ^(NSURL *file1, NSURL *file2)
                              {
                                  // compare
                                  NSDate *file1Date;
                                  [file1 getResourceValue:&file1Date forKey:NSURLContentModificationDateKey error:nil];
                                  
                                  NSDate *file2Date;
                                  [file2 getResourceValue:&file2Date forKey:NSURLContentModificationDateKey error:nil];
                                  
                                  // Ascending:
                                  return [file1Date compare: file2Date];
                                  // Descending:
                                  //return [file2Date compare: file1Date];
                              }];
    
    for (NSURL *lastFile in sortedContent) {
        OZZipWriteStream *stream= [zipFile writeFileInZipWithName:[NSString stringWithFormat:@"CrashReports/%@",lastFile.lastPathComponent]
                                                 compressionLevel:OZZipCompressionLevelBest];
        
        NSData *data = [NSData dataWithContentsOfFile:[lastFile path]];
        
        [stream writeData:data];
        [stream finishedWriting];
    }
   
    [zipFile close];
    
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:@"Support Archive Created."];
    [alert setInformativeText:[NSString stringWithFormat:@"Support log files have been placed in file '%@' on your Desktop. \n\nPlease email the attachment to shotflow@captureintegration.com", zipFileName]];
    [alert setAlertStyle:NSInformationalAlertStyle];
    [alert runModal];
}



#pragma mark - SyncManager functions

- (IBAction)turnSyncOn:(id)sender {
    DDLogVerbose(@"Turning Sync ON");
    dispatch_async(dispatch_get_main_queue(), ^{
//    [[QBDataImporter sharedImporter] setInitialLoad:NO];
//    [[QBSyncManager sharedManager] startSyncing];
    
    [turnSyncOnBtn setEnabled:NO];
    [turnSyncOffBtn setEnabled:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:kSyncingEnabledNotification object:[QBSyncManager sharedManager]];
    });
}

- (IBAction)turnSyncOff:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
    DDLogVerbose(@"Turning Sync OFF");
    [[QBSyncManager sharedManager] stopSyncing];
    [turnSyncOnBtn setEnabled:YES];
    [turnSyncOffBtn setEnabled:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:kSyncingDisabledNotification object:[QBSyncManager sharedManager]];
    });
}

- (IBAction)forceUpdate:(id)sender {
    
    [[QBDataImporter sharedImporter] setForceUpdate:YES];
    [[QBSyncManager sharedManager] manualSync];
}


-(void) toggleSyncMenus:(NSNotification*) note {
    if ([note.name isEqualToString:kBeganSyncNotification]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.turnSyncOnBtn setEnabled:NO];
            [self.turnSyncOffBtn setEnabled:YES];
            [self.manualSyncMenu setEnabled:NO];
        });
    }
    else if ([note.name isEqualToString:kFinishedSyncNotification]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.turnSyncOnBtn setEnabled:NO];
            [self.turnSyncOffBtn setEnabled:YES];
            [self.manualSyncMenu setEnabled:YES];
        });
    }
    else if ([note.name isEqualToString:kErrorSyncingNotification]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.turnSyncOnBtn setEnabled:YES];
            [self.turnSyncOffBtn setEnabled:NO];
            [self.manualSyncMenu setEnabled:YES];
        });
    }
    else if ([note.name isEqualToString:kConnectionOfflineNotification]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.turnSyncOnBtn setEnabled:YES];
            [self.turnSyncOffBtn setEnabled:NO];
            [self.manualSyncMenu setEnabled:YES];
        });
    }
    
}
- (IBAction)startManualSync:(id)sender {
    [[QBDataImporter sharedImporter] setInitialLoad:NO];
    [[QBSyncManager sharedManager] manualSync];
    
}

#pragma mark - Core Data stack

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize mainMOC = _mainMOC;


- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file.
    
    NSURL *appSupportURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:[NSString stringWithFormat:@"ShotFlow One Capture"]];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
	
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ShotFlowOneCapture" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}



- (void) resetPersistentStore {
    [_mainMOC reset];
    
    NSPersistentStoreCoordinator *coordinator = self.persistentStoreCoordinator;
    
    NSError *error;
    for (NSPersistentStore *store in coordinator.persistentStores) {
        BOOL removed = [coordinator removePersistentStore:store error:&error];
        if (!removed) {
            DDLogError(@"Unable to remove persistent store");
        }
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationDocumentsDirectory = [self applicationDocumentsDirectory];
    NSArray *oldStores = [fileManager contentsOfDirectoryAtURL:applicationDocumentsDirectory includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    
    for (NSURL *oldURL in oldStores) {
        [fileManager removeItemAtURL:oldURL error:nil];
    }
    
    
    NSURL *url = [applicationDocumentsDirectory URLByAppendingPathComponent:@"ShotFlow One Capture.shotflowdata"];
    
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    
    if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:options error:&error]) {
        if (error != nil) {
            [[QBDBInfo info] alertMe:error];
        }
        else
        {
            DDLogError(@"Error creating local storage.");
        }
    }
    
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. (The directory for the store is created, if necessary.)
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *applicationDocumentsDirectory = [self applicationDocumentsDirectory];
    BOOL shouldFail = NO;
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    
    // Make sure the application files directory is there
    NSDictionary *properties = [applicationDocumentsDirectory resourceValuesForKeys:@[NSURLIsDirectoryKey] error:&error];
    if (properties) {
        if (![properties[NSURLIsDirectoryKey] boolValue]) {
            failureReason = [NSString stringWithFormat:@"Expected a folder to store application data, found a file (%@).", [applicationDocumentsDirectory path]];
            shouldFail = YES;
        }
    } else if ([error code] == NSFileReadNoSuchFileError) {
        error = nil;
        [fileManager createDirectoryAtPath:[applicationDocumentsDirectory path] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    if (!shouldFail && !error) {
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        NSURL *url = [applicationDocumentsDirectory URLByAppendingPathComponent:@"ShotFlow One Capture.shotflowdata"];
        
        NSDictionary *options = @{
                                  NSMigratePersistentStoresAutomaticallyOption : @YES,
                                  NSInferMappingModelAutomaticallyOption : @YES
                                  };
        
        if (![coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:options error:&error]) {
            coordinator = nil;
        }
        _persistentStoreCoordinator = coordinator;
    }
    
    if (shouldFail || error) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        if (error) {
            dict[NSUnderlyingErrorKey] = error;
        }
        error = [NSError errorWithDomain:@"com.captureintegration.shotflowone" code:9999 userInfo:dict];
        [[NSApplication sharedApplication] presentError:error];
        [NSApp terminate:self];
    }
    return _persistentStoreCoordinator;
}



- (NSManagedObjectContext *)mainMOC {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_mainMOC) {
        return _mainMOC;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    
    _mainMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_mainMOC setPersistentStoreCoordinator:coordinator];
    
    return _mainMOC;
}

#pragma mark - Core Data Saving and Undo support

- (IBAction)saveAction:(id)sender {
    // Performs the save action for the application, which is to send the save: message to the application's managed object context. Any encountered errors are presented to the user.
    if (![[self mainMOC] commitEditing]) {
        DDLogVerbose(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }
    
    NSError *error = nil;
    if ([[self mainMOC] hasChanges] && ![[self mainMOC] save:&error]) {
        [[NSApplication sharedApplication] presentError:error];
    }
}



- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window {
    // Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
    return [[self mainMOC] undoManager];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    // Save changes in the application's managed object context before the application terminates.
    
    
    if ([[QBSyncManager sharedManager] needsSignOut]) {
        [[QBSyncManager sharedManager] unassignSet:_selectedSet];
    }
    
    
    
    C1ProController *C1P = [C1ProController sharedInstance];
    
    if ([C1P didBegin]) {
        [C1P prepareToTerminate];
        return NSTerminateLater;
    }
    
    
    if (!_mainMOC) {
        return NSTerminateNow;
    }
    
    if (![[self mainMOC] commitEditing]) {
        DDLogVerbose(@"%@:%@ unable to commit editing to terminate", [self class], NSStringFromSelector(_cmd));
        return NSTerminateCancel;
    }
    
    if (![[self mainMOC] hasChanges]) {
        return NSTerminateNow;
    }
    
    NSError *error = nil;
    if (![[self mainMOC] save:&error]) {
        
        // Customize this code block to include application-specific recovery steps.
        BOOL result = [sender presentError:error];
        if (result) {
            return NSTerminateCancel;
        }
        
        NSString *question = NSLocalizedString(@"Could not save changes while quitting. Quit anyway?", @"Quit without saves error question message");
        NSString *info = NSLocalizedString(@"Quitting now will lose any changes you have made since the last successful save", @"Quit without saves error question info");
        NSString *quitButton = NSLocalizedString(@"Quit anyway", @"Quit anyway button title");
        NSString *cancelButton = NSLocalizedString(@"Cancel", @"Cancel button title");
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:question];
        [alert setInformativeText:info];
        [alert addButtonWithTitle:quitButton];
        [alert addButtonWithTitle:cancelButton];
        
        NSInteger answer = [alert runModal];
        
        if (answer == NSAlertFirstButtonReturn) {
            return NSTerminateCancel;
        }
    }
    
    return NSTerminateNow;
}

#pragma mark - Misc Menu Commands

- (IBAction)generatePanels:(id)sender {
    [[C1ProController sharedInstance] generatePanelsAdmin:loginView.usernameField.stringValue Password:loginView.passwordField.stringValue];
}

- (IBAction)initiateCapture:(id)sender {
    [[C1ProController sharedInstance] takeCapture];
}

- (IBAction)cleanUpFavorites:(id)sender {
    [[C1ProController sharedInstance] cleanUpFavorites];
}

- (void) toggleCameraMenu:(NSNotification *)note {
    if ([note.name isEqualToString:kCameraOfflineNotification])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            _takeCaptureMenuItem.enabled = NO;
        });
        
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            _takeCaptureMenuItem.enabled = YES;
        });
    }
}


@end
