//
//  SimplePingClient.h
//  CI Capture Manager
//
//  Created by Josh Booth on 6/22/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimplePing.h"

@interface SimplePingClient : NSObject<SimplePingDelegate>

+(void)pingHostname:(NSString*)hostName andResultCallback:(void(^)(NSString* latency))result;

@end
