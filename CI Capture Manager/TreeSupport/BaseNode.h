/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 Generic multi-use node object used with NSOutlineView and NSTreeController.
 */
@class QBManagedObject;
@interface BaseNode : NSObject <NSCoding, NSCopying>

// default grouping titles
+ (NSString *)untitledName;    // node with not title set
+ (NSString *)groupingName;
+ (NSString *)associatedName;
+ (NSString *)addName;

@property (strong) NSString *nodeTitle;
@property (strong) NSMutableArray *children;
@property (strong) QBManagedObject *obj;

@property (assign) BOOL isLeaf;
@property (assign) BOOL isGrouping;
@property (assign) BOOL isAssociated;

@property (readonly) BOOL isSpecialGroup;


- (instancetype)initLeaf;

@property (nonatomic, getter=isDraggable, readonly) BOOL draggable;

- (NSComparisonResult)compare:(BaseNode *)aNode;

@property (nonatomic, readonly, copy) NSArray *mutableKeys;

- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (void)removeObjectFromChildren:(id)obj;
@property (nonatomic, readonly, copy) NSArray *descendants;
@property (nonatomic, readonly, copy) NSArray *allChildLeafs;
@property (nonatomic, readonly, copy) NSArray *groupChildren;
- (BOOL)isDescendantOfOrOneOfNodes:(NSArray *)nodes;
- (BOOL)isDescendantOfNodes:(NSArray *)nodes;

@end
