//
//  MainTableCellView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 10/8/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "MainTableCellView.h"
#import "Shot.h"
#import "AppDelegate.h"
#import "C1ProController.h"
#import "QBField.h"

#import "DateValueTransformer.h"

@implementation MainTableCellView

@synthesize key;
@synthesize shot;

static dispatch_once_t onceToken;

NSString *const DateValueTransformerName = @"DateValueTransformer";
NSString *const TimestampValueTransformerName = @"TimestampValueTransformer";

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
}

- (void) viewDidMoveToSuperview {
    [super viewDidMoveToSuperview];
    
    dispatch_once(&onceToken, ^{
        [NSValueTransformer setValueTransformer:[[DateValueTransformer alloc] initWithDateType:kDateTypeDate] forName:DateValueTransformerName];
        [NSValueTransformer setValueTransformer:[[DateValueTransformer alloc] initWithDateType:kDateTypeTimestamp] forName:TimestampValueTransformerName];
    });
}


- (void) setupCellView {
    
//    if (![[self textField] canDrawConcurrently]) [[self textField] setCanDrawConcurrently:YES];
//    
//    NSDictionaryController *dictionaryController = [[NSDictionaryController alloc] init];
//    [dictionaryController bind:NSContentDictionaryBinding toObject:shot withKeyPath:@"params" options:nil];
//    
//    id selection = [[[dictionaryController arrangedObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"key = %@", key]] firstObject];
//    
//    if ([key containsString:@"Date"])
//    {
//        if ([[[shot fieldNamed:key] field_type] isEqualToString:@"timestamp"])
//        {
//            [self.textField bind:NSValueBinding
//                        toObject:selection
//                     withKeyPath:@"value"
//                         options:@{NSValueTransformerNameBindingOption : TimestampValueTransformerName,
//                                   NSContinuouslyUpdatesValueBindingOption : [NSNumber numberWithInt:1]}];
//        }
//        else
//        {
//            [self.textField bind:NSValueBinding
//                        toObject:selection
//                     withKeyPath:@"value"
//                         options:@{NSValueTransformerNameBindingOption : DateValueTransformerName,
//                                   NSContinuouslyUpdatesValueBindingOption : [NSNumber numberWithInt:1]}];
//        }
//        
//        
//    }
//    else
//    {
//        [self.textField bind:NSValueBinding
//                    toObject:selection
//                 withKeyPath:@"value"
//                     options:@{
//                               NSContinuouslyUpdatesValueBindingOption : [NSNumber numberWithInt:1]
//                               } ];
//    }

    
    if ([key isEqualToString:@"# of Captures"])
    {
        [self.textField bind:NSValueBinding toObject:shot withKeyPath:@"numberOfCaptures" options:nil];
    }
    else if ([key isEqualToString:@"Status"])
    {
        [self.textField bind:NSValueBinding toObject:shot withKeyPath:@"status" options:nil];
    }
    else if ([key isEqualToString:@"Shot Duration"])
    {
        [self.textField bind:NSValueBinding toObject:shot withKeyPath:@"durationString" options:nil];
        self.textField.editable = NO;
    }
    else
    {
        if ([[shot params] objectForKey:key]) {
            if ([key containsString:@"Date"])
            {

                NSDate *date = [shot convertDateFromImport:[[shot params] valueForKey:key]];
                self.textField.stringValue = [shot localDateStringForDate:date
                                                              dateFormat:nil
                                                                   field:[shot fieldNamed:key]];
            }
            else {
                self.textField.stringValue = [[shot params] valueForKey:key];
            }
        }
        else {
            self.textField.stringValue = @"";
        }
    }
    

    
    if (
        ([key containsString:@"Notes"]) ||
        ([key containsString:@"Description"]) ||
        ([key isEqualToString:@"Title"]) ||
        ([key isEqualToString:@"Naming Convention"])
        ){
        self.textField.alignment = NSTextAlignmentLeft;
    }
    else {
        self.textField.alignment = NSTextAlignmentCenter;
    }
    
    
}



@end
