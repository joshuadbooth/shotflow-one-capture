//
//  DetailsViewController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 11/24/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "DetailsTabViewController.h"

#import "AppDelegate.h"
#import "DetailViewWindowController.h"
#import "ProductDataTabViewController.h"

#import "C1ProController.h"

#import "M1HeaderCell.h"
#import "M1TableRowView.h"

#import "QBDBInfo.h"


#import "QBDataExporter.h"

#import "QBTable.h"
#import "QBField.h"

#import "AddShotCellView.h"

#import "DetailsOutlineViewController.h"

@import QuartzCore;
#import "objc/runtime.h"




@implementation DetailsTabViewController {
    AppDelegate *app;
    
    QBDataExporter *exporter;
    C1ProController *C1PC;
    
    DetailViewWindowController *parentController;
    ProductDataTabViewController *pdrsController;
    
    
    NSManagedObjectContext *mainMOC;
    
    SetList *uiSetList;
    //DetailsOutlineViewController *detailsOutlineViewController;
    BOOL setupComplete;
    BOOL isAwake;
}


@synthesize detailTableView;

static void * DetailViewContext = "DetailViewContext";

#pragma mark  - Init & Setup -

- (void) awakeFromNib {
    [super awakeFromNib];
    if (!isAwake) {
        isAwake = YES;
        app = [AppDelegate sharedInstance];
        
        C1PC = [C1ProController sharedInstance];
        exporter = [QBDataExporter sharedExporter];
        
        parentController = DetailViewWindowController.sharedController;
        pdrsController = parentController.pdrTabViewController;
        
        
        mainMOC = app.mainMOC;
        [self bind:@"uiSetList" toObject:app withKeyPath:@"selectedSetList" options:@{NSAllowsNullArgumentBindingOption : [NSNumber numberWithBool:YES]}];
        
    
         [_setListNameTextField bind:NSValueBinding toObject:uiSetList withKeyPath:@"name" options:nil];
         [_priorityTextField bind:NSValueBinding toObject:uiSetList withKeyPath:@"priority" options:nil];
         [_statusTextField bind:NSValueBinding toObject:uiSetList withKeyPath:@"status" options:nil];
         [_jobidTextField bind:NSValueBinding toObject:uiSetList withKeyPath:@"job_id" options:nil];
         [_typeTextField bind:NSValueBinding toObject:uiSetList withKeyPath:@"type" options:nil];
        
        
        
        
        //uiSetList = app.selectedSetList;
        
        //    [self.selectNextButton bind:@"enabled" toObject:shotsArrayController withKeyPath:@"canSelectNext" options:nil];
        //    [self.selectPreviousButton bind:@"enabled" toObject:shotsArrayController withKeyPath:@"canSelectPrevious" options:nil];
        
        
        [self registerObservations];
    }
}


- (void) viewWillAppear {
    [super viewWillAppear];

    // Do view setup here.
    [self reloadData];
   
}

- (void) registerObservations {
    
    [self addObserver:self forKeyPath:@"uiSetList" options:NSKeyValueObservingOptionNew context:DetailViewContext];
    //[app.selectedSetList addObserver:self forKeyPath:@"self" options:NSKeyValueObservingOptionNew context:DetailViewContext];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processSelection:) name:kSelectionDidChangeNotification object:nil];

}


- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if (context == DetailViewContext)
    {
//        if ([uiSetList isNotEqualTo:app.selectedSetList])
//        {
            //uiSetList = app.selectedSetList;
            [self reloadData];
//        }
    }
}


- (void) processSelection:(NSNotification*) note {
    
    SetList *incomingSetList;
    id noteObject = [mainMOC existingObjectWithID:note.object error:nil];
    
    if ([noteObject isKindOfClass:[SetList class]]) {
        incomingSetList  = noteObject;
    }
    else {
        incomingSetList = [noteObject setlist];
    }
    
    if ([uiSetList isNotEqualTo:incomingSetList]) {
        [self reloadData];
    }
}


- (void) reloadData {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *jobID = uiSetList.job_id;
        NSString *name = uiSetList.name;
        NSString *status = uiSetList.status;
        NSString *priority = uiSetList.priority;
        NSString *type = uiSetList.type;
        
        if (name.length) [_setListNameTextField setStringValue:name];
        if (priority.length) [_priorityTextField setStringValue:priority];
        if (status.length) [_statusTextField setStringValue:status];
        if (jobID.length) [_jobidTextField setStringValue:jobID];
        if (type.length) [_typeTextField setStringValue:type];
        
        
        [_stackView setVisibilityPriority: uiSetList.priority.length? NSStackViewVisibilityPriorityMustHold : NSStackViewVisibilityPriorityNotVisible forView:_priorityView];
        [_stackView setVisibilityPriority: uiSetList.status.length? NSStackViewVisibilityPriorityMustHold : NSStackViewVisibilityPriorityNotVisible forView:_statusView];
        [_stackView setVisibilityPriority: uiSetList.job_id.length? NSStackViewVisibilityPriorityMustHold : NSStackViewVisibilityPriorityNotVisible forView:_jobIDView];
        [_stackView setVisibilityPriority: uiSetList.type.length? NSStackViewVisibilityPriorityMustHold : NSStackViewVisibilityPriorityNotVisible forView:_typeView];
    });
}

- (void) dealloc {
    [app removeObserver:self forKeyPath:@"selectedSetList"];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"TableColumnWidthsDidChangeNotification" object:self];
}



#pragma mark next/previous shot
- (IBAction)nextShot:(id)sender {
    
}

- (IBAction)previousShot:(id)sender {
    
}



@end
