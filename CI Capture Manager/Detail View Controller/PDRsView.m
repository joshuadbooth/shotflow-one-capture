//
//  SamplesView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 11/24/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "PDRsView.h"

@implementation PDRsView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

@end
