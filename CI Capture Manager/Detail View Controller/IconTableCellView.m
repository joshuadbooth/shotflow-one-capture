//
//  IconTableCellView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "IconTableCellView.h"
#import "AppDelegate.h"
#import "DetailViewWindowController.h"
#import "Shot.h"
#import "SetList.h"
#import "QBDBInfo.h"
#import "C1ProController.h"

#import "QBDataExporter.h"
#import  "M1TableRowView.h"

@interface IconTableCellView ()

@property BOOL hasAlert;

@end

@implementation IconTableCellView {
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    QBDataExporter *exporter;
    C1ProController *C1PC;
}
@synthesize linkedButton;
@synthesize alertImage;
@synthesize cloneShotButton;
@synthesize doneButton;
@synthesize iconStackView;
@synthesize shot;
@synthesize hasAlert;

NSString *kCloneShotNotification = @"kCloneShotNotification";

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
    
}

- (void) dealloc {
    self.shot = nil;
}


-(void) viewDidMoveToSuperview {
    [super viewDidMoveToSuperview];
    
    [self setupCellView];
    
    [self setNeedsDisplay:YES];
}


- (void) setupCellView {
    app = [AppDelegate sharedInstance];
    mainMOC = app.mainMOC;
    C1PC = [C1ProController sharedInstance];
    exporter = [QBDataExporter sharedExporter];
    
    
    [self.alertImage bind:@"hidden" toObject:shot withKeyPath:@"hasAlert" options:@{NSValueTransformerNameBindingOption : NSNegateBooleanTransformerName}];
    [self.linkedButton bind:@"hidden" toObject:shot withKeyPath:@"isLinked" options:@{NSValueTransformerNameBindingOption : NSNegateBooleanTransformerName}];
    
    
    if ([self.linkedButton isHidden]) {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityNotVisible forView:self.linkedButton];
    } else {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityMustHold forView:self.linkedButton];
    }
    
    [self.doneButton bind:@"state" toObject:shot withKeyPath:@"isComplete" options:nil];
}


-(IBAction)cloneCurrentShot:(id)sender{
    
    //[[AppDelegate sharedInstance].detailView.detailsTabViewController cloneShot:self.shot];

    [[NSNotificationCenter defaultCenter] postNotificationName:kCloneShotNotification object:self userInfo:@{@"shotID" : self.shot.objectID}];
    
    
    
    
}

-(IBAction)markAsComplete:(id)sender {
    [mainMOC performBlock:^{
        [mainMOC save:nil];
        
        BOOL isCurrentlyCompleted = shot.isComplete;
        BOOL isSetListCompleted = shot.setlist.isComplete;
        
        BOOL allows0Captures = [[QBDBInfo info].configurationData objectForKey:@"ALLOWS0CAPTURES"]? [[[QBDBInfo info].configurationData valueForKey:@"ALLOWS0CAPTURES"] boolValue] : NO;
        
        if (shot.numberOfCaptures == 0)
        {
            if (!allows0Captures) {
                
                NSError *noCapturesError = [NSError errorWithDomain:@"com.shotflow1.C1Controller" code:2000
                                                           userInfo:@{NSLocalizedDescriptionKey : @"No Recorded Captures", NSLocalizedFailureReasonErrorKey : @"You tried to mark a shot as complete, but haven't recorded any captures in Capture One.\n\nYou must have at least one recorded capture to mark a shot as complete."}];
                [[QBDBInfo info] alertMe:noCapturesError];
                shot.isComplete = NO;
            }
            else
            {
                if (!isCurrentlyCompleted)
                {
                    NSAlert *noCapturesAlert = [[NSAlert alloc] init];
                    [noCapturesAlert setMessageText:@"No Recorded Captures"];
                    [noCapturesAlert setInformativeText:@"No captures have been recorded for this shot. Are you sure you want to mark this as complete?"];
                    [noCapturesAlert addButtonWithTitle:@"No"];
                    [noCapturesAlert addButtonWithTitle:@"Yes"];
                    
                    [noCapturesAlert setAlertStyle:NSWarningAlertStyle];
                    
                    
                    NSModalResponse response = [noCapturesAlert runModal];
                    
                    if (response == NSAlertFirstButtonReturn)
                    {
                        shot.isComplete = NO;
                    }
                    else if (response == NSAlertSecondButtonReturn)
                    {
                        shot.isComplete = YES;
                    }
                }
                else
                {
                    shot.isComplete = NO;
                }
            }
        }
        else
        {
            if (isCurrentlyCompleted)
            {
                shot.isComplete = NO;
            }
            else
            {
                shot.isComplete = YES;
            }
        }
        
        NSError *saveError = nil;
        
        if (shot.isComplete != isCurrentlyCompleted) {
            if (!shot.isComplete)
            {
                if (shot.numberOfCaptures > 0) {
                    shot.status = @"In Progress";
                    DDLogInfo(@"Shot '%@' didResume at %@", shot.name, [shot prettyDate:[NSDate date]]);
                }
                else {
                    shot.status = @"Ready";
                }
                
                [shot setDateCompleted:nil];
            }
            else
            {
                [shot setDateCompleted:[NSDate date]];
                shot.status = @"Completed";
                DDLogInfo(@"Shot '%@' didEnd at %@", shot.name, [shot prettyDate:[NSDate date]]);
            }
            
            [shot updateDateModified];
            [mainMOC save:&saveError];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [C1PC initiateSave:^{
                    if ([[C1PC activeShot] isEqualTo:[self shot]] && [C1PC isTiming])
                    {
                        [C1PC changeToShot:nil];
                    }
                    [exporter postToServer:shot onComplete:nil];
                }];
            });
        }
        
        [shot.setlist checkAllShotsCompleted];
        
        if (shot.setlist.isComplete != isSetListCompleted) {
            [mainMOC save:&saveError];
            dispatch_async(dispatch_get_main_queue(), ^{
                [exporter postToServer:shot.setlist onComplete:nil];
            });
        }
    }];
}
@end
