//
//  DetailsTabView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 11/24/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "DetailsView.h"

@implementation DetailsView


- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}


@end
