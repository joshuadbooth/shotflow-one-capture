//
//  DetailsOutlineViewController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 5/26/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "M1TableView.h"
#import "OutlineViewArrayController.h"


@class SetList;
@interface DetailsOutlineViewController : NSViewController <NSOutlineViewDelegate, NSOutlineViewDataSource>

@property (nonatomic, strong) IBOutlet M1TableView *outlineView;

@end
