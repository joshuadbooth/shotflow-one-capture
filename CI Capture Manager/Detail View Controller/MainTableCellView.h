//
//  MainTableCellView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 10/8/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "M1TableViewCell.h"

@class Shot, MainTableCellView;

@interface MainTableCellView : M1TableViewCell <NSTextFieldDelegate>

@property (nonatomic, weak) Shot* shot;
@property (nonatomic, retain) NSString *key;

- (void) setupCellView;

@end
