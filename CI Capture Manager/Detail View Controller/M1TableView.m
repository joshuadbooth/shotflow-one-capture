//
//  M1TableView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 10/15/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "M1TableView.h"
#import "SetList.h"
#import "Shot.h"
#import "AppDelegate.h"
#import "SetList.h"
#import "Shot.h"


@implementation M1TableView {
    AppDelegate *app;
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}


- (void) viewDidMoveToSuperview {
    [super viewDidMoveToSuperview];
}

- (void) awakeFromNib {
    [super awakeFromNib];
    app = [AppDelegate sharedInstance];
}


- (id)makeViewWithIdentifier:(NSString *)identifier owner:(id)owner {
    id view = [super makeViewWithIdentifier:identifier owner:owner];
    
    if ([identifier isEqualToString:NSOutlineViewDisclosureButtonKey]) {
        // Do your customization
        
        [(NSButton *)view setImage:[NSImage imageNamed:@"disclosure-right"]];
        [(NSButton *)view setAlternateImage:[NSImage imageNamed:@"disclosure-down"]];
    }
    
    return view;
}

- (NSInteger) indexOfTableColumnWithIdentifier:(NSString*)columnName {
    return [[self tableColumns] indexOfObject:[self tableColumnWithIdentifier:columnName]];
}

- (void) selectRowIndex:(NSUInteger) row {
    [self selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
}

@synthesize selectedObject = _selectedObject;


- (id) selectedObject
{
    id anObject;
    NSInteger selectedRow = [self selectedRow];
    if (selectedRow == -1) return nil;
    anObject = [self itemAtRow:selectedRow];
    return anObject;
}

- (void) setSelectedObject:(id)anObject {
    if (anObject) {
        [self expandItemAndParentItems:anObject];
        NSInteger objectRow = [self rowForItem:anObject];
        if (objectRow != -1) {
            [self selectRowIndex:objectRow];
            _selectedObject = anObject;
            [self scrollRowToVisible:objectRow];
        }
    }
    else{
        _selectedObject = nil;
    }
}
- (void) deselectAllRows {
    self.selectedObject = nil;
    [self selectRowIndexes:[NSIndexSet indexSet] byExtendingSelection:NO];
}

- (void) selectObject:(id)anObject {
    
    [self setSelectedObject:anObject];
    
}


- (void) expandItemAndParentItems:(nullable id)item {
    
            [self expandItem:[item parent]];
}

- (void) reloadData {
    if (!_isReloadingData){
        _isReloadingData = YES;
        
        //id selectedObject = [self selectedObject];
        [super reloadData];
        //if (selectedObject) [self selectObject:selectedObject];
        self.selectedObject = app.selectedShot;
        _isReloadingData = NO;
    }
}


- (void) insertItemAtIndex:(NSUInteger)index inParent:(id)parent withAnimation:(NSTableViewAnimationOptions)animationOptions {
    [self insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:index] inParent:parent withAnimation:animationOptions];
}


- (id) itemForView:(NSView*)view {
    id returnItem = nil;
    
    NSUInteger row = [self rowForView:view];
    if (row == NSNotFound) return nil;
    
    returnItem = [self itemAtRow:row];
    return returnItem;
}

@end
