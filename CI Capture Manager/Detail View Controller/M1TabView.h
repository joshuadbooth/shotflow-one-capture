//
//  M1TabView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 10/26/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface M1TabView : NSTabView

@end
