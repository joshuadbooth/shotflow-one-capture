//
//  IconTableCellView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "M1TableViewCell.h"

@class Shot, M1TableRowView;

@interface IconTableCellView : M1TableViewCell

extern NSString *kCloneShotNotification;
@property (strong) IBOutlet NSStackView *iconStackView;
@property (nonatomic, weak) Shot* shot;

@property IBOutlet NSButton *linkedButton;
@property IBOutlet NSImageView *alertImage;

@property (weak) IBOutlet NSButton *cloneShotButton;
@property (weak) IBOutlet NSButton *doneButton;

- (IBAction)cloneCurrentShot:(id)sender;
- (IBAction)markAsComplete:(id)sender;
- (void) setupCellView;
@end
