//
//  SamplesTabViewController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 10/26/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "ProductDataTabViewController.h"
#import "AppDelegate.h"
#import "DetailViewWindowController.h"

#import "QBDBInfo.h"
#import "M1HeaderCell.h"
#import "M1TableRowView.h"

#import "Shot.h"
#import "ProductDataRecord.h"

#import "QBTable.h"
#import "QBField.h"

static void * pdrsDVcontext = "pdrsDVcontext";

@interface ProductDataTabViewController ()
@property (nonatomic, strong) M1ArrayController *pdrsArrayController;

@end

@implementation ProductDataTabViewController {
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    BOOL setupComplete;
    BOOL isAwake;
}


@synthesize pdrsDetailTableView;

@synthesize selectedShot;
@synthesize pdrsArrayController;

@synthesize shotNameField;

static void * samplesShotACcontext = @"samplesShotACContext";


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}


-(void) awakeFromNib {
    [super awakeFromNib];
    if (!isAwake) {
        isAwake = YES;
        app = [AppDelegate sharedInstance];
        mainMOC = app.mainMOC;
        selectedShot = app.selectedShot;
        
        if (!setupComplete) {
            [app addObserver:self forKeyPath:@"selectedShot" options:NSKeyValueObservingOptionNew context:pdrsDVcontext];
            [self setupTable];
        }
        
        
    }
    
}

- (void) viewWillAppear {
    
    selectedShot = app.selectedShot;
    [self reloadData];
    
}


-(void) setupTable {
   
    QBTable *pdrTable = [QBTable tableNamed:@"Product Data" inMOC:mainMOC];
    NSArray *detailColumns = [[pdrTable fields_detailView] valueForKeyPath:@"name"];

    pdrsDetailTableView.columnAutoresizingStyle = NSTableViewNoColumnAutoresizing;

    while([[pdrsDetailTableView tableColumns] count] > 0) {
        [pdrsDetailTableView removeTableColumn:[[pdrsDetailTableView tableColumns] lastObject]];
    }
    
    for (NSString* headerName in detailColumns) {

        NSTableColumn *newCol = [[NSTableColumn alloc] initWithIdentifier:headerName];
        
        //newCol.headerCell.stringValue = headerName;
        //newCol.headerCell.alignment = NSTextAlignmentCenter;
        newCol.width = 150;
        newCol.minWidth = 100;
        
        if ([headerName isEqualToString:[detailColumns firstObject]]) {
            newCol.width = 150;
            newCol.resizingMask =  NSTableColumnUserResizingMask | NSTableColumnAutoresizingMask;
        } else if ([headerName rangeOfString:@"Description"].location != NSNotFound) {
            newCol.width = 200;
            
            newCol.resizingMask =  NSTableColumnUserResizingMask;
        } else if (![headerName isEqualToString:[detailColumns lastObject]]) {
            newCol.width = 150;
            newCol.resizingMask =  NSTableColumnUserResizingMask;
        } else {
            newCol.width = 150;
            newCol.resizingMask =  NSTableColumnUserResizingMask | NSTableColumnAutoresizingMask;
        }
        
        [pdrsDetailTableView addTableColumn:newCol];
    }
    
    NSTableHeaderView *headerView = pdrsDetailTableView.headerView;
    
    for (NSTableColumn *newCol in pdrsDetailTableView.tableColumns) {
        
        M1HeaderCell *headerCell = [[M1HeaderCell alloc] initTextCell:newCol.identifier];
        headerCell.font = [NSFont systemFontOfSize:10];
        
        if ([newCol.identifier isEqualToString:@"Sample SKU"]) headerCell.stringValue = @"SKU";
        
        if ([newCol isEqualTo:pdrsDetailTableView.tableColumns.firstObject]) {
            headerCell.needsLeftBorder = YES;
        }
        
        [headerCell drawWithFrame:NSMakeRect(0, 0, newCol.width, headerView.frame.size.height) inView:headerView];
        headerCell.alignment = NSTextAlignmentCenter;
        
        [newCol setHeaderCell:headerCell];
    }
    
    setupComplete = YES;
    
    [pdrsDetailTableView setNeedsDisplay:YES];
}

#pragma mark - Refreshing view
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
   
    if (context == pdrsDVcontext) {
        [self reloadData];
    }
}

#pragma mark - NSTableViewDelegate Methods
- (NSView*) tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    
    ProductDataRecord *pdr = [[selectedShot.productdatarecords sortedArrayUsingDescriptors:@[app.sortByPrimaryKeyValue]] objectAtIndex:row];
    NSString *columnName = [tableColumn identifier];
    
    NSTableCellView *cellView = [tableView makeViewWithIdentifier:@"detailsTableCellView" owner:tableView];
    
    
    if ([columnName containsString:@"Date"]) {
        cellView.textField.alignment = NSTextAlignmentCenter;
        cellView.textField.stringValue = [pdr xmpDate:[pdr.params valueForKey:[tableColumn identifier]]];
    }
    else {
        
        
        if ([columnName containsString:@"Description"]){
            cellView.textField.alignment = NSTextAlignmentLeft;
            cellView.textField.stringValue = [pdr.params valueForKey:[tableColumn identifier]];
        } else {
            cellView.textField.alignment = NSTextAlignmentCenter;
            cellView.textField.stringValue = [pdr.params valueForKey:[tableColumn identifier]];
        }
        
    }
    return cellView;
}

-(void) tableViewColumnDidResize:(NSNotification *)notification {
    
    NSRange  visibleRows = NSRangeFromString([NSString stringWithFormat:@"{0,%ld}", (long)[pdrsDetailTableView numberOfRows]]);
    [NSAnimationContext beginGrouping];
    //[[NSAnimationContext currentContext] setDuration:0];
    [pdrsDetailTableView noteHeightOfRowsWithIndexesChanged:[NSIndexSet indexSetWithIndexesInRange:visibleRows]];
    [NSAnimationContext endGrouping];
}

- (CGFloat) tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row {
    
    ProductDataRecord *pdr = [[selectedShot.productdatarecords sortedArrayUsingDescriptors:@[app.sortByPrimaryKeyValue]] objectAtIndex:row];
    if (pdr != nil) {
        @try {
            
           NSFont *standardFont = [NSFont systemFontOfSize:14.0];
            
            NSMutableArray * contentHeights = [NSMutableArray array];
            
            for (NSTableColumn *col in [tableView tableColumns]) {
                
                NSString *output = [pdr.params valueForKey:[col identifier]];
                
                NSTextStorage *textStorage = [[NSTextStorage alloc] initWithString:output];
                NSTextContainer *textContainer = [[NSTextContainer alloc] initWithContainerSize:NSMakeSize(col.width, FLT_MAX)];
                NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
                
                [layoutManager addTextContainer:textContainer];
                [textStorage addLayoutManager:layoutManager];
                [textStorage addAttribute:NSFontAttributeName value:standardFont range:NSMakeRange(0, [textStorage length])];
                [textContainer setLineFragmentPadding:0.0];
                
                [layoutManager glyphRangeForTextContainer:textContainer];
                NSRect rect = [layoutManager usedRectForTextContainer:textContainer];
                float textHeight = rect.size.height;
                [contentHeights addObject:[NSNumber numberWithFloat:textHeight]];
                
            }
            
            NSNumber *max = [contentHeights valueForKeyPath:@"@max.floatValue"];
            
            float new_height = ceil(max.floatValue) + 10;
            
            if (new_height < 23) {
                return 23.0;
            } else {
                return new_height;
            }
        }
        @catch (NSException *exception) {
            return 23.0;
        }
    } else {
        return 23.0;
    }
    
}

- (NSTableRowView*) tableView:(NSTableView *)tableView rowViewForRow:(NSInteger)row {
    return [[M1TableRowView alloc] init];    
}


#pragma mark - NSTableViewDataSource Methods
-(NSInteger) numberOfRowsInTableView:(NSTableView *)tableView {
    
    NSInteger count = selectedShot.productdatarecords.count;
    return count;
}


-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    ProductDataRecord *pdr = [[selectedShot.productdatarecords sortedArrayUsingDescriptors:@[app.sortByPrimaryKeyValue]] objectAtIndex:row];
    
    return (pdr !=nil) ? [pdr.params valueForKey:[tableColumn identifier]] : @"";
}

- (void) reloadData {
    selectedShot = app.selectedShot;
    
    if (selectedShot !=nil) {
        shotNameField.stringValue = [selectedShot title];
    } else {
        shotNameField.stringValue = @"";
        shotNameField.placeholderString = @"No Shot Selected";
    }
    [pdrsDetailTableView reloadData];
    [pdrsDetailTableView setNeedsDisplay:YES];
}




@end
