//
//  DetailViewWindowController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/3/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "DetailViewWindowController.h"
#import "AppDelegate.h"
#import "QBDBInfo.h"
#import "QBQuery.h"

#import "SetList.h"
#import "Shot.h"

#import "ListViewController.h"
#import "SetListsArrayController.h"
#import "ShotsArrayController.h"
#import "IconTableCellView.h"
#import "M1HeaderCell.h"
#import "M1TableRowView.h"

#import "C1ProController.h"

static void * DVSetListACcontext = @"DVSetListACContext";
static void * DVShotACcontext = @"DVShotACContext";


@implementation DetailViewWindowController {
    AppDelegate *app;
}

@synthesize setList;
@synthesize detailTableView;
@synthesize setupComplete;
@synthesize tabView;
@synthesize detailsTabView;
@synthesize pdrsTabView;
@synthesize pdrTabViewController;
@synthesize detailsTabViewController;

#pragma mark - NEW -


+(instancetype) sharedController {
    static DetailViewWindowController *sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedController = [[self alloc] initWithWindowNibName:@"DetailViewWindowController"];
    });
    return sharedController;
}

- (void)tabView:(NSTabView *)tabView willSelectTabViewItem:(NSTabViewItem *)tabViewItem {
    if ([tabViewItem.label isEqualToString:@"Details"]) {
        DDLogVerbose(@"Switching to Details Tab");
        //[detailsTabViewController reloadData];
    } else if ([tabViewItem.label isEqualToString:@"Product Data"]) {
        DDLogVerbose(@"Switching to Samples Tab");
        //[pdrTabViewController reloadData];
    }
    
}

#pragma mark - Init & Setup -

-(void)awakeFromNib {
    [super awakeFromNib];
    
}

- (id) initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    return self;
}

-(void) dealloc {
    DDLogVerbose(@"dealloc");
}

//- (void) keyDown:(NSEvent *)theEvent {
//    if (([theEvent modifierFlags] & NSCommandKeyMask) && ([theEvent keyCode] == 40)) {
//        [[C1ProController sharedInstance] takeCapture];
//    }
//}

- (void) windowWillClose:(NSNotification *)notification {

    
}


- (void)windowDidLoad {
    [super windowDidLoad];
    
    app = [AppDelegate sharedInstance];
    [self.window setBackgroundColor:[NSColor whiteColor]];

    [tabView setWantsLayer:YES];
    [tabView.layer setBackgroundColor:[NSColor whiteColor].CGColor];
    

}


-(void) showWindow:(id)sender {
    self.window.delegate = self;
    [super showWindow:sender];
}



- (IBAction)closeDetailView:(id)sender{
    [self.window close];
}


@end
