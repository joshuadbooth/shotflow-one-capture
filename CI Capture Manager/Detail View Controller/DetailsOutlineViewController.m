//
//  DetailsOutlineViewController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 5/26/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "DetailsOutlineViewController.h"

#import "AppDelegate.h"

#import "OutlineViewController.h"

#import "QBDBInfo.h"
#import "QBSyncManager.h"
#import "QBQuery.h"
#import "QBDataExporter.h"
#import "C1ProController.h"

#import <CoreData/CoreData.h>

#import "ShotsArrayController.h"

#import "SetList.h"
#import "Shot.h"
#import "QBTable.h"


#import "DetailsTabViewController.h"
#import "M1TableView.h"
#import "M1HeaderCell.h"
#import "M1TableRowView.h"


#import "IconTableCellView.h"
#import "MainTableCellView.h"
#import "AddShotCellView.h"


@import QuartzCore;
#import "objc/runtime.h"

#pragma mark -

typedef void (^completionBlock) (void);

@interface DetailsOutlineViewController () <NSTextFieldDelegate, NSOutlineViewDelegate, NSOutlineViewDataSource>

@end

static void * DVOutlineViewContext = @"DVOutlineViewContext";

@implementation DetailsOutlineViewController {
    
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    NSManagedObjectContext *bgMOC;
    
    QBDataExporter *exporter;
    C1ProController *C1PC;
    
    QBDBInfo *QBinfo;
    
    OutlineViewArrayController *outlineViewArrayController;
    
    SetList *uiSetList;
    NSPredicate *filteredChildrenPredicate;
    
    BOOL isAwake;
    BOOL isAddingShot;
    
    BOOL didRegisterObservations;
    BOOL tableSetupComplete;
    
    NSMutableArray *detailColumns;
    NSArray *cachedColumnWidths;
    __block NSMutableDictionary *tableColumnWidths;
}

@synthesize outlineView;


- (void) awakeFromNib {
    [super awakeFromNib];
    if (!isAwake) {
        isAwake = YES;
        isAddingShot = NO;
        
        app = [AppDelegate sharedInstance];
        mainMOC = app.mainMOC;
        C1PC = [C1ProController sharedInstance];
        exporter = [QBDataExporter sharedExporter];
        QBinfo = [QBDBInfo info];
        //uiSetList = app.selectedSetList;
        
        [self bind:@"uiSetList" toObject:app withKeyPath:@"selectedSetList" options:@{
                                                                                      NSValidatesImmediatelyBindingOption : [NSNumber numberWithBool:YES],
                                                                                      NSAllowsNullArgumentBindingOption: [NSNumber numberWithBool:YES]
                                                                                      }];
        
        //[app addObserver:self forKeyPath:@"filteredChildrenPredicate" options:NSKeyValueObservingOptionNew context:DVOutlineViewContext];
        //[app addObserver:self forKeyPath:@"selectedSetList" options:NSKeyValueObservingOptionNew context:DVOutlineViewContext];
       
        if (!tableSetupComplete) [self setupTable];
        [self registerObservations:nil];
        [outlineView expandItem:nil expandChildren:YES];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do view setup here.

    tableColumnWidths = [NSMutableDictionary dictionary];
    
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"DVTableColumnWidths"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDictionary dictionary] forKey:@"DVTableColumnWidths"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    
}

- (void)viewWillAppear {
    [super viewWillAppear];
    
    [outlineView reloadData];
    [outlineView expandItem:nil expandChildren:YES];
    [outlineView scrollRowToVisible:[outlineView rowForItem:app.selectedShot]];
    if (app.selectedShot) [outlineView selectObject:app.selectedShot];
}


- (void) registerObservations:(NSNotification*) notification {
    if (!didRegisterObservations) {
        didRegisterObservations = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(processUpdates:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:mainMOC];
        
//        [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(processUpdates:)
//                                                     name:NSManagedObjectContextObjectsDidChangeNotification
//                                                   object:mainMOC];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(addShot:)
                                                     name:kDV_AddShotNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(processSelection:)
                                                     name:kSelectionDidChangeNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateShotCount:)
                                                     name:kCaptureCountUpdatedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(cloneShot:)
                                                     name:kCloneShotNotification
                                                   object:nil];
        
        [app addObserver:self forKeyPath:@"filteredChildrenPredicate" options:NSKeyValueObservingOptionNew context:DVOutlineViewContext];
        [app addObserver:self forKeyPath:@"selectedSetList" options:NSKeyValueObservingOptionNew context:DVOutlineViewContext];
        
        //[outlineView reloadData];
    }
}



- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFinishedSyncNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDV_AddShotNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:mainMOC];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextObjectsDidChangeNotification object:mainMOC];
}

#pragma mark - Table Setup

- (void) setupTable {
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"DVTableColumnWidths"]) {
        tableColumnWidths = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"DVTableColumnWidths"]];
        
    }
    
    QBTable *shotsTable = [QBTable tableNamed:@"Shots" inMOC:mainMOC];
    NSMutableArray *colNames = [NSMutableArray arrayWithArray:[[shotsTable fields_detailView] valueForKeyPath:@"name"]];
    
    if ([colNames containsObject:@"Title"]) [colNames removeObject:@"Title"];
    
    detailColumns = [NSMutableArray arrayWithArray: @[@"Title",@"IconColumn"]];
    [detailColumns addObjectsFromArray:colNames];
    
    DDLogVerbose(@"DetailColumns: %@", detailColumns);
    // remove all but iconColumn, then rebuild the columns from detailView Clist
    
    while([[outlineView tableColumns] count] > 2) {
        [outlineView removeTableColumn:[[outlineView tableColumns] lastObject]];
    }
    
    
    NSRect frame = [outlineView headerView].frame;
    frame.size.height = 25;
    outlineView.headerView.frame = frame;
    NSTableHeaderView *headerView = outlineView.headerView;
    
    
    
    // Title Column Setup
    NSTableColumn *titleColumn = [outlineView tableColumnWithIdentifier:@"Title"];
    titleColumn.width = 200.0;
    M1HeaderCell *titleHeaderCell = [[M1HeaderCell alloc] initTextCell:@"Title"];
    titleHeaderCell.needsLeftBorder = NO;
    [titleHeaderCell drawWithFrame:NSMakeRect(0, 0, titleColumn.width, headerView.frame.size.height) inView:headerView];
    titleHeaderCell.alignment = NSTextAlignmentCenter;
    [titleColumn setHeaderCell:titleHeaderCell];
    
    
    // Icon Column Setup
    NSTableColumn *iconColumn = [outlineView tableColumnWithIdentifier:@"IconColumn"];
    
    iconColumn.width = 125.0;
    M1HeaderCell *iconHeaderCell = [[M1HeaderCell alloc] initTextCell:@""];
    iconHeaderCell.needsLeftBorder = YES;
    
    [iconHeaderCell drawWithFrame:NSMakeRect(0, 0, iconColumn.width, headerView.frame.size.height) inView:headerView];
    iconHeaderCell.alignment = NSTextAlignmentCenter;
    
    [iconColumn setHeaderCell:iconHeaderCell];
    
    
    
    // Rest of the Columns
    for (NSString* headerName in detailColumns) {
        if ([headerName isEqualToString:@"IconColumn"] || [headerName isEqualToString:@"Title"]) continue;
        NSTableColumn *newCol = [[NSTableColumn alloc] initWithIdentifier:headerName];
        newCol.width = 200;
        newCol.minWidth = 100;
        
        if ([headerName isEqualToString:@"# of Captures"])
        {
            newCol.minWidth = 100.0;
            newCol.width = 100.0;
            newCol.resizingMask = NSTableColumnNoResizing;
        }
        else if ([headerName rangeOfString:@"Notes"].location != NSNotFound)
        {
            [newCol setWidth: 300.f];
            newCol.resizingMask =  NSTableColumnUserResizingMask;
            
        }
        else if (![headerName isEqualToString:[detailColumns lastObject]])
        {
            newCol.width = 200;
            newCol.resizingMask =  NSTableColumnUserResizingMask;
        }
        else if ([headerName isEqualToString:[detailColumns lastObject]])
        {
            newCol.width = 200;
            newCol.resizingMask =  NSTableColumnUserResizingMask | NSTableColumnAutoresizingMask;
            
        }
        M1HeaderCell *headerCell = [[M1HeaderCell alloc] initTextCell:headerName];
        
        [headerCell drawWithFrame:NSMakeRect(0, 0, newCol.width, headerView.frame.size.height) inView:headerView];
        headerCell.alignment = NSTextAlignmentCenter;
        
        [newCol setHeaderCell:headerCell];
        
        [outlineView addTableColumn:newCol];
        
        
    }
    
    DDLogVerbose(@"TableColumns: \n\t%@", [[[outlineView tableColumns] valueForKeyPath:@"identifier"] componentsJoinedByString:@"\n\t"]);
    tableSetupComplete = YES;
    //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    //            [self calculateAverageColumnWidths];
    //        });
    
}

- (void) calculateAverageColumnWidths {
    
    NSArray *cols = [NSArray arrayWithArray:detailColumns];
    
    for (NSString* headerName in cols) {
        
        // get average width of content for 25 rows for each column
        
        NSTableColumn *col = [outlineView tableColumnWithIdentifier:headerName];
        if (col == nil) {
            continue;
        }
        if ([headerName isEqualToString:@"# of Captures"] || [headerName isEqualToString:@"IconColumn"]) {
            [tableColumnWidths setObject:[NSNumber numberWithFloat:col.width] forKey:headerName];
            continue;
        }
        else if ([headerName rangeOfString:@"Notes"].location != NSNotFound) {
            [tableColumnWidths setObject:[NSNumber numberWithFloat:col.width] forKey:headerName];
            continue;
        }
        else {
            
            NSArray *currentShots = [[uiSetList orderedParentedShots] filteredArrayUsingPredicate:app.filteredChildrenPredicate];
            if (currentShots.count == 0) return;
            NSFont *standardFont = [NSFont systemFontOfSize:14.0];
            
            NSMutableArray * contentWidths = [NSMutableArray array];
            int shotCount = 0;
            
            for (Shot* refShot in currentShots) {
                if (![currentShots containsObject:refShot]) {
                    return;
                }
                if (shotCount == 100) {
                    break;
                }
                
                NSString *output = [refShot.params valueForKey:headerName];
                if (output == nil) {
                    output = @"This is some default text.";
                }
                NSTextStorage *textStorage = [[NSTextStorage alloc] initWithString:output];
                NSTextContainer *textContainer = [[NSTextContainer alloc] initWithContainerSize:NSMakeSize(col.width, FLT_MAX)];
                NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
                
                [layoutManager addTextContainer:textContainer];
                [textStorage addLayoutManager:layoutManager];
                [textStorage addAttribute:NSFontAttributeName value:standardFont range:NSMakeRange(0, [textStorage length])];
                [textContainer setLineFragmentPadding:0.0];
                
                [layoutManager glyphRangeForTextContainer:textContainer];
                NSRect rect = [layoutManager usedRectForTextContainer:textContainer];
                
                float textWidth = rect.size.width;
                
                [contentWidths addObject:[NSNumber numberWithFloat:textWidth]];
                shotCount++;
            }
            
            
            NSNumber * averageWidth = [contentWidths valueForKeyPath:@"@avg.floatValue"];
            
            [tableColumnWidths setObject:averageWidth forKey:headerName];
            
        }
    }
    
    
    NSMutableDictionary *tableColsCopy = [NSMutableDictionary dictionaryWithDictionary:tableColumnWidths];
    
    // calculate appropriate size for column based on average values and store in defaults
    
    for (id headerName in tableColumnWidths) {
        
        NSTableColumn *col = [outlineView tableColumnWithIdentifier:headerName];
        if (col == nil) {
            continue;
        }
        
        M1HeaderCell *headerCell = col.headerCell;
        
        float headerWidth = headerCell.cellSize.width;
        float newColWidth = ceil([[tableColsCopy valueForKey:headerName] floatValue]);
        float newWidth;
        
        if (newColWidth < headerWidth) {
            
            //col.minWidth = headerWidth + 30;
            newWidth = ceil(headerWidth + 30.);
            
            
        } else if (newColWidth > 400.0) {
            
            newWidth = 400.0;
            
        }
        else {
            newWidth = newColWidth + 30.;
        }
        
        [tableColsCopy setValue:@(newWidth) forKey:headerName];
        
    }
    
    // if values are different from the cached values, notify the table to change column widths.
    
    if (![tableColsCopy isEqualToDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"DVTableColumnWidths"]]){
        
        tableColumnWidths = [NSMutableDictionary dictionaryWithDictionary:tableColsCopy];
        [[NSUserDefaults standardUserDefaults] setValue:tableColsCopy forKey:@"DVTableColumnWidths"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TableColumnWidthsDidChangeNotification" object:self];
        });
    }
}

- (void) applyColumnWidths {
    
    NSDictionary *tableColWidthsCopy = [NSDictionary dictionaryWithDictionary:tableColumnWidths];
    
    for (id headerName in tableColWidthsCopy) {
        
        NSTableColumn *col = [outlineView tableColumnWithIdentifier:headerName];
        if (col == nil) {
            continue;
        }
        
        float storedColWidth = [[tableColWidthsCopy valueForKey:headerName] floatValue];
        
        M1HeaderCell *headerCell = col.headerCell;
        float headerWidth = headerCell.cellSize.width;
        
        
        if (storedColWidth < headerWidth) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                col.minWidth = headerWidth + 30;
                col.width = headerWidth + 30;
            });
            
        } else if (storedColWidth > 400.0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                col.width = 400.0;
            });
        }
        else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                col.width = storedColWidth; // 30 buffer is already applied
            });
        }
    }
}



#pragma mark - Handling Updates -

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (context == DVOutlineViewContext) {
        
        filteredChildrenPredicate = app.filteredChildrenPredicate;
        //if ([app.selectedSetList isNotEqualTo:uiSetList]){
        //uiSetList = app.selectedSetList;
            [outlineView reloadData];
            [outlineView expandItem:nil expandChildren:YES];
        //}
        //[outlineViewArrayController bind:@"contentSet" toObject:app.selectedSetList withKeyPath:@"orderedParentedShots" options:nil];
        //      outlineViewArrayController.sortDescriptors = @[app.defaultSorting];
        //      [outlineViewArrayController rearrangeObjects];
    }
}

- (void) processUpdates:(NSNotification *)note
{
    if (![NSThread isMainThread]) return;
    if (isAddingShot) return;
    [outlineView reloadData];
    return;
    
    
//
//    
//    NSSet* insertedObjects = [[note.userInfo objectForKey:NSInsertedObjectsKey]
//                              filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"(class == %@) OR (class == %@)", [SetList class], [Shot class]]];
//     NSSet* updatedObjects = [[note.userInfo objectForKey:NSUpdatedObjectsKey]
//                             filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"(class == %@) OR (class == %@)", [SetList class], [Shot class]]];
//    NSSet* deletedObjects = [[note.userInfo objectForKey:NSDeletedObjectsKey]
//                             filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"(class == %@) OR (class == %@)", [SetList class], [Shot class]]];
////
////    
//    NSArray *shotsToInsert = [[insertedObjects.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [Shot class]]]  sortedArrayUsingDescriptors:@[app.sortByRecordID]];
//    
//    NSArray *shotsToUpdate = [[updatedObjects.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [Shot class]]]  sortedArrayUsingDescriptors:@[app.sortByRecordID]];
//    
//    NSArray *shotsToRemove = [[deletedObjects.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [Shot class]]]  sortedArrayUsingDescriptors:@[app.sortByRecordID]];
//  
//    
//    [outlineView beginUpdates];
//    for (Shot* shot in shotsToUpdate) {
//        
//        NSInteger shotRow = [outlineView rowForItem:shot];
//        if (shotRow == -1) continue;
//        
//        for (int i=0; i < outlineView.numberOfColumns; i++) {
//            NSString *columnName = [[[outlineView tableColumns]objectAtIndex:0] identifier];
//            NSTableCellView *view = [outlineView viewAtColumn:i row:shotRow makeIfNecessary:NO];
//            if (view == nil) continue;
//            
//            if ([[shot.params valueForKey:columnName] isNotEqualTo:view.textField.stringValue]) {
//                [view setNeedsDisplay:YES];
//            }
//        }
//    }
//    
////    
////    for (Shot *uiShot in shotsToInsert) {
////        id uiParent = uiShot.parent;
////        
////        NSInteger insertionIndex = [[uiParent filteredChildren] indexOfObject:uiShot];
////        
////        [outlineView insertItemAtIndex:insertionIndex
////                              inParent:uiParent
////                         withAnimation:NSTableViewAnimationSlideLeft];
////    }
//    
//    [outlineView endUpdates];
}

- (void) mainMOCDidSave:(NSNotification*)note {
}

-(void) updateShotCount:(NSNotification*) note
{
//    NSManagedObjectID *shotID = [note.userInfo valueForKey:@"shotID"];
//    Shot *shot = [Shot existingWithObjectID:shotID inContext:mainMOC];
    
    //[outlineView reloadItem:shot];
    //[outlineView reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:[outlineView rowForItem:shot]] columnIndexes:[NSIndexSet indexSetWithIndex:[[outlineView tableColumns] indexOfObject:[outlineView tableColumnWithIdentifier:@"# of Captures"]]]];
    
}
#pragma mark - Adding Shots

- (void) cloneShot:(NSNotification *)note {
    if (isAddingShot) return;
    isAddingShot = YES;
    
    NSInteger senderRow = [outlineView rowForView:note.object];
    id senderObject = [outlineView itemAtRow:senderRow];
    NSManagedObjectID* senderObjectID = [senderObject objectID];
    DDLogVerbose(@"senderObject: %@", [senderObject description]);
    //isAddingShot = NO;
    [self setContext];
    [bgMOC performBlock:^{
        
        Shot *sourceShot = [Shot existingWithObjectID:senderObjectID inContext:bgMOC];
        Shot *clonedShot = [Shot insertNewObjectIntoContext:bgMOC];
        
        BOOL isAssociated = sourceShot.isLinked;
        clonedShot.params = [NSMutableDictionary dictionaryWithDictionary:sourceShot.params];
        
        for (NSString *propertyName in [[sourceShot entity] propertiesByName]) {
            if (([propertyName isEqualToString:@"subShots"]) ||
                ([propertyName isEqualToString:@"samples"]) ||
                ([propertyName isEqualToString:@"productdatarecords"])){
                continue;
            }
            [clonedShot setValue:[sourceShot valueForKey:propertyName] forKey:propertyName];
        }
        
        
        
        clonedShot.previousParams = nil;
        
        
        clonedShot.online = NO;
        clonedShot.isComplete = 0;
        
        clonedShot.status = @"Ready";
        [clonedShot setShotDuration:[NSNumber numberWithDouble:0]];
        clonedShot.numberOfCaptures = 0;
        
        clonedShot.record_id = [[QBDBInfo info] getLatestRecordIDforTable:@"Shots" orEntity:nil inMOC:bgMOC isTemporary:YES];
        
        [clonedShot setTitle:[clonedShot.title stringByAppendingString:@" (CLONE)"]];
        
        [clonedShot setParam:clonedShot.record_id forKey:@"Record ID#"];
        
        clonedShot.setlist = sourceShot.setlist;
//        clonedShot.parent = sourceShot.parent;
        clonedShot.date_created = [NSDate date];

        //[clonedShot setParam:[clonedShot convertDateForPOST:clonedShot.date_created] forKey:@"Date Created"];
        //[clonedShot setParam:[clonedShot convertDateForPOST:clonedShot.date_modified] forKey:@"Date Modified"];
        clonedShot.dateCompleted = nil;
        
        
        
        Shot *parentShot;
        
        if (isAssociated)
        {
            if (sourceShot.topShot == nil || [sourceShot.topShot isEqualTo: self])  // is it the topShot?
            {
                parentShot = sourceShot;
            }
            else
            {
                parentShot = sourceShot.topShot;
            }
        }
        else
        {
            parentShot = sourceShot;
        }
        
        clonedShot.topShot = parentShot;
        clonedShot.linkedShotRecordID = parentShot.record_id;
        
        sourceShot.isLinked = YES;
        clonedShot.isLinked = YES;
        
        [clonedShot linkAssociatedShots];
        
        clonedShot.parent = nil;
        
        if (parentShot.naming_convention.length > 0){
            clonedShot.naming_convention = [parentShot.naming_convention stringByAppendingString:QBinfo.defaultSeparator];
        }
        
        if ([parentShot.params objectForKey:@"Shot Type"]) {
            [clonedShot setParam:[parentShot.params valueForKey:@"Shot Type"] forKey:@"Shot Type"];
        }
        
        if ([parentShot.params objectForKey:@"Type"]) {
            [clonedShot setParam:[parentShot.params valueForKey:@"Type"] forKey:@"Type"];
        }
        
        
        [clonedShot addSamples:parentShot.samples];
        [clonedShot addProductdatarecords:parentShot.productdatarecords];
        
        // Save & Select new Row and begin editing...
        NSError *saveError = nil;
        if ([bgMOC save:&saveError])
        {
            [mainMOC performBlock:^{
                [mainMOC save:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    Shot *uiShot = [Shot existingWithObjectID:clonedShot.objectID inContext:mainMOC];
                    id uiParent = uiShot.parent;
                    
                    
                    NSInteger insertionIndex = [[uiParent filteredChildren] indexOfObject:uiShot];
                    if ([uiParent isKindOfClass:[SetList class]]) uiParent = nil;
                    
                    [outlineView insertItemAtIndex:insertionIndex
                                          inParent:uiParent
                                     withAnimation:NSTableViewAnimationSlideDown];
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        
                        app.selectedShot = uiShot;
                        [C1PC changeToShot:uiShot];
                        
                        [[NSNotificationCenter defaultCenter]postNotificationName:kSelectionDidChangeNotification
                                                                           object:uiShot.objectID
                                                                         userInfo:@{@"sender" : @"none"}];
                        
                        NSInteger col = [outlineView indexOfTableColumnWithIdentifier:@"Title"];
                        NSInteger row = [outlineView rowForItem:uiShot];
                        if (uiParent != nil) [outlineView reloadItem:uiParent];
                        
                        [outlineView editColumn:col
                                            row:row
                                      withEvent:nil
                                         select:YES];
                        isAddingShot = NO;
                    });
                });
            }];
            
        }
    }];
}

- (NSString*) associatedGroupName:(Shot*)shot {
    Shot *topShot = [shot topShot];
    NSArray *array = [topShot orderedParentedSubShots];
    
    NSArray *titlesArray = [array valueForKeyPath:@"title"];
    NSString *longestString = [NSString string];
    NSString *shortestString = [titlesArray firstObject];
    
    
    for (int i=1; i <= shortestString.length; i++) {
        NSString *testString = [shortestString substringToIndex:i];
        BOOL shouldBreak = false;
        for (NSString *title in titlesArray) {
            if ([[title lowercaseString] rangeOfString:[testString lowercaseString]].location == NSNotFound) {
                shouldBreak = true;
                break;
            }
        }
        if (shouldBreak) break;
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
        [characterSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
        longestString = [testString stringByTrimmingCharactersInSet:characterSet];
    }
    if (longestString.length <= 3) longestString = [topShot name];
    return longestString;
}


#pragma mark - Editing

- (void) controlTextDidEndEditing:(NSNotification *)obj {
    
    id sender = obj.object;
   
    NSInteger senderRow = [outlineView rowForView:sender];
    NSInteger senderCol = [outlineView columnForView:sender];
    NSTableCellView *view = [outlineView viewAtColumn:senderCol row:senderRow makeIfNecessary:NO];
    NSString *columnName =  [[[outlineView tableColumns ] objectAtIndex:senderCol] identifier];
    
    Shot *uiShot = [outlineView itemAtRow:senderRow];
    NSString *newValue = view.textField.stringValue;
    
    [self setContext];
    
    [bgMOC performBlock:^{
        Shot *shot = [Shot existingWithObjectID:uiShot.objectID inContext:bgMOC];
        
        if ([columnName isEqualToString:@"Title"]) {
            if (![shot.title isEqualToString:newValue]) shot.title = newValue;
        }
        else if ([columnName isEqualToString:@"Naming Convention"])
        {
            if (![shot.naming_convention isEqualToString:newValue]) shot.naming_convention = newValue;
        }
        else if ([columnName isEqualToString:@"Status"])
        {
            if (![shot.status isEqualToString:newValue]) shot.status = newValue;
        }
        else if (![[shot.params valueForKey:columnName] isEqualToString:newValue])
        {
            [shot setParam:newValue forKey:columnName];
        }
        
        [shot setDate_modified:[NSDate date]];
        
        NSError *bgSaveError;
        [bgMOC save:&bgSaveError];
        if (bgSaveError)
        {
            [QBinfo alertMe:bgSaveError];
        }
        [mainMOC performBlock:^{
            [mainMOC save:nil];
            [exporter postToServer:[Shot existingWithObjectID:shot.objectID inContext:mainMOC] onComplete:nil];
        }];
    }];
}


#pragma mark - Handling Selection Notifications
-(void) processSelection:(NSNotification*) note {
    
    id sender = [note.userInfo valueForKey:@"sender"];
    if ([sender isEqualTo:self]) return;
    
    id target = note.object;
    id targetObject;
    
    
    if ([target isKindOfClass:[DummyObject class]])
    {
        targetObject = [target parent];
    }
    else
    {
        targetObject = [mainMOC existingObjectWithID:target error:nil];
    }
    
    [outlineView selectObject:targetObject];

}

#pragma mark - NSOutlineView Datasource Methods

-(NSInteger) outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
    NSInteger itemCount;
    NSArray *children;
    if (app == nil) [self awakeFromNib];
    if (!item)
    {
        children = [[uiSetList orderedParentedShots] filteredArrayUsingPredicate:app.filteredChildrenPredicate];
    }
    else
    {
        children = [[item orderedParentedSubShots] filteredArrayUsingPredicate:app.filteredChildrenPredicate];
    }
    
    itemCount = [children filteredArrayUsingPredicate:app.searchFieldPredicate].count;
    return itemCount;
}

-(id) outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    id returnChild;
    NSArray *children;
    
    if (item == nil)
    {
        children = [[uiSetList orderedParentedShots] filteredArrayUsingPredicate:app.filteredChildrenPredicate];
    }
    else
    {
        children = [[item orderedParentedSubShots] filteredArrayUsingPredicate:app.filteredChildrenPredicate];
    }
    
    returnChild = [[children filteredArrayUsingPredicate:app.searchFieldPredicate] objectAtIndex:index];
    return returnChild;
}

-(id) outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
    return item;
}

-(BOOL) outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    if (!item)
    {
        return YES;
    }
    else
    {
        NSArray *children = [[[item orderedParentedSubShots] filteredArrayUsingPredicate:app.filteredChildrenPredicate] filteredArrayUsingPredicate:app.searchFieldPredicate];
        return children.count > 0;
    }
}


#pragma mark - NSOutlineView Delegate Methods
// -------------------------------------------------------------------------------
//	viewForTableColumn:tableColumn:item
// -------------------------------------------------------------------------------
- (NSView *)outlineView:(NSOutlineView *)outline viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    NSString *columnName = [tableColumn identifier];
    
    if ([item isKindOfClass:[DummyObject class]]) {
        [[QBDBInfo info] alertString:@"Dummy Object!"];
        if ([columnName isEqualToString:@"Title"]) {
            AddShotCellView *cellView = [outline makeViewWithIdentifier:@"AddShotCellView" owner:outline];
            cellView.dummyobj = (DummyObject*) item;
            [cellView setNeedsDisplay:YES];
            return cellView;
        }
        else
        {
            MainTableCellView *cellView = [outlineView makeViewWithIdentifier:@"MainTableCellView" owner: outlineView];
            cellView.textField.editable = NO;
            return cellView;
        }
    }
    
    Shot *shot = (Shot*) item;
    
    if ([columnName isEqualToString:@"IconColumn"])
    {
        IconTableCellView *cellView = [outlineView makeViewWithIdentifier:@"IconTableCellView" owner:outlineView];
        [cellView bind:@"shot" toObject:shot withKeyPath:@"self" options:nil];
        [cellView setupCellView];
        //cellView.shot = shot;
        [cellView setNeedsDisplay:YES];
        return cellView;
        
    }
    else if ([columnName isEqualToString:@"Title"])
    {
        MainTableCellView *cellView;
        if (shot.isLinked) {
            cellView = [outlineView makeViewWithIdentifier:@"AssociatedTitleCellView" owner: outlineView];
            // cellView = [_outlineView makeViewWithIdentifier:@"MainTableCellView" owner: _outlineView];
        }
        else {
            cellView = [outlineView makeViewWithIdentifier:@"MainTableCellView" owner:outlineView];
        }
        
        [cellView bind:@"shot" toObject:(Shot*)item withKeyPath:@"self" options:nil];
        [cellView bind:@"key" toObject:tableColumn withKeyPath:@"identifier" options:nil];
//        [cellView.textField bind: NSValueBinding toObject:cellView withKeyPath: @"objectValue.params.Title" options: nil];
        cellView.textField.delegate = self;
        [cellView setupCellView];
        
        // store the key in the identifier for easy access
        //cellView.shot = shot;
        //cellView.key = [tableColumn identifier];
        
        [cellView.textField setAlignment:NSTextAlignmentLeft];
        [cellView.textField setEditable:YES];
        
        //[cellView setNeedsDisplay:YES];
        return cellView;
    }
    else
    {
        MainTableCellView *cellView = [outlineView makeViewWithIdentifier:@"MainTableCellView" owner: outlineView];
        
        
        // store the key in the identifier for easy access
        [cellView bind:@"shot" toObject:(Shot*)item withKeyPath:@"self" options:nil];
        [cellView bind:@"key" toObject:tableColumn withKeyPath:@"identifier" options:nil];
        [cellView setupCellView];
        //NSString *keyPath = [NSString stringWithFormat:@"objectValue.params.%@", columnName];
        //[cellView.textField bind: NSValueBinding toObject:cellView withKeyPath: keyPath options: nil];
        
        //cellView.shot = shot;
        //cellView.key = [tableColumn identifier];
        
        cellView.textField.delegate = self;
        
        if ([columnName isEqualToString:@"# of Captures"])
        {
            [cellView.textField setAlignment:NSTextAlignmentCenter];
            [cellView.textField setEditable:NO];
        }
        else
        {
            [cellView.textField setAlignment:NSTextAlignmentLeft];
            [cellView.textField setEditable:YES];
        }
        
        [cellView setNeedsDisplay:YES];
        return cellView;
    }
}



- (void)outlineViewSelectionDidChange:(NSNotification *)note {
    DDLogVerbose(@"DidChange: selectedObject: %@", outlineView.selectedObject);
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)selectedObj
{
    if ([selectedObj isKindOfClass:[DummyObject class]])  {
        return NO;
    }
    else {
        
        if ([selectedObj isKindOfClass:[SetList class]])
        {
            app.selectedShot = nil;
            app.selectedSetList = (SetList*) selectedObj;
            
            [C1PC changeToShot:nil];
        }
        else if ([selectedObj isKindOfClass:[Shot class]])
        {
            app.selectedShot = selectedObj;
            app.selectedSetList = [selectedObj setlist];
            
            [C1PC changeToShot:selectedObj];
        }
        else if (selectedObj == nil) {
            DDLogVerbose(@"No item selected");
            app.selectedShot = nil;
            app.selectedSetList = nil;
            [C1PC changeToShot:nil];
        }
        else
        {
            DDLogVerbose(@"SelectedItem is not SetList or Shot");
            app.selectedShot = nil;
            app.selectedSetList = nil;
            
            [C1PC changeToShot:nil];
        }
        
        // post selection notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kSelectionDidChangeNotification
                                                            object:(!selectedObj)? nil : [selectedObj objectID]
                                                          userInfo:@{@"sender" : self}];
        
        return YES;
    }
}

-(CGFloat) outlineView:(NSOutlineView *)outlineView heightOfRowByItem:(id)item {
    {
        
        if (![item isKindOfClass:[Shot class]])
        {
            return 26.0;
        }
        else
        {
            Shot *shot = (Shot*)item;
            @try {
                NSFont *standardFont = [NSFont systemFontOfSize:14.0];
                NSMutableArray * contentHeights = [NSMutableArray array];
                
                for (NSTableColumn *col in [outlineView tableColumns]) {
                    if (([[col identifier] isEqualToString:@"IconColumn"]) ||
                        ([[shot.params valueForKey:[col identifier]]isKindOfClass:[NSNumber class]]))
                    {
                        [contentHeights addObject:[NSNumber numberWithFloat:26.0]];
                    }
                    else {
                        
                        NSString *output = [shot.params valueForKey:[col identifier]];
                        
                        NSTextStorage *textStorage = [[NSTextStorage alloc] initWithString:output];
                        NSTextContainer *textContainer = [[NSTextContainer alloc] initWithContainerSize:NSMakeSize(col.width, FLT_MAX)];
                        NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
                        
                        [layoutManager addTextContainer:textContainer];
                        [textStorage addLayoutManager:layoutManager];
                        [textStorage addAttribute:NSFontAttributeName value:standardFont range:NSMakeRange(0, [textStorage length])];
                        [textContainer setLineFragmentPadding:0.0];
                        
                        [layoutManager glyphRangeForTextContainer:textContainer];
                        NSRect rect = [layoutManager usedRectForTextContainer:textContainer];
                        float textHeight = rect.size.height;
                        [contentHeights addObject:[NSNumber numberWithFloat:textHeight]];
                    }
                }
                
                NSNumber *max = [contentHeights valueForKeyPath:@"@max.floatValue"];
                float new_height = ceil(max.floatValue) + 10;
                if (new_height < 26) {
                    //  DDLogVerbose(@"RowHeight: %f", 26.0);
                    return 26.0;
                } else {
                    return new_height;
                }
            }
            @catch (NSException *exception)
            {
                return 26.0;
            }
        }
    }
}

- (NSTableRowView*) outlineView:(NSOutlineView *)outlineView rowViewForItem:(id)obj {
    
    M1TableRowView *rowView;
    
    if (![obj isKindOfClass:[Shot class]]) return [[M1TableRowView alloc] init];
    
    Shot *shot = (Shot*)obj;
    
    if (shot.isLinked) {
        rowView = [[M1TableRowView alloc] init];
        rowView.isAssociated = YES;
        rowView.fillAlpha = .05;
        
        if ([shot isEqualTo:shot.topShot] || [shot isEqualTo:[[shot.topShot orderedParentedSubShots] lastObject]])
        {
            rowView.isLast = YES;
            rowView.fillAlpha = .1;
        }
    }
    else
    {
        rowView = [[M1TableRowView alloc] init];
    }
    return rowView;
    
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
    // don't allow empty node names
    //return (fieldEditor.string.length == 0 ? NO : YES);
    return YES;
}


#pragma mark - Core Data Methods
- (void) setContext {
    bgMOC = nil;
    
    bgMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [bgMOC setParentContext:mainMOC];
}

- (void) initiateSave:(completionBlock)completionBlock {
    // Save the MOC
    DDLogVerbose(@"***OV MOC Save");
    
    __block NSError *error = nil;
    if (bgMOC.hasChanges){
        if ([bgMOC save:&error]) {
            if (error != nil) {
                [[QBDBInfo info] alertMe:error];
            }
            else {
                
                [mainMOC save:nil];
                
                if (completionBlock != nil){
                    completionBlock();
                }
            }
            
            
        } else {
            DDLogError(@"TC: Error Saving mainMOC: %@", error);
        }
    }
    else
    {
        if (completionBlock != nil){
            completionBlock();
        }
        
    }
    
}

@end
