//
//  M1TableView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 10/15/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface M1TableView : NSOutlineView

@property (nonatomic, assign) BOOL isReloadingData;
@property (nonatomic, nullable, weak) id selectedObject;

- (void) selectRowIndex:(NSUInteger) row;
- (void) selectObject:(nonnull id)anObject;

- (void)insertItemAtIndex:(NSUInteger)index inParent:(nullable id)parent withAnimation:(NSTableViewAnimationOptions)animationOptions;
- (void) expandItemAndParentItems:(nullable id)item;
- (void) deselectAllRows;

- (NSInteger) indexOfTableColumnWithIdentifier:(NSString*)columnName;
- (id) itemForView:(NSView*)view;




@end
