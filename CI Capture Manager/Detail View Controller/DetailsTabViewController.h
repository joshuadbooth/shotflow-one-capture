//
//  DetailsViewController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 11/24/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "DetailsView.h"

#import "DetailsOutlineViewController.h"

#import "M1TableView.h"
#import "IconTableCellView.h"
#import "MainTableCellView.h"

#import "M1ArrayController.h"
#import "SetListsArrayController.h"
#import "ShotsArrayController.h"
#import "SetList.h"
#import "Shot.h"



@class DetailsTabViewController;

@interface DetailsTabViewController : NSViewController 


@property (nonatomic, strong) IBOutlet M1TableView *detailTableView;
@property (strong) IBOutlet DetailsOutlineViewController *detailsOutlineViewController;


@property (strong) IBOutlet NSStackView *stackView;
@property (strong) IBOutlet NSTextField *setListNameTextField;
@property (strong) IBOutlet NSView *jobIDView;
@property (strong) IBOutlet NSTextField *jobidLabel;
@property (strong) IBOutlet NSTextField *jobidTextField;
@property (strong) IBOutlet NSView *typeView;
@property (strong) IBOutlet NSTextField *typeLabel;
@property (strong) IBOutlet NSTextField *typeTextField;
@property (strong) IBOutlet NSView *statusView;
@property (strong) IBOutlet NSTextField *statusLabel;
@property (strong) IBOutlet NSTextField *statusTextField;
@property (strong) IBOutlet NSView *priorityView;
@property (strong) IBOutlet NSTextField *priorityLabel;
@property (strong) IBOutlet NSTextField *priorityTextField;




@property (strong) IBOutlet NSButton *selectPreviousButton;
@property (strong) IBOutlet NSButton *addButton;
@property (strong) IBOutlet NSButton *selectNextButton;

- (void) reloadData;


- (IBAction)addShot:(id)sender;
- (IBAction)nextShot:(id)sender;
- (IBAction)previousShot:(id)sender;

@end
