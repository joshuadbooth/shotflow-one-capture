//
//  SamplesTabViewController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 10/26/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Shot.h"
#import "M1ArrayController.h"

@interface ProductDataTabViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate>

@property (nonatomic, strong) IBOutlet NSTableView *pdrsDetailTableView;
@property (nonatomic, strong) IBOutlet NSTextField *shotNameField;
@property (nonatomic, strong) Shot *selectedShot;


- (void) setupTable;
- (void) reloadData;

@end
