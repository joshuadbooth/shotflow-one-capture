//
//  DetailViewWindowController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/3/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "IconTableCellView.h"
#import "MainTableCellView.h"
#import "M1TableView.h"
#import "M1TabView.h"
#import "ProductDataTabViewController.h"
#import "DetailsTabViewController.h"
#import "DetailsOutlineViewController.h"


@class SetList, ShotsArrayController;

@interface DetailViewWindowController : NSWindowController <NSTabViewDelegate, NSTextFieldDelegate, NSWindowDelegate>

+(instancetype) sharedController;

@property (strong) IBOutlet DetailsTabViewController *detailsTabViewController;
@property (strong) IBOutlet ProductDataTabViewController *pdrTabViewController;
@property (strong) IBOutlet NSTabViewItem *detailsTabView;
@property (strong) IBOutlet NSTabViewItem *pdrsTabView;

@property (strong) IBOutlet NSTableView *pdrsTableView;

@property (strong) IBOutlet DetailsOutlineViewController *detailsOutlineViewController;

@property (nonatomic, strong) ShotsArrayController *shotsArrayController;
@property (nonatomic, strong) M1ArrayController *samplesArrayController;


@property (nonatomic, strong) SetList *setList;
@property (strong) IBOutlet M1TableView *detailTableView;
@property (strong) IBOutlet M1TabView *tabView;

@property (strong) IBOutlet IconTableCellView *iconTableCellView;

@property (nonatomic) BOOL setupComplete;

@property (weak) IBOutlet NSButton *closeButton;
- (IBAction)closeDetailView:(id)sender;


@end
