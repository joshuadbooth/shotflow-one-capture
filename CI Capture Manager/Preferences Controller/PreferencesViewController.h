//
//  PreferencesViewController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/1/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface PreferencesViewController : NSViewController

@end
