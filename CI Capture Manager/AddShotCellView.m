//
//  AddShotCellView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "AddShotCellView.h"

@implementation AddShotCellView
NSString *kAddShotNotification = @"AddShotNotification";


- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

- (IBAction)addShot:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kAddShotNotification object:self];
}


@end
