//
//  InfoViewController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@import AppKit;

@interface InfoViewController : NSViewController

extern NSString *kUpdateStatusNotification;

@property (weak) IBOutlet NSProgressIndicator *syncProgress;
@property (weak) IBOutlet NSTextField *syncStatus;
@property (weak) IBOutlet NSButton *syncButton;

- (IBAction)trySync:(id)sender;

- (void) updateListViewStatus:(NSString*)statusText;

@end
