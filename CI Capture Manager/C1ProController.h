//
//  C1ProController.h
//  QBApiTest2
//
//  Created by Josh Booth on 7/20/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <ScriptingBridge/ScriptingBridge.h>
#import "C1Pro.h"

#import "SCEventListenerProtocol.h"
#import "SCEvents.h"
#import "SCEvent.h"

#import "M1FileWatcher.h"

#import "Shot.h"
#import "ProductDataRecord.h"



typedef void (^completionBlock) (void);

@interface C1ProController : NSObject <SCEventListenerProtocol, SBApplicationDelegate> {
    SCEvents *events;
}

@property (nonatomic) id delegate;

extern NSString *kInfoDownloadedForShotNotification;
extern NSString *kCaptureCountUpdatedNotification;
extern NSString *kCameraOfflineNotification;
extern NSString *kCameraOnlineNotification;

@property (nonatomic, retain) C1ProApplication *C1Pro;
@property (nonatomic, retain) C1ProDocument *session;
@property (nonatomic, retain) C1ProCamera *camera;
@property (nonatomic, retain) NSString *imageFormat;

@property (nonatomic, retain) NSError *C1VersionError;
@property (nonatomic, retain) NSError *C1RunningError;
@property (nonatomic, retain) NSError *C1CatalogError;

@property (nonatomic, retain) NSURL *watchFolder;
@property (nonatomic) BOOL isWatching;

@property (nonatomic) BOOL isTiming;

@property (nonatomic, strong) Shot *activeShot;
@property (nonatomic, strong) NSString *captureName;

@property (nonatomic, strong) NSString *fileName;
@property (nonatomic, strong) NSString *captureID;

@property (nonatomic) BOOL didBegin;
@property (nonatomic) int attempts;

@property (nonatomic) dispatch_queue_t c1ControllerQueue;
@property (nonatomic, retain) NSOperationQueue *xmpOperationQueue;

//@property (nonatomic, retain) NSManagedObjectContext *privateMOC;

+ (C1ProController *) sharedInstance;
- (NSString*) generateCaptureUUID;

- (void) getSessionInfo;
- (BOOL) validateSession:(C1ProDocument*)document;
- (void) getCameraInfo;

- (void) initiateSave:(completionBlock)completionBlock;
- (void) prepareToTerminate;

- (BOOL) startWatching;
- (BOOL) stopWatching;
- (BOOL) setupEventListener;
- (void) assignCaptureFolder;

- (void) takeCapture;
- (void) cleanUpFavorites;

- (BOOL) changeShots;
- (BOOL) changeToShot:(Shot*)toShot;
- (void) setNextCaptureNaming;
- (void) writeXMPtoFile:(NSString*)filepath;

- (void) generatePanelsAdmin:(NSString*)username Password:(NSString*)password;

@end
