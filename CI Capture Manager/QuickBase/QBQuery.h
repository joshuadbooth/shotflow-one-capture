//
//  QBQuery.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/16/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RXMLElement.h"
#import "QBDBInfo.h"

@class QBTable, QBField;

@interface QBQuery : NSObject

@property (nonatomic, retain) NSString *query;
@property (nonatomic, readonly) NSString *safeQuery;   // automatic
@property (nonatomic, readonly) NSString *fullQuery;   // automatic
@property (nonatomic, readonly) NSURL *queryURL;       // automatic
@property (nonatomic, retain) NSMutableString *rawParams;
@property (nonatomic, retain) NSMutableString *rawQueryString;
@property (nonatomic, retain) NSString *queryAction;
@property (nonatomic, retain) NSString *resultsString;

@property (nonatomic, retain) NSString *tableName;
@property (nonatomic, strong) NSString *tableID;
@property (nonatomic, strong) QBTable *table;
@property (nonatomic, strong) QBField  *field;

@property (nonatomic, strong) NSMutableString* payload;


@property (nonatomic, retain) NSError *queryError;

typedef void (^SuccessBlock)(RXMLElement*);
typedef void (^FailureBlock)(NSError *_Nullable);

//@property (nonatomic, copy, nullable) RXMLElement* (^successCallback)(RXMLElement*);
//@property (nonatomic, copy, nullable) void (^failureCallback)(NSError **);
@property (nonatomic, copy, nullable) SuccessBlock successCallback;
@property (nonatomic, copy, nullable) FailureBlock failureCallback;

+(QBQuery*) authenticate;

+(QBQuery*) queryForAction:(NSString*)action
                   inTable:(NSString*)tableName
                     inMOC:(NSManagedObjectContext*)moc
                withParams:(NSDictionary*)params
             includeTicket:(BOOL)includeTicket;

+(QBQuery*) defaultQueryInTable:(NSString*)table
                          inMOC:(NSManagedObjectContext*)moc
                withQueryString:(NSString*)queryString;


-(NSString*) createQueryStringForAction:(NSString*)action
                                inTable:(NSString*)tableName
                                  inMOC:(NSManagedObjectContext*)moc
                             withParams:(NSDictionary*)params
                          includeTicket:(BOOL)includeTicket;

- (RXMLElement*) execute:(NSError**)error;

- (void) executeWithSuccessCallback:(SuccessBlock)success
                   failureCallback : (FailureBlock)failure;

//- (void) executeWithSuccessCallback:(RXMLElement* _Nullable (RXMLElement* _Nonnull xmlResults))success
//                   failureCallback : (void(^_Nullable)(NSError ** error))failure;

- (NSError*) generateError:(RXMLElement*)rootXML;


+ (NSString *) buildQueryStringForTable:(NSString*)tableName
                              fieldName:(NSString*)fname
                     comparisonOperator:(NSString*)co
                           compareValue:(id)cValue
                                  inMOC:(NSManagedObjectContext*)moc;

+ (NSString*) defaultQueryString;

@end
