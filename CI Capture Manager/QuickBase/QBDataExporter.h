//
//  QBDataExporter.h
//  QBApiTest2
//
//  Created by Josh Booth on 7/8/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "QBDBInfo.h"

#import "Set.h"

typedef void (^completionBlock)(void);
typedef void (^errorBlock)(void);
typedef void (^recoveryBlock) (void);


@interface QBDataExporter : NSObject

//@property (nonatomic, strong) NSManagedObjectContext *mainMOC;

@property (nonatomic, strong) QBDBInfo *QBinfo;
@property (nonatomic) BOOL isPostingUpdates;
@property (nonatomic, retain) NSMutableArray *objectsBeingUpdated;


+ (id) sharedExporter;


- (void) setContext;
- (void) operationAddRecords:(NSArray <NSManagedObjectID*> *) objects onComplete:(completionBlock)completionBlock;
- (void) operationUpdateRecords:(NSArray <NSManagedObjectID*> *) objects onComplete:(completionBlock)completionBlock;

- (void) contextUpdated:(NSNotification *) note;

- (void) postToServer:(id)obj;
- (void) postToServer:(id)obj onComplete:(completionBlock)completionBlock;

- (NSString *) convertDateForPOST: (NSDate *) date;

#pragma mark - Many to Many
- (void) createLinkedShotID:(NSManagedObjectID*)shotID withSampleID:(NSManagedObjectID*)sampleID inMOC:(NSManagedObjectContext *)moc;
- (void) createLinkedShotID:(NSManagedObjectID*)shotID withPDRID:(NSManagedObjectID*)pdrID inMOC:(NSManagedObjectContext *)moc;
- (void) createLinkedGroupingRID:(NSNumber*)groupingRID withPDRIDs:(NSArray*)pdrIDs inMOC:(NSManagedObjectContext *)moc;


@end
