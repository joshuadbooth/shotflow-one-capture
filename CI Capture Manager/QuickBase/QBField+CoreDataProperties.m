//
//  QBField+CoreDataProperties.m
//  CI Capture Manager
//
//  Created by Josh Booth on 3/17/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QBField+CoreDataProperties.h"

@implementation QBField (CoreDataProperties)

@dynamic base_type;
@dynamic fid;
@dynamic field_type;
@dynamic formula;
@dynamic mode;
@dynamic name;
@dynamic mastag;
@dynamic isPrimary;
@dynamic table;
@dynamic pkForTable;
@dynamic refForTable;
@dynamic mastagTable;

@end
