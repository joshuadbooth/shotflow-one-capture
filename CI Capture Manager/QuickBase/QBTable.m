//
//  QBTable.m
//  CI Capture Manager
//
//  Created by Josh Booth on 2/9/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "QBTable.h"
#import "AppDelegate.h"
#import "QBField.h"
#import "RXMLElement.h"
#import "SF1XMLRecord.h"

@implementation QBTable

// Insert code here to add functionality to your managed object subclass

#pragma mark - Easy Creation -

+ (NSString *)entityName {
    return @"QBTable";
}

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context withName:(NSString*)tname DBID:(NSString*)db
{
    QBTable* obj = [NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                           inManagedObjectContext:context];
    obj.name = tname;
    obj.dbid = db;
    
    NSError *permIDError = nil;
    [context obtainPermanentIDsForObjects:@[obj] error:&permIDError];
    return obj;
}

-(NSString*) description {
    return [NSString stringWithFormat:@"%@ - %@", self.dbid, self.name];
}


+ (instancetype) tableNamed:(NSString *)tname inMOC:(NSManagedObjectContext *)moc
{    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", tname];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"QBTable"];
    request.predicate = predicate;
    NSError *fetchError = nil;
    
    NSArray *tables = [moc executeFetchRequest:request error:&fetchError];
    QBTable *table =  [tables firstObject];
    
    if (fetchError == nil) {
        return table;
    } else {
        DDLogDebug(@"Error Fetching Table '%@' ErrDetails: %@", tname, fetchError);
        return nil;
    }
}

-(QBField*) fieldForMastagTableNamed:(NSString *)tableName {
    QBTable *targetTable = [QBTable tableNamed:tableName inMOC:self.managedObjectContext];
    NSSet *mFields = [self.mastagFields filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"mastag == %@", targetTable.alias]];
    if (mFields.count > 1) {
        DDLogWarn(@"WARNING: Multiple mastag fields for %@ found and may produce errors. Fields: %@", tableName, [mFields valueForKeyPath:@"description"]);
    }
    QBField *mField = [mFields anyObject];
    return mField;
}

- (NSString*) fieldNameForMastagTableNamed:(NSString*)tableName {
    if (self.mastagFields.count == 0) {
        @throw [NSException exceptionWithName:@"NoMastagFieldsException"
                                       reason:[NSString stringWithFormat:@"Table '%@' doesn't have any mastag fields", tableName]
                                     userInfo:nil];
        return nil;
    }
    QBField *mField = [self fieldForMastagTableNamed:tableName];
    return mField.name;
}


- (QBField*)fieldNamed:(NSString*)fnm {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@", fnm];
    return ([[self.fields filteredSetUsingPredicate:predicate] anyObject]);
}

- (QBField*)fieldWithID:(NSString*)fid {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"fid = %@", fid];
    return ([[self.fields filteredSetUsingPredicate:predicate] anyObject]);
}


- (NSSet*) fields_illegal {
    
    NSArray *illegalFieldTypes = @[@"URL", @"recordid", @"dblink", @"userid"];
    NSArray *illegalFieldModes = @[@"virtual", @"lookup", @"summary"];
    NSArray *illegalFieldNames = @[@"Date Created", @"Date Modified"];
    
    NSPredicate *illegalPredicate = [NSPredicate predicateWithFormat:
                                     @"(%K != nil)OR(%K IN %@)OR(%K IN %@)OR(%K IN %@)",
                                     @"formula",
                                     @"field_type", illegalFieldTypes,
                                     @"mode", illegalFieldModes,
                                     @"name", illegalFieldNames];
    
    NSSet *illegalFields = [self.fields filteredSetUsingPredicate:illegalPredicate];
    
    return illegalFields;
}

- (id) fields_detailView {
    NSArray *fids = [self.clist_detailview componentsSeparatedByString:@"."];
   
    NSMutableArray *array = [NSMutableArray array];
    for (id fid in fids) {
        QBField *field = [self fieldWithID:fid];
        if (field)[array addObject:field];
    }
    return [NSArray arrayWithArray:array];

}

- (id) fields_xmp {
    NSArray *fids = [self.clist_xmp componentsSeparatedByString:@"."];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(fid IN %@)", fids];
    NSMutableArray *array = [NSMutableArray array];
    for (id fid in fids) {
        [array addObject:[self fieldWithID:fid]];
    }
    
    return [NSArray arrayWithArray:array];
}

#pragma mark - Field Type Conversion - 

- (id) convertValue:(NSString*)val toCorrectValueTypeForFieldNamed:(NSString*)fieldName {
    QBField *targetedField = [self fieldNamed:fieldName];
    
    if (targetedField == nil) {
        DDLogVerbose(@"ERROR: field named '%@' not Found", fieldName);
        return nil;
    }
    
    if ([targetedField.base_type isEqualToString:@"text"]) {
        return val;
    }
    else if ([targetedField.base_type isEqualToString:@"float"]) {
        return [NSNumber numberWithFloat:[val floatValue]];
    }
    else if ([targetedField.base_type rangeOfString:@"int"].location != NSNotFound) {
        return [NSNumber numberWithInteger:[val integerValue]];
    }
    else if ([targetedField.base_type isEqualToString:@"bool"]) {
        @try {
            return [NSNumber numberWithBool:[val boolValue]];
        } @catch (NSException *exception) {
            DDLogVerbose(@"targetedField Base Type Exception: %@", exception);
            return [NSNumber numberWithInteger:[val integerValue]];
        }
    }
    else {
        return nil;
    }
    
}



- (id) convertXMLRecordPKValueToCorrectType:(SF1XMLRecord*)record {
    
    if ([self.primaryKey.base_type isEqualToString:@"text"]) {
        return [record childWithID:self.primaryKey.fid].text;
    }
    else if ([self.primaryKey.base_type isEqualToString:@"float"]) {
        return [NSNumber numberWithDouble:[record childWithID:self.primaryKey.fid].textAsDouble];
    }
    else if ([self.primaryKey.base_type rangeOfString:@"int"].location != NSNotFound) {
        return [NSNumber numberWithInteger:[record childWithID:self.primaryKey.fid].textAsInt];
    }
    else
    {
        @throw [NSException exceptionWithName:@"UndeterminedBaseTypeException" reason:@"Base Type is not text, float, or int." userInfo:nil];
        return [record childWithID:self.primaryKey.fid].text;
    }
}

- (id) convertXMLRecordValue:(SF1XMLRecord*)record toCorrectValueTypeForFieldNamed:(NSString*)fieldName {
    QBField *targetedField = [self fieldNamed:fieldName];
//    NSLog(@"targetedField: %@", targetedField.name);
    id returnValue;
    if (targetedField == nil) {
        DDLogVerbose(@"ERROR: field named '%@' not Found", fieldName);
        returnValue = nil;
    }
    
    if ([targetedField.base_type isEqualToString:@"text"]) {
        returnValue = [record childWithID:targetedField.fid].text;
    }
    else if ([targetedField.base_type isEqualToString:@"float"]) {
//        NSLog(@"returnValue: %@", [NSNumber numberWithDouble:[record childWithID:targetedField.fid].textAsDouble]);
        returnValue = [NSNumber numberWithDouble:[record childWithID:targetedField.fid].textAsDouble];
    }
    else if ([targetedField.base_type rangeOfString:@"int"].location != NSNotFound) {
//        NSLog(@"returnValue: %@", [NSNumber numberWithInteger:[record childWithID:targetedField.fid].textAsInt]);
        returnValue = [NSNumber numberWithInteger:[record childWithID:targetedField.fid].textAsInt];
    }
    else if ([targetedField.base_type isEqualToString:@"bool"]) {
        @try {
            returnValue =  [NSNumber numberWithBool:[record childWithID:targetedField.fid].textAsInt];
        } @catch (NSException *exception) {
            DDLogVerbose(@"targetedField Base Type Exception: %@", exception);
            returnValue =  [NSNumber numberWithInteger:[record childWithID:targetedField.fid].textAsInt];
        }
    }
    else {
        returnValue =  nil;
    }
    
    return returnValue;
}

@end
