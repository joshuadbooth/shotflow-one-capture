//
//  QBTable+CoreDataProperties.m
//  CI Capture Manager
//
//  Created by Josh Booth on 3/17/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QBTable+CoreDataProperties.h"

@implementation QBTable (CoreDataProperties)

@dynamic clist_detailview;
@dynamic clist_query;
@dynamic clist_xmp;
@dynamic date_modified;
@dynamic dbid;
@dynamic fields_detailView;
@dynamic fields_illegal;
@dynamic fields_xmp;
@dynamic name;
@dynamic alias;
@dynamic key_fid;
@dynamic fields;
@dynamic mastagFields;
@dynamic primaryKey;
@dynamic refForField;

@end
