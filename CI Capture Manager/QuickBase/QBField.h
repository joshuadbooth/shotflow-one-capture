//
//  QBField.h
//  CI Capture Manager
//
//  Created by Josh Booth on 2/9/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class QBTable;

NS_ASSUME_NONNULL_BEGIN

@interface QBField : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+ (NSString *)entityName;
+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context withName:(NSString*)fnm FID:(NSString*)fID;

+ (instancetype) fieldNamed:(NSString*)fnm inTableNamed:(NSString*)tnm inMOC:(NSManagedObjectContext*)moc;
+ (instancetype) fieldWithID:(NSString*)fid inTableNamed:(NSString*)tnm inMOC:(NSManagedObjectContext*)moc;

@end

NS_ASSUME_NONNULL_END

#import "QBField+CoreDataProperties.h"
