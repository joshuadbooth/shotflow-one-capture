
//
//  QBDataExporter.m
//  QBApiTest2
//
//  Created by Josh Booth on 7/8/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "QBDataExporter.h"
#import "RXMLElement.h"

#import <CoreData/CoreData.h>
#import "QBDataImporter.h"
#import "QBSyncManager.h"
#import "QBQuery.h"

#import "SetList.h"
#import "Shot.h"
#import "ProductDataRecord.h"
#import "Sample.h"
#import "Station+CoreDataClass.h"

#import "QBTable.h"
#import "QBField.h"




@interface QBDataExporter ()
@property (strong, nonatomic) AppDelegate *app;
@property (nonatomic, strong) NSOperationQueue *operationManager;

@end

@implementation QBDataExporter {
    NSManagedObjectContext *mainMOC;
    NSManagedObjectContext *bgMOC;
    // NSManagedObjectContext *privateMOC;
}

@synthesize QBinfo;
@synthesize app;
@synthesize isPostingUpdates;
@synthesize operationManager;


+ (id) sharedExporter {
    static QBDataExporter *sharedExporter  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedExporter = [[self alloc] init];
    });
    return sharedExporter;
}


- (instancetype) init {
    self = [super init];
    
    app = [AppDelegate sharedInstance];
    QBinfo = [QBDBInfo info];
    
    
    mainMOC = app.mainMOC;
    
    return self;
}


- (void) postToServer:(id)obj {
    [self postToServer:obj onComplete:nil];
}

- (void) postToServer:(id)obj onComplete:(completionBlock)completionBlock {
    if (operationManager == nil) {
        operationManager = [[QBSyncManager sharedManager] uploadOperationQueue];
    }
    
    if (![obj online]){
        // Add Record
        [self operationAddRecord:obj onComplete:completionBlock];
    }
    else {
        // Update Record
        [self operationUpdateRecord:[obj objectID] onComplete:completionBlock];
    }
}

#pragma mark - Context Info
#pragma mark -

- (NSManagedObjectContext*) privateContext {
    
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [context setParentContext:mainMOC];
    DLog(@"\n\n\tQBExporter()\n\t\tprivateMOC: %@  \n\t\tmainMOC: %@", context, mainMOC);
    return context;
}



#pragma mark - Inserting Objects -

-(void) operationAddRecord:(id) obj onComplete:(completionBlock)completionBlock
{
        id objID = [obj objectID];
        
        NSBlockOperation *operation = [NSBlockOperation  blockOperationWithBlock:^{
            
            NSManagedObjectContext *privateMOC = [self privateContext];
            
            [privateMOC performBlockAndWait:^{
                NSError *fetchError = nil;
                id object = [privateMOC existingObjectWithID:objID error:&fetchError];
               
                if (fetchError != nil ) {
                    [QBinfo alertMe:fetchError];
                }
                else {
                    
                    DDLogVerbose(@"Operation: AddRecord - Object: (%@)%@", [object class], [object name]);
                    
                    if ([object isKindOfClass:[Set class]] )
                    {
                        
                    }
                    else if ([object isKindOfClass:[SetList class]])
                    {
                        
                    }
                    else if ([object isKindOfClass:[Shot class]])
                    {
                        Shot* shot = (Shot*) object;
                        
                        NSMutableDictionary *postParams = [NSMutableDictionary dictionaryWithDictionary:[shot listPropertiesToPOST]];
                        
                        QBQuery *addRecordQuery = [QBQuery queryForAction:@"API_AddRecord" inTable:@"Shots" inMOC:privateMOC withParams:postParams includeTicket:YES];
                        
                        NSError *addError = nil;
                        
                        DDLogVerbose(@"addRecordString: %@", addRecordQuery.query);
                        RXMLElement *postResults = [addRecordQuery execute:&addError];
                        
                        if (addError != nil) {
                            [QBinfo alertMe:addError];
                        }
                        else
                        {
                            DDLogVerbose(@"AddRecord Results: \n%@", addRecordQuery.resultsString);
                            @try {
                                
                                shot.record_id = [shot recordAsNSNumber:[postResults child:@"rid"].textAsInt];
                                shot.date_modified = [shot convertDateFromImport:[postResults child:@"update_id"].text];
                                shot.update_id = [postResults child:@"update_id"].text;
                                
                                [shot setParam:shot.record_id forKey:@"Record ID#"];
                                [shot setParam:shot.update_id forKey:@"Date Modified"];
                                
                                shot.online = YES;
                                
                                // Add new ProductData links, if necessary.
                                if (shot.productdatarecords.count > 0)
                                {
                                    QBTable *linkedShotsAndPDRsTable = [QBTable tableNamed:@"Linked Shots and Product Data" inMOC:privateMOC];
                                    QBField *relatedShot = [linkedShotsAndPDRsTable fieldForMastagTableNamed:@"Shots"];
                                    QBField *relatedPDR = [linkedShotsAndPDRsTable fieldForMastagTableNamed:@"Product Data"];
                                    
                                    NSSet *pdrs = [NSSet setWithSet:shot.productdatarecords];
                                    for (ProductDataRecord* pdr in pdrs) {
                                        
                                        QBQuery *addPDR = [QBQuery queryForAction:@"API_AddRecord"
                                                                          inTable:@"Linked Shots and Product Data"
                                                                            inMOC:privateMOC
                                                                       withParams:@{
                                                                                    [NSString stringWithFormat:@"_fid_%@", relatedShot.fid] : shot.record_id,
                                                                                    [NSString stringWithFormat:@"_fid_%@", relatedPDR.fid] : pdr.primaryKeyValue
                                                                                    }
                                                                    includeTicket:YES];
                                        NSError *uploadError = nil;
                                        RXMLElement *uploadResults = [addPDR execute:&uploadError];
                                        DDLogDebug(@"addPDR results: %@", addPDR.resultsString);
                                        
                                        if (uploadError != nil) {
                                            [QBinfo alertMe:uploadError];
                                        }
                                    }
                                    
                                }
                                
                                // Add new Linked Samples & Shots, if necessary
                                if (shot.samples.count > 0)
                                {
                                    QBTable *linkedSamplesAndShotsTable = [QBTable tableNamed:@"Linked Samples and Shots" inMOC:privateMOC];
                                    QBField *relatedShot = [linkedSamplesAndShotsTable fieldForMastagTableNamed:@"Shots"];
                                    QBField *relatedSample = [linkedSamplesAndShotsTable fieldForMastagTableNamed:@"Samples"];
                                    
                                    for (Sample *sample in shot.samples) {
                                        
                                        QBQuery *addSampleShot = [QBQuery queryForAction:@"API_AddRecord"
                                                                                 inTable:@"Linked Samples and Shots"
                                                                                   inMOC:privateMOC
                                                                              withParams:@{
                                                                                           [NSString stringWithFormat:@"_fid_%@", relatedShot.fid] : shot.record_id,
                                                                                           [NSString stringWithFormat:@"_fid_%@", relatedSample.fid] : sample.record_id
                                                                                           }
                                                                           includeTicket:YES];
                                        NSError *uploadError = nil;
                                        RXMLElement *uploadResults = [addSampleShot execute:&uploadError];
                                        
                                        if (uploadError != nil) {
                                            [QBinfo alertMe:uploadError];
                                        }
                                        else
                                        {
                                            // nothing needs to happen right?
                                        }
                                    }
                                }
                                
                                DDLogVerbose(@"ID: %@, modified: %@", shot.record_id, [shot prettyDate:shot.date_modified]);
                            }
                            @catch (NSException *exception) {
                                DDLogVerbose(@"Exception updating modification date & record id: %@", exception);
                            }
                        }
                    }
                    else if ([object isKindOfClass:[Station class]])
                    {
                        Station *station = (Station*) object;
                        
                        NSMutableDictionary *postParams = [NSMutableDictionary dictionaryWithDictionary:[station listPropertiesToPOST]];
                        
                        QBQuery *addRecordQuery = [QBQuery queryForAction:@"API_AddRecord" inTable:@"Stations" inMOC:privateMOC withParams:postParams includeTicket:YES];
                        
                        NSError *addError = nil;
                        
                        DDLogVerbose(@"addRecordString: %@", addRecordQuery.query);
                        RXMLElement *postResults = [addRecordQuery execute:&addError];
                        
                        if (addError != nil) {
                            [QBinfo alertMe:addError];
                        }
                        else
                        {
                            DDLogVerbose(@"AddRecord Results: \n%@", addRecordQuery.resultsString);
                            @try
                            {
                                station.record_id = [station recordAsNSNumber:[postResults child:@"rid"].textAsInt];
                                station.date_modified = [station convertDateFromImport:[postResults child:@"update_id"].text];
                                station.update_id = [postResults child:@"update_id"].text;
                                
                                [station setParam:station.record_id forKey:@"Record ID#"];
                                [station setParam:station.update_id forKey:@"Date Modified"];
                                
                                station.online = YES;
                                
                                DDLogVerbose(@"ID: %@, modified: %@", station.record_id, [station prettyDate:station.date_modified]);
                            }
                            @catch (NSException *exception) {
                                DDLogVerbose(@"Exception updating modification date & record id: %@", exception);
                            }
                        }

                    }
                    else
                    {
                        [QBinfo alertString:@"Unable to determine upload type"];
                    }
                }
                
                // Save privateMOC
                
                    __block NSError *error = nil;
                    [privateMOC save:&error];
                    
                    if (error != nil) {
                        [QBinfo alertMe:error];
                    }
                    else {
                        DDLogVerbose(@"Operation:Add: privateMOC Changes Saved.");
                        [mainMOC performBlock:^{
                            [mainMOC save:&error];
                        }];
                    }
//                } else {
//                    DDLogVerbose(@"****Operation:Add: privateMOC: No changes detected.");
//                }
            }];
            
            
        }]; // end of Block Operation
    
        operation.completionBlock = completionBlock;
        
        [operationManager addOperation:operation];
        
    
  
}

#pragma mark - Updating Objects
- (void) operationUpdateRecords:(NSArray <NSManagedObjectID *> *)objects onComplete:(completionBlock)completionBlock
{
    if (operationManager == nil) {
        operationManager = [[QBSyncManager sharedManager] uploadOperationQueue];
    }

    [self operationUpdateRecord:objects onComplete:completionBlock];
}

- (void) operationUpdateRecord:(id)obj onComplete:(completionBlock)completionBlock
{
    if (operationManager == nil) {
        operationManager = [[QBSyncManager sharedManager] uploadOperationQueue];
    }

    NSMutableArray *objIDs = [NSMutableArray array];
    
    if (![obj isKindOfClass:[NSArray class]] && ![obj isKindOfClass:[NSMutableArray class]])
    {
        [objIDs addObject:obj];
    }
    else
    {
        [objIDs addObjectsFromArray:obj];
    }
    NSError *saveError = nil;
    //if ([mainMOC hasChanges]) [mainMOC save:&saveError];
    
    for (NSManagedObjectID *objID in objIDs)
    {
        NSBlockOperation *operation = [NSBlockOperation  blockOperationWithBlock:^{
            NSManagedObjectContext *privateMOC = [self privateContext];
            [privateMOC performBlockAndWait: ^{
                
                NSError *fetchError = nil;
                
                id object = [privateMOC existingObjectWithID:objID error:&fetchError];
                
                if (fetchError != nil )
                {
                    [QBinfo alertMe:fetchError];
                }
                else if (object == nil)
                {
                    return;
                }
                else
                {
                    DDLogVerbose(@"Operation: UpdateRecord - Object: (%@)%@", [object class], [object name]);
                    NSArray *changedParams = [object updatedParams];
                    if (changedParams.count == 0) {
                        return;
                    }
                    else
                    {
                        NSError *updateError = nil;
                        // Sets
                        if ([object isKindOfClass:[Set class]])
                        {
                            Set *localSet = (Set*) object;
                            
                            NSMutableDictionary *postParams = [NSMutableDictionary dictionaryWithDictionary:[localSet listPropertiesToPOST]];
                            
                            QBQuery *updateQuery = [QBQuery queryForAction:@"API_EditRecord" inTable:@"Sets" inMOC:privateMOC withParams:postParams includeTicket:YES];
                            
                            RXMLElement *updateResults = [updateQuery execute:&updateError];
                            
                            if (updateError !=nil) {
                                [[QBDBInfo info] alertMe:updateError];
                            } else {
                                DDLogVerbose(@"Update Results: %@\n\n%@", localSet.name, updateQuery.resultsString);
                                @try {
                                    
                                    localSet.date_modified = [localSet convertDateFromImport:[updateResults child:@"update_id"].text];
                                    localSet.update_id = [updateResults child:@"update_id"].text;
                                    localSet.record_id = [localSet recordAsNSNumber:[updateResults child:@"rid"].textAsInt];
                                    
                                    //localSet.previousParams = localSet.params;
                                    DDLogVerbose(@"ID: %@, modified: %@", localSet.record_id, [localSet prettyDate:localSet.date_modified]);
                                }
                                @catch (NSException *exception) {
                                    DDLogVerbose(@"Exception updating %@ modification date & record id: %@", [object class], exception);
                                }
                                
                            }
                        }
                        // Set Lists
                        else if ([object isKindOfClass:[SetList class]])
                        {
                            DDLogVerbose(@"Updating SetList: %@", [object name]);
                            
                            SetList *localSetList = (SetList*)object;
                            
                            NSMutableDictionary *postParams = [NSMutableDictionary dictionaryWithDictionary:[localSetList listPropertiesToPOST]];
                            
                            [postParams setObject:@"1" forKey:@"msInUTC"];
                            
                            QBQuery *updateQuery = [QBQuery queryForAction:@"API_EditRecord" inTable:@"Set Lists" inMOC:privateMOC withParams:postParams includeTicket:YES];
                            
                            RXMLElement *updateResults = [updateQuery execute:&updateError];
                            
                            if (updateError !=nil) {
                                [[QBDBInfo info] alertMe:updateError];
                            } else {
                                DDLogVerbose(@"Update Results: %@\n\n%@", localSetList.name, updateQuery.resultsString);
                                @try {
                                    
                                    localSetList.date_modified = [localSetList convertDateFromImport:[updateResults child:@"update_id"].text];
                                    localSetList.update_id = [updateResults child:@"update_id"].text;
                                    localSetList.record_id = [localSetList recordAsNSNumber:[updateResults child:@"rid"].textAsInt];
                                    DDLogVerbose(@"ID: %@, modified: %@", localSetList.record_id, [localSetList prettyDate:localSetList.date_modified]);
                                }
                                @catch (NSException *exception) {
                                    DDLogVerbose(@"Exception updating %@ modification date & record id: %@", [object class], exception);
                                }
                                
                            }
                        }
                        // Shots
                        else if ([object isKindOfClass:[Shot class]])
                        {
                            __block Shot *localShot = [Shot existingWithObjectID:objID inContext:privateMOC];
                            
                            NSMutableDictionary *postParams = [NSMutableDictionary dictionaryWithDictionary:[localShot listPropertiesToPOST]];
                            
                            QBQuery *updateQuery = [QBQuery queryForAction:@"API_EditRecord" inTable:@"Shots" inMOC:privateMOC withParams:postParams includeTicket:YES];
                            
//                            RXMLElement *updateResults = [updateQuery execute:&updateError];
                            [updateQuery executeWithSuccessCallback:^(RXMLElement *updateResults) {
                                NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
                                [context setParentContext:[AppDelegate sharedInstance].mainMOC];
                                [context performBlock:^{
                                    Shot *localShot = [Shot existingWithRecordID:[NSNumber numberWithInt:[updateResults child:@"rid"].textAsInt] inContext:context];
                                    if (updateQuery.queryError !=nil)
                                    {
                                        [[QBDBInfo info] alertMe:updateQuery.queryError];
                                    }
                                    else
                                    {
                                        DDLogVerbose(@"Update Results: %@\n\n%@", localShot.name, updateQuery.resultsString);
                                        @try {
                                            
                                            [localShot setDate_modified:[localShot convertDateFromImport:[updateResults child:@"update_id"].text]];
                                            localShot.update_id = [updateResults child:@"update_id"].text;
                                            localShot.previousParams = localShot.params;
                                            
                                            DDLogVerbose(@"ID: %@, modified: %@", localShot.record_id, [localShot prettyDate:localShot.date_modified]);
                                        }
                                        @catch (NSException *exception) {
                                            DDLogVerbose(@"Exception updating %@ modification date & record id: %@", [object class], exception);
                                        }
                                        
                                    }
                                }];
                            }
                                                                                 failureCallback:^(NSError *error) {
                                                                                     [[QBDBInfo info] alertMe:error];
                                                                                 }];
   
                        }
                        else if ([object isKindOfClass:[Station class]])
                        {
                            Station *localStation = [Station existingWithObjectID:objID inContext:privateMOC];
                            
                            NSMutableDictionary *postParams = [NSMutableDictionary dictionaryWithDictionary:[localStation listPropertiesToPOST]];
                            
                            QBQuery *updateQuery = [QBQuery queryForAction:@"API_EditRecord" inTable:@"Stations" inMOC:privateMOC withParams:postParams includeTicket:YES];
                            
                            RXMLElement *updateResults = [updateQuery execute:&updateError];
                            
                            if (updateError !=nil) {
                                [[QBDBInfo info] alertMe:updateError];
                            }
                            else
                            {
                                DDLogVerbose(@"Update Results: %@\n\n%@", localStation.name, updateQuery.resultsString);
                                @try {
                                    
                                    localStation.date_modified = [localStation convertDateFromImport:[updateResults child:@"update_id"].text];
                                    localStation.update_id = [updateResults child:@"update_id"].text;
                                    localStation.previousParams = localStation.params;
                                }
                                @catch (NSException *exception) {
                                    DDLogVerbose(@"Exception updating %@ modification date & record id: %@", [object class], exception);
                                }
                            }
                        }
                        // TODO: add PDRs, Samples for Linking
                        else {
                            DDLogVerbose(@"Requesting to upload unknown object");
                        } // end of possible types of updatable objects
                    } // end of else
                } // end of Fetch Object block
                
                
                // Save the privateMOC
                
                __block NSError *error = nil;
                [privateMOC save:&error];
                
                if (error != nil) {
                    [QBinfo alertMe:error];
                }
                else {
                    DDLogVerbose(@"Operation:Update: privateMOC Changes Saved.");
                    [mainMOC performBlockAndWait:^{
                        [mainMOC save:&error];
                    }];
                }
                //                } else {
                //                    DDLogVerbose(@"****Operation:Update: privateMOC: No changes detected.");
                //                }
                
            }]; // end of privateMOC performBlock
            
            
        }]; // end of Block operation
        
        operation.completionBlock = completionBlock;
        
        [operationManager addOperation:operation];
    }
    
    
}

#pragma mark - Groupings





#pragma mark - Many to Many -
- (void) createLinkedShotID:(NSManagedObjectID*)shotID withSampleID:(NSManagedObjectID*)sampleID inMOC:(NSManagedObjectContext *)moc {
    
    NSManagedObjectContext *tempMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    tempMOC.parentContext = moc;
    
    [tempMOC performBlockAndWait:^{
        Shot *shot = [Shot existingWithObjectID:shotID inContext:tempMOC];
        Sample *sample = [Sample existingWithObjectID:sampleID inContext:tempMOC];
        
        
        QBTable *linkedSamplesShotsTable = [QBTable tableNamed:@"Linked Samples and Shots" inMOC:moc];
        QBField *relatedShotField = [linkedSamplesShotsTable fieldForMastagTableNamed:@"Shots"];
        QBField *relatedSampleField = [linkedSamplesShotsTable fieldForMastagTableNamed:@"Samples"];
        
        QBQuery *addSampleShot = [QBQuery queryForAction:@"API_AddRecord"
                                                 inTable:linkedSamplesShotsTable.name
                                                   inMOC:tempMOC
                                              withParams:@{
                                                           [NSString stringWithFormat:@"_fid_%@", relatedShotField.fid] : shot.record_id,
                                                           [NSString stringWithFormat:@"_fid_%@", relatedSampleField.fid] : sample.record_id
                                                           }
                                           includeTicket:YES];
        
        NSError *uploadError = nil;
        RXMLElement *uploadResults = [addSampleShot execute:&uploadError];
        
        if (uploadError != nil)
        {
            [QBinfo alertMe:uploadError];
        }
        else
        {
            DDLogVerbose(@"Link Shot (%@) and Sample (%@) uploadResults: %@", shot.description, sample.description, uploadResults.text); // nothing needs to happen right?
        }
    }];
}

- (void) createLinkedShotID:(NSManagedObjectID*)shotID withPDRID:(NSManagedObjectID*)pdrID inMOC:(NSManagedObjectContext *)moc {
    //[operationManager addOperationWithBlock:^{
        NSManagedObjectContext *tempMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        tempMOC.parentContext = moc;
        
        [tempMOC performBlockAndWait:^{
            Shot *shot = [Shot existingWithObjectID:shotID inContext:tempMOC];
            ProductDataRecord *pdr = [ProductDataRecord existingWithObjectID:pdrID inContext:tempMOC];
            
            QBTable *linkedShotsAndPDRsTable = [QBTable tableNamed:@"Linked Shots and Product Data" inMOC:tempMOC];
            
            QBField *relatedShotField = [linkedShotsAndPDRsTable fieldForMastagTableNamed:@"Shots"];
            QBField *relatedPDRField = [linkedShotsAndPDRsTable fieldForMastagTableNamed:@"Product Data"];
            
            QBQuery *addPDRShot = [QBQuery queryForAction:@"API_AddRecord"
                                                     inTable:linkedShotsAndPDRsTable.name
                                                    inMOC:tempMOC
                                                  withParams:@{
                                                               [NSString stringWithFormat:@"_fid_%@", relatedShotField.fid] : shot.record_id,
                                                               [NSString stringWithFormat:@"_fid_%@", relatedPDRField.fid] : pdr.primaryKeyValue
                                                               }
                                               includeTicket:YES];
            
            NSError *uploadError = nil;
            RXMLElement *uploadResults = [addPDRShot execute:&uploadError];
            
            if (uploadError != nil)
            {
                [QBinfo alertMe:uploadError];
            }
            else
            {
                DDLogVerbose(@"Link Shot (%@) and PDR (%@) uploadResults: %@", shot.description, pdr.description, uploadResults.text); // nothing needs to happen right?
            }
            [tempMOC save:nil];
        }];
    //}];
}

- (void) createLinkedGroupingRID:(NSNumber*)groupingRID withPDRIDs:(NSArray*)pdrIDs inMOC:(NSManagedObjectContext *)moc {
    //[operationManager addOperationWithBlock:^{
    NSManagedObjectContext *tempMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    tempMOC.parentContext = moc;
    
    [tempMOC performBlock:^{
        
        QBTable *linkedGroupingAndPDRsTable = [QBTable tableNamed:@"Linked Groupings and Product Data" inMOC:tempMOC];
        QBField *relatedGroupingField = [linkedGroupingAndPDRsTable fieldForMastagTableNamed:@"Groupings"];
        QBField *relatedPDRField = [linkedGroupingAndPDRsTable fieldForMastagTableNamed:@"Product Data"];
        
        for (NSManagedObjectID *pdrID in pdrIDs) {
            ProductDataRecord *pdr = [ProductDataRecord existingWithObjectID:pdrID inContext:tempMOC];
            
            NSString *queryString = [NSString stringWithFormat:@"{%@.TV.%@}AND{%@.TV.%@}", relatedGroupingField.fid, groupingRID, relatedPDRField.fid, pdr.primaryKeyValue];
            
            QBQuery *existingGroupingPDR = [QBQuery queryForAction:@"API_DoQuery"
                                                           inTable:linkedGroupingAndPDRsTable.name
                                                             inMOC:tempMOC
                                                        withParams:@{
                                                                     @"query" : queryString,
                                                                     @"fmt" : @"structured",
                                                                     }
                                                     includeTicket:YES];
            NSError *uploadError = nil;
            
            RXMLElement *existingQueryResults = [existingGroupingPDR execute:&uploadError];
            if (uploadError == nil)
            {
                NSArray *existingRecords = [[existingQueryResults child:@"table.records"] children:@"record"];
                if (existingRecords.count > 0)
                {
                    continue;
                }
            }
            else
            {
                DDLogError(@"Error checking for existing linked Grouping and PDR: %@", uploadError);
                uploadError = nil;
            }
            
            
            
            QBQuery *addGroupingPDR = [QBQuery queryForAction:@"API_AddRecord"
                                                      inTable:linkedGroupingAndPDRsTable.name
                                                        inMOC:tempMOC
                                                   withParams:@{
                                                                [NSString stringWithFormat:@"_fid_%@", relatedGroupingField.fid] : groupingRID,
                                                                [NSString stringWithFormat:@"_fid_%@", relatedPDRField.fid] : pdr.primaryKeyValue
                                                                }
                                                includeTicket:YES];
            
            RXMLElement *uploadResults = [addGroupingPDR execute:&uploadError];
            
            if (uploadError != nil)
            {
                //[QBinfo alertMe:uploadError];
                DDLogError(@"Error linking PDR: %@ to GroupingRID: %@\n\tDetails:\n\t%@", pdr.primaryKeyValue, groupingRID, uploadError);
            }
            else
            {
                DDLogVerbose(@"Link Grouping (%@) and PDR (%@) Successful. uploadResults: %@", groupingRID, pdr.primaryKeyValue, uploadResults.text); // nothing needs to happen right?
            }
        }
    }];
}



#pragma mark - Date Conversion
- (NSString*) convertDateForPOST:(NSDate *)date {
    
    //    DDLogVerbose(@"Date To Convert: %@", date);
    NSString *dateString = nil;
    long newInt = [date timeIntervalSince1970] * 1000;
    dateString = [NSString stringWithFormat:@"%ld", newInt];
    //    DDLogVerbose(@"Converted String: %@", dateString);
    
    return dateString;
}

#pragma mark - Context functions
- (void) setContext {
    NSManagedObjectContext *privateMOC = nil;
    privateMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [privateMOC setParentContext:mainMOC];
    
}



@end
