//
//  QBSyncManager.m
//
//  Created by Josh Booth on 7/15/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "QBSyncManager.h"

#import "QBDBInfo.h"
#import "QBDataImporter.h"
#import "QBDataExporter.h"

#import "QBQuery.h"

#import "Reachability.h"

#import "Shot.h"
#import "AppDelegate.h"
#import "C1ProController.h"
#import "ListViewController.h"


#import "SetsArrayController.h"
#import "SetListsArrayController.h"
#import "ShotsArrayController.h"

#import "SimplePingClient.h"



@interface QBSyncManager () {
    Reachability *internetReachable;
    NSBlockOperation *checkBeforeSyncing;
    SimplePingClient *pingClient;
    
}

@end


@implementation QBSyncManager {
    AppDelegate *app;
}

NSString *kBeganSyncNotification = @"QBSyncManagerStartedSyncing";
NSString *kFinishedSyncNotification = @"QBSyncManagerFinishedSyncing";
NSString *kErrorSyncingNotification = @"QBSyncManagerErrorSyncing";
NSString *kConnectionOfflineNotification = @"QBSyncManagerConnectionOffline";
NSString *kConnectionOnlineNotification = @"QBSyncManagerConnectionOnline";
NSString *kSyncingDisabledNotification = @"QBSyncManagerSyncingDisabled";
NSString *kSyncingEnabledNotification = @"QBSyncManagerSyncingEnabled";
NSString *kSelectiveImportCompleteNotification = @"QBSyncManagerSelectiveImportComplete";

@synthesize manager;
@synthesize timer;
@synthesize canSync;
@synthesize isSyncing;
@synthesize delegate;

@synthesize QBimporter;
@synthesize QBexporter;

@synthesize importDispatchQueue;
@synthesize exportDispatchQueue;
@synthesize uploadOperationQueue;
@synthesize importOperationQueue;

@synthesize beganSyncNotification;
@synthesize finishedSyncNotification;
@synthesize errorSyncNotification;
@synthesize connectionOfflineNotification;
@synthesize connectionOnlineNotification;
@synthesize isLoading;
@synthesize needsSignOut;

@synthesize lastImportDate;
@synthesize dateFormatter;

@synthesize uploadList;

#pragma mark - Initialization -
//  TODO: ADD QUEUE FOR UPDATES WHEN UNREACHABLE

+ (id) sharedManager {
    static QBSyncManager *sharedManager  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id) init {
    
    self = [super init];
    
    app = [AppDelegate sharedInstance];
    isLoading = YES;
    
    manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    QBimporter = [QBDataImporter sharedImporter];
    QBexporter = [QBDataExporter sharedExporter];
    
    importDispatchQueue = dispatch_queue_create("com.shotflowone.importQueue", DISPATCH_QUEUE_CONCURRENT);
    exportDispatchQueue = dispatch_queue_create("com.shotflowone.exportQueue", DISPATCH_QUEUE_CONCURRENT);
    
    
    
    importOperationQueue = [[NSOperationQueue alloc] init];
    importOperationQueue.name = @"importOperationQueue";
    importOperationQueue.underlyingQueue = importDispatchQueue;
    
    uploadOperationQueue = [[NSOperationQueue alloc] init];
    uploadOperationQueue.name = @"uploadOperationQueue";
    uploadOperationQueue.underlyingQueue = exportDispatchQueue;
    
    
    beganSyncNotification = [NSNotification notificationWithName:kBeganSyncNotification object:self];
    finishedSyncNotification = [NSNotification notificationWithName:kFinishedSyncNotification object:self];
    errorSyncNotification = [NSNotification notificationWithName:kErrorSyncingNotification object:self];
    
    connectionOfflineNotification = [NSNotification notificationWithName:kConnectionOfflineNotification object:self];
    connectionOnlineNotification = [NSNotification notificationWithName:kConnectionOnlineNotification object:self ];
    
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(syncingOffline:) name:kConnectionOfflineNotification object:self];
    [center addObserver:self selector:@selector(syncingOnline:) name:kConnectionOnlineNotification object:self];
    
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateStyle: NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle: NSDateFormatterShortStyle];
    [dateFormatter setCalendar:calendar];
    
    
    
    [self checkInternet];
    
//   [SimplePingClient pingHostname:@"8.8.8.8"
//                              andResultCallback:^(NSString *latency) {
//                                  NSLog(@"your latency is: %@", latency ? latency : @"unknown");
//                              }];
    
    return self;
}

-(NSString*) lastImportDateString {
    return [dateFormatter stringFromDate:lastImportDate];
}

- (void)loadDataForSet {
    
    Set* set = app.selectedSet;
    
    set.activeUser = [[QBDBInfo info] userID];
    [set setParam:[[QBDBInfo info] userID] forKey:@"Active User"];
    
    [self assignSet:set];
    
    DDLogVerbose(@"activeUser: %@", set.activeUser);
    DDLogVerbose(@"Load Data for Set: %@", app.selectedSet.name);
    
    
    
    [QBimporter setMainSetObjectID:[set objectID]];
    QBimporter.initialLoad = YES;
    [self startSyncing];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];

    dispatch_async(importDispatchQueue, ^{
        if (canSync)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [center postNotification:beganSyncNotification];
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [center postNotification:connectionOfflineNotification];
            });
        }
        
        
        [QBimporter loadSet:[set objectID]];
        lastImportDate = [NSDate date];
        
        
        DDLogInfo(@"************************************\t\t\tDone Loading Data.\n\n");
//        [QBimporter setInitialLoad:NO];
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            [center postNotification:finishedSyncNotification];
//            [QBimporter setInitialLoad:NO];
//        });
        
        
        if (canSync) {
            [C1ProController sharedInstance];
        }
        else {
            [C1ProController sharedInstance];
            dispatch_async(dispatch_get_main_queue(), ^{
                [center postNotification:connectionOfflineNotification];
            });
        }
    });
}

- (void) assignSet:(Set*)set {
    
    QBQuery *assignSetQuery = [QBQuery queryForAction:@"API_EditRecord" inTable:@"Sets" inMOC:app.mainMOC withParams:[set listPropertiesToPOST] includeTicket:YES];
    NSError *assignSetError = nil;
    [assignSetQuery execute:&assignSetError];
    if (assignSetError!= nil) {
        [[QBDBInfo info]alertMe:assignSetError];
    } else {
        DDLogInfo(@"AssignSet Results: %@", assignSetQuery.resultsString);
        needsSignOut = YES;
    }
    
}

- (void) unassignSet:(Set*)set {
    if (set == nil)
    {
        set = [AppDelegate sharedInstance].selectedSet;
    }
    
    set.activeUser = @"";
    [set setParam:@"" forKey:@"Active User"];
    
    QBQuery *assignSetQuery = [QBQuery queryForAction:@"API_EditRecord" inTable:@"Sets" inMOC:app.mainMOC withParams:[set listPropertiesToPOST] includeTicket:YES];
    NSError *assignSetError = nil;
    [assignSetQuery execute:&assignSetError];
    if (assignSetError!= nil)
    {
        [[QBDBInfo info]alertMe:assignSetError];
    }
    else
    {
        DDLogVerbose(@"SignOutSet Results: %@", assignSetQuery.resultsString);
        needsSignOut = NO;
    }
    
    [[AppDelegate sharedInstance].mainMOC save:nil];
}

#pragma mark - Syncing -

- (void) startSyncing {

    if (canSync) {
    timer = [NSTimer scheduledTimerWithTimeInterval:300.0 target:self selector:@selector(startImport) userInfo:nil repeats:YES];
        if (!QBimporter.initialLoad) [timer fire];
    }
}

- (void) manualSync {
//    if (timer !=nil) {
//        [timer fire];
//    } else {
//        [self startSyncing];
//    }
    if (!isSyncing) {
    [self startImport];
    }
}

- (void) stopSyncing {
    
    [timer invalidate];
    timer = nil;
}
#pragma mark - Importing -
- (void) startImport {
    
    if ([QBimporter initialLoad]) {
        return;
    }
    
    DDLogVerbose(@"SYNC: begin Import");
    
    if (canSync)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotification:beganSyncNotification];
        });
        
        [[C1ProController sharedInstance] changeToShot:app.selectedShot];
        
        dispatch_async(importDispatchQueue, ^{
            self.isSyncing = YES;
            [QBimporter importData];
//            self.isSyncing = NO;
            lastImportDate = [NSDate date];
        //dispatch_async(dispatch_get_main_queue(), ^{
//                QBimporter.initialLoad = NO;
//                [[NSNotificationCenter defaultCenter] postNotification:finishedSyncNotification];
//            });
        });
    }
    else {
        DDLogWarn(@"Unable to Sync. No Internet Connection");
    }
}


#pragma mark - Connectivity & Availability -
- (void) syncingOffline:(NSNotification *)note {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.isSyncing = NO;
        self.canSync = NO;
        [app.manualSyncMenu setEnabled:NO];
        [[QBSyncManager sharedManager] presentSyncError];
    });
}

-(void) syncingOnline:(NSNotification *) note {
   
    
    dispatch_async(dispatch_get_main_queue(), ^{
         self.canSync = YES;
        [app.manualSyncMenu setEnabled:YES];
    });
    
}


- (void) presentSyncError {
    
    if (isLoading) return; // don't present error on startup
    
    NSError *internetUnavailable = [[NSError alloc] initWithDomain:@"com.captureintegration.QBSyncManager"
                                                              code:-100
                                                          userInfo:@{
                                                                     NSLocalizedDescriptionKey : @"Cannot access the server. Internet connection may be down or the server may be unavailable.",
                                                                     NSLocalizedRecoveryOptionsErrorKey : @[@"Disable Syncing", @"Disable for 5 min"],
                                                                     NSLocalizedRecoverySuggestionErrorKey : @"If going offline, try disabling auto-syncing to the server. Initiating a manual sync will re-enable once your internet connection returns."
                                                                     }];
    float waitTime = 300.0; // 5 minutes
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    __block NSModalResponse response;
    
    if (![prefs boolForKey:@"SuppressConnectionErrorDialog"]) {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setShowsSuppressionButton:YES];
        [alert setAlertStyle:NSInformationalAlertStyle];
        [alert setMessageText:[internetUnavailable localizedDescription]];
        [alert setInformativeText:[internetUnavailable localizedRecoverySuggestion]];
        [alert addButtonWithTitle:[[internetUnavailable localizedRecoveryOptions] firstObject]];
        [alert addButtonWithTitle:[[internetUnavailable localizedRecoveryOptions] lastObject]];
        
        
        response = [alert runModal];
        
        if([[alert suppressionButton]state]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SuppressConnectionErrorDialog"];
            [[NSUserDefaults standardUserDefaults] setInteger:response forKey:@"ConnectionErrorResponse"];
            [prefs synchronize];
        }
        
        switch (response) {
            case NSAlertFirstButtonReturn:
                DDLogVerbose(@"Disable Auto-Sync");
                [[QBSyncManager sharedManager] stopSyncing];
                break;
                
            case NSAlertSecondButtonReturn:
                DDLogVerbose(@"Disable Syncing Temporarily");
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[QBSyncManager sharedManager] stopSyncing];
                    DDLogVerbose(@"Stopped Syncing at %@", [[QBDBInfo info] prettyDate:[NSDate date]]);
                    DDLogVerbose(@"Will Resume Syncing at %@", [[QBDBInfo info] prettyDate:[NSDate dateWithTimeInterval:waitTime sinceDate:[NSDate date]]]);
                    [NSThread sleepForTimeInterval:waitTime];
                    [[QBSyncManager sharedManager] startSyncing];
                    DDLogVerbose(@"Resumed Syncing at %@", [[QBDBInfo info] prettyDate:[NSDate date]]);
                });
                
                break;
                
            default:
                break;
        }
    });
    } else {
        response = [[NSUserDefaults standardUserDefaults] integerForKey:@"ConnectionErrorResponse"];
        
        switch (response) {
            case NSAlertFirstButtonReturn:
                DDLogVerbose(@"Disable Auto-Sync");
                [[QBSyncManager sharedManager] stopSyncing];
                break;
                
            case NSAlertSecondButtonReturn:
                DDLogVerbose(@"Disable Syncing Temporarily");
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[QBSyncManager sharedManager] stopSyncing];
                    DDLogVerbose(@"Stopped Syncing at %@", [[QBDBInfo info] prettyDate:[NSDate date]]);
                    DDLogVerbose(@"Will Resume Syncing at %@", [[QBDBInfo info] prettyDate:[NSDate dateWithTimeInterval:waitTime sinceDate:[NSDate date]]]);
                    [NSThread sleepForTimeInterval:waitTime];
                    [[QBSyncManager sharedManager] startSyncing];
                    DDLogVerbose(@"Resumed Syncing at %@", [[QBDBInfo info] prettyDate:[NSDate date]]);
                });
                
                break;
                
            default:
                break;
        }

        
    }
    
}

- (void) checkInternet {
    internetReachable = [Reachability reachabilityWithHostName:@"shotflow1.quickbase.com"];
    
    internetReachable.reachableBlock = ^(Reachability *reach) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DDLogVerbose(@"Internet REACHABLE!");
            [[NSNotificationCenter defaultCenter] postNotificationName:kConnectionOnlineNotification object:[QBSyncManager sharedManager]];
        });
    };
    
    
    
    internetReachable.unreachableBlock = ^(Reachability *reach) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DDLogVerbose(@"Internet UNREACHABLE!");
            [[NSNotificationCenter defaultCenter] postNotificationName:kConnectionOfflineNotification object:[QBSyncManager sharedManager]];
        });

    };
    
     [internetReachable startNotifier];
}



@end
