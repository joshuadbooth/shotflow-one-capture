//
//  QBSyncManager.h
//  QBApiTest2
//
//  Created by Josh Booth on 7/15/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "QBDBInfo.h"

#import "QBManagedObject.h"
#import "Set.h"
#import "SetList.h"
#import "Shot.h"
#import "ProductDataRecord.h"
#import "Station+CoreDataClass.h"
#import "Sample.h"

#import <AFNetworking/AFNetworking.h>

#import "QBDataImporter.h"
#import "QBDataExporter.h"

@protocol QBSyncManagerDelegate <NSObject>

-(void) syncStatusChanged: (BOOL)syncStatus;

@end

@class QBDataImporter, QBDataExporter, Set, SetList, Shot, Sample, ProductDataRecord, Station;

typedef void (^completionBlockWithObjects)(NSMutableArray* objectTree);

@interface QBSyncManager : NSObject
extern NSString *kBeganSyncNotification;
extern NSString *kFinishedSyncNotification;
extern NSString *kErrorSyncingNotification;
extern NSString *kConnectionOfflineNotification;
extern NSString *kConnectionOnlineNotification;
extern NSString *kSyncingDisabledNotification;
extern NSString *kSyncingEnabledNotification;

extern NSString *kSelectiveImportCompleteNotification;


@property (nonatomic, strong) AFURLSessionManager *manager;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) BOOL canSync;
@property (nonatomic) BOOL isSyncing;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) BOOL needsSignOut;
@property (nonatomic, retain) NSDate *lastImportDate;
@property (nonatomic, retain) NSDateFormatter *dateFormatter;

@property (nonatomic)  id <QBSyncManagerDelegate> delegate;
@property (nonatomic) dispatch_queue_t importDispatchQueue;
@property (nonatomic) dispatch_queue_t exportDispatchQueue;

@property (nonatomic, retain) QBDataImporter *QBimporter;
@property (nonatomic, retain) QBDataExporter *QBexporter;

@property (nonatomic, retain) NSOperationQueue *uploadOperationQueue;
@property (nonatomic, retain) NSOperationQueue *importOperationQueue;


@property NSNotification *QBSyncStatusNotification;
@property NSNotification *beganSyncNotification;
@property NSNotification *finishedSyncNotification;
@property NSNotification *errorSyncNotification;
@property NSNotification *connectionOfflineNotification;
@property NSNotification *connectionOnlineNotification;


@property (nonatomic, retain) NSMutableArray *uploadList;

+ (id) sharedManager;


- (void) startSyncing;
- (void) stopSyncing;
- (void) manualSync;

- (void) loadDataForSet;
- (void) assignSet:(Set*)set;
- (void) unassignSet:(Set*)set;


- (NSString*) lastImportDateString;

- (void) presentSyncError;

@end
