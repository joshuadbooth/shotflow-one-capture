//
//  QBTable+CoreDataProperties.h
//  CI Capture Manager
//
//  Created by Josh Booth on 3/17/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QBTable.h"

NS_ASSUME_NONNULL_BEGIN

@interface QBTable (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *clist_detailview;
@property (nullable, nonatomic, retain) NSString *clist_query;
@property (nullable, nonatomic, retain) NSString *clist_xmp;
@property (nullable, nonatomic, retain) NSDate *date_modified;
@property (nullable, nonatomic, retain) NSString *dbid;
@property (nullable, nonatomic, retain) id fields_detailView;
@property (nullable, nonatomic, retain) id fields_illegal;
@property (nullable, nonatomic, retain) id fields_xmp;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *alias;
@property (nullable, nonatomic, retain) NSString *key_fid;
@property (nullable, nonatomic, retain) NSSet<QBField *> *fields;
@property (nullable, nonatomic, retain) NSSet<QBField *> *mastagFields;
@property (nullable, nonatomic, retain) QBField *primaryKey;
@property (nullable, nonatomic, retain) QBField *refForField;

@end

@interface QBTable (CoreDataGeneratedAccessors)

- (void)addFieldsObject:(QBField *)value;
- (void)removeFieldsObject:(QBField *)value;
- (void)addFields:(NSSet<QBField *> *)values;
- (void)removeFields:(NSSet<QBField *> *)values;

- (void)addMastagFieldsObject:(QBField *)value;
- (void)removeMastagFieldsObject:(QBField *)value;
- (void)addMastagFields:(NSSet<QBField *> *)values;
- (void)removeMastagFields:(NSSet<QBField *> *)values;

@end

NS_ASSUME_NONNULL_END
