//
//  QBDBInfo.m
//  QBDBInfo
//
//  Created by Josh Booth on 7/8/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//



#define HOSTURL @"https://shotflow1.quickbase.com/db/"


#import "QBDBInfo.h"
@import Cocoa;

#import "QBSyncManager.h"
#import "RXMLElement.h"
#import "SF1XMLRecord.h"

#import "AppDelegate.h"
#import "LoginController.h"
#import "SetRegistrationController.h"

#import "QBQuery.h"
#import "QBTable.h"
#import "QBField.h"

@implementation QBDBInfo {
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    NSManagedObjectContext *bgMOC;
}

@synthesize baseURL;
@synthesize DBID;
@synthesize didAuthenticate;
@synthesize loggedIn;

// Set by USER
@synthesize userID;
@synthesize username;
@synthesize password;
@synthesize ticket;


@synthesize configurationData;
@synthesize tableIDs;
@synthesize detailView_clist;
@synthesize detailViewFieldIDsDict;

@synthesize xmp_clist;
@synthesize xmpFieldIDsDict;

@synthesize shots_xmp_clist;
@synthesize shots_xmpFieldIDsDict;

@synthesize samplesDetailView_clist;
@synthesize samplesDetailViewFieldIDsDict;
@synthesize table_clists;

@synthesize defaultSeparator;



#pragma mark - Init functions

+ (instancetype) info {
    static QBDBInfo *info = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        info = [[self alloc] init];
    });
    
    return info;
}

- (instancetype) init {
    self = [super init];
    
    return self;
}



- (id) initWithUsername:(NSString*)user AndPassword:(NSString*)pass {
        DDLogVerbose(@"QBDBInfo: initWithUsername: %@", user);
    self = [QBDBInfo info];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(connectionStatusChanged:) name: @"QBSyncManagerConnectionOffline" object:nil];
    [center addObserver:self selector:@selector(connectionStatusChanged:) name: @"QBSyncManagerConnectionOnline" object:nil];
    
    self.username = user;
    self.password = pass;
    
    self.baseURL = HOSTURL;
    self.didAuthenticate = NO;
    
    app = [AppDelegate sharedInstance];
    mainMOC = app.mainMOC;
    
    
    __block NSError *saveError;
    [mainMOC performBlockAndWait:^{
        if ([QBTable tableNamed:@"main" inMOC:mainMOC] == nil) {
            [QBTable insertNewObjectIntoContext:mainMOC withName:@"main" DBID:@"main"];
            [mainMOC save:&saveError];
        }
    
        if (saveError!=nil) {
            [self alertMe:saveError];
        }
        
    }];
    
    return self;
    
}

- (void) retrieveTableIDsAndInfo {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self downloadTablesAndFields];
    });
}

- (void) connectionStatusChanged:(NSNotification*) note {
    
    if ([note.name isEqualToString:@"QBSyncManagerConnectionOffline"])
    {
        self.loggedIn = NO;
    }
    else if ([note.name isEqualToString:@"QBSyncManagerConnectionOnline"])
    {
        NSError *loginError = nil;
        if ([self authenticate:&loginError])
        {
            DDLogVerbose(@"Reauthenticating...");
            if (loginError == nil)
            {
//                [self downloadTablesAndFields];
            }
            else
            {
                [[QBDBInfo info]alertMe:loginError];
            }
        }
        
        
    } else {
        // for further development options
    }
    
}

#pragma mark - Quickbase Log In
- (BOOL) authenticate:(NSError **)error {
   
    if ([[QBSyncManager sharedManager] canSync])
    {
        QBQuery *authQuery = [QBQuery authenticate];
        QBQuery *newAuthQuery = [QBQuery queryForAction:@"API_Authenticate"
                                                inTable:@"main"
                                                  inMOC:app.mainMOC
                                             withParams:@{@"username": self.username,
                                                          @"password" : self.password,
                                                          @"hours" : @"16",
                                                          }
                                          includeTicket:NO];
        
        NSError *authError = nil;
        RXMLElement *rootXML = [authQuery execute:&authError];
        
        if (authError != nil)
        {
            [self alertMe:authError];
            self.didAuthenticate = NO;
            *error = authError;
            return NO;
        }
        else
        {
            self.ticket = [rootXML child:@"ticket"].text;
            self.userID = [rootXML child:@"userid"].text;
            
            DDLogVerbose(@"Successfully logged in user: %@", self.username);
            DDLogVerbose(@"ticket: %@\n\n", ticket);
            
            self.didAuthenticate = YES;
            self.loggedIn = YES;
            return YES;
        }
    }
    else
    {
        return NO;
    }
}

#pragma mark - Get Granted DBs

- (NSArray*) grantedDBs {
    NSMutableArray *returnArray = [NSMutableArray array];
    
    QBQuery *grantedDBsQuery = [[QBQuery alloc] init];
    grantedDBsQuery.queryAction = @"API_GrantedDBs";
    grantedDBsQuery.query = [NSString stringWithFormat:@"main?a=%@&username=%@&password=%@&withembeddedtables=0", grantedDBsQuery.queryAction, username, password];
    
    NSError *queryError = nil;
    RXMLElement *resultsXML = [grantedDBsQuery execute:&queryError];
    if (queryError != nil)
    {
        [self alertMe:queryError];
        return nil;
    }
    else
    {
        NSArray *dbs = [[resultsXML child:@"databases"] children:@"dbinfo"];
        
        for (RXMLElement *dbinfo in dbs) {
            NSString *dbname = [dbinfo child:@"dbname"].text;
            NSString *dbid = [dbinfo child:@"dbid"].text;
            
            [returnArray addObject:@{@"dbname" : dbname,
                                     @"dbid" : dbid
                                     }];
        }
        
        [returnArray sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"dbname" ascending:YES]]];
    }
    
    return returnArray;
}

#pragma mark - App Configuration Data
- (void) downloadConfigurationData:(completionBlock)completionBlock {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if ([prefs objectForKey:@"appConfigurations"] && ![[QBSyncManager sharedManager] canSync])
    {
        configurationData = [NSMutableDictionary dictionaryWithDictionary:[prefs valueForKey:@"appConfigurations"]];
        //configurationData = [[prefs valueForKey:@"appConfigurations"] mutableCopy];
    }
    else
    {
        configurationData = [NSMutableDictionary dictionary];
        
        QBQuery *configurationsQuery = [QBQuery queryForAction:@"API_DoQuery" inTable:@"APP Configurations" inMOC:mainMOC withParams:nil includeTicket:YES];
        NSError *configQueryError = nil;
        
        RXMLElement *results = [configurationsQuery execute:&configQueryError];
        NSArray *resultsArray = [results children:@"record"];
        if (configQueryError == nil)
        {
            for (RXMLElement *xmlResult in resultsArray) {
                NSDictionary *resultDict = [xmlResult recordAsDictionary];
                [configurationData setValue:[resultDict valueForKey:@"data"] forKey:[[resultDict valueForKey:@"name"] uppercaseString]];
            }
            [prefs setObject:configurationData forKey:@"appConfigurations"];
            [prefs synchronize];
        }
        else
        {
            DDLogError(@"Error downloading Config Data: %@", configQueryError);
            [[QBDBInfo info] alertMe:configQueryError];
        }
    }
    
    if ([configurationData objectForKey:[@"defaultValueSeparationCharacter" uppercaseString]])
    {
        defaultSeparator = [configurationData valueForKey:[@"defaultValueSeparationCharacter" uppercaseString]];
    }
    else
    {
        defaultSeparator = @"_";
    }
    
    if ([configurationData objectForKey:@"SORTORDERSHOTS"])
    {
        NSString *sortKey = [configurationData valueForKey:@"SORTORDERSHOTS"];
        NSSortDescriptor *configSorting = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:YES comparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSString *objA;
            NSString *objB;
            
            objA = [obj1 isKindOfClass:[NSString class]]? obj1 : [obj1 stringValue];
            objB = [obj2 isKindOfClass:[NSString class]]? obj2 : [obj2 stringValue];
            
            return [objA compare:objB options:NSNumericSearch | NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch];
        }];
        
        app.shotSorting = configSorting;
    }
    
    if ([configurationData objectForKey:@"SORTORDERSUBSHOTS"]){
        NSString *sortKey = [configurationData valueForKey:@"SORTORDERSUBSHOTS"];
        NSSortDescriptor *configSorting = [NSSortDescriptor sortDescriptorWithKey:sortKey ascending:YES comparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSString *objA;
            NSString *objB;
            
            objA = [obj1 isKindOfClass:[NSString class]]? obj1 : [obj1 stringValue];
            objB = [obj2 isKindOfClass:[NSString class]]? obj2 : [obj2 stringValue];
            
            return [objA compare:objB options:NSNumericSearch | NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch];
        }];
        
        app.subShotSorting = configSorting;
    }
    
    if (completionBlock!=nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock();
        });
    }
}

#pragma mark - Table Management

- (void) createAppIDTable {
    
    [mainMOC performBlockAndWait:^{
        
        QBTable *appIDtable = [self getTableByName:@"App_ID" inMOC:mainMOC];
        if (appIDtable == nil)
        {
            [QBTable insertNewObjectIntoContext:mainMOC withName:@"App_ID" DBID:self.DBID];
        }
        else if ([appIDtable.dbid isNotEqualTo:self.DBID])
        {
            [app resetPersistentStore];
            [QBTable insertNewObjectIntoContext:mainMOC withName:@"App_ID" DBID:self.DBID];
            [QBTable insertNewObjectIntoContext:mainMOC withName:@"main" DBID:@"main"];
        }
        
        [mainMOC save:nil];
    }];
}

- (void) downloadTablesAndFields:(completionBlock)completionBlock {
    
    NSOperationQueue *importOperationQueue = [[QBSyncManager sharedManager] importOperationQueue];
    
    NSBlockOperation *tablesAndFieldsBlock = [NSBlockOperation blockOperationWithBlock: ^{
        if ([[QBSyncManager sharedManager] canSync]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[AppDelegate sharedInstance].setRegistrationView showWindow:nil];
                [[[AppDelegate sharedInstance].loginView window] close];
                [AppDelegate sharedInstance].setRegistrationView.statusLabel.stringValue = @"Downloading critical information...";
                [AppDelegate sharedInstance].setRegistrationView.setListMenu.enabled = NO;
            });
            
            bgMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [bgMOC setParentContext:mainMOC];
            
            [bgMOC performBlockAndWait:^{
                DDLogVerbose(@"Downloading table IDs");
                QBQuery *tableInfo = [QBQuery queryForAction:@"API_GetSchema"
                                                     inTable:@"App_ID"
                                                       inMOC:bgMOC
                                                  withParams:nil
                                               includeTicket:YES];
                
                
                NSError *tableIDerror = nil;
                
                RXMLElement *rootXML = [tableInfo execute:&tableIDerror];
                
                if (tableIDerror != nil) {
                    [self alertMe:tableIDerror];
                }
                else
                {
                    NSRegularExpression *regex = [NSRegularExpression
                                                  regularExpressionWithPattern:@"\\(UTC(.*?)\\)"
                                                  options:NSRegularExpressionSearch
                                                  error:nil];
                    
                    
                    NSString *dateFormat = [rootXML child:@"date_format"].text;
                    // *** parse into Objective-C formatter (DD ~> dd, YYYY ~> yyyy) *** //
                    dateFormat = [dateFormat stringByReplacingOccurrencesOfString:@"m" withString:@"M"];
                    dateFormat = [dateFormat stringByReplacingOccurrencesOfString:@"D" withString:@"d"];
                    dateFormat = [dateFormat stringByReplacingOccurrencesOfString:@"Y" withString:@"y"];
                    [[QBDBInfo info] setDateFormat:dateFormat];
                     
                     
                    [[QBDBInfo info] setTimeZone:[rootXML child:@"time_zone"].text];
                    
                    NSArray *matches = [regex matchesInString:self.timeZone options:0 range:NSMakeRange(0, [[self timeZone] length])];
                    
                    for (NSTextCheckingResult *match in matches) {
                        NSRange matchRange = [match rangeAtIndex:1];
                        NSString *matchString = [self.timeZone substringWithRange:matchRange];
                        self.timeZone = matchString;
                    }
                    NSArray *split = [self.timeZone componentsSeparatedByString:@":"];
                    NSInteger minsOffset = [[split objectAtIndex:1] integerValue];
                    NSInteger hoursOffset = [[split objectAtIndex:0] integerValue];
                   
                    self.timeZoneOffset = (hoursOffset * 60 * 60) + ((hoursOffset < 0)? (-1 * minsOffset * 60) : (minsOffset * 60));
                    
                    NSMutableSet *tables = [NSMutableSet set];
                    NSArray *chdbids = [[rootXML child:@"table.chdbids"] children:@"chdbid"];
                    
                    for (RXMLElement *db in chdbids)
                    {
                        NSString *alias = [[db attribute:@"name"] uppercaseString];
                        QBTable *qbTable = [self getTableByAlias:alias inMOC:bgMOC];
                        if (qbTable == nil) {
                            qbTable = [QBTable insertNewObjectIntoContext:bgMOC withName:alias DBID:db.text];
                            qbTable.alias = [alias uppercaseString];
                        }
                        [tables addObject:qbTable];
                    };
                    
                    
                    for (QBTable *qbTable in tables)
                    {
                        //DDLogVerbose(@"Downloading table info for dbid: %@", qbTable.dbid);
                        QBQuery *dbQuery = [[QBQuery alloc]init];
                        dbQuery.queryAction = @"API_GetSchema";
                        dbQuery.query = [NSString stringWithFormat:@"%@?a=%@&ticket=%@",  qbTable.dbid, dbQuery.queryAction, [[QBDBInfo info] ticket]];
                        NSString* safeQuery = dbQuery.safeQuery;
                        dbQuery.query = safeQuery;
                        
                        
                        NSError *dbError;
                        RXMLElement *dbResult = [dbQuery execute:&dbError];
                        if (dbError!= nil) {
                            DDLogVerbose(@"dbError: %@", dbError);
                        }
                        else
                        {
                            qbTable.name = [dbResult child:@"table.name"].text;
                            
                            qbTable.date_modified = [self convertDateFromImport:[dbResult child:@"table.original.mod_date"].text];
                            qbTable.key_fid = [dbResult child:@"table.original.key_fid"].text;
                            
                            for (RXMLElement *field in [[dbResult child:@"table.fields"] children:@"field"]) {
                                
                                QBField *qbf = [qbTable fieldNamed:[field child:@"label"].text];
                                if (qbf == nil) {
                                    qbf = [QBField insertNewObjectIntoContext:qbTable.managedObjectContext withName:[field child:@"label"].text FID:[field attribute:@"id"]];
                                }
                                
                                qbf.field_type = [field attribute:@"field_type"];
                                qbf.base_type = [field attribute:@"base_type"];
                                qbf.mode = [field attribute:@"mode"];
                                
                                qbf.formula = [field child:@"formula"].text;
                                qbf.mastag = [field child:@"mastag"].text;
                                qbf.table = qbTable;
                                
                                if (qbf.mastag.length > 0) {
                                    [qbTable addMastagFieldsObject:qbf];
                                }
                                else
                                {
                                    if ([qbTable.mastagFields containsObject:qbf]) [qbTable removeMastagFieldsObject:qbf];
                                }
                                
                                if ([qbf.fid isEqualToString:qbTable.key_fid]){
                                    qbf.isPrimary = [NSNumber numberWithBool:YES];
                                    qbTable.primaryKey = qbf;
                                }
                                
                            }
                            
                            if (qbTable.primaryKey == nil)
                            {
                                qbTable.primaryKey = [qbTable fieldNamed:@"Record ID#"];
                                [[qbTable fieldNamed:@"Record ID#"] setIsPrimary:[NSNumber numberWithBool:YES]];
                            }
                            
                            RXMLElement *apiQuery = [[[[dbResult child:@"table.queries"] children:@"query"] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(RXMLElement* evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                                if ([[evaluatedObject child:@"qyname"].text isEqualToString:@"API_Query"]) {
                                    return YES;
                                } else {
                                    return NO;
                                }
                            }]] firstObject];
                            
                            RXMLElement *apiDetailview = [[[[dbResult child:@"table.queries"] children:@"query"] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(RXMLElement* evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                                if ([[evaluatedObject child:@"qyname"].text isEqualToString:@"API_Detail_View"]) {
                                    return YES;
                                } else {
                                    return NO;
                                }
                            }]] firstObject];
                            
                            RXMLElement *apiXMPview = [[[[dbResult child:@"table.queries"] children:@"query"] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(RXMLElement* evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                                if ([[evaluatedObject child:@"qyname"].text isEqualToString:@"API_XMP_View"]) {
                                    return YES;
                                } else {
                                    return NO;
                                }
                            }]] firstObject];
                            
                            
                            qbTable.clist_query = [apiQuery child:@"qyclst"].text;
                            qbTable.clist_detailview = [apiDetailview child:@"qyclst"].text;
                            qbTable.clist_xmp = [apiXMPview child:@"qyclst"].text;
                            
                            // Alert & remove if API_Detail or API_XMP query clists contains fields missing from API_Query
                            
                            NSMutableSet *QuerySet = [NSMutableSet setWithArray:[qbTable.clist_query componentsSeparatedByString:@"."]];
                            NSMutableSet *DetailViewSet = [NSMutableSet setWithArray:[qbTable.clist_detailview componentsSeparatedByString:@"."]];
                            NSMutableSet *XMP_Set = [NSMutableSet setWithArray:[qbTable.clist_xmp componentsSeparatedByString:@"."]];
                            
                            
                            // API_Detail
                            
                            NSMutableSet *missingDetails = [NSMutableSet setWithSet:DetailViewSet];
                            [missingDetails minusSet:QuerySet];
                            
                            if (missingDetails.count > 0){
                                NSMutableString *missingDetailsString = [NSMutableString stringWithFormat:@"\n\nDetails View Fields missing from API_Query in Table: %@\n", qbTable.name];
                                for (NSString *missingFID in missingDetails) {
                                    QBField *missingField = [qbTable fieldWithID:missingFID];
                                    
                                    [missingDetailsString appendFormat:@"\t%@ - %@\n", missingFID, missingField.name];
                                }
                                [missingDetailsString appendString:@"\nThe missing fields have been removed as a precaution.\n\n"];
                                DDLogVerbose(@"%@", missingDetailsString);
                                [self alertString:missingDetailsString];
                                [DetailViewSet minusSet:missingDetails];
                                
                                DDLogVerbose(@"Old detailsString: %@", qbTable.clist_detailview);
                                DDLogVerbose(@"New detailsString: %@", [[DetailViewSet allObjects]componentsJoinedByString:@"."]);
                                DDLogVerbose(@"Missing Fields: %@", [[missingDetails allObjects]componentsJoinedByString:@"."]);
                                qbTable.clist_detailview = [[DetailViewSet allObjects]componentsJoinedByString:@"."];
                            }
                            
                            
                            // API_XMP
                            
                            NSMutableSet *missingXMPs = [NSMutableSet setWithSet:XMP_Set];
                            [missingXMPs minusSet:QuerySet];
                            
                            if (missingXMPs.count > 0){
                                NSMutableString *missingXMPsString = [NSMutableString stringWithFormat:@"\n\nXMP View Fields missing from API_Query in Table: %@\n", qbTable.name];
                                for (NSString *missingFID in missingXMPs) {
                                    QBField *missingField = [qbTable fieldWithID:missingFID];
                                    
                                    [missingXMPsString appendFormat:@"\t%@ - %@\n", missingFID, missingField.name];
                                }
                                [missingXMPsString appendString:@"\nThe missing fields have been removed as a precaution.\n\n"];
                                DDLogVerbose(@"%@", missingXMPsString);
                                [self alertString:missingXMPsString];
                                [XMP_Set minusSet:missingXMPs];
                                
                                DDLogVerbose(@"Old XMP Clist: %@", qbTable.clist_xmp);
                                DDLogVerbose(@"New XMP Clist: %@", [[XMP_Set allObjects]componentsJoinedByString:@"."]);
                                DDLogVerbose(@"Missing Fields: %@", [[missingXMPs allObjects]componentsJoinedByString:@"."]);
                                qbTable.clist_xmp = [[XMP_Set allObjects]componentsJoinedByString:@"."];
                            }
                        }
                        
                    }
                    
                    
                    for (QBTable *table in tables) {
                        @try {
                            for (QBField *field in table.mastagFields) {
                                NSString *tableAlias = field.mastag;
                                QBTable* refTable = [self getTableByAlias:tableAlias inMOC:bgMOC];
                                field.mastagTable = refTable;
                            }
                        } @catch (NSException *exception) {
                            DDLogVerbose(@"Exception linking mastag tables: %@", exception);
                        }
                        
                    }
                    [bgMOC save:nil];
                }
            }];
        }
        else
        {
            DDLogVerbose(@"No Internets!");
            // IF NO INTERNET CONNECTION
        }
    }]; // end of operation block
    
    
    tablesAndFieldsBlock.completionBlock = ^void() {
        DDLogVerbose(@"Tables & Fields Download Complete.");
        
        NSManagedObjectContext *bgMOC2 = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [bgMOC2 setParentContext:mainMOC];
        
        [bgMOC2 performBlockAndWait:^{
            
            NSMutableString *logString = [NSMutableString stringWithFormat:@"\nTables: "];
            
            for (QBTable *table in [[QBDBInfo info] QBTablesInMOC:bgMOC2]) {
                [logString appendFormat:@"\n\t%@ : %@", table.dbid, table.name];
            }
            [logString appendString:@"\n\n"];
            
            DDLogVerbose(@"%@", logString);
        }];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[QBDBInfo info] downloadConfigurationData:completionBlock];
        });
    };
    
    [importOperationQueue addOperation:tablesAndFieldsBlock];
}

- (void) deleteAllTables {
    
    NSArray *qbTables = [self QBTablesInMOC:mainMOC];
    for (QBTable *table in [NSArray arrayWithArray:qbTables]) {
        if ([table.name isEqualToString:@"main"] || [table.name isEqualToString:@"App_ID"]) continue;
        
        [mainMOC deleteObject:table];
    }
    
    [mainMOC save:nil];
    
}

#pragma mark - Table ID functions

- (NSString*) clist_QueryForTable:(NSString *)tableName  {
    return [self clist_QueryForTable:tableName inMOC:mainMOC];
}
- (NSString*) clist_QueryForTable:(NSString*)tableName inMOC:(NSManagedObjectContext*)MOC {
    return [[QBTable tableNamed:tableName inMOC:MOC] clist_query];
}

- (NSString*) getIDforTable:(NSString*)tableName inMOC:(NSManagedObjectContext*)MOC {
    QBTable *table = [QBTable tableNamed:tableName inMOC:MOC];
    
    return table.dbid;
}

- (NSString*) getFieldIDforFieldNamed:(NSString*)fieldname inTableNamed:(NSString*)tableName inMOC:(NSManagedObjectContext *)MOC{
    
    QBField *field = [QBField fieldNamed:fieldname inTableNamed:tableName inMOC:MOC];
    return field.fid;
}

- (NSString*) getFieldNameforFieldID:(NSString*)fieldID inTableNamed:(NSString*)tableName inMOC:(NSManagedObjectContext *)MOC{
    
    QBField *field = [QBField fieldWithID:fieldID inTableNamed:tableName inMOC:MOC];
    return field.name;
}

#pragma mark - Shared functions
- (id) getTableByID:(NSString*)dbid inMOC:(NSManagedObjectContext*)MOC{
    
    if (MOC == nil) MOC = mainMOC;
    NSError *fetchError;
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"QBTable" inManagedObjectContext:MOC]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dbid LIKE %@", dbid];
    fetch.predicate = predicate;
    
    NSArray *results = [MOC executeFetchRequest:fetch error:&fetchError];
    
    if (fetchError == nil) {
        if ([results count] == 0) return nil;
        
        id fetchedObject = [results firstObject];
        //        DDLogVerbose(@"foundObject: %@", fetchedObject);
        return fetchedObject;
        
    } else {
        return nil;
    }
}

- (id) getTableByName:(NSString*)tname inMOC:(NSManagedObjectContext*)MOC{
    //    DDLogVerbose(@"Getting related Entity");
    if (MOC == nil) MOC = mainMOC;
    NSError *fetchError;
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"QBTable" inManagedObjectContext:MOC]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name = %@", tname];
    fetch.predicate = predicate;
    
    NSArray *results = [MOC executeFetchRequest:fetch error:&fetchError];
    
    if (fetchError == nil) {
        if ([results count] == 0) return nil;
        
        id fetchedObject = [results firstObject];
        //        DDLogVerbose(@"foundObject: %@", fetchedObject);
        return fetchedObject;
        
    } else {
        return nil;
    }
}

- (id) getTableByAlias:(NSString*)alias inMOC:(NSManagedObjectContext*)MOC {
    //    DDLogVerbose(@"Getting related Entity");
    if (MOC == nil) MOC = mainMOC;
    NSError *fetchError;
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"QBTable" inManagedObjectContext:MOC]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"alias = %@", alias];
    fetch.predicate = predicate;
    
    NSArray *results = [MOC executeFetchRequest:fetch error:&fetchError];
    
    if (fetchError == nil) {
        if ([results count] == 0) return nil;
        
        id fetchedObject = [results firstObject];
        //        DDLogVerbose(@"foundObject: %@", fetchedObject);
        return fetchedObject;
        
    } else {
        return nil;
    }
}

- (NSArray*) QBTablesInMOC:(NSManagedObjectContext *)MOC  {
    if (MOC == nil) MOC = mainMOC;
    NSError *fetchError;
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:@"QBTable" inManagedObjectContext:MOC]];
    NSArray *results = [MOC executeFetchRequest:fetch error:&fetchError];
    
    if (fetchError == nil) {
        return results;
    }
    else {
        DDLogVerbose(@"Error fetching tables");
        return nil;
    }
}

- (NSArray*) entitiesWithObjectIDs:(NSArray*)idArray inMOC:(NSManagedObjectContext*)MOC {
    NSMutableArray *returnArray = [NSMutableArray array];
    
    for (NSManagedObjectID* objID in idArray) {
        NSError *findError = nil;
        id foundObj = [MOC existingObjectWithID:objID error:&findError];
        if (findError != nil) {
            DDLogVerbose(@"Error finding with ObjectID: %@ in MOC: %@", objID, MOC);
            DDLogVerbose(@"\tError Details: \n\t%@", findError);
            continue;
        }
        
        if (foundObj != nil) {
            [returnArray addObject:foundObj];
        }
        else
        {
            DDLogVerbose(@"Error finding with ObjectID: %@ in MOC: %@", objID, MOC);
            DDLogVerbose(@"\tNo object found with that ObjectID");
        }
    }
    
    return returnArray;
}

- (id) getRelatedEntity:(NSString*)entity withRecordID:(NSNumber *)recordID inManagedObjectContext:(NSManagedObjectContext*)moc error:(NSError**)error {
    //    DDLogVerbose(@"Getting related Entity");
    
    NSError *fetchError;
    
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"record_id = %@", recordID];
    fetch.predicate = predicate;
    
    NSArray *results = [moc executeFetchRequest:fetch error:&fetchError];
    
    if (fetchError == nil) {
        if ([results count] == 0) return nil;
        id fetchedObject = results.firstObject;
        return fetchedObject;
    }
    else
    {
        *error = fetchError;
        return nil;
    }
}

- (id) getRelatedEntity:(NSString*)entity withPrimaryKeyValue:(id)pkValue inManagedObjectContext:(NSManagedObjectContext*)moc error:(NSError**)error {
    NSError *fetchError;
//    NSLog(@"class of testingPKvalue: %@", [pkValue class]);
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:moc]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.primaryKeyValue = %@", pkValue];
    //fetch.predicate = predicate;
    
    NSArray *results = [[moc executeFetchRequest:fetch error:&fetchError] filteredArrayUsingPredicate:predicate];
    
    if (fetchError == nil) {
        if ([results count] == 0) return nil;
        NSError *tempError = nil;
        id fetchedObject = [moc existingObjectWithID:[results.firstObject objectID] error:&tempError];
        //        DDLogVerbose(@"foundObject: %@", fetchedObject);
        return fetchedObject;
        
    } else {
        *error = fetchError;
        return nil;
    }
}

- (NSNumber*) getLatestRecordIDforTable:(NSString*)tableName
                               orEntity:(id)object
                                  inMOC:(NSManagedObjectContext*)MOC
                            isTemporary:(BOOL)isTemporary {
    // gets last used record ID and ups by 99 for temp RID
    NSInteger lastRecordID;
    if (([[QBSyncManager sharedManager] canSync]) && (tableName != nil))
    {
        QBQuery* lastRIDQuery = [QBQuery queryForAction:@"API_GetNumRecords"
                                                inTable:tableName
                                                  inMOC:MOC
                                             withParams:nil
                                          includeTicket:YES];
        NSError *error = nil;
        
        RXMLElement *results = [lastRIDQuery execute:&error];
        if (error != nil) {
            [[QBDBInfo info]alertMe:error];
        } else {
            lastRecordID = [results child:@"num_records"].textAsInt;
        }
    }
    else
    {
        DDLogVerbose(@"entityName: %@", [object className]);
        
        NSFetchRequest *fetch = [[NSFetchRequest alloc] initWithEntityName:[object className]];
        NSManagedObjectContext *context = [object managedObjectContext];
        
        
        [fetch setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"record_id" ascending:YES]]];
        NSArray *results = [context executeFetchRequest:fetch error:nil];
        
        if (results.count > 0) {
            lastRecordID = [[results valueForKeyPath:@"@max.record_id"] intValue];
        } else {
            lastRecordID = 9999999;
        }
    }
    
    if (isTemporary) lastRecordID+=999;
    
    return [NSNumber numberWithInteger:lastRecordID];
    
}




- (BOOL) checkEntityForName: (NSString*) entity withRecordID: (NSNumber *) recordID inManagedObjectContext:(NSManagedObjectContext*) moc {
    NSError *error = nil;
    
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:moc]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"record_id == %@", recordID];
    fetch.predicate = predicate;
    NSUInteger count = [moc countForFetchRequest:fetch error:&error];
    
    //    id found = [self.privateManagedObjectContext executeFetchRequest:fetch error:&error].firstObject;
    
    if (error != nil) {
        [self alertMe:error];
    }
    
    if (count > 0) {
        //        DDLogVerbose(@"***\tFound Existing %@", [[found entity] name] );
        return true;
    } else {
        //        DDLogVerbose(@"***\tCreating New %@", entity);
        return false;
    }
    
}

- (NSNumber *) recordAsNSNumber: (NSInteger)num {
    return [NSNumber numberWithInteger:num];
}


- (NSDate*) convertDateFromImport:(NSString*)dateString {
    
    double seconds = dateString.doubleValue;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(seconds/1000)];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone systemTimeZone]];
    [format setDateStyle: NSDateFormatterMediumStyle];
    [format setTimeStyle: NSDateFormatterMediumStyle];
    [format setCalendar:calendar];
    
    //        DDLogVerbose(@"Converted Date: %@", [format stringFromDate:date]);
    return date;
}

- (NSString*) convertDateForPOST:(NSDate *)date {
    
    //    DDLogVerbose(@"Date To Convert: %@", date);
    NSString *dateString = nil;
    long newInt = [date timeIntervalSince1970] * 1000;
    dateString = [NSString stringWithFormat:@"%ld", newInt];
    //    DDLogVerbose(@"Converted String: %@", dateString);
    
    return dateString;
}

- (id) waybackDate:(NSDate*)date convertForPOST:(BOOL)shouldConvert {
    NSDate *now;
    if (date == nil) {
        now = [NSDate date];
    } else {
        now = date;
    }
    
    NSDate *sevenDaysAgo;
    NSString *APPCLIENT = [self.configurationData valueForKey:@"APPCLIENT"];
    if (([APPCLIENT isEqualToString:@"Hero Apparel"]) || ([APPCLIENT isEqualToString:@"Hero Gadgets"])){
        sevenDaysAgo = [now dateByAddingTimeInterval:-156*7*24*60*60]; // 3 years ago
    } else {
        sevenDaysAgo = [now dateByAddingTimeInterval:-7*24*60*60];
    }
    
//    DDLogDebug(@"WayBackDate: %@", [self prettyDate:sevenDaysAgo]);
    
    if (shouldConvert){
        NSString *aWeekAgoForQuery = [self prettyDateForPOST:sevenDaysAgo];
        return aWeekAgoForQuery;
    } else {
        return sevenDaysAgo;
    }
}

-(NSString*) prettyDate:(NSDate*) date{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone:[NSTimeZone systemTimeZone]];
    [format setDateStyle: NSDateFormatterMediumStyle];
    [format setTimeStyle: NSDateFormatterMediumStyle];
    [format setCalendar:calendar];
    
    return [format stringFromDate:date];
}

- (NSString*) prettyDateForPOST:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setTimeZone: [NSTimeZone systemTimeZone]];
    [format setDateStyle: NSDateFormatterShortStyle];
    [format setTimeStyle: NSDateFormatterNoStyle];
    [format setCalendar:calendar];
    
    return [[format stringFromDate:date] stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
}


#pragma mark - Alerts

- (void) alertMe:(NSError*) error {
    if ([[error userInfo] objectForKey:NSUnderlyingErrorKey])
    {
        NSError *underError = [[error userInfo] objectForKey:NSUnderlyingErrorKey];
        [self alertMe:underError andThen:nil];
    }
    else
    {
        [self alertMe:error andThen:nil];
    }
    
}

- (void) alertMe:(NSError *)error andThen:(completionBlock)completionblock
{
    dispatch_async(dispatch_get_main_queue(), ^{
        DDLogError(@"ERROR: %@", error);
        
        NSAlert *alert = [[NSAlert alloc] init];
        
        [alert setMessageText:[error localizedDescription]];
        
        NSMutableString *infoTest = [[NSMutableString alloc] init];
        //[infoTest appendFormat:@"%@\n\n", [error localizedDescription]];
        
        if ([error localizedFailureReason]) [infoTest appendFormat:@"%@\n\n", [error localizedFailureReason]];
        if ([error localizedRecoverySuggestion]) [infoTest appendFormat:@"%@\n\n", [error localizedRecoverySuggestion]];
        
        [alert setInformativeText:infoTest];
        [alert.window setTitle:@"ERROR"];
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert runModal];
        
        if (completionblock !=nil) {
            completionblock();
        }
    });
}

- (void) alertMe:(NSError*)error recoveryBlock1:(recoveryBlock __nonnull)recoveryBlock1 recoveryBlock2:(recoveryBlock __nullable)recoveryBlock2 recoveryBlock3:(recoveryBlock __nullable)recoveryBlock3
{    
    //dispatch_sync(dispatch_get_main_queue(), ^{
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"ERROR"];
        
        NSMutableString *infoTest = [[NSMutableString alloc] init];
        [infoTest appendFormat:@"%@\n\n", [error localizedDescription]];
        if ([error localizedFailureReason]) [infoTest appendFormat:@"%@\n\n", [error localizedFailureReason]];
        if ([error localizedRecoverySuggestion]) [infoTest appendFormat:@"%@\n\n", [error localizedRecoverySuggestion]];
        
        for (id recoveryOption in [error localizedRecoveryOptions]) {
            [alert addButtonWithTitle:recoveryOption];
        }
        
        [alert setInformativeText:infoTest];
        
        [alert setAlertStyle:NSWarningAlertStyle];
        
        DDLogError(@"ERROR: %@", error);
        NSModalResponse response = [alert runModal];
        
        if (response == NSAlertFirstButtonReturn) {
            dispatch_async(dispatch_get_main_queue(), ^{recoveryBlock1();});
            
        }
        else if (response == NSAlertSecondButtonReturn) {
            dispatch_async(dispatch_get_main_queue(), ^{recoveryBlock2();});
            
        }
        else if (response == NSAlertThirdButtonReturn) {
            dispatch_async(dispatch_get_main_queue(), ^{recoveryBlock3();});
        }
        else {
            DDLogError(@"ERROR: no recovery block for response");
        }

        
    //});
    
}

- (void) alertString: (NSString *)string {
    dispatch_async(dispatch_get_main_queue(), ^{
        DDLogWarn(@"NOTE: AlertString(): %@", string);
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"NOTE"];
        [alert setInformativeText:string];
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert runModal];
    });
}



@end
