//
//  QBDataImporter.m
//  QBApiTest
//
//  Created by Josh Booth on 7/2/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//


#import "QBDataImporter.h"

#import "AppDelegate.h"
#import "QBSyncManager.h"
#import "QBDataExporter.h"
#import "QBQuery.h"
#import "QBTable.h"
#import "QBField.h"

#import "RXMLElement.h"
#import "SF1XMLRecord.h"

#import "InfoViewController.h"
#import "Station+CoreDataClass.h"
#import "Set.h"
#import "SetList.h"
#import "Shot.h"
#import "ProductDataRecord.h"
#import "Sample.h"




@interface QBDataImporter () {
    AppDelegate *app;
    
    SetsArrayController *setsArrayController;
    SetListsArrayController *setListsArrayController;
    ShotsArrayController *shotsArrayController;
    
    NSManagedObjectContext *mainMOC;
    QBDBInfo *QBinfo;
    
    float maxQueries;
    
    NSOperationQueue *importOperationQueue;
    QBDataExporter *exporter;
    
}

@end


@implementation QBDataImporter


@synthesize lastCheck;
@synthesize mainSetObjectID;
@synthesize forceUpdate;
@synthesize initialLoad;


+ (id) sharedImporter {
    static QBDataImporter *sharedImporter  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedImporter = [[self alloc] init];
    });
    return sharedImporter;
}


- (instancetype) init {
    self = [super init];
    app = [AppDelegate sharedInstance];
    
    QBinfo = [QBDBInfo info];
    
    mainMOC = app.mainMOC;
    
    self.lastCheck = [NSDate dateWithNaturalLanguageString:@"January 1, 2015"];
    
    
    setsArrayController = [SetsArrayController sharedController];
    setListsArrayController = [SetListsArrayController sharedController];
    shotsArrayController = [ShotsArrayController sharedController];
    
    maxQueries = 85;
    exporter = [QBDataExporter sharedExporter];
    
    return self;
    
}
#pragma mark - Context Info
#pragma mark -

- (NSManagedObjectContext*) privateContext {
    NSManagedObjectContext *privateMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [privateMOC setParentContext:mainMOC];
    //DDLogVerbose(@"\n\n\tprivateMOC: %@  \n\tmainMOC: %@", privateMOC, mainMOC);
    return privateMOC;
}



- (void) initiateSave:(NSManagedObjectContext*)context {
    //DDLogVerbose(@"***Importer Save");
    //    if ([context hasChanges]){
    __block NSError *error = nil;
    [context save:&error];
    if (error != nil) {
        [QBinfo alertMe:error];
    }
    else {
        
        [mainMOC performBlock:^{
            [mainMOC save:&error];
        }];
        
        
    }
    //    } else {
    //        DDLogVerbose(@"No changes detected.");
    //    }
}

// TODO: TODO: If shot importing is in use, or contains different values, present a dialog for choosing
#pragma mark - Data Import -

- (void) loadSet:(NSManagedObjectID*) setID {
    self.initialLoad = YES;
    self.mainSetObjectID = setID;
    
    
    [self importData];
    
}

- (void) importData {
    importOperationQueue = [[QBSyncManager sharedManager] importOperationQueue];
    DDLogVerbose(@"ImportOperationQueue: %@", importOperationQueue);
    DDLogVerbose(@"Current Import Operations: %d", importOperationQueue.operationCount);
    
    
    if ([[QBSyncManager sharedManager] canSync])
    {
        NSBlockOperation *setListOperation = [NSBlockOperation blockOperationWithBlock:^{
            [self importSetLists];
        }];
        
        NSBlockOperation *shotsOperation = [NSBlockOperation blockOperationWithBlock:^{
            [self importShots];
        }];
        [shotsOperation addDependency:setListOperation];
        
        NSBlockOperation *pdrOperation = [NSBlockOperation blockOperationWithBlock:^{
            [self importProductData];
        }];
        [pdrOperation addDependency:shotsOperation];
        
        
        NSBlockOperation *samplesOperation = [NSBlockOperation blockOperationWithBlock:^{
            [self importSamples];
        }];
        [samplesOperation addDependency:pdrOperation];
        
        NSBlockOperation *wrapUpOperation = [NSBlockOperation blockOperationWithBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.initialLoad = NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:kFinishedSyncNotification
                                                                    object:[QBSyncManager sharedManager]];
                [[QBSyncManager sharedManager] setIsSyncing:NO];
                [[QBSyncManager sharedManager] setLastImportDate:[NSDate date]];
            });
        }];
        [wrapUpOperation addDependency:samplesOperation];
        
        //[importOperationQueue addOperation:wrapUpOperation];
        
        [importOperationQueue addOperations:@[setListOperation, shotsOperation, pdrOperation, samplesOperation, wrapUpOperation] waitUntilFinished:YES];
        
        self.initialLoad = NO;
        self.forceUpdate = NO;
    }
}

#pragma mark - Stations
- (NSArray *) importStations
{
    importOperationQueue = [[QBSyncManager sharedManager] importOperationQueue];
    __block NSMutableArray *returnArray = [NSMutableArray array];
    
    
    NSBlockOperation *stationOperation = [NSBlockOperation blockOperationWithBlock: ^{
        DDLogVerbose(@"\n\n");
        DDLogVerbose(@"Importing Stations");
        
        NSString *tablename = @"Stations";
        
        NSManagedObjectContext *privateMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        privateMOC.parentContext = mainMOC;
        
        DDLogDebug(@"ImportStations MOC: %@", privateMOC);
        [privateMOC performBlockAndWait:^{
            
            //NSMutableArray *onlineStations = [NSMutableArray array];
            
            // NSFetchRequest *fetchStationsRequest = [NSFetchRequest fetchRequestWithEntityName:@"Station"];
            
            //NSError *fetchStationsError = nil;
            //NSArray *fetchedStations = [privateMOC executeFetchRequest:fetchStationsRequest error:&fetchStationsError];

           
            
            
            //NSMutableSet *localStations = [NSMutableSet setWithArray:[fetchedStations valueForKeyPath:@"hardwareUUID"]];
            
            
            QBTable *stationsTable = [QBTable tableNamed:@"Stations" inMOC:privateMOC];
            
            QBQuery *importStationsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                           inTable:stationsTable.name
                                                             inMOC:privateMOC
                                                        withParams:@{
                                                                     @"includeRids" : @"1",
                                                                     @"clist": stationsTable.clist_query,
                                                                     @"slist" : @"1.3",
                                                                     @"fmt" : @"structured"
                                                                     }
                                                     includeTicket:YES];
            
            
            NSError *importStationsError = nil;
            
            RXMLElement *importStationsQueryResults = [importStationsQuery execute:&importStationsError];
            
            if (importStationsError == nil)
            {
                NSArray *stationsXMLRecords = [[importStationsQueryResults child:@"table.records"] children:@"record"];
                DDLogVerbose(@"# of Stations: %lu\n\n", (unsigned long)stationsXMLRecords.count);
                
                for (RXMLElement *stationRXML in stationsXMLRecords)
                {
                    SF1XMLRecord *stationXMLRecord = [SF1XMLRecord sf1RecordWithXMLRecord:stationRXML fromTable:stationsTable];
                    
                    [returnArray addObject:stationXMLRecord];
                }
            }
            else
            {
                [QBinfo alertMe:importStationsError];
            }
        }];
    }];
    
    [importOperationQueue addOperations:@[stationOperation] waitUntilFinished:YES];
    return returnArray;
}


#pragma mark - Sets
- (Set*) getMainSetInMOC:(NSManagedObjectContext*)moc
{
    return [Set existingWithObjectID:app.selectedSet.objectID inContext:moc];
}

- (void) importSets
{
    importOperationQueue = [[QBSyncManager sharedManager] importOperationQueue];
    [importOperationQueue addOperationWithBlock:^{
        DDLogVerbose(@"\n\n");
        DDLogVerbose(@"Importing Sets");
        
        NSString *tablename = @"Sets";
        
        NSManagedObjectContext *privateMOC = [self privateContext];
        DDLogDebug(@"ImportSets MOC: %@", privateMOC);
        [privateMOC performBlockAndWait:^
        {
            // *** Garbage Collection Setup *** //
            NSMutableArray *onlineSets = [NSMutableArray array];
            NSMutableSet *localSets = [NSMutableSet setWithArray:[[privateMOC executeFetchRequest:[NSFetchRequest fetchRequestWithEntityName:@"Set"] error:nil] valueForKeyPath:@"record_id"]];
            
            
            QBTable *setsTable = [QBTable tableNamed:@"Sets" inMOC:privateMOC];
            
            QBQuery *getSetsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                    inTable:@"Sets"
                                                      inMOC:privateMOC
                                                 withParams:@{@"qid" : @"1",
                                                              @"includeRids" : @"1",
                                                              @"clist": setsTable.clist_query,
                                                              @"slist" : @"1.3",
                                                              @"fmt" : @"structured"
                                                              }
                                              includeTicket:YES];
            
            
           
            [getSetsQuery executeWithSuccessCallback:^(RXMLElement *setsQueryResults) {
                [privateMOC performBlockAndWait:^{
                    NSArray *setsRecords = [[setsQueryResults child:@"table.records"] children:@"record"];
                    DDLogVerbose(@"# of Sets: %lu\n\n", (unsigned long)setsRecords.count);
                    
                    for (RXMLElement *setsRXML in setsRecords)
                    {
                        
                        SF1XMLRecord *setsRecordXML = [SF1XMLRecord sf1RecordWithXMLRecord:setsRXML fromTable:setsTable];
                        NSNumber *recordID = setsRecordXML.recordID;
                        
                        [onlineSets addObject:recordID];
                        
                        NSString *xmlRecordStatus = setsRecordXML.status;
                        // First check to see if already exists... if so, do not create a new entity
                        
                        Set *set = [Set existingWithRecordID:recordID inContext:privateMOC];
                        
                        if (set == nil) {
                            // does not exist, create new set
                            set = [Set insertWithXMLData:setsRecordXML intoContext:privateMOC];
                            [localSets addObject:set.record_id];
                        }
                        else // Set already exists locally
                        {
                            // Update set information always
                            [set populateParams:setsRecordXML];
                        }
                        
                    }
                    
                    // **** Gargabe Collection **** //
                    [localSets minusSet:[NSSet setWithArray:onlineSets]]; // removes all sets just queried from online
                    
                    for (NSNumber *rid in localSets) {
                        DDLogVerbose(@"local set missing from online: %@", [Set existingWithRecordID:rid inContext:privateMOC]);
                        Set *tbdlSet = [Set existingWithRecordID:rid inContext:privateMOC];
                        
                        NSMutableSet *objectsToDelete = [NSMutableSet setWithObject:tbdlSet];
                        [objectsToDelete unionSet:tbdlSet.setlists];
                        for (SetList *tbdlSetList in tbdlSet.setlists) {
                            [objectsToDelete unionSet:tbdlSetList.shots];
                        }
                        
                        DDLogVerbose(@"Garbage CleanUp: %@", [[objectsToDelete sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"className" ascending:YES]]] valueForKeyPath:@"description"]);
                        
                        for (id deletedObject in objectsToDelete) {
                            [privateMOC deleteObject:deletedObject];
                        }
                    }
                    
                    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Set"];
                    NSArray *sets = [privateMOC executeFetchRequest:fetch error:nil];
                    DDLogVerbose(@"Current Sets Info: %@", sets);
                    
                    
                    [[QBDataImporter sharedImporter] initiateSave:privateMOC];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        DDLogVerbose(@"Importing Sets Complete Success Block!");
                        [[NSNotificationCenter defaultCenter] postNotificationName:kFinishedSyncNotification object:[QBDataImporter sharedImporter]];
                    });
                }];
            }
                                     failureCallback:^(NSError *getSetsError) {
                                         [QBinfo alertMe:getSetsError];
                                     }
             ];
            
            DDLogVerbose(@"Importing Sets Complete!");
        }];
    }];
}

#pragma mark - Set Lists


- (void) importSetLists {
    importOperationQueue = [[QBSyncManager sharedManager] importOperationQueue];
    //DDLogVerbose(@"\n\n");
    DDLogVerbose(@"Importing Set Lists");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateStatusNotification
                                                        object:self
                                                      userInfo:@{@"statusText" : @"Downloading Set Lists"}];
    
    NSString *tablename = @"Set Lists";
    
    NSMutableArray *addedSetLists = [[NSMutableArray alloc] init];
    
    NSManagedObjectContext *privateMOC = [self privateContext];
    DDLogDebug(@"ImportSetLists MOC: %@", privateMOC);
    [privateMOC performBlockAndWait:^{
        // *** Local Set Lists modified later than server version  *** //
        NSMutableArray *objsToUpload = [NSMutableArray array];
        
        // *** Garbage Collection Setup *** //
        NSMutableArray *onlineSetLists = [NSMutableArray array];
        NSMutableSet *localSetLists = [NSMutableSet setWithArray:[[privateMOC executeFetchRequest:[NSFetchRequest fetchRequestWithEntityName:@"SetList"] error:nil] valueForKeyPath:@"record_id"]];
        
        
        Set* mainSet = [self getMainSetInMOC:privateMOC];
        QBTable *setListsTable = [QBTable tableNamed:@"Set Lists" inMOC:privateMOC];
        
        
        //     Only retrieves SetLists related to this set...
        
        QBField *relatedSetField = [setListsTable fieldForMastagTableNamed:@"Sets"];
  
        NSString *query = [NSString stringWithFormat:@"{%@.TV.'%@'}", relatedSetField.fid, mainSet.record_id];
        
        
        NSError *importSetListsError = nil;
        
        QBQuery *setListsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                 inTable:tablename
                                                   inMOC:privateMOC
                                              withParams:@{@"query" : query,
                                                           @"clist": setListsTable.clist_query,
                                                           @"fmt" : @"structured",
                                                           @"includeRids" : @"1"}
                                           includeTicket:YES];
        
        
        
        RXMLElement *setListQueryResults = [setListsQuery execute:&importSetListsError];
        
        if (importSetListsError == nil)
        {
            DDLogVerbose(@"# of Set Lists: %lu\n\n", (unsigned long)[[[setListQueryResults child:@"table.records"] children:@"record"] count]);
            
            for (RXMLElement *setListRXML in [[setListQueryResults child:@"table.records"] children:@"record"])
            {
                SF1XMLRecord *setListRecordXML = [SF1XMLRecord sf1RecordWithXMLRecord:setListRXML fromTable:setListsTable];
                
                NSNumber *recordID = setListRecordXML.recordID;
                NSString *xmlRecordStatus = setListRecordXML.status;
                
                [onlineSetLists addObject:recordID];
                
                // check if already exists....
                
                SetList *setlist = [SetList existingWithRecordID:recordID inContext:privateMOC];
                
                
                
                if (setlist == nil)
                {
                    // SetList does not already exist.  Create new setlist if necessary.
                    if ([app.hiddenStatuses containsObject:xmlRecordStatus])
                    {   // Skip creating the object if status is supposed to be hidden
                        continue;
                    }
                    
                    setlist = [SetList insertWithXMLData:setListRecordXML intoContext:privateMOC];
                    
                    setlist.set = mainSet;
                    [localSetLists addObject:setlist.record_id];
                    
                    [addedSetLists addObject:setlist];
                }
                else
                {
                    if ([app.hiddenStatuses containsObject:xmlRecordStatus])
                    {   // Delete the object if status is supposed to be hidden
                        [privateMOC deleteObject:setlist];
                        continue;
                    }
                    
                    
                    NSComparisonResult isModified = [setlist compareModDateToRecord:setListRecordXML];
                    if (forceUpdate) isModified = NSOrderedAscending;
                    
                    switch (isModified)
                    {
                        case NSOrderedAscending:
                            // stored entity is older than imported data
                            [setlist populateParams:setListRecordXML];
                            break;
                            
                        case NSOrderedDescending:
                            //DDLogVerbose(@"Update REMOTE Set List");
                            [objsToUpload addObject:setlist];
                            break;
                        
                        case NSOrderedSame:
                        default:
                            break;
                    }
                }
            };
        }
        else
        {
            DDLogError(@"ERROR Importing SetLists: %@", importSetListsError);
        }
        
        if ([objsToUpload count] > 0)
        {
            // *** Update local objects that are modified later than cloud *** //
            DDLogVerbose(@"The following Set Lists will need to update the Studio version: %@", [[objsToUpload valueForKeyPath:@"name"] componentsJoinedByString:@", "]);
            [[QBDataExporter sharedExporter] operationUpdateRecords:[objsToUpload valueForKeyPath:@"objectID"] onComplete:^{
                DLog(@"%lu Set Lists updated.", (unsigned long)[objsToUpload count]);
            }];
        }
        
        
        // *** Garbage Collection *** //
        [localSetLists minusSet:[NSSet setWithArray:onlineSetLists]];
        
        for (NSNumber *rid in localSetLists) {
            SetList *tbdlSetList = [SetList existingWithRecordID:rid inContext:privateMOC];
            
            NSMutableSet *objectsToDelete = [NSMutableSet setWithObject:tbdlSetList];
            [objectsToDelete unionSet:tbdlSetList.shots];
            
            DDLogVerbose(@"Garbage CleanUp: %@", [[objectsToDelete sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"className" ascending:YES]]] valueForKeyPath:@"description"]);
            
            for (id deletedObject in objectsToDelete) {
                [privateMOC deleteObject:deletedObject];
            }
        }
        
        [self initiateSave:privateMOC];
        
        
    }]; // end of performBlockAndWait
}

#pragma mark - Shots
- (void) importShots {
    /**********************************
     1.) Get list of Set List IDs
     2.) Query Shots table
     2.1) If Related Set list matches, import
     **********************************/
    
    
    DDLogVerbose(@"\n\n");
    DDLogVerbose(@"Importing Shots");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateStatusNotification
                                                        object:self
                                                      userInfo:@{@"statusText" : @"Downloading Shots"}];
    
    NSString *tablename = @"Shots";
    NSMutableArray *addedShots = [[NSMutableArray alloc] init];
    
    
    NSManagedObjectContext *privateMOC = [self privateContext];
    DDLogDebug(@"ImportShots MOC: %@", privateMOC);
    [privateMOC performBlockAndWait:^{
        
        // *** Local Shots modified later than server version  *** //
        NSMutableArray *objsToUpload = [NSMutableArray array];
        
        NSMutableArray *onlineShots = [NSMutableArray array];
         NSMutableSet *localShots = [NSMutableSet setWithArray:[[privateMOC executeFetchRequest:[NSFetchRequest fetchRequestWithEntityName:@"Shot"] error:nil] valueForKeyPath:@"record_id"]];
        
        QBTable *shotsTable = [QBTable tableNamed:@"Shots" inMOC:privateMOC];
        
        // only load shots for available Set Lists... so load an array of Set Lists to work through.
        Set *mainSet = [Set existingWithObjectID:app.selectedSet.objectID inContext:privateMOC];
        
        
        NSArray *loadedSetLists = [mainSet orderedSetLists];
        NSArray *setListIDs = [loadedSetLists valueForKeyPath:@"record_id"];
        
        if ([setListIDs count] > 0)
        {
            NSString *queryString;
            
            if (setListIDs.count <= 10) {
                queryString = [QBQuery buildQueryStringForTable:@"Shots"
                                                      fieldName:@"Related Set List"
                                             comparisonOperator:@"="
                                                   compareValue:setListIDs
                                                          inMOC:privateMOC];
            }
            
            else
            {
                queryString = [QBQuery defaultQueryString];
            }
            
            QBQuery *shotsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                  inTable:@"Shots"
                                                    inMOC:privateMOC
                                               withParams:@{@"query" : queryString,
                                                            @"clist":shotsTable.clist_query,
                                                            @"fmt" : @"structured",
                                                            @"slist" : @"3",
                                                            @"includeRids" : @"1"
                                                            }
                                            includeTicket:YES];
            
            
            
            NSError *shotsQueryError = nil;
            RXMLElement *shotsQueryResults = [shotsQuery execute:&shotsQueryError];
            
            if (shotsQueryError != nil) {
                [QBinfo alertMe:shotsQueryError];
            }
            else
            {
                NSArray *shotRecords = [[shotsQueryResults child:@"table.records"] children:@"record"];
                
                for (RXMLElement *shotRXML in shotRecords) {
                    
                    SF1XMLRecord *shotXMLRecord = [SF1XMLRecord sf1RecordWithXMLRecord:shotRXML fromTable:shotsTable];
                    NSNumber *recordID = shotXMLRecord.recordID;
                    NSString *xmlRecordStatus = shotXMLRecord.status;
                    NSNumber *xmlRelatedSetlist = [shotXMLRecord valueForMastagTable:@"Set Lists"];
                    
                    [onlineShots addObject:recordID];
                    
                    Shot *shot = [Shot existingWithRecordID:recordID inContext:privateMOC];
                    
                    // **** Verify XMLRecord's Set List is Local & Available **** //
                    if (![setListIDs containsObject:xmlRelatedSetlist])
                    {
                        shotXMLRecord = nil;
                        continue;
                    }
                    
                    // **** Check to see if Shot already exists locally **** //
                    if (shot == nil)
                    {
                        // if record has a hiddenStatus, do nothing;
                        if ([app.hiddenStatuses containsObject:xmlRecordStatus]) {
                            continue;
                        }
                        
                        // Shot doesn't exist, so create new Shot & link to Set List
                        shot = [Shot insertWithXMLData:shotXMLRecord intoContext:privateMOC];
                        
                        [addedShots addObject:shot];
                        
                    }
                    else
                    {
                        // Shot already exists
                        
                        // Shot exists - if status is hidden, delete the local object
                        if ([app.hiddenStatuses containsObject:xmlRecordStatus]) {
                            [privateMOC deleteObject:shot];
                            continue;
                        }
                        
                        
                        NSComparisonResult isModified = [shot compareModDateToRecord:shotXMLRecord];
                        if (forceUpdate) isModified = NSOrderedAscending;
                        
                        switch (isModified) {
                            case NSOrderedAscending: // existing shot is older than shotRecord
                                
                                DDLogVerbose(@"LOCAL Shot: %@ needs to be updated.", shot.description);
                                if ([xmlRelatedSetlist isNotEqualTo:shot.related_setlist])
                                {
                                    [shot.setlist removeShotsObject:shot];
                                    shot.parent = nil;
                                    
                                    DDLogVerbose(@"shot.parent: %@", [shot.parent description]);
                                }
                                
                                [shot populateParams:shotXMLRecord];
                                
                                if (([shot.status isEqualToString:@"Completed"]) && ([self weeksSinceDateModified:shot.dateCompleted] > 4))
                                {
                                    [privateMOC deleteObject:shot];
                                }
                                else
                                {
                                    [addedShots addObject:shot];
                                }
                                break;
                                
                            case NSOrderedDescending:
                                // Local Shot is newer than Cloud Shot
                                // stored entity is newer than imported data
                                [objsToUpload addObject:shot];
                                break;
                            case NSOrderedSame:
                            default:
                                break;
                        }
                        
                    }
                    
                    
                }
            }
            
            
            // *** Update local objects that are modified later than cloud *** //
            if ([objsToUpload count] > 0)
            {
                DDLogVerbose(@"The following Shots will need to update the Studio version: %@", [[objsToUpload valueForKeyPath:@"name"] componentsJoinedByString:@", "]);
                [[QBDataExporter sharedExporter] operationUpdateRecords:[objsToUpload valueForKeyPath:@"objectID"] onComplete:^{
                    DLog(@"%lu Shots updated.", (unsigned long)[objsToUpload count]);
                }];
            }
            
            
            
            // **** Shot Linkage **** //
            for (Shot* addedShot in addedShots.reverseObjectEnumerator.allObjects) {
                [addedShot linkAssociatedShots];
            }
        }
        
        // **** Garbage Collection *** //
        [localShots minusSet:[NSSet setWithArray:onlineShots]];
        NSMutableSet *objectsToDelete = [NSMutableSet set];
        
        for (NSNumber *rid in localShots) {
            Shot *tbdlShot = [Shot existingWithRecordID:rid inContext:privateMOC];
            
            [objectsToDelete addObject:tbdlShot];
        }
        if (objectsToDelete.count > 0)
        {
            DDLogVerbose(@"Garbage CleanUp: %@", [[objectsToDelete sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"className" ascending:YES]]] valueForKeyPath:@"description"]);
            
            for (id deletedObject in objectsToDelete) {
                [privateMOC deleteObject:deletedObject];
            }
        }
        
        [self initiateSave:privateMOC];
        
    }]; // end of performBlockAndWait
    
    
    
}
#pragma mark - Product Data

- (void) importProductData {
    DDLogVerbose(@"\n\n");
    DDLogVerbose(@"Getting Product Data");
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateStatusNotification
                                                        object:self
                                                      userInfo:@{@"statusText" : @"Downloading Product Data"}];
    
    // **********************************
    // 1.) Get List of loaded Set Lists
    // 1.1) Get list of ShotIDs
    // 2.) Query Linked Shots and Product Data
    // 2.1) If Related Shot is in Shot IDs
    // 2.1.1) Add RelatedPDR to pdrIDs
    // 2.1.2) Add linkedSPDRrecord to flaggedLinkedSPDRs
    // 3.) Query Product Data
    // 3.1) If RecordID is in pdrIDs
    // 3.1.1) Import PDR
    // 3.1.2) If PDRrid is in flaggedLinkedSPDRs relatedPDRs
    // 3.1.2.1) Get Shot where RID matches RelatedShot
    // 3.1.2.2) Add PDR to Shot
    // **********************************
    
    NSManagedObjectContext *privateMOC = [self privateContext];
    DDLogDebug(@"ImportPDRs MOC: %@", privateMOC);
    [privateMOC performBlockAndWait:^{
        
        NSMutableArray *onlinePDRs = [NSMutableArray array];
        NSMutableSet *localPDRs = [NSMutableSet setWithArray:[[privateMOC executeFetchRequest:[NSFetchRequest fetchRequestWithEntityName:@"ProductDataRecord"] error:nil] valueForKeyPath:@"primaryKeyValue"]];
        
        QBTable *productDataTable = [QBTable tableNamed:@"Product Data" inMOC:privateMOC];
        QBTable *linkedShotsAndProductDataTable = [QBTable tableNamed:@"Linked Shots and Product Data" inMOC:privateMOC];
        
        // **********************************
        // 1.) Get List of loaded Set Lists
        // **********************************
        id mainSet = [self getMainSetInMOC:privateMOC];
        
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"SetList" inManagedObjectContext:privateMOC];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        [request setSortDescriptors:@[app.sortByRecordID]];
        
        NSPredicate *relatedSetPredicate = [NSPredicate predicateWithFormat:@"(related_set == %@) AND NOT(status IN %@)", [mainSet record_id], app.hiddenStatusesForImport];
        
        NSError *fetchError = nil;
        NSArray *loadedSetLists = [[privateMOC executeFetchRequest:request error:&fetchError] filteredArrayUsingPredicate:relatedSetPredicate];
        
        BOOL skipImport = NO;
        if (fetchError != nil) {
            [QBinfo alertMe:fetchError];
            skipImport = YES;
        }
        
        // **********************************
        // 1.1) Get list of ShotIDs
        // **********************************
        
        NSMutableSet *shots = [NSMutableSet set];
        
        for (SetList* setlist in loadedSetLists) {
            [shots unionSet:setlist.shots];
        }
        
        NSArray *shotIDs = [shots valueForKeyPath:@"@distinctUnionOfObjects.primaryKeyValue"];
        
        if (shotIDs != nil && [shotIDs count] == 0)
        {
            skipImport = YES;
        }
        
        
        if (!skipImport)
        {
            // **********************************
            // 2.) Query Linked Shots and Product Data
            // 2.1) If Related Shot is in Shot IDs
            // 2.1.1) Add RelatedPDR to pdrIDs  -- need to determine what primary key is for particular entity
            // 2.1.2) Add linkedSPDRrecord to flaggedLinkedSPDRs
            // **********************************
            
            NSMutableSet *pdrIDs = [NSMutableSet set];
            NSMutableSet *flaggedLinkedSPDRs = [NSMutableSet set];
            
            QBQuery *linkedSPDRsQuery = [QBQuery defaultQueryInTable:@"Linked Shots and Product Data"
                                                               inMOC:privateMOC
                                                     withQueryString:@""];
            
            
            NSError *linkedSPDRsQueryError = nil;
            RXMLElement *linkedSPDRsQueryResults = [linkedSPDRsQuery execute:&linkedSPDRsQueryError];
            if (linkedSPDRsQueryError != nil) {
                [QBinfo alertMe:linkedSPDRsQueryError];
            }
            else
            {
                NSArray *linkedSPDRrecords = [[linkedSPDRsQueryResults child:@"table.records"] children:@"record"];
                
                for (RXMLElement *linkedSPDRrxml in linkedSPDRrecords) {
                    SF1XMLRecord *linkedSPDRrecordXML = [SF1XMLRecord sf1RecordWithXMLRecord:linkedSPDRrxml fromTable:linkedShotsAndProductDataTable];
                    
                    id relatedShotID = [linkedSPDRrecordXML valueForMastagTable:@"Shots"];
                    id relatedPDRID = [linkedSPDRrecordXML valueForMastagTable:@"Product Data"];
                    
                    if ([shotIDs containsObject:relatedShotID] && relatedPDRID != nil)
                    {
                        [pdrIDs addObject:relatedPDRID];
                        [flaggedLinkedSPDRs addObject:linkedSPDRrecordXML];
                    }
                }
                
            }
            
            
            // **********************************
            // 3.) Query Product Data
            // 3.1) If RecordID is in pdrIDs
            // 3.1.1) Import PDR
            // **********************************
            
            QBQuery *pdrQuery = [QBQuery defaultQueryInTable:@"Product Data" inMOC:privateMOC withQueryString:@""];
            
            NSError* pdrQueryError = nil;
            RXMLElement *pdrQueryResults = [pdrQuery execute:&pdrQueryError];
            
            if (pdrQueryError != nil) {
                [QBinfo alertMe:pdrQueryError];
            }
            else
            {
                NSArray *pdrRecords = [[pdrQueryResults child:@"table.records"] children:@"record"];
                
                for (RXMLElement *pdrRXML in pdrRecords) {
                    SF1XMLRecord *pdrXMLRecord = [SF1XMLRecord sf1RecordWithXMLRecord:pdrRXML fromTable:productDataTable];
                    
                    NSNumber *pdrRecordRID = pdrXMLRecord.recordID;
                    
                    id pdrRecordPKV = [pdrXMLRecord primaryKeyValue];
                    
                    //#ifdef DEBUG
                    //                DLog(@"checking PDR: %@", pdrRecordPKV);
                    //                DLog(@"Dictionary: %@", [pdrXMLRecord recordAsDictionary]);
                    //#endif
                    
                    [onlinePDRs addObject:pdrRecordPKV];
                    
                    
                    if ([pdrIDs containsObject:pdrRecordPKV])
                    {
                        ProductDataRecord *pdr = [ProductDataRecord existingWithPrimaryKeyValue:pdrRecordPKV inContext:privateMOC];
                        
                        if (pdr == nil)
                        {
                            pdr = [ProductDataRecord insertWithXMLData:pdrXMLRecord intoContext:privateMOC];
                        }
                        else //  pdr already exists locally
                        {
                            [pdr populateParams:pdrXMLRecord];
                        }
                        
                        // **********************************
                        //  3.1.2) If PDRrid is in flaggedLinkedSPDRs relatedPDRs
                        //  3.1.2.1) Get Shot where RID matches RelatedShot
                        //  3.1.2.2) Add PDR to Shot
                        // **********************************
                        
                        NSSet *filteredSet = [flaggedLinkedSPDRs filteredSetUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(SF1XMLRecord*  _Nonnull linkedSPDRsf1, NSDictionary<NSString *,id> * _Nullable bindings)
                                                                                            {
                                                                                                
                                                                                                BOOL returnValue = [pdr.primaryKeyValue isEqualTo: [linkedSPDRsf1 valueForMastagTable:@"Product Data"]];
                                                                                                return returnValue;
                                                                                            } ]];
                        
                        
                        for (SF1XMLRecord *flaggedLinkedSPDRxmlRecord in filteredSet)
                        {
                            
                            id related_pdr = [flaggedLinkedSPDRxmlRecord valueForMastagTable:@"Product Data"];
                            id related_shot = [flaggedLinkedSPDRxmlRecord valueForMastagTable:@"Shots"];
                            
                            
                            if ([related_pdr isEqualTo: pdr.primaryKeyValue]) {
                                //Shot *shot = [QBinfo getRelatedEntity:@"Shot" withPrimaryKeyValue:related_shot inManagedObjectContext:privateMOC error:nil];
                                Shot *shot = [Shot existingWithPrimaryKeyValue:related_shot inContext:privateMOC];
                                if (shot!=nil)
                                {
                                    [shot addProductdatarecordsObject:pdr];
                                    if (shot.orderedPDRString.length == 0)
                                    {
                                        shot.orderedPDRString = pdr.primaryKeyValue;
                                    }
                                    else if (![shot.orderedPDRString containsString:pdr.primaryKeyValue])
                                    {
                                        shot.orderedPDRString = [shot.orderedPDRString stringByAppendingFormat:@", %@", pdr.primaryKeyValue];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        // **** Garbage Collection *** //
        [localPDRs minusSet:[NSSet setWithArray:onlinePDRs]];
        NSMutableSet *objectsToDelete = [NSMutableSet set];
        
        for (id pkv in localPDRs) {
            ProductDataRecord *tbdlPDR = [ProductDataRecord existingWithPrimaryKeyValue:pkv inContext:privateMOC];
            [objectsToDelete addObject:tbdlPDR];
        }
        
        if (objectsToDelete.count > 0)
        {
            DDLogVerbose(@"Garbage CleanUp: %@", [[objectsToDelete sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"className" ascending:YES]]] valueForKeyPath:@"description"]);
            
            for (id deletedObject in objectsToDelete) {
                [privateMOC deleteObject:deletedObject];
            }
        }

        
        [self initiateSave:privateMOC];
    }]; // end of performBlockAndWait
    
}

- (void) importPDRforSampleID:(NSManagedObjectID *)sampleID usingContext:(NSManagedObjectContext*)targetMOC completionBlock:(completionBlockWithObjects)completionBlock {
    
    //NSManagedObjectContext *pdrImportMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    //pdrImportMOC.parentContext = targetMOC;
    
    NSManagedObjectContext *pdrImportMOC = targetMOC;
    
    __block NSMutableArray *addedPDRs = [NSMutableArray array];
    //[pdrImportMOC performBlockAndWait:^{
    
    Sample *sample = [Sample existingWithObjectID:sampleID inContext:pdrImportMOC];
    
    QBTable *pdrTable = [QBTable tableNamed:@"Product Data" inMOC:pdrImportMOC];
    QBField *pdrPrimaryField = pdrTable.primaryKey;
    
    
    NSString *query = [NSString stringWithFormat:@"{%@.TV.'%@'}", pdrPrimaryField.fid, sample.relatedPDR];
    
    
    // ***** Query Product Data Table ***** /
    
    QBQuery *pdrsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                         inTable:@"Product Data"
                                           inMOC:pdrImportMOC
                                      withParams:@{@"query" : query,
                                                   @"clist" : pdrTable.clist_query,
                                                   @"includeRids" : @"1",
                                                   @"fmt" : @"structured"
                                                   }
                                   includeTicket:YES];
    
    
    RXMLElement *pdrQueryResults = [pdrsQuery execute:nil];
    
    NSArray *pdrRecords = [[pdrQueryResults child:@"table.records"] children:@"record"];
    for (RXMLElement *pdrRXML in pdrRecords)
    {
        
        SF1XMLRecord *pdrRecordXML = [SF1XMLRecord sf1RecordWithXMLRecord:pdrRXML fromTable:pdrTable];
        
        NSNumber *pdrRID = pdrRecordXML.recordID;
        
        ProductDataRecord *pdr = [ProductDataRecord existingWithRecordID:pdrRID inContext:pdrImportMOC];
        
        if (pdr == nil)
        {
            pdr = [ProductDataRecord insertWithXMLData:pdrRecordXML intoContext:pdrImportMOC];
        }
        else
        {
            DDLogVerbose(@"PDR RID %@ already existed.", pdrRID);
        }
        
        [pdr addSamplesObject:sample];
        [addedPDRs addObject:pdr.objectID];
    }
    
    [pdrImportMOC save:nil];
    
    
    if (completionBlock) completionBlock(addedPDRs);
    pdrImportMOC = nil;
}

#pragma mark - Samples

- (void) importSamples {
    DDLogVerbose(@"\n\n");
    DDLogVerbose(@"Getting Samples");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUpdateStatusNotification
                                                        object:self
                                                      userInfo:@{@"statusText" : @"Downloading Samples"}];
    
    // **********************************************
    // 1. import Samples where relatedPDR matches available PDRs;
    // 2. Query linked Samples + Shots and for any available shotIDs;
    // 3. Get Sample IDs from those results
    // 4. Import those samples.
    // **********************************************
    
    
    NSManagedObjectContext *privateMOC = [self privateContext];
    DDLogDebug(@"ImportSamples MOC: %@", privateMOC);
    [privateMOC performBlockAndWait:^{
        
        // *** Local Samples modified later than server version  *** //
        NSMutableArray *objsToUpload = [NSMutableArray array];
        
        NSMutableArray *onlineSamples = [NSMutableArray array];
        NSMutableSet *localSamples = [NSMutableSet setWithArray:[[privateMOC executeFetchRequest:[NSFetchRequest fetchRequestWithEntityName:@"Sample"] error:nil] valueForKeyPath:@"record_id"]];
        
        
        
        QBTable *samplesTable = [QBTable tableNamed:@"Samples" inMOC:privateMOC];
        QBTable *linkedSamplesAndShotsTable = [QBTable tableNamed:@"Linked Samples and Shots" inMOC:privateMOC];
        
        
        Set *mainSet = [[QBDataImporter sharedImporter] getMainSetInMOC:privateMOC];
        
        NSArray *arrayOfSetLists = [mainSet.setlists sortedArrayUsingDescriptors:@[app.sortByPriority]];
        
        // **********************************************
        // Sets for IDs for PDRs, Shots, and Samples
        // **********************************************
        NSMutableSet *pdrIDs = [NSMutableSet set];
        NSMutableSet *shotIDs = [NSMutableSet set];
        NSMutableSet *sampleIDs = [NSMutableSet set];
        
        
        for (SetList *sl in arrayOfSetLists) {
            [shotIDs unionSet:[sl.shots valueForKeyPath:@"record_id"]];
            
            for (Shot *shot in sl.shots) {
                [pdrIDs unionSet:[shot.productdatarecords valueForKeyPath:@"primaryKeyValue"]];
            }
        }
        
        
        if ([arrayOfSetLists count] > 0 && ([shotIDs count] > 0 || [pdrIDs count] > 0))
        {
            
            // **********************************************
            // Query Sample Shots to get records
            // where Related Shot matches an available shotID
            // ***********************************************
            
            NSString *sampleShotsClist = linkedSamplesAndShotsTable.clist_query;
            
            NSString* query = [QBQuery defaultQueryString];
            
            QBQuery *sampleShotsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                        inTable:@"Linked Samples and Shots"
                                                          inMOC:privateMOC
                                                     withParams:@{@"query" : query,
                                                                  @"clist" : sampleShotsClist,
                                                                  @"includeRids" : @"1",
                                                                  @"fmt" : @"structured"
                                                                  }
                                                  includeTicket:YES];
            
            
            
            RXMLElement *sampleShotsQueryResults = [sampleShotsQuery execute:nil];
            
            NSArray *sampleShotRecords = [[sampleShotsQueryResults child:@"table.records"] children:@"record"];
            NSMutableArray *flaggedSampleShotRecords = [NSMutableArray array];
            
            NSString *relatedShotFID = [[linkedSamplesAndShotsTable fieldForMastagTableNamed:@"Shots"] fid];
            
            NSPredicate *samplesShotsPredicate = [NSPredicate predicateWithBlock:^BOOL(RXMLElement*  _Nullable record, NSDictionary<NSString *,id> * _Nullable bindings) {
                NSNumber *recordRelatedShot = [NSNumber numberWithInteger:[record childWithID: relatedShotFID].textAsInt];
                return [shotIDs containsObject:recordRelatedShot];
            }];
            
            NSArray *filteredSampleShotRecords = [sampleShotRecords filteredArrayUsingPredicate:samplesShotsPredicate];
            
            for (RXMLElement *ssRXML in filteredSampleShotRecords) {
                SF1XMLRecord *ssRecordXML = [SF1XMLRecord sf1RecordWithXMLRecord:ssRXML fromTable:linkedSamplesAndShotsTable];
                
                id related_sample = [ssRecordXML valueForMastagTable:@"Samples"];
                
                [sampleIDs addObject:related_sample];
                [flaggedSampleShotRecords addObject:ssRecordXML];
            }
            
            // **********************************************
            // 3.) Query Samples table
            // 3.1) If Related PDR is in list of PDRids, IMPORT
            // 3.2) If RecordID is in list of SampleIDs, IMPORT
            // **********************************************
            
            query = [QBQuery defaultQueryString];
            
            QBQuery *samplesQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                    inTable:samplesTable.name
                                                      inMOC:privateMOC
                                                 withParams:@{@"query" : query,
                                                              @"clist" : samplesTable.clist_query,
                                                              @"slist" : @"3",
                                                              @"includeRids" : @"1",
                                                              @"fmt" : @"structured"
                                                              }
                                              includeTicket:YES];
            NSError *samplesQueryError;
            RXMLElement *samplesQueryResults = [samplesQuery execute:&samplesQueryError];
            
            
            // **********************************************
            // 1. check each sample record to see if its recordID or relatedPDR match the appropriate list.
            // 1.1 if NO: ignore and continue.
            //    1.2 if YES: create sample object.
            // **********************************************
            
            
            NSArray *sampleRecords = [[samplesQueryResults child:@"table.records"] children:@"record"];
            
            NSPredicate *sampleRecordsPredicate = [NSPredicate predicateWithBlock:^BOOL(RXMLElement*  _Nullable record, NSDictionary<NSString *,id> * _Nullable bindings) {
                SF1XMLRecord *sampleRecordXML = [SF1XMLRecord sf1RecordWithXMLRecord:record fromTable:samplesTable];
                id relatedPDRvalue = [sampleRecordXML valueForMastagTable:@"Product Data"];
                NSNumber *sampleRID = sampleRecordXML.recordID;
                return ([pdrIDs containsObject:relatedPDRvalue]) || ([sampleIDs containsObject:sampleRID]);
            }];
            
            NSArray *filteredSampleRecords = [sampleRecords filteredArrayUsingPredicate:sampleRecordsPredicate];
            
            for (id sampleRXML in filteredSampleRecords) {
                
                SF1XMLRecord *sampleRecordXML = [sampleRXML isKindOfClass:[RXMLElement class]]? [SF1XMLRecord sf1RecordWithXMLRecord:sampleRXML fromTable:samplesTable] : sampleRXML;
                
                id relatedPDRvalue = [sampleRecordXML valueForMastagTable:@"Product Data"];
                
                NSNumber *sampleRID = sampleRecordXML.recordID;
                
                [onlineSamples addObject:sampleRID];
                
                Sample *sample = [Sample existingWithRecordID:sampleRID inContext:privateMOC];
                
                if (sample == nil)
                {
                    sample = [Sample insertWithXMLData:sampleRecordXML intoContext:privateMOC];
                }
                else
                {
                    // Shot already exists
                    NSComparisonResult isModified = [sample compareModDateToRecord:sampleRecordXML];
                    if (forceUpdate) isModified = NSOrderedAscending;
                    
                    switch (isModified) {
                        case NSOrderedAscending: // existing shot is older than shotRecord
                            [sample populateParams:sampleRecordXML];
                            break;
                            
                        case NSOrderedDescending:
                            // stored entity is newer than imported data
                            [objsToUpload addObject:sample];
                            
                            break;
                        case NSOrderedSame:
                        default:
                            break;
                    }
                }
                
                // *** Update local objects that are modified later than cloud *** //
                if ([objsToUpload count] > 0)
                {
                    DDLogVerbose(@"The following Samples will need to update the Studio version: %@", [[objsToUpload valueForKeyPath:@"primaryKeyValue"] componentsJoinedByString:@", "]);
                    [[QBDataExporter sharedExporter] operationUpdateRecords:[objsToUpload valueForKeyPath:@"objectID"] onComplete:^{
                        DLog(@"%lu Samples updated.", (unsigned long)[objsToUpload count]);
                    }];
                }
                
                // ***** LINK PDRs ***** //
                if ([sample.params objectForKey:[samplesTable fieldNameForMastagTableNamed:@"Product Data"]])
                {
                    sample.relatedPDR = [sample convertedValueForMastagTable:@"Product Data"];
                    
                    ProductDataRecord *relatedPDRrecord = [ProductDataRecord existingWithPrimaryKeyValue:sample.relatedPDR inContext:privateMOC];
                    
                    if (relatedPDRrecord !=nil)
                    {
                        sample.productdatarecord = relatedPDRrecord;
                    }
                    else
                    {
                        // (Happens Later) Import PDR if not already loaded & link to Sample & Shot
                    }
                    
                }
                
                // ***************
                //  Link to Shot
                // ***************
                for (SF1XMLRecord *flaggedSSrecord in flaggedSampleShotRecords)
                {
                    id related_sample = [flaggedSSrecord valueForMastagTable:@"Samples"];
                    id related_shot = [flaggedSSrecord valueForMastagTable:@"Shots"];
                    
                    if ([related_sample isEqualTo: sampleRID])
                    {
                        Shot *shot = [Shot existingWithRecordID:related_shot inContext:privateMOC];
                        
                        if (shot != nil)
                        {
                            @try {
                                [sample addShotsObject:shot];
                            }
                            @catch (NSException *exception) {
                                DDLogVerbose(@"Exception assigning Shot %@ to Sample %@: %@", shot.name, sample.name, exception);
                            }
                            
                        }
                        else {
                            //DDLogVerbose(@"No related shot for Sample");
                        }
                    }
                }
                
                if (sample.relatedPDR != nil)
                {
                    for (Shot *shot in sample.shots) {
                        ProductDataRecord *samplePDR = sample.productdatarecord;
                        if (samplePDR == nil)
                        {
                            [[QBDataImporter sharedImporter] importPDRforSampleID:sample.objectID usingContext:privateMOC completionBlock:nil];
                            samplePDR = sample.productdatarecord;
                        }
                        
                        [[QBDataImporter sharedImporter] linkZombiePDR:sample.productdatarecord toShot:shot];
                    }
                }
            }
        }
        
        
        // **** Garbage Collection *** //
        [localSamples minusSet:[NSSet setWithArray:onlineSamples]];
        NSMutableSet *objectsToDelete = [NSMutableSet set];
        
        for (NSNumber *rid in localSamples) {
            Sample *tbdlSample = [Sample existingWithRecordID:rid inContext:privateMOC];
            
            [objectsToDelete addObject:tbdlSample];
        }
        if (objectsToDelete.count > 0)
        {
            DDLogVerbose(@"Garbage CleanUp: %@", [[objectsToDelete sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"className" ascending:YES]]] valueForKeyPath:@"description"]);
            
            for (id deletedObject in objectsToDelete) {
                [privateMOC deleteObject:deletedObject];
            }
        }
        
        [self initiateSave:privateMOC];
        //        NSLog(@"");
    }]; // end of performBlockAndWait
    
}

- (void) importSampleRIDs:(NSArray *)samplesArray
                forShotID:(NSManagedObjectID *)shotID
                 usingMOC:(NSManagedObjectContext *) parentMOC
          completionBlock:(completionBlockWithObjects)completionBlock
{
    __block NSMutableArray *importedSamples = [NSMutableArray array];
    
    NSManagedObjectContext *samplesImportMOC = parentMOC;
    
    Shot *targetShot = [Shot existingWithObjectID:shotID inContext:samplesImportMOC];
    
    QBTable *samplesTable = [QBTable tableNamed:@"Samples" inMOC:samplesImportMOC];
    QBField *barCodeValueField = [samplesTable fieldNamed:@"API_BarCodeValue"];
    
    
    NSMutableArray *localSamples = [NSMutableArray array];
    NSMutableArray *missingBarCodeValues = [NSMutableArray array];
    
    // Check Samples array for already local Samples
    for (NSString *sampleBCV in samplesArray)
    {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Sample"];
        NSPredicate *barCodePredicate = [NSPredicate predicateWithFormat:@"barCodeValue == %@", sampleBCV];
        //[fetchRequest setPredicate:barCodePredicate];
        NSArray *existingSamples = [samplesImportMOC executeFetchRequest:fetchRequest error:nil];
        
        Sample *sample = [[existingSamples filteredArrayUsingPredicate:barCodePredicate] firstObject];
        if (sample != nil) {
            [localSamples addObject:sample];
        }
        else {
            [missingBarCodeValues addObject:sampleBCV];
        }
    }
    
    // If there are any Samples that aren't Local, import
    if (missingBarCodeValues.count > 0)
    {
        NSMutableString *fullQuery = [NSMutableString string];
        
        for (int i=0; i<missingBarCodeValues.count; i++) {
            id missingSampleRID = [missingBarCodeValues objectAtIndex:i];
            NSString *query = [NSString stringWithFormat:@"{%@.EX.'%@'}", barCodeValueField.fid, missingSampleRID];
            [fullQuery appendFormat:@"%@", query];
            if (i+1 != missingBarCodeValues.count) [fullQuery appendFormat:@"OR"];
        }
        
        // ***** Query Samples Table ***** //
        NSString *samplesClist = samplesTable.clist_query;
        QBQuery *samplesQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                inTable:@"Samples"
                                                  inMOC:samplesImportMOC
                                             withParams:@{@"query" : fullQuery,
                                                          @"clist" : samplesClist,
                                                          @"includeRids" : @"1",
                                                          @"fmt" : @"structured"
                                                          }
                                          includeTicket:YES];
        
        
        RXMLElement *samplesQueryResults = [samplesQuery execute:nil];
        
        NSArray *samplesRecords = [[samplesQueryResults child:@"table.records"] children:@"record"];
        
        for (RXMLElement *sampleRecordXML in samplesRecords)
        {
            Sample *sample = [Sample insertWithXMLData:sampleRecordXML intoContext:samplesImportMOC];
            [localSamples addObject:sample];
        }
        
        
        [samplesImportMOC save:nil];
        //                [mainMOC performBlock:^{
        //                    [mainMOC save:nil];
        //                }];
    }
    
    [targetShot addSamples:[NSSet setWithArray:localSamples]];
    
    for (Sample *sample in localSamples)
    {
        // **** LINK to Shot **** //
        //Query Linked Samples and Shots table to verify record doesn't already exist.
        
        QBTable *linkedSamplesAndShotsTable = [QBTable tableNamed:@"Linked Samples and Shots" inMOC:samplesImportMOC];
        QBField *relatedShotField = [linkedSamplesAndShotsTable fieldForMastagTableNamed:@"Shots"];
        QBField *relatedSampleField = [linkedSamplesAndShotsTable fieldForMastagTableNamed:@"Samples"];
        
        NSString *queryString = [NSString stringWithFormat:@"{%@.TV.'%@'}AND{%@.TV.'%@'}", relatedShotField.fid, targetShot.record_id, relatedSampleField.fid, sample.primaryKeyValue];
        
        QBQuery *onlineCheckQuery = [QBQuery defaultQueryInTable:@"Linked Samples and Shots"
                                                           inMOC:samplesImportMOC
                                                 withQueryString:queryString];
        
        NSError *checkError = nil;
        RXMLElement *results = [onlineCheckQuery execute:&checkError];
        if (checkError != nil) {
            [[QBDBInfo info] alertMe:checkError];
        }
        else
        {
            NSArray *lssRecords = [[results child:@"table.records"] children:@"record"];
            if (lssRecords.count == 0){
                [exporter createLinkedShotID:targetShot.objectID withSampleID:sample.objectID inMOC:samplesImportMOC];
            }
        }
        
        // **** Import PDRS and link to Shot **** //
        
        [self importPDRforSampleID:sample.objectID
                      usingContext:samplesImportMOC
                   completionBlock:^(NSMutableArray *pdrArray) {
                       
                       if (sample.productdatarecord != nil)
                       {
                           for (Shot *shot in sample.shots) {
                               [self linkZombiePDR:sample.productdatarecord toShot:shot];
                           }
                       }
                       DDLogVerbose(@"done linking zombies for %@", sample);
                   }];
    }
    
    
    
    [importedSamples addObjectsFromArray:localSamples];
    
    [samplesImportMOC save:nil];
    
    if (completionBlock != nil) completionBlock(importedSamples);
}

#pragma mark - Groupings

- (void) importGroupingForPDRstring:(NSString*)pdrString onComplete:(void (^_Nullable)(NSNumber *groupingRID))onComplete {
    
    DDLogVerbose(@"\n\n");
    DDLogVerbose(@"Importing Groupings for PDR string: %@", pdrString);
    
    
    importOperationQueue = [[QBSyncManager sharedManager] importOperationQueue];
    NSBlockOperation *groupingBlock = [NSBlockOperation blockOperationWithBlock:^{
        NSManagedObjectContext *privateMOC = [self privateContext];
        DDLogDebug(@"ImportGroupings MOC: %@", privateMOC);
        [privateMOC performBlock:^
         {
             SF1XMLRecord *targetGrouping = nil;
             
             QBTable *groupingsTable = [QBTable tableNamed:@"Groupings" inMOC:privateMOC];
             NSString *groupingsQueryString = [QBQuery buildQueryStringForTable:@"Groupings"
                                                                      fieldName:@"API_ProductsInGrouping"
                                                             comparisonOperator:@"="
                                                                   compareValue:pdrString
                                                                          inMOC:privateMOC];
             
             QBQuery *groupingsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                       inTable:@"Groupings"
                                                         inMOC:privateMOC
                                                    withParams:@{@"query" : groupingsQueryString,
                                                                 @"includeRids" : @"1",
                                                                 @"clist": groupingsTable.clist_query? groupingsTable.clist_query : @"a",
                                                                 @"fmt" : @"structured"
                                                                 }
                                                 includeTicket:YES];
             
             
             NSError *getGroupingsError = nil;
             RXMLElement *groupingsQueryResults = [groupingsQuery execute:&getGroupingsError];
             if (getGroupingsError == nil)
             {
                 NSArray *groupingXMLrecords = [[groupingsQueryResults child:@"table.records"] children:@"record"];
                 for (RXMLElement *groupingRXML in groupingXMLrecords) {
                     SF1XMLRecord *groupingRecordXML = [SF1XMLRecord sf1RecordWithXMLRecord:groupingRXML fromTable:groupingsTable];
                     if ([[groupingRecordXML valueForFieldNamed:@"API_ProductsInGrouping"] isEqualToString:pdrString])
                     {
                         targetGrouping = groupingRecordXML;
                         break;
                     }
                 }
                 
                 if (targetGrouping != nil) // If exsiting grouping was found
                 {
                     onComplete(targetGrouping.recordID);
                 }
                 else
                 {
                     QBQuery *postGroupingQuery = [QBQuery queryForAction:@"API_AddRecord"
                                                                  inTable:@"Groupings"
                                                                    inMOC:privateMOC
                                                               withParams:@{
                                                                            [NSString stringWithFormat:@"_fid_%@", [groupingsTable fieldNamed:@"Name"].fid]                   : pdrString,
                                                                            [NSString stringWithFormat:@"_fid_%@", [groupingsTable fieldNamed:@"API_ProductsInGrouping"].fid] : pdrString,
                                                                            }
                                                            includeTicket:YES];
                     NSError *postError = nil;
                     RXMLElement *postResults = [postGroupingQuery execute:&postError];
                     
                     if (postError == nil)
                     {
                         NSNumber *groupingRID = [NSNumber numberWithInteger:[postResults child:@"rid"].textAsInt];
                         onComplete(groupingRID);
                     }
                     else
                     {
                         [QBinfo alertMe:postError];
                     }
                     
                 }
             }
             else
             {
                 [QBinfo alertMe:getGroupingsError];
             }
         }];
    }];
    [importOperationQueue addOperation:groupingBlock];
    
}
#pragma mark - Miscellaneous

- (NSArray*) subdivideArrayByMaxQueryLength:(NSArray*) sourceArray {
    
    float queryWaves = ceil((sourceArray.count / maxQueries));
    
    //DDLogVerbose(@"# of objects for Query: %lu, MaxQueries: %f, QueryWaves: %f", (unsigned long)sourceArray.count, maxQueries, queryWaves);
    NSMutableArray *testWaves = [NSMutableArray array];
    
    for (float wave = 1; wave <= queryWaves; wave++) {
        int rangeLength;
        if (wave != queryWaves){
            rangeLength = maxQueries;
        } else {
            rangeLength = sourceArray.count - ((queryWaves - 1) * maxQueries);
        }
        
        [testWaves addObject:[sourceArray subarrayWithRange:NSMakeRange((wave-1) * maxQueries, rangeLength)]];
        //DDLogVerbose(@"Wave: %f, %@ - %@, total: %lu", wave, [[testWaves objectAtIndex:(wave-1)] firstObject], [[testWaves objectAtIndex:(wave-1)] lastObject], (unsigned long)[[testWaves objectAtIndex:(wave-1)] count]);
    }
    
    return testWaves;
}

- (void) linkZombiePDR:(id)pdr toShot:(Shot*)shot
{
    NSManagedObjectContext *MOC = shot.managedObjectContext;
    QBTable *pdrTable = [QBTable tableNamed:@"Product Data" inMOC:MOC];
    
    if ([pdr isKindOfClass:[ProductDataRecord class]])
    {
        // pdr exists, so we don't need to import it
        if (![shot.productdatarecords containsObject:pdr])
        {
            [shot addProductdatarecordsObject:pdr];
        }
    }
    else if ([pdr isKindOfClass:[NSNumber class]])
    {
        // pdr is pdrPrimaryKeyValue - which may need to be imported
        id pdrPKV = pdr;
        ProductDataRecord *pdr = [ProductDataRecord existingWithPrimaryKeyValue:pdrPKV inContext:MOC];
        
        if (pdr == nil)
        {
            NSError *fetchPDRsError = nil;
            QBQuery *fetchPDRQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                     inTable:@"Product Data"
                                                       inMOC:MOC
                                                  withParams:@{@"query" : [NSString stringWithFormat:@"{%@.TV.'%@'}", pdrTable.primaryKey.fid, pdrPKV],
                                                               @"clist" : pdrTable.clist_query,
                                                               @"fmt" : @"structured",
                                                               @"includeRIDs" : @"1"
                                                               } includeTicket:YES];
            
            RXMLElement *fetchedPDRresults = [fetchPDRQuery execute:&fetchPDRsError];
            
            if (fetchPDRsError!= nil)
            {
                DDLogVerbose(@"Error linking zombie PDR: %@", fetchPDRsError);
            }
            else
            {
                RXMLElement *pdrRecordXML = [fetchedPDRresults child:@"table.records.record"];
                if (pdrRecordXML != nil) {
                    pdr = [ProductDataRecord insertWithXMLData:pdrRecordXML intoContext:MOC];
                    [shot addProductdatarecordsObject:pdr];
                }
            }
        }
    }
    else if ([pdr isKindOfClass:[NSString class]])
    {
        // pdr is pdrPrimaryKeyValue - which may need to be imported
        id pdrPKV = pdr;
        ProductDataRecord *pdr = [ProductDataRecord existingWithPrimaryKeyValue:pdrPKV inContext:MOC];
        
        if (pdr == nil)
        {
            NSError *fetchPDRsError = nil;
            QBQuery *fetchPDRQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                     inTable:@"Product Data"
                                                       inMOC:MOC
                                                  withParams:@{@"query" : [NSString stringWithFormat:@"{%@.TV.'%@'}", pdrTable.primaryKey.fid, pdrPKV],
                                                               @"clist" : pdrTable.clist_query,
                                                               @"fmt" : @"structured",
                                                               @"includeRIDs" : @"1"
                                                               } includeTicket:YES];
            
            RXMLElement *fetchedPDRresults = [fetchPDRQuery execute:&fetchPDRsError];
            
            if (fetchPDRsError!= nil)
            {
                DDLogVerbose(@"Error linking zombie PDR: %@", fetchPDRsError);
            }
            else
            {
                RXMLElement *pdrRecordXML = [fetchedPDRresults child:@"table.records.record"];
                if (pdrRecordXML != nil) {
                    pdr = [ProductDataRecord insertWithXMLData:pdrRecordXML intoContext:MOC];
                    [shot addProductdatarecordsObject:pdr];
                }
            }
        }
    }
    else
    {
        DDLogError(@"Trying to Link ZombiePDR with unknown PDR type: %@", pdr);
        return;
    }
    
    NSError *mocSaveError = nil;
    [MOC save:&mocSaveError];
    
    if (mocSaveError)
    {
        DDLogError(@"Error saving MOC linkZombiePDR() %@ to shot: %@\nDetails:\n%@", pdr, shot.name, mocSaveError);
    }
    
    //Query Linked Shots and Product Data table to verify record doesn't already exist.
    QBTable *linkedShotsAndProductDataTable = [QBTable tableNamed:@"Linked Shots and Product Data" inMOC:MOC];
    QBField *relatedShotField = [linkedShotsAndProductDataTable fieldForMastagTableNamed:@"Shots"];
    QBField *relatedPDRField = [linkedShotsAndProductDataTable fieldForMastagTableNamed:@"Product Data"];
    
    NSString *queryString = [NSString stringWithFormat:@"{%@.TV.'%@'}AND{%@.EX.'%@'}", relatedShotField.fid, shot.record_id, relatedPDRField.fid, [pdr primaryKeyValue]];
    
    QBQuery *onlineCheckQuery = [QBQuery defaultQueryInTable:@"Linked Shots and Product Data"
                                                       inMOC:MOC
                                             withQueryString:queryString];
    
    NSError *checkError = nil;
    RXMLElement *results = [onlineCheckQuery execute:&checkError];
    if (checkError != nil) {
        [[QBDBInfo info] alertMe:checkError];
    }
    else
    {
        NSArray *lspdrRecords = [[results child:@"table.records"] children:@"record"];
        if (lspdrRecords.count == 0){
            [exporter createLinkedShotID:shot.objectID withPDRID:[pdr objectID] inMOC:MOC];
        }
    }
}

- (void) saveFieldIDs:(NSDictionary*)fields forTable:(NSString*)tableName {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:fields forKey:tableName];
    [prefs synchronize];
}

- (int) weeksSinceDateModified:(NSDate*)dateModified {
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    
    // unsigned int unitFlags = NSDayCalendarUnit | NSMonthCalendarUnit;
    unsigned int unitFlags = NSDayCalendarUnit;
    
    NSDateComponents *conversionInfo = [sysCalendar components:unitFlags fromDate:dateModified];
    int weeks = ceil([conversionInfo day]/7);
    //DDLogVerbose(@"%d weeks since modified.", weeks);
    return weeks;
    
}
@end
