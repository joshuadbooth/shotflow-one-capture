//
//  QBQuery.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/16/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "QBQuery.h"
#import "QBTable.h"
#import "QBField.h"
#import "QBDBInfo.h"
#import "QBSyncManager.h"
#import <AFNetworking/AFNetworking.h>


@implementation QBQuery {
    QBDBInfo *QBinfo;
}

@synthesize query;

@synthesize safeQuery;
@synthesize fullQuery;
@synthesize queryURL;
@synthesize rawParams;
@synthesize rawQueryString;

@synthesize resultsString;
@synthesize queryError;
@synthesize tableName;

@synthesize successCallback;
@synthesize failureCallback;



- (NSString*) debugDescription {
    NSMutableString *returnString = [NSMutableString string];
    [returnString appendFormat:@"QBQuery: \n\t"];
    [returnString appendFormat:@"queryAction: %@\n\t", self.queryAction];
    [returnString appendFormat:@"tableName: %@\n\t", self.tableName];
    [returnString appendFormat:@"rawParams: %@\n\n\t", self.queryAction];
    [returnString appendFormat:@"rawQueryString: %@\n\n\t", self.rawQueryString];
    [returnString appendFormat:@"resultsString: %@\n\n", self.resultsString];
    
    return returnString;
}

- (NSString*) description {
    NSMutableString *returnString = [NSMutableString string];
    [returnString appendFormat:@"QBQuery: %@ %@", self.queryAction, self.tableName];
    return returnString;
}

- (id) init {
    self = [super init];
    return self;
}

+ (QBQuery*) defaultQueryInTable:(NSString *)table
                           inMOC:(NSManagedObjectContext*)moc
                 withQueryString:(NSString *)queryString {

    QBTable *queryTable = [QBTable tableNamed:table inMOC:moc];
    NSString *clist = queryTable.clist_query;
    if (clist.length == 0) {
        clist = @"a";
    }
    
    QBQuery *defaultQuery = [QBQuery queryForAction:@"API_DoQuery"
                                            inTable:table
                                              inMOC:moc
                                         withParams:@{
                                                      @"query" : queryString,
                                                      @"clist":clist,
                                                      @"fmt" : @"structured",
                                                      @"slist" : @"3",
                                                      @"includeRids" : @"1"
                                                      }
                                      includeTicket:YES];
    return defaultQuery;
    
}

+ (NSString*) defaultQueryString {
    NSString *qS = [NSString stringWithFormat:@"{'2'.OAF.'%@'}", [[QBDBInfo info] waybackDate:nil convertForPOST:YES]];
    qS = @"";
    return qS;
}

+(QBQuery*) queryForAction:(NSString*)action
                   inTable:(NSString*)tableName
                     inMOC:(NSManagedObjectContext*)moc
                withParams:(NSDictionary*)params
             includeTicket:(BOOL)includeTicket {

    QBQuery *aQuery = [[QBQuery alloc] init];
    aQuery.tableName = tableName;
    [aQuery createQueryStringForAction:action
                               inTable:tableName
                                 inMOC:moc
                            withParams:params
                         includeTicket:includeTicket];
    
    return aQuery;
}

+(QBQuery*) authenticate {
    QBQuery *authQuery = [[QBQuery alloc] init];
    authQuery.queryAction = @"API_Authenticate";
    
    NSString *returnQuery = [NSString stringWithFormat:@"main?a=API_Authenticate&username=%@&password=%@&hours=16", [[QBDBInfo info] username], [[QBDBInfo info] password]];
    
    authQuery.query = returnQuery;
    
    return authQuery;
}

-(NSString*) createQueryStringForAction:(NSString*)action
                                inTable:(NSString*)tName
                                  inMOC:(NSManagedObjectContext*)moc
                             withParams:(NSDictionary*)params
                          includeTicket:(BOOL)includeTicket
{
    self.rawParams = [NSMutableString string];
    self.rawQueryString = [NSMutableString stringWithFormat:@"%@", [[QBDBInfo info] baseURL]];
    
    self.payload = [NSMutableString string];
    
    self.tableName = tName;
    if (![tName  isEqual: @"main"])
    {
        QBTable *table = [QBTable tableNamed:tName inMOC:moc];
        self.tableID = table.dbid;
    }
    else
    {
        self.tableID = @"main";
    }
    
    
    self.queryAction = action;
    NSMutableString *returnQuery = [NSMutableString stringWithFormat:@"%@?a=%@", self.tableID, action];
    [rawQueryString appendString:returnQuery];
    
    
    
    if ((includeTicket) && ([[QBDBInfo info] ticket]!= nil)) [returnQuery appendFormat:@"&ticket=%@", [[QBDBInfo info] ticket]];
    if ((includeTicket) && ([[QBDBInfo info] ticket]!= nil)) [rawQueryString appendFormat:@"&ticket=%@", [[QBDBInfo info] ticket]];
    
    if ((includeTicket) && ([[QBDBInfo info] ticket]!= nil)) [self.payload appendFormat:@"<ticket>%@</ticket>\n", [[QBDBInfo info] ticket]];
    
    
    NSMutableArray *newParams = [NSMutableArray array];
    
    if (params)
    {
        __block NSMutableArray *array = [NSMutableArray array];
        
        [params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop)
        {
            NSDictionary *dict = [NSDictionary dictionaryWithObjects:@[key, [params objectForKey:key]] forKeys:@[@"keyName", @"val"]];
            [array addObject:dict];
        }];
        
        NSArray *sortedByValue = [array sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *item1, NSDictionary *item2) {
            id val1 = item1[@"val"];
            id val2 = item2[@"val"];
            
            if ([val1 isKindOfClass:[NSString class]] || [val1 isKindOfClass:[NSConstantString class]]){
                
            } else {
                val1 = [val1 stringValue];
            }
            
            if ([val2 isKindOfClass:[NSString class]] || [val2 isKindOfClass:[NSConstantString class]]){
                
            } else {
                val2 = [val2 stringValue];
            }

            /* Puts blank values last for better compatibility with QB */
            return -1*[val1
                    compare:val2
                    options: NSCaseInsensitiveSearch|NSForcedOrderingSearch];
        }];
        
        // ***  URL Encode values *** //
        for (id obj in sortedByValue) {
            id key = [obj valueForKey:@"keyName"];
            /* code that uses the returned key */
            [rawParams appendFormat:@"&%@=%@", key, [params valueForKey:key]];
            
            
            NSString *closingTag = key;
            if ([key hasPrefix:@"<field fid="])
            {
                closingTag = @"field";
            }
            [self.payload appendFormat:@"<%@>%@</%@>\n", key, [params valueForKey:key], closingTag];
            
            CFStringRef cString = (__bridge CFStringRef)([NSString stringWithFormat:@"%@",[params valueForKey:key]]);
            
            CFStringRef encodedString = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                cString,
                                                                                NULL,
                                                                                CFSTR(":/?#[]@!$&'()*+,;="),
                                                                                kCFStringEncodingUTF8);

            NSString *newEncodedString = CFBridgingRelease(encodedString);
            [newParams addObject:[NSDictionary dictionaryWithObjects:@[key, newEncodedString] forKeys:@[@"key", @"val"]]];
            
        }
        
    }
    
    for (id obj in newParams) {
        id key = [obj valueForKey:@"key"];
        id val = [obj valueForKey:@"val"];
        /* code that uses the returned key */
        [returnQuery appendFormat:@"&%@=%@", key, val];
    }

    // *** Signify Dates are in milliseconds in UTC if adding or editing a record *** //
    if ([action isEqualToString:@"API_AddRecord"] || [action isEqualToString:@"API_EditRecord"])
    {
        [returnQuery appendString:@"&msInUTC=1"];
        [rawParams appendString:@"&msInUTC=1"];
        [self.payload appendString:@"<msInUTC>1</msInUTC>\n"];
    }
     
    [rawQueryString appendString:rawParams];
    self.query = returnQuery;
    
    return returnQuery;
    
}

- (NSString*) userInfo
{
    if (QBinfo == nil) QBinfo = [QBDBInfo info];
    return [NSString stringWithFormat:@"&username=%@&password=%@", QBinfo.username, QBinfo.password];
}


+ (NSSet*) keyPathsForValuesAffectingFullQuery {
    return [NSSet setWithObjects:@"query", @"safeQuery", nil];
}

+ (NSSet*) keyPathsForValuesAffectingSafeQuery {
    return [NSSet setWithObject:@"query"];
}

+ (NSSet*) keyPathsForValuesAffectingQueryURL {
    return [NSSet setWithObjects:@"query", @"safeQuery", @"fullQuery", nil];
}

-(NSString*) safeQuery {
    return [query stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
}

-(NSString*) fullQuery {
    
    NSString *full = [NSString stringWithFormat:@"%@%@", [[QBDBInfo info] baseURL],query];
//    NSString *full = [NSString stringWithFormat:@"%@%@", [[QBDBInfo info] baseURL],[self safeQuery]]; // old way
    return full;
}

- (NSURL*) queryURL {
 
    return [NSURL URLWithString:[self fullQuery]];
}

- (RXMLElement*) execute:(NSError**)error
{
    NSError *executionError = nil;
    resultsString = [[NSString alloc] initWithContentsOfURL:[self queryURL] encoding:NSUTF8StringEncoding error:&executionError];
    
    if (executionError !=nil) {
        // *error = executionError;
        [[QBDBInfo info]alertMe:executionError];
        DDLogError(@"QBQuery Execution Error: %@", executionError);
        
        return nil;
    }
    else
    {
      // no immediate errors
      RXMLElement *xmlResults = [RXMLElement elementFromXMLString:resultsString encoding:NSUTF8StringEncoding];
      
        if (![[xmlResults child:@"errcode"].text isEqualToString:@"0"]) {
            // if xmlResults shows error
            *error = [self generateError:xmlResults];
            return nil;
        } else {
            // no errors, return results as RXMLelement.
            return xmlResults;
        }        
    }
    
    return nil;
}

-(void)executeWithSuccessCallback:(SuccessBlock)onSuccess failureCallback:(FailureBlock)onFailure
{
    
    if (onSuccess != nil) self.successCallback = onSuccess;
    if (onFailure != nil) self.failureCallback = onFailure;
    
    
    
    AFURLSessionManager *manager = [[QBSyncManager sharedManager] manager];
    NSURLRequest *request = [NSURLRequest requestWithURL:[self queryURL]];
    
    NSMutableURLRequest *xmlRequest = [NSMutableURLRequest requestWithURL:[self queryURL]];
    
    [xmlRequest setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
    [xmlRequest setHTTPMethod:@"POST"];
    [xmlRequest setValue:self.queryAction forHTTPHeaderField:@"QUICKBASE-ACTION"];
    
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request
                                                completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                    if (error !=nil)
                                                    {
                                                        DDLogError(@"QBQuery Execution Error: %@", error);
                                                        if (self.failureCallback) self.failureCallback(error);
                                                    }
                                                    else
                                                    {
                                                        // no immediate errors
                                                        RXMLElement *xmlResults = [RXMLElement elementFromXMLData:responseObject];
                                                        
                                                        if (![[xmlResults child:@"errcode"].text isEqualToString:@"0"]) {
                                                            // if xmlResults shows error
                                                            NSError *returnError = [self generateError:xmlResults];
                                                            if (self.failureCallback!= nil) self.failureCallback(returnError);
                                                        }
                                                        else {
                                                            // no errors, return results as RXMLelement.
                                                            if (self.successCallback != nil) self.successCallback([xmlResults copy]);
                                                        }
                                                    }
                                                }];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [dataTask resume];
}



- (NSError*) generateError:(RXMLElement *)rootXML {
    NSInteger errCode = [rootXML child:@"errcode"].textAsInt;
    NSString *errText = [rootXML child:@"errtext"].text;
    NSString *errDetail = [NSString stringWithFormat:@"%@", [rootXML child:@"errdetail"].text];

#ifdef DEBUG
    errDetail = [errDetail stringByAppendingFormat:@"\n\nAction: %@\nTargetDB: %@",  self.queryAction, self.tableName];
#endif
    
    NSMutableDictionary *errorDict = [NSMutableDictionary dictionary];
    [errorDict setValue:errText forKey:NSLocalizedDescriptionKey];
    [errorDict setValue:errDetail forKey:NSLocalizedFailureReasonErrorKey];
    
    self.queryError = [NSError errorWithDomain:@"com.showflow1.qb" code:errCode userInfo:errorDict];
    DDLogError(@"QBQuery ERROR: %@", [errDetail stringByAppendingString:[NSString stringWithFormat:@"\n\n rawParams: %@", rawParams]]);
    return self.queryError;
}


- (NSString*) defaultQueryParams {
    QBDBInfo *qb = [QBDBInfo info];
    return [NSString stringWithFormat:@"&ticket=%@", qb.ticket];
}

+ (QBQuery*) queryBuilder {
    QBQuery *query = [[QBQuery alloc] init];
    return query;
}

+ (NSString *) buildQueryStringForTable:(NSString*)tableName
                              fieldName:(NSString*)fname
                     comparisonOperator:(NSString*)co
                           compareValue:(id)cValue
                                  inMOC:(NSManagedObjectContext *)moc
{
    // **** Get Types & Count of Compare Values **** //
    NSMutableArray *compareValues;
    
    
    if ([cValue isKindOfClass:[NSArray class]]) {
        compareValues = [NSMutableArray array];
        
        for (id cov in cValue) {
            if ([cov isKindOfClass:[NSString class]]) {
                [compareValues addObject:cov];
            }
            else
            {
                // if not string, convert to string
                [compareValues addObject:[cov stringValue]];
            }
        }
        
    }
    else if (![cValue isKindOfClass:[NSString class]])
    {
        [compareValues addObject:[cValue stringValue]];
    }
    
    NSDictionary *operators = @{
                                @"contains" : @"CT",
                                @"does not contain" : @"XCT",
                                @"is" : @"EX",
                                @"=" : @"EX",
                                @"is not" : @"XEX",
                                @"==" : @"TV",
                                @"!=" : @"XEX",
                                @"true value" : @"TV",
                                @"has" : @"HAS",
                                @"xhas" : @"XHAS",
                                @"starts with" : @"SW",
                                @"does not start with" : @"XSW",
                                @"before" : @"BF",
                                @"on or before" : @"OBF",
                                @"after" : @"AF",
                                @"on or after" : @"OAF",
                                @"<" : @"LT",
                                @"<=" : @"LTE",
                                @">" : @"GT",
                                @">=" : @"GTE",
                                @"during" : @"IR",
                                @"not during" : @"XIR"
                                };
    
    if (![[operators allKeys]containsObject:[co lowercaseString]]){
        [[QBDBInfo info]alertString:[NSString stringWithFormat:@"ERROR: BuildQuery(): Improper Query Comparison: '%@' not found.", [co lowercaseString]]];
        return @"";
    }
    
    NSString *compareOp = [operators valueForKey:[co lowercaseString]];
    
    QBTable *qbTable = [QBTable tableNamed:tableName inMOC:moc];
    QBField *field = [qbTable fieldNamed:fname];
    
    if (field == nil){
        [[QBDBInfo info] alertString:[NSString stringWithFormat:@"ERROR: BuildQuery(): Field id for '%@' in table '%@' not found.",fname, tableName]];
        return @"";
    }
    
    NSMutableString *returnQuery = [NSMutableString string];
    
    for (NSInteger i=(compareValues.count - 1); i>=0; i--) {
        NSString* cov = [compareValues objectAtIndex:i];
        [returnQuery appendFormat:@"{%@.%@.'%@'}", field.fid, compareOp, cov];
        if (i != 0) [returnQuery appendString:@"OR"];
    }
    
    DDLogVerbose(@"BuildQuery(): %@", returnQuery);
    return (NSString*)returnQuery;
    
}

@end
