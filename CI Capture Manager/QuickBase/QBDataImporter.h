//
//  QBDataImporter.h
//  QBApiTest
//
//  Created by Josh Booth on 7/2/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QBDBInfo.h"
#import "QBSyncManager.h"

@class Set, SetList, Shot, ProductDataRecord, Sample, Station;
@class RXMLElement, SF1XMLRecord;

typedef void (^completionBlockWithObjects)(NSMutableArray* objectTree);

@interface QBDataImporter : NSObject

@property (nonatomic, retain) NSDate *lastCheck;
@property (nonatomic) BOOL initialLoad;

@property (nonatomic, strong) NSManagedObjectID *mainSetObjectID;

@property (nonatomic) BOOL forceUpdate;


+ (id) sharedImporter;

- (void) loadSet:(NSManagedObjectID*) setID;


#pragma mark - Importing
- (void) importData;

- (NSArray*) importStations;
- (void) importSets;
- (void) importSetLists;
- (void) importShots;
- (void) importProductData;
- (void) importSamples;
- (void) importGroupingForPDRstring:(NSString*)pdrString onComplete:(void (^_Nullable)(NSNumber *groupingRID))onComplete;

#pragma mark - Selective Import

- (void) importSampleRIDs:(NSArray *) samplesArray
                forShotID:(NSManagedObjectID *)shotID
                 usingMOC:(NSManagedObjectContext *) parentMOC
          completionBlock:(completionBlockWithObjects)completionBlock;

- (void) importPDRforSampleID:(NSManagedObjectID *)sampleID
                 usingContext:(NSManagedObjectContext *)targetMOC
              completionBlock:(completionBlockWithObjects)completionBlock;


//- (void) importSamplesforPDR:(ProductDataRecord*)pdr completionBlock:(completionBlockWithObjects)completionBlock;
//- (void) importPDRs:(NSArray <NSManagedObjectID*> *)PDRsArray forShotID:(NSManagedObjectID*)shotID completionBlock:(completionBlockWithObjects)completionBlock;




@end
