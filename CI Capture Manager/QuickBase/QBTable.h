//
//  QBTable.h
//  CI Capture Manager
//
//  Created by Josh Booth on 2/9/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


NS_ASSUME_NONNULL_BEGIN

@class QBField, RXMLElement;

@interface QBTable : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

+ (NSString *)entityName;

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context
                                  withName:(NSString*)tname
                                      DBID:(NSString*)db;

+ (instancetype)tableNamed:(NSString*)tname inMOC:(NSManagedObjectContext*)moc;

- (QBField*)fieldNamed:(NSString*)fnm;
- (QBField*)fieldWithID:(NSString*)fid;

- (id) convertXMLRecordPKValueToCorrectType:(RXMLElement*)record;
- (id) convertXMLRecordValue:(RXMLElement*)record toCorrectValueTypeForFieldNamed:(NSString*)fieldName;
- (id) convertValue:(NSString*)val toCorrectValueTypeForFieldNamed:(NSString*)fieldName;


- (NSString*) fieldNameForMastagTableNamed:(NSString*)tableName;
- (QBField*) fieldForMastagTableNamed:(NSString*)tableName;

@end

NS_ASSUME_NONNULL_END

#import "QBTable+CoreDataProperties.h"
