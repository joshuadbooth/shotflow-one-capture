//
//  QBField.m
//  CI Capture Manager
//
//  Created by Josh Booth on 2/9/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "QBField.h"
#import "QBTable.h"
#import "AppDelegate.h"

@implementation QBField

// Insert code here to add functionality to your managed object subclass
+ (NSString *)entityName {
    return @"QBField";
}

-(NSString*) description {
    return [NSString stringWithFormat:@"%@ - %@ (%@)", self.fid, self.name, self.table.name];
}

- (NSString*) debugDescription {
    return [self.description stringByAppendingFormat:@"(mode:%@) (base_type:%@)", self.mode, self.base_type];
}

+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context withName:(NSString*)fnm FID:(NSString*)fID
{
    QBField* obj = [NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                           inManagedObjectContext:context];
    obj.name = fnm;
    obj.fid = fID;
    
    NSError *permIDError = nil;
    [context obtainPermanentIDsForObjects:@[obj] error:&permIDError];
    return obj;
}


+ (instancetype) fieldNamed:(NSString*)fnm inTableNamed:(NSString*)tnm inMOC:(NSManagedObjectContext*)moc
{
    QBTable *table = [QBTable tableNamed:tnm inMOC:moc];
    if (table == nil)
    {
        @throw [NSException exceptionWithName:@"NullTableException" reason:[NSString stringWithFormat:@"Unable to retrieve field located in table '%@'", tnm] userInfo:nil];
        return nil;
    }
    return [table fieldNamed:fnm];
}

+ (instancetype) fieldWithID:(NSString*)fid inTableNamed:(NSString*)tnm inMOC:(NSManagedObjectContext*)moc
{
    QBTable *table = [QBTable tableNamed:tnm inMOC:moc];
    if (table == nil)
    {
        @throw [NSException exceptionWithName:@"NullTableException" reason:[NSString stringWithFormat:@"Unable to retrieve field located in table '%@'", tnm] userInfo:nil];
        return nil;
    }
    return [table fieldWithID:fid];
}

@end
