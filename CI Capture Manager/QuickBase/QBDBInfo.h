//
//  QBDBInfo.h
//  QBApiTest2
//
//  Created by Josh Booth on 7/8/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

@import Foundation;
@import CoreData;

//#import "RXMLElement.h"
//#import "QBSyncManager.h"
//
//#import "Set.h"
//#import "SetList.h"
//#import "Shot.h"
//#import "Station+CoreDataClass.h"

typedef void (^completionBlock) (void);
typedef void (^recoveryBlock) (void);
typedef void (^errorBlock)(void);

@interface QBDBInfo : NSObject

@property (nonatomic, retain) NSString* baseURL;

@property (nonatomic, retain) NSString* ticket;

@property (nonatomic, retain) NSString* userID;
@property (nonatomic, retain) NSString* username;
@property (nonatomic, retain) NSString* password;

@property (nonatomic, retain) NSString* DBID;

@property (nonatomic, retain) NSMutableDictionary *configurationData;

@property (nonatomic, retain) NSMutableDictionary *tableIDs;

@property (nonatomic, strong) NSString *timeZone;
@property (nonatomic) double timeZoneOffset; // in seconds
@property (nonatomic, strong) NSString *dateFormat;


@property (nonatomic, retain) NSArray * detailView_clist;
@property (nonatomic, retain) NSMutableDictionary *detailViewFieldIDsDict;
@property (nonatomic, retain) NSArray * xmp_clist;
@property (nonatomic, retain) NSMutableDictionary *xmpFieldIDsDict;

@property (nonatomic, retain) NSArray * shots_xmp_clist;
@property (nonatomic, retain) NSMutableDictionary *shots_xmpFieldIDsDict;

@property (nonatomic, retain) NSMutableDictionary *table_clists;

@property (nonatomic, retain) NSArray * samplesDetailView_clist;
@property (nonatomic, retain) NSMutableDictionary *samplesDetailViewFieldIDsDict;

@property (nonatomic, strong) NSString *defaultSeparator;

@property BOOL didAuthenticate;
@property BOOL loggedIn;


+ (instancetype) info;
- (instancetype) initWithUsername:(NSString*)user AndPassword:(NSString*)pass;


- (NSArray*) grantedDBs;
- (void) createAppIDTable;


- (void) downloadTablesAndFields;
- (void) downloadTablesAndFields:(completionBlock)completionBlock;
- (void) downloadConfigurationData:(completionBlock)completionBlock;

- (NSString*) clist_QueryForTable:(NSString*)tableName inMOC:(NSManagedObjectContext*)MOC;

- (NSArray*) QBTablesInMOC:(NSManagedObjectContext*)MOC;
- (void) deleteAllTables;

- (id) getTableByID:(NSString*)dbid inMOC:(NSManagedObjectContext*)MOC;
- (id) getTableByName:(NSString*)tname inMOC:(NSManagedObjectContext*)MOC;
- (id) getTableByAlias:(NSString*)alias inMOC:(NSManagedObjectContext*)MOC;


- (NSString*) getIDforTable:(NSString*)tableName
                      inMOC:(NSManagedObjectContext*)MOC;

- (NSString*) getFieldIDforFieldNamed:(NSString*)fieldname
                         inTableNamed: (NSString*) tableName
                                inMOC:(NSManagedObjectContext*)MOC;
- (NSString*) getFieldNameforFieldID:(NSString*)fieldID
                        inTableNamed:(NSString*)tableName
                               inMOC:(NSManagedObjectContext*)MOC;

- (NSNumber*) getLatestRecordIDforTable:(NSString*)tableName
                               orEntity:(id)object
                                  inMOC:(NSManagedObjectContext*)MOC
                            isTemporary:(BOOL)temporary;

- (BOOL) authenticate:(NSError**)error;

- (BOOL) checkEntityForName:(NSString*)entity
               withRecordID: (NSNumber *)recordID
     inManagedObjectContext:(NSManagedObjectContext*) moc;

- (id) getRelatedEntity:(NSString*)entity
           withRecordID:(NSNumber *)recordID
 inManagedObjectContext:(NSManagedObjectContext*)moc
                  error:(NSError**)error;

- (id) getRelatedEntity:(NSString*)entity
    withPrimaryKeyValue:(id)pkValue
 inManagedObjectContext:(NSManagedObjectContext*)moc
                  error:(NSError**)error;

- (NSArray*) entitiesWithObjectIDs:(NSArray*)idArray
                            inMOC:(NSManagedObjectContext*)MOC;

- (id) waybackDate:(NSDate*)date
    convertForPOST:(BOOL)shouldConvert;

- (NSString*) convertDateForPOST:(NSDate *)date;

- (void) alertMe:(NSError*) error;
- (void) alertMe:(NSError *)error andThen:(completionBlock) block;

- (void) alertMe:(NSError*)error
  recoveryBlock1:(recoveryBlock __nonnull)recoveryBlock1
  recoveryBlock2:(recoveryBlock __nullable)recoveryBlock2
  recoveryBlock3:(recoveryBlock __nullable)recoveryBlock3;

- (void) alertString: (NSString *)string;

- (NSNumber *) recordAsNSNumber: (NSInteger)num;
- (NSString*) prettyDate:(NSDate*) date;

@end
