//
//  QBField+CoreDataProperties.h
//  CI Capture Manager
//
//  Created by Josh Booth on 3/17/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QBField.h"

NS_ASSUME_NONNULL_BEGIN

@interface QBField (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *base_type;
@property (nullable, nonatomic, retain) NSString *fid;
@property (nullable, nonatomic, retain) NSString *field_type;
@property (nullable, nonatomic, retain) NSString *formula;
@property (nullable, nonatomic, retain) NSString *mode;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *mastag;
@property (nullable, nonatomic, retain) NSNumber *isPrimary;
@property (nullable, nonatomic, retain) QBTable *table;
@property (nullable, nonatomic, retain) QBTable *pkForTable;
@property (nullable, nonatomic, retain) QBTable *refForTable;
@property (nullable, nonatomic, retain) QBTable *mastagTable;

@end

NS_ASSUME_NONNULL_END
