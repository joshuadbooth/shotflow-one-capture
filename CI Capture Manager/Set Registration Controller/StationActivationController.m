//
//  StationActivationController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 8/29/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "StationActivationController.h"
#import "AppDelegate.h"
#import "QBDataExporter.h"
#import "QBTable.h"
#import "QBQuery.h"
#import "Station+CoreDataClass.h"
#import "QBDBInfo.h"
#import "RXMLElement.h"
#import "RXMLElement+Supplemental.h"
#import "SF1XMLRecord.h"

@implementation StationActivationController {
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    QBDBInfo *QBinfo;
    NSError *maximumStationActivationError;
}


- (instancetype) init {
    if (self = [super init])
    {
        app = [AppDelegate sharedInstance];
        mainMOC = app.mainMOC;
        QBinfo = [QBDBInfo info];
        
        maximumStationActivationError = [[NSError alloc]
                                         initWithDomain:@"com.shotflow.StationsActivationController"
                                         code:1040
                                         userInfo:@{
                                                    NSLocalizedDescriptionKey : @"Maximum Activated Stations Exceeded.",
                                                    NSLocalizedRecoverySuggestionErrorKey : @"Contact your ShotFlow One system administrator to deactivate unused stations and try again. \n\nShotFlow One will now close.",
                                                    NSLocalizedFailureReasonErrorKey : @"There are too many ShotFlow One Stations currently activated.",
                                                    }];
    }
    
    return self;
}
- (void) verifyCurrentStation
{
    // *** 1.) Get Current Station Info
    // *** 2.) Query Stations Online
    // *** 2.1.) Current Station Online? Create / Update
    // *** 3.) Check Activation Status
    // *** 4.) Verify Active Number of Stations
    // *** 5.) Activate / Assign Station to Set
    
    
    
    // *** 1.) Get Current Station Info
    NSProcessInfo *pInfo = [NSProcessInfo processInfo];
    
    NSString *hostName = [pInfo hostName];
    NSString *osVersion = [pInfo operatingSystemVersionString];
    NSString *serialNumber = [app getSystemSerialNumber];
    NSString *systemUUID = [app getSystemUUID];
    NSNumber *RAM = @([pInfo physicalMemory] / (int)pow(1024, 2));
    
    NSInteger maxNumberOfActiveStations = [[QBinfo.configurationData valueForKey:@"MAXNUMBEROFACTIVESTATIONS"] integerValue];
    
    
    // *** 2.) Query Stations Online
    DDLogVerbose(@"\n\n");
    DDLogVerbose(@"Importing Stations");
    
    NSManagedObjectContext *privateMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    privateMOC.parentContext = mainMOC;
    
    DDLogDebug(@"ImportStations MOC: %@", privateMOC);
    [privateMOC performBlockAndWait:^{
        
        QBTable *stationsTable = [QBTable tableNamed:@"Stations" inMOC:privateMOC];
        
        QBQuery *importStationsQuery = [QBQuery queryForAction:@"API_DoQuery"
                                                       inTable:stationsTable.name
                                                         inMOC:privateMOC
                                                    withParams:@{
                                                                 @"includeRids" : @"1",
                                                                 @"clist": stationsTable.clist_query,
                                                                 @"slist" : @"1.3",
                                                                 @"fmt" : @"structured"
                                                                 }
                                                 includeTicket:YES];
        
        
        NSError *importStationsError = nil;
        
        RXMLElement *importStationsQueryResults = [importStationsQuery execute:&importStationsError];
        
        if (importStationsError == nil)
        {
            NSArray *stationsXMLRecords = [[importStationsQueryResults child:@"table.records"] children:@"record"];
            DDLogVerbose(@"# of Stations: %lu\n\n", (unsigned long)stationsXMLRecords.count);
            
            NSInteger currentNumberOfActiveStations = 0;
            
            NSMutableArray *stationSF1XMLRecords = [NSMutableArray array];
            for (RXMLElement *stationRXML in stationsXMLRecords)
            {
                SF1XMLRecord *stationXMLRecord = [SF1XMLRecord sf1RecordWithXMLRecord:stationRXML fromTable:stationsTable];
                if ([[stationXMLRecord valueForFieldNamed:@"Active"] isEqualTo:[NSNumber numberWithBool:YES]])
                {
                    currentNumberOfActiveStations++;
                }
                [stationSF1XMLRecords addObject:stationXMLRecord];
            }
            
            
            // *** 2.1.) Current Station Online? Create / Update
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"primaryKeyValue == %@", systemUUID];
            SF1XMLRecord *existingStation = [[stationSF1XMLRecords filteredArrayUsingPredicate:predicate] firstObject];
            
            if (existingStation == nil)
            {
                // *** 4.) Verify Active Number of Stations
                if (currentNumberOfActiveStations >= maxNumberOfActiveStations)
                {
                    // Generate MaximumStationActivationError
                    [QBinfo alertMe:maximumStationActivationError andThen:^{
                        [NSApp terminate:self];
                    }];
                }
                else
                {
                    // Record does not exist, so needs to be created
                    Station *station = [Station insertNewObjectIntoContext:privateMOC];
                    station.name = hostName;
                    station.hardwareUUID = systemUUID;
                    station.serialNumber = serialNumber;
                    
                    
                    [station setParam:hostName forKey:@"Name"];
                    [station setParam:systemUUID forKey:@"Hardware UUID"];
                    [station setParam:serialNumber forKey:@"Serial Number"];
                    [station setParam:osVersion forKey:@"OS"];
                    [station setParam:[[RAM stringValue] substringWithRange:NSMakeRange(0, 2)] forKey:@"RAM"];
                    [station setParam:@"1" forKey:@"Active"];
                    
                    station.online = NO;
                    station.isActive = YES;
                    
                    [privateMOC save:nil];
                    [[QBDataExporter sharedExporter] postToServer:station];
                }
                
            }
            else if ([[existingStation valueForFieldNamed:@"Active"] isEqualTo:[NSNumber numberWithBool:NO]])
            {
                if (currentNumberOfActiveStations >= maxNumberOfActiveStations)
                {
                    // Generate MaximumStationActivationError
                    [QBinfo alertMe:maximumStationActivationError andThen:^{
                        [NSApp terminate:self];
                    }];
                }
                else
                {
                    // Record does not exist, so needs to be created
                    Station *station = [Station existingWithHardwareUUID:existingStation.primaryKeyValue inContext:privateMOC];
                    if (station == nil) {
                        station = [Station insertWithXMLData:existingStation intoContext:privateMOC];
                        
                    }
                    
                    [station setParam:@"1" forKey:@"Active"];
                    station.isActive = YES;
                    
                    [privateMOC save:nil];
                    [[QBDataExporter sharedExporter] postToServer:station];
                }
            }
        }
        else
        {
            [QBinfo alertMe:importStationsError];
        }
    }];
    
}
@end
