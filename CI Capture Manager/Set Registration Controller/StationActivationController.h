//
//  StationActivationController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 8/29/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StationActivationController : NSObject
- (void) verifyCurrentStation;
@end
