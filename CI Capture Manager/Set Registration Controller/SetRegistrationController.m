//
//  SetRegistrationController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "SetRegistrationController.h"
#import "AppDelegate.h"
#import "QBSyncManager.h"
#import "QBDBInfo.h"
#import "QBDataImporter.h"
#import "Set.h"
#import "ListViewController.h"
#import "SetsArrayController.h"
#import "QBTable.h"
#import "QBQuery.h"
#import "StationActivationController.h"
@import QuartzCore;

@interface SetRegistrationController () <NSAnimationDelegate>
- (IBAction)popupChanged:(id)sender;

@end

@implementation SetRegistrationController {
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    QBDBInfo *QBinfo;
    BOOL isAwake;
    M1ArrayController *setsArrayController;
    __block BOOL keepSpinning;
}

- (void) showWindow:(id)sender {
    [super showWindow:sender];
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    if (!isAwake) {
        app = [AppDelegate sharedInstance];
        mainMOC = app.mainMOC;
        QBinfo = [QBDBInfo info];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(refreshList:)
                                                     name:kFinishedSyncNotification
                                                   object:nil];

        _statusLabel.stringValue = @"Downloading Sets...";
        
        self.okButton.enabled = NO;
        self.setListMenu.enabled = NO;
        
        
        NSArray *tables = [QBinfo QBTablesInMOC:mainMOC];
        if (![[QBSyncManager sharedManager]canSync])
        {
            if ([tables count] <= 2)
            {
                [QBinfo alertString:@"There is not enough data to continue working with ShotFlow One.\n\nPlease have an active internet connection and run the application again to properly synchronize data for offline capture."];
                
                [[NSApplication sharedApplication]terminate:self];
            }
            else
            {
                [self refreshList:nil];
            }
        }
        
        
        setsArrayController = [[M1ArrayController alloc] init];
        
        setsArrayController.entityName = @"Set";
        setsArrayController.automaticallyPreparesContent = YES;
        setsArrayController.managedObjectContext = mainMOC;
        
        setsArrayController.avoidsEmptySelection = YES;
        setsArrayController.automaticallyRearrangesObjects = YES;
        setsArrayController.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"set_name" ascending:YES]];
        
        [setsArrayController fetch:nil];
        
        
        [_setListMenu removeAllItems];
        _setListMenu.enabled = NO;
    }
}

- (void)windowDidLoad {
    [super windowDidLoad];
    
    [[self window] setBackgroundColor:[NSColor whiteColor]];
    
}

-(NSImage *)colorizeImage:(NSImage *)baseImage color:(NSColor *)tint {
    
    NSImage *newImage = [baseImage copy];
    if (tint) {
        [newImage lockFocus];
        [tint set];
        NSRect imageRect = {NSZeroPoint, [newImage size]};
        NSRectFillUsingOperation(imageRect, NSCompositeSourceAtop);
        [newImage unlockFocus];
    }

    return newImage;
}


-(void) refreshList:(NSNotification *)note {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        keepSpinning = NO;

        StationActivationController *stationController = [[StationActivationController alloc] init];
        [stationController verifyCurrentStation];
        
        _statusLabel.stringValue = @"Choose a Set for this Station:";
        
        [_setListMenu removeAllItems];
        
        @try {
            
            for (Set *set in setsArrayController.arrangedObjects) {
                [_setListMenu addItemWithTitle:set.name];
                [_setListMenu itemWithTitle:set.name].enabled = set.isAvailable;
            }
            
        }
        @catch (NSException *exception) {
            //            DDLogVerbose(@"exception: %@", exception);
        }
        
        _setListMenu.enabled = YES;
        [_setListMenu setNeedsDisplay:YES];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        id lastUsed = [prefs objectForKey:@"Last Used Set Name"];
        if (lastUsed)
        {
            @try {
                id proposedItem = [_setListMenu itemWithTitle:[prefs valueForKey:@"Last Used Set Name"]];
                if (proposedItem != nil && [[prefs valueForKey:@"Last Used Set Name"] length] > 0)
                {
                    [_setListMenu selectItem:proposedItem];
                }
                else
                {
                    NSInteger firstAvailableItemIndex = [[setsArrayController arrangedObjects] indexOfObjectPassingTest:^BOOL(Set*  _Nonnull set, NSUInteger idx, BOOL * _Nonnull stop) {
                        return set.isAvailable;
                    }];
                    
                    if (firstAvailableItemIndex != NSNotFound)
                    {
                        [_setListMenu selectItemWithTitle:[[[setsArrayController arrangedObjects]objectAtIndex:firstAvailableItemIndex] name]];
                        [self popupChanged:self];
                        [setsArrayController setSelectionIndex:firstAvailableItemIndex];
                    }
                    else
                    {
                        [_setListMenu selectItemAtIndex:0];
                    }
                }
            }
            @catch (NSException *exception) {
                [_setListMenu selectItemAtIndex:0];
                [self popupChanged:self];
            }
        }
        else
        {
            NSInteger firstAvailableItemIndex = [[setsArrayController arrangedObjects] indexOfObjectPassingTest:^BOOL(Set*  _Nonnull set, NSUInteger idx, BOOL * _Nonnull stop) {
                return set.isAvailable;
            }];
            
            if (firstAvailableItemIndex != NSNotFound)
            {
                [_setListMenu selectItemWithTitle:[[[setsArrayController arrangedObjects]objectAtIndex:firstAvailableItemIndex] name]];
                [self popupChanged:self];
                [setsArrayController setSelectionIndex:firstAvailableItemIndex];
            }
            else
            {
                [_setListMenu selectItemAtIndex:0];
            }
        }
        
        if (_setListMenu.selectedItem.enabled) {
            _okButton.enabled = YES;
        }
        
    });
}

- (IBAction)loadDataForSelectedSet:(id)sender {
    
    if (![_setListMenu selectedItem].enabled){
        return;
    }
    NSString *selectedTitle = _setListMenu.titleOfSelectedItem;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    DDLogVerbose(@"Title of selected set: %@", selectedTitle);
    
    [prefs setValue:selectedTitle forKey:@"Last Used Set Name"];
    [prefs synchronize];
    
    
    Set *selectedSet = (Set*)[setsArrayController objectWithName:selectedTitle];
    
    app.selectedSet = selectedSet;
    
    for (Set* set in setsArrayController.arrangedObjects) {
        [set setParam:@"" forKey:@"Active User"];
        if ([set isEqualTo:app.selectedSet]) continue;
        set.activeUser = @"";
    }
     
    [mainMOC save:nil];
    
    [[QBSyncManager sharedManager] loadDataForSet];
    
    
    
    ListViewController *listView = [[ListViewController alloc] initWithWindowNibName:@"ListViewController"];
    [AppDelegate sharedInstance].listView = listView;
    
    [listView showWindow:self];
    [self.window close];
    
}


- (IBAction)refreshData:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [_refreshButton.superview setWantsLayer:YES];
        _refreshButton.layer.anchorPoint = CGPointMake(.5, .5);
        _refreshButton.layer.position = CGPointMake(_refreshButton.frame.origin.x + (_refreshButton.frame.size.width/2), _refreshButton.frame.origin.y + (_refreshButton.frame.size.height/2));
        _refreshButton.layer.shouldRasterize = YES;
        
        
        
        
        CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.fromValue = [NSNumber numberWithFloat:0.0f];
        animation.toValue = [NSNumber numberWithFloat: -M_PI*2];
        animation.duration = 2.0f;
        animation.repeatCount = 0;
        animation.delegate = self;
        [_refreshButton.layer addAnimation:animation forKey:@"SpinAnimation"];
        
        
        //CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        //animation.fromValue = [NSNumber numberWithFloat:0.0f];
        //animation.toValue = [NSNumber numberWithFloat: -M_PI*2];
        //animation.duration = 2.0f;
        //animation.repeatCount = INFINITY;
        //[_refreshButton.layer addAnimation:animation forKey:@"SpinAnimation"];
        
        
        keepSpinning = YES;
        [[QBDataImporter sharedImporter] importSets];
    });
    
}

- (void) animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if (flag && keepSpinning)
    {
        [_refreshButton.layer removeAnimationForKey:@"SpinAnimation"];
        
        CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.fromValue = [NSNumber numberWithFloat:0.0f];
        animation.toValue = [NSNumber numberWithFloat: -M_PI*2];
        animation.duration = 2.0f;
        animation.repeatCount = 0;
        animation.delegate = self;
        [_refreshButton.layer addAnimation:animation forKey:@"SpinAnimation"];
    }
}

- (IBAction)popupChanged:(id)sender {
  DDLogVerbose(@"Selecting Set: %@", _setListMenu.titleOfSelectedItem);
    if (_setListMenu.selectedItem.enabled) {
        _okButton.enabled = YES;
    }
}
@end
