//
//  SetRegistrationController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SetRegistrationController : NSWindowController
@property (strong) IBOutlet NSPopUpButton *setListMenu;
@property (weak) IBOutlet NSButton *okButton;
@property (weak) IBOutlet NSTextField *statusLabel;

@property (weak) IBOutlet NSImageView *bannerImage;
@property (weak) IBOutlet NSView *alwaysAssignCheckbox;
@property (weak) IBOutlet NSButton *refreshButton;

- (IBAction)loadDataForSelectedSet:(id)sender;
- (IBAction)refreshData:(id)sender;

@end
