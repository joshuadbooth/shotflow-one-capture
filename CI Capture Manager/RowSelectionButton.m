//
//  RowSelectionButton.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/10/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "RowSelectionButton.h"
#import "AppDelegate.h"
#import "SetList.h"
#import "Shot.h"

#import "OutlineView.h"
#import "M1TableView.h"
//#import "DetailViewWindowController.h"
#import "ShotsArrayController.h"

#import "DetailsTabViewController.h"
#import "DetailViewWindowController.h"
#import "C1ProController.h"


@interface RowSelectionButton () {
    AppDelegate *app;
    C1ProController *C1PC;
}

@end


@implementation RowSelectionButton

- (void) awakeFromNib {
    [super awakeFromNib];
    
    app = [AppDelegate sharedInstance];
    C1PC = [C1ProController sharedInstance];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

-(void)mouseDown:(NSEvent *)theEvent {
    
    id superTable = nil;
    superTable = self;
    
    
    while ((![superTable isKindOfClass:[M1TableView class]]) &&
           (![superTable isKindOfClass:[OutlineView class]])) {
        superTable = [superTable superview];
    }
    
    
    
    if ([superTable isKindOfClass:[OutlineView class]]) {
        // List View
        
        //get what current selected object is...
        
        id selectedObject = [superTable selectedObject];
        
        NSInteger buttonRow = [superTable rowForView:self];
        id buttonObject = [superTable itemAtRow:buttonRow];
        
        if (selectedObject == nil)
        {
            [superTable selectObject:buttonObject];
            
            if ([buttonObject isKindOfClass:[SetList class]])
            {
                app.selectedShot = nil;
                app.selectedSetList = (SetList*) buttonObject;
                
                [C1PC changeToShot:nil];
            }
            else if ([buttonObject isKindOfClass:[Shot class]])
            {
                app.selectedShot = buttonObject;
                app.selectedSetList = [buttonObject setlist];
                
                [C1PC changeToShot:buttonObject];
            }
            else if (buttonObject == nil) {
                DDLogVerbose(@"No item selected");
                app.selectedShot = nil;
                app.selectedSetList = nil;
                [C1PC changeToShot:nil];
            }
            else
            {
                DDLogVerbose(@"SelectedItem is not SetList or Shot");
                app.selectedShot = nil;
                app.selectedSetList = nil;
                
                [C1PC changeToShot:nil];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kSelectionDidChangeNotification object:[buttonObject objectID] userInfo:@{@"sender" : self}];
        }
        else if ([buttonObject isNotEqualTo:selectedObject])
        {
            if (([selectedObject isKindOfClass:[Shot class]]) &&
                ([buttonObject isKindOfClass:[SetList class]]) &&
                ([buttonObject isEqualTo:[selectedObject setlist]])
                ){
                //[superTable selectRowIndex:[superTable rowForItem:selectedObject]];
            }
            else
            {
                
                if ([buttonObject isKindOfClass:[SetList class]])
                {
                    app.selectedShot = nil;
                    app.selectedSetList = (SetList*) buttonObject;
                    
                    [C1PC changeToShot:nil];
                }
                else if ([buttonObject isKindOfClass:[Shot class]])
                {
                    app.selectedShot = buttonObject;
                    app.selectedSetList = [buttonObject setlist];
                    
                    [C1PC changeToShot:buttonObject];
                }
                else if (buttonObject == nil) {
                    DDLogVerbose(@"No item selected");
                    app.selectedShot = nil;
                    app.selectedSetList = nil;
                    [C1PC changeToShot:nil];
                }
                else
                {
                    DDLogVerbose(@"SelectedItem is not SetList or Shot");
                    app.selectedShot = nil;
                    app.selectedSetList = nil;
                    
                    [C1PC changeToShot:nil];
                }
                
                //[superTable selectRowIndex:buttonRow];
                [[NSNotificationCenter defaultCenter] postNotificationName:kSelectionDidChangeNotification object:[buttonObject objectID] userInfo:@{@"sender" : self}];
            }
        }
        else if ([buttonObject isEqualTo:selectedObject])
        {
            // nothing needs to happen
        }
        [super mouseDown:theEvent];
        
    }
    else
    {
        // Detail View
        DDLogVerbose(@"Detail View");

        id selectedObject = [superTable selectedObject];
        
        NSInteger buttonRow = [superTable rowForView:self];
        id buttonObject = [superTable itemAtRow:buttonRow];
        
        if (selectedObject == nil)
        {
            [superTable selectObject:buttonObject];
            
            if ([buttonObject isKindOfClass:[Shot class]])
            {
                app.selectedShot = buttonObject;
                [C1PC changeToShot:buttonObject];
            }
            else if (buttonObject == nil) {
                DDLogVerbose(@"No item selected");
                app.selectedShot = nil;
                [C1PC changeToShot:nil];
            }
            else
            {
                DDLogVerbose(@"SelectedItem is not SetList or Shot");
                app.selectedShot = nil;
                [C1PC changeToShot:nil];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kSelectionDidChangeNotification object:[buttonObject objectID] userInfo:@{@"sender" : self}];
        }
        else if ([buttonObject isNotEqualTo:selectedObject])
        {
            if ([buttonObject isKindOfClass:[Shot class]])
                {
                    app.selectedShot = buttonObject;
                    [C1PC changeToShot:buttonObject];
                }
                else if (buttonObject == nil) {
                    DDLogVerbose(@"No item selected");
                    app.selectedShot = nil;
                    [C1PC changeToShot:nil];
                }
                else
                {
                    DDLogVerbose(@"SelectedItem is not SetList or Shot");
                    app.selectedShot = nil;
                    [C1PC changeToShot:nil];
                }
                
            //[superTable selectRowIndex:buttonRow];
                [[NSNotificationCenter defaultCenter] postNotificationName:kSelectionDidChangeNotification object:[buttonObject objectID] userInfo:@{@"sender" : self}];
            
        }
        [super mouseDown:theEvent];
    }
    
    
    
    //    [superTable setNeedsDisplay:YES];
    
}


- (void) selectRowInSuperTable:(id)targetTable {
    
    id detailView =  targetTable;
    
    [detailView selectRowIndexes:[NSIndexSet indexSetWithIndex:[detailView rowForView:self]] byExtendingSelection:NO];
    
}


@end
