//
//  JobTableCellView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/3/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "SetListTableCellView.h"
#import "DetailViewWindowController.h"
#import "AppDelegate.h"
#import "OutlineView.h"
#import "SetList.h"
#import "QBDataExporter.h"

#import "BooleanValueTransformer.h"

@import QuartzCore;

@implementation SetListTableCellView {
    BOOL didRegisterObservations;
    AppDelegate *app;
    BooleanValueTransformer *booleanValueTransformer;
}


@synthesize setlistID;
@synthesize setlist;
@synthesize completeButton;
@synthesize infoButton;
@synthesize sampleModeButton;
@synthesize iconStackView;


static void * setListTCVcontext = @"setListTCVcontext";

-(id) initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    self.setlistID = [[NSManagedObjectID alloc] init];
    return self;
}


- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}
- (void) awakeFromNib {
    [super awakeFromNib];
    app = [AppDelegate sharedInstance];
    //    [self addObserver:self forKeyPath:@"setlist.isComplete" options:0 context:nil];
    //    [completeButton setState:[setlist isComplete]];
    //    [completeButton setEnabled:[setlist isComplete]];
}


-(IBAction)showInfo:(id)sender{
    
    
    app.selectedSetList = self.setlist;
    [app.detailView showWindow:self];
}

- (IBAction)markCompleted:(id)sender {
    
    [setlist checkAllShotsCompleted];
    
    BOOL isCurrentlyCompleted = setlist.isComplete;
    
    if ([setlist canComplete])
    {
        if (!isCurrentlyCompleted) {
            setlist.status = @"Completed";
            setlist.isComplete = YES;
            
        }
        else
        {
            [setlist checkAllShotsCompleted];
            setlist.isComplete = NO;
        }
        
        [setlist updateDateModified];
        [app.mainMOC save:nil];
    }
    else
    {
        // TODO: dialog not all shots are complete... blah blah blah
    }
    
    if (setlist.isComplete != isCurrentlyCompleted) {
        [[QBDataExporter sharedExporter] postToServer:setlist onComplete:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [app.mainMOC performBlock:^{
                    if (setlist.isComplete)
                    {
                        [app.mainMOC deleteObject:setlist];
                        [app.mainMOC save:nil];
                    }
                }];
                
            });
        }];
        
    }
    
    
}

-(void) viewDidMoveToSuperview {
    [super viewDidMoveToSuperview];
    
    [self setNeedsDisplay:YES];

    
}
- (void) assignSetList:(SetList*)obj {
    self.setlist = obj;
    [self setupCellView];
    [self registerForObservations];
}

-(void) registerForObservations {
    if (!didRegisterObservations) {

//        [self addObserver:self forKeyPath:@"setlist" options:NSKeyValueObservingOptionNew context:setListTCVcontext];
//[app addObserver:self forKeyPath:@"filteredChildrenPredicate" options:NSKeyValueObservingOptionNew context:setListTCVcontext];
        
        
        //TODO: ANIMATE COLOR FADES LATER
        
        //[self.textField addObserver:self forKeyPath:@"textColor" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:setListTCVcontext];
        
        //[self addObserver:self forKeyPath:@"statusLabelColor" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:setListTCVcontext];
        
        didRegisterObservations = YES;
    }
}

- (NSColor*) statusLabelColor {
    if (app.hideReady || app.hideCompleted || app.hideInProgress) {
        if ([app.uiStatusPredicate evaluateWithObject:self.setlist])
        {
            return [NSColor disabledControlTextColor];
            //return [NSColor redColor];
        }
        else
        {
            return [NSColor controlTextColor];
        }
    }
    else
    {
        return [NSColor controlTextColor];
    }
}

- (void) setupCellView {
    [self registerForObservations];
    
    [self.textField bind:@"stringValue" toObject:setlist withKeyPath:@"name" options:nil];
    [completeButton bind:@"enabled" toObject:setlist withKeyPath:@"canComplete" options:nil];
    [completeButton bind:@"state" toObject:setlist withKeyPath:@"isComplete" options:nil];
    
    [self.textField bind:NSTextColorBinding toObject:self withKeyPath:@"statusLabelColor" options:nil];
    
    //booleanValueTransformer = [BooleanValueTransformer valueTransformerForName:@"statusLabelTextColorValueTransformer" valueTrue:[NSColor redColor] valueFalse:[NSColor controlTextColor]];
    //NSColor *testColor = [booleanValueTransformer transformedValue:[setlist shouldHide]];
    
//    NSLog(@"\n\n\t%@ (%@) \n\t\tshouldHide? %@\n\t\t[%@]\n\t\ttestColor: %@\n\n", [setlist name], setlist.status, setlist.shouldHide? @"YES" : @"NO", [app.statusesToHideInUI componentsJoinedByString:@", "], testColor);
    
    //[self.textField bind:NSTextColorBinding toObject:self withKeyPath:@"statusLabelColor" options:nil];
    
    
    if (![setlist sampleMode])
    {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityNotVisible forView:sampleModeButton];
    }
    else
    {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityMustHold forView:sampleModeButton];
    }
}

- (void) dealloc {
    if (didRegisterObservations){
        @try {
            //[self removeObserver:self forKeyPath:@"isComplete"];
            [self removeObserver:self forKeyPath:@"setlist"];
            
        }
        @catch (NSException *exception)
        {
            DDLogVerbose(@"Exception: %@", exception);
        }
    }
    
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"textColor"] && context == setListTCVcontext){
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.textField removeObserver:self forKeyPath:@"textColor"];
//        
//            NSColor *oldColor = [change valueForKey:@"old"];
//            NSColor *newColor = [change valueForKey:@"new"];
//            
//            self.textField.textColor = oldColor;
//            
//            [NSAnimationContext
//             runAnimationGroup:
//             ^(NSAnimationContext *context) {
//                 context.duration = 10.;
//                 context.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionEaseOut];
//                 self.textField.animator.textColor = newColor;
//             }
//             completionHandler:
//             ^{
//                 //self.textField.textColor = newColor;
//                 [self.textField addObserver:self forKeyPath:@"textColor" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:setListTCVcontext];
//             }];
//        });
        
    }
    
}
@end
