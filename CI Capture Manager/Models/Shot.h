//  Shot.h
//  CI Capture Manager
//
//  Created by Josh Booth on 10/13/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QBManagedObject.h"

@class ProductDataRecord, Sample, SetList;

NS_ASSUME_NONNULL_BEGIN

static NSString * _ShotContext = @"_ShotContext";
typedef void (^completionBlock) (void);

@interface Shot : QBManagedObject



/***** Custom Getters / Setters *****/

@property (nonatomic, assign) NSInteger numberOfCaptures;
@property (nonatomic, assign) BOOL  isComplete;
@property (nonatomic, assign) BOOL  isLinked;



@property (readonly) BOOL hasAlert; // reads NSNumber back as boolean

@property (nullable, nonatomic) NSArray *associatedShots;
@property (nonatomic, readonly) BOOL canBeCompleted;


@property (nullable, nonatomic)  id parent;
@property (nonatomic, readonly) NSArray* children; // necessary for tree controller
@property (nonatomic, readonly) NSArray* filteredChildren; // necessary for tree controller


@property (nonatomic, assign) BOOL sampleDataDownloaded;


-(NSDictionary*) dataForXMP;
-(void) setupLinks;
-(void) linkAssociatedShots;
-(NSString*) listAssociatedShots;

-(NSNumber*) related_setlist;

@property (nonatomic, readonly) NSArray *orderedLinkedShots;
@property (nonatomic, readonly) NSArray *orderedParentedLinkedShots;

@property (nonatomic, readonly) NSArray *orderedSubShots;
@property (nonatomic, readonly) NSArray *orderedParentedSubShots;
@property (nonatomic, readonly) NSArray *orderedPDRs;

@property (nonatomic, readonly) NSString *durationString;
- (NSString*) durationStringFromInterval:(NSTimeInterval)timeInterval;

#pragma mark - Sample Mode Processing
- (NSArray *) lookupValuesFromTitle;
- (void) processSampleMode:(completionBlock)onComplete;
- (NSString *) generatePDRGroupString;
- (NSString *) regenerateStringUsingTemplate:(NSString*)stringTemplate;


@end

NS_ASSUME_NONNULL_END

#import "Shot+CoreDataProperties.h"
