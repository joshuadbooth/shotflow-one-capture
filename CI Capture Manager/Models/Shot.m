//
//  Shot.m
//  CI Capture Manager
//
//  Created by Josh Booth on 10/13/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "Shot.h"
#import "ProductDataRecord.h"
#import "Sample.h"
#import "SetList.h"
#import "AppDelegate.h"
#import "QBDBInfo.h"
#import "QBTable.h"
#import "QBField.h"
#import "DummyObject.h"
#import "SF1XMLRecord.h"

#import "QBDataImporter.h"
#import "QBDataExporter.h"

@implementation Shot {
    NSMutableSet *ascShots;
    AppDelegate *app;
    DummyObject *dummy;
}


@synthesize parent = _parent;
@synthesize associatedShots;

@synthesize numberOfCaptures = _numberOfCaptures;
@synthesize isComplete = _isComplete;
@synthesize isLinked = _isLinked;


@synthesize sampleDataDownloaded;
@synthesize orderedLinkedShots;
@synthesize orderedParentedLinkedShots;

@synthesize orderedPDRs;

@synthesize durationString;

- (NSString *) tableName {
    return @"Shots";
}


+ (NSString *)entityName {
    return @"Shot";
}

+(BOOL) contextShouldIgnoreUnmodeledPropertyChanges {
    return NO;
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)theKey {
    
    BOOL automatic = [super automaticallyNotifiesObserversForKey:theKey];
    if (
        ([theKey isEqualToString:@"params"]) ||
        ([theKey isEqualToString:@"changedParams"]) ||
        ([theKey isEqualToString:@"title"]) ||
        ([theKey isEqualToString:@"linkedShotRecordID"]) ||
        ([theKey isEqualToString:@"naming_convention"]) ||
        ([theKey isEqualToString:@"numberOfCaptures"]) ||
        ([theKey isEqualToString:@"isComplete"]) ||
        ([theKey isEqualToString:@"isLinked"])
        ){
        automatic = NO;
    }
    else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    return automatic;
}

#pragma mark - Descriptions
- (NSString*) description {
    return [super description];
}

- (NSString*) debugDescription {
    __block    NSMutableString *prettyParams = [NSMutableString string];
    
    for (id key in [[self.params allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyParams appendFormat:@"\t%@: %@\n", key, [self.params valueForKey:key]];
    }
    
    NSMutableString *prettyPreviousParams = [NSMutableString string];
    
    for (id key in [[self.previousParams allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyPreviousParams appendFormat:@"\t%@: %@\n", key, [self.previousParams valueForKey:key]];
    }
    
    NSMutableString *desc = [NSMutableString stringWithFormat:
@"\nRecord ID: %@\n\
Name: %@\n\
SetList: %@\n\
Status: %@\n\
SampleDataDownloaded: %@\n\
isLinked: %d\n\
\tlinkedShotRecordID: %@\n\
\tTopShot: %@\n\
\tParent: %@\n\
SubShots (Parented):\n\t%@\n\
SubShots (All):\n\t%@\n",
                             self.record_id,
                             self.name,
                             [self.setlist name],
                             self.status,
                             self.sampleDataDownloaded? @"YES" : @"NO",
                             self.isLinked,
                             self.linkedShotRecordID,
                             
                             [self topShot].description,
                             [_parent description],
                             [[[self orderedParentedSubShots] valueForKeyPath:@"description"] componentsJoinedByString:@"\n\t"],
                             [[[self orderedSubShots] valueForKeyPath:@"description"] componentsJoinedByString:@"\n\t"]
                             ];
    
    [desc appendFormat:@"PDRs: \n\t%@\n", [[[self sortedChildren:@"productdatarecords" sortedBy:@"record_id" ascending:YES] valueForKeyPath:@"name"] componentsJoinedByString:@"\n\t"]];
    [desc appendFormat:@"Samples: \n\t%@\n", [[[self sortedChildren:@"samples" sortedBy:@"record_id" ascending:YES] valueForKeyPath:@"name"] componentsJoinedByString:@"\n\t"]];
    [desc appendFormat:@"Params: \n%@\n", prettyParams];
    
    return desc;
}



// Insert code here to add functionality to your managed object subclass
#pragma mark - needed for tree
- (NSArray*) children {
    
    if (!self.isLinked)
    {
        return @[];
    }
    else
    {
        NSCompoundPredicate *filterPredicate = [NSCompoundPredicate
                                                andPredicateWithSubpredicates:@[[AppDelegate sharedInstance].filteredChildrenPredicate
                                                                                ]];
        
        if ([self isEqualTo:self.topShot] || self.topShot == nil) {
            NSMutableArray *shotsPlusAdd = [NSMutableArray arrayWithArray:[self.orderedParentedSubShots filteredArrayUsingPredicate:filterPredicate]];
            
            if (shotsPlusAdd.count == 0) return @[];
            
            if (dummy == nil) {
                dummy = [DummyObject dummyWithParent:self andType:kDummyTypeAddShot];
            }
            [shotsPlusAdd addObject: dummy];
            
            return shotsPlusAdd;
        }
        else
        {
            return @[];
        }
        
    }

    
}

- (NSArray*) filteredChildren {
    return [self.children filteredArrayUsingPredicate:[AppDelegate sharedInstance].searchFieldPredicate];
}

+ (NSSet*) keyPathsForValuesAffectingChildren {
       return [NSSet setWithObject:@"subShots"];
//    return nil;
}


+ (NSSet*) keyPathsForValuesAffectingParent {
    return [NSSet setWithArray:@[@"setlist", @"topShot", @"dummy"]];
}


+ (NSSet*) keyPathsForValuesAffectingName {
    return [NSSet setWithObject:@"title"];
}


- (NSString*) name {
    
    return [self primitiveValueForKey:@"title"];
}

- (void) setParent:(id)p {
    _parent = p;
}

- (id) parent {
    id returnValue;
    NSError *checkError;
    
    SetList *setlist = [self.setlist findInManagedObjectContext:self.managedObjectContext error:nil];
    
    Shot *topShot = [self.topShot findInManagedObjectContext:self.managedObjectContext error:nil];
    
    if (_parent != nil)
    {
        if ([_parent isKindOfClass:[SetList class]])
        {
            Shot *topShot = [self.topShot findInManagedObjectContext:self.managedObjectContext error:nil];
            
            if (topShot != nil && [topShot isNotEqualTo: self] && [topShot.setlist isEqualTo:self.setlist])
            {
                returnValue = topShot;
            }
            else if ([_parent isEqualTo:setlist])
            {
                returnValue = _parent;
            }
            else
            {
                returnValue = self.setlist;
            }
        }
        else
        {
            if ([[[_parent findInManagedObjectContext:self.managedObjectContext error:nil] setlist] isEqualTo:setlist])
            {
                returnValue = _parent;
            }
            else
            {
                returnValue = setlist;
            }
        }
    }
    else
    {
        // **** If not linked, return setlist **** //
        if (!self.isLinked)
        {
            returnValue = self.setlist;
        }
        else
        {
            // **** Get Top Shot **** //
            if (self.topShot == nil)
            {
                [self linkAssociatedShots];
            }
            
            if ([self isEqualTo:self.topShot]) // Return setlist
            {
                returnValue = self.setlist;
            }
            else // Return Top Shot
            {
                if ([self.topShot.setlist isEqualTo:self.setlist])
                {
                    returnValue = self.topShot;
                }
                else
                {
                    returnValue = self.setlist;
                }
            }
        }
    }
    
    self.parent = returnValue; // assign parent to prevent recalculation
    return returnValue;
}

- (NSArray *) orderedPDRs {
    NSArray *components = [self.orderedPDRString componentsSeparatedByString:@", "];
    NSMutableArray *returnArray = [NSMutableArray array];
    
    for (NSString* pdrComponent in components) {
        
        ProductDataRecord *pdr = [[self.productdatarecords filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"primaryKeyValue == %@", pdrComponent]] anyObject];
        if (pdr != nil)
        {
            [returnArray addObject:pdr];
        }
    }
    
    
    return returnArray;
}

#pragma mark - Custom Getters/Setters

#pragma mark Title
- (void) setTitle:(NSString *)title {
    [self willChangeValueForKey:@"title"];
    [self setPrimitiveValue:title forKey:@"title"];
    [self setParam:title forKey:@"Title"];
    [self didChangeValueForKey:@"title"];
}

#pragma mark linkedShotRecordID
-(void) setLinkedShotRecordID:(NSNumber *)linkedShotRecordID {
    [self willChangeValueForKey:@"linkedShotRecordID"];
    [self setPrimitiveValue:linkedShotRecordID forKey:@"linkedShotRecordID"];
    NSString *key = [self fieldNameForMastagTable:@"Shots"];
    [self setParam:linkedShotRecordID forKey:key];
    [self didChangeValueForKey:@"linkedShotRecordID"];
}

#pragma mark naming_convention
- (void) setNaming_convention:(NSString *)naming {
    [self willChangeValueForKey:@"naming_convention"];
    [self setPrimitiveValue:naming forKey:@"naming_convention"];
    [self setParam:naming forKey:@"Naming Convention"];
    [self didChangeValueForKey:@"naming_convention"];
}

#pragma mark numberOfCaptures
-(void) setNumberOfCaptures:(NSInteger)numberOfCaptures {
    [self willChangeValueForKey:@"numberOfCaptures"];
    //[self setPrimitiveValue:[NSNumber numberWithInteger:numberOfCaptures] forKey:@"numberOfCaptures"];
    _numberOfCaptures = numberOfCaptures;
    [self setParam:[NSString stringWithFormat:@"%ld",(long)numberOfCaptures] forKey:@"# of Captures"];
    [self didChangeValueForKey:@"numberOfCaptures"];
}

#pragma mark isComplete
-(void) setIsComplete:(BOOL)isComplete {
    [self willChangeValueForKey:@"isComplete"];
    _isComplete = isComplete;
    [self setParam:isComplete? @"1" : @"0" forKey:@"isComplete"];
    if (isComplete) self.status = @"Completed";
    [self didChangeValueForKey:@"isComplete"];
    
}
#pragma mark isLinked
-(void) setIsLinked:(BOOL)isLinked {
    [self willChangeValueForKey:@"isLinked"];
    _isLinked = isLinked;
    [self setParam:isLinked? @"1" : @"0" forKey:@"isAssociated"];
    [self didChangeValueForKey:@"isLinked"];
}

- (NSNumber*) related_setlist {
    id returnValue = self.setlist.primaryKeyValue;
    if (returnValue == nil) {
        returnValue = [self.params valueForKey:[self fieldNameForMastagTable:@"Set Lists"]];
    }
    return returnValue;
}

#pragma mark Date Complete

- (void) setDateCompleted:(NSDate *)dateCompleted {
    [self willChangeValueForKey:@"dateCompleted"];
    
    [self setPrimitiveValue:dateCompleted forKey:@"dateCompleted"];
    if (dateCompleted != nil)
    {
        [self setParam:[self convertDateForPOST:dateCompleted] forKey:@"Date Shot"];
    }
    else
    {
        [self setParam:@"" forKey:@"Date Shot"];
    }
    
    [self didChangeValueForKey:@"dateCompleted"];
    
}

#pragma mark Shot Duration

- (void) setShotDuration:(NSNumber * _Nonnull )shotDuration
{
    [self willChangeValueForKey:@"shotDuration"];
    [self setPrimitiveValue:shotDuration forKey:@"shotDuration"];
    [self setParam:[self durationString] forKey:@"Shot Duration"];
    [self didChangeValueForKey:@"shotDuration"];
    
}

#pragma mark - Misc functions

- (BOOL) hasAlert {
    if (self.isComplete) return NO;
    return self.needsAttention;
    
}

- (BOOL) canBeCompleted {
    if (!self.isLinked)
    {
        return (self.numberOfCaptures > 0);
    }
    else
    {
        if (self.numberOfCaptures == 0) return NO;
        
        NSMutableArray *availableShots = [NSMutableArray arrayWithArray:[self.orderedParentedSubShots filteredArrayUsingPredicate:[AppDelegate sharedInstance].filteredChildrenPredicate]];
        [availableShots filterUsingPredicate:[NSPredicate predicateWithFormat:@"numberOfCaptures = 0"]];
        
        return (availableShots.count == 0);
    }
}

#pragma mark - Sample Mode

- (NSArray *) lookupValuesFromTitle {
    NSMutableArray *returnArray = [NSMutableArray array];
    if ([self.title hasPrefix:@"**"])
    {
    NSString *barcodeString = [[self.title componentsSeparatedByString:@"**"] lastObject];
    NSArray *barcodeValues = [barcodeString componentsSeparatedByString:[QBDBInfo info].defaultSeparator];
    
    [barcodeValues enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *val = [obj stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (val.length > 0 && ![returnArray containsObject:val]) [returnArray addObject:val];
    }];
    
    //[returnArray sortUsingSelector:@selector(compare:)];
    }
    else
    {
        // TODO: generate an error...
    }
    
    return returnArray;
}

- (NSString *) generatePDRGroupString {
    NSString *returnString;
    
    NSArray *sortedPDRs = [[self.productdatarecords valueForKeyPath:@"primaryKeyValue"] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:(NSCaseInsensitiveSearch | NSNumericSearch | NSDiacriticInsensitiveSearch)];
    }]]];
    
    returnString = [sortedPDRs componentsJoinedByString:@", "];
    
    return returnString;
}

- (void) processSampleMode:(completionBlock)onComplete {
    AppDelegate *app = [AppDelegate sharedInstance];
    NSManagedObjectContext *mainMOC = app.mainMOC;
    
    dispatch_queue_t sampleModeQueue = dispatch_queue_create("sampleModeQueue", DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(sampleModeQueue, ^{
        NSManagedObjectContext *sampleModeContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        sampleModeContext.parentContext = mainMOC;
        
        [sampleModeContext performBlock:^{
            Shot *shot = [Shot existingWithObjectID:self.objectID inContext:sampleModeContext];
            NSArray *lookupValues = [shot lookupValuesFromTitle];
            
            [[QBDataImporter sharedImporter] importSampleRIDs: lookupValues
                                                   forShotID: self.objectID
                                                    usingMOC: sampleModeContext
                                             completionBlock:
             ^(NSMutableArray *importedSamples) {
                 NSMutableArray *pdrValues = [NSMutableArray array];
                 //NSArray *pdrValues = [importedSamples valueForKeyPath:@"@distinctUnionOfObjects.productdatarecord.primaryKeyValue"];
                 for (Sample *sample in importedSamples) {
                     NSString *pkv = sample.productdatarecord.primaryKeyValue;
                     if (![pdrValues containsObject: pkv])
                     {
                         [pdrValues addObject:pkv];
                     }
                 }
                 
                 if (importedSamples.count == 0)
                 {
                     NSMutableString *errorString = [NSMutableString stringWithString:@"None of following samples could not be located: "];
                     NSMutableArray *missingSample = [NSMutableArray array];
                     for (id sampleRID in lookupValues)
                     {
                        [missingSample addObject:sampleRID];
                        [errorString appendFormat:@"\n\t%@", sampleRID];
                     }
                     [errorString appendString:@"\n\nPlease double check your sample values and try again."];
                     self.sampleDataDownloaded = false;
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[QBDBInfo info] alertString:errorString];
                     });
                     
                     shot.title = @"Enter Sample Values";
                     
                     [sampleModeContext save:nil];
                     [mainMOC performBlock:^{
                         [mainMOC save:nil];
                         
                         if (onComplete)
                         {
                             onComplete();
                         }
                     }];
                     return;
                 }
                 else if (lookupValues.count > importedSamples.count)
                 {
                     NSArray *sampleRIDs = [importedSamples valueForKeyPath:@"record_id.stringValue"];
                     NSMutableString *errorString = [NSMutableString stringWithString:@"The following samples could not be located: "];
                     NSMutableArray *missingSample = [NSMutableArray array];
                     for (id sampleRID in lookupValues) {
                         if (![sampleRIDs containsObject:sampleRID]) {
                             [missingSample addObject:sampleRID];
                             [errorString appendFormat:@"\n\t%@", sampleRID];
                         }
                     }
                     [errorString appendString:@"\n\nThe shot has been updated with any successfully located Samples."];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[QBDBInfo info] alertString:errorString];
                     });
                 }
                 
                 
                 // *** Re-generate the Shot Title
                 NSMutableArray *titleComponents = [NSMutableArray array];
                 [titleComponents addObjectsFromArray:[pdrValues filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]]];
                 NSString *pdrString = [titleComponents componentsJoinedByString:@", "];
                 shot.orderedPDRString = pdrString;
                 
                 
                 NSString *titleTemplate = [[QBDBInfo info].configurationData valueForKey:[@"defaultSampleModeShotTitle" uppercaseString]];
                 NSString *newTitle = [shot regenerateStringUsingTemplate:titleTemplate];
                 
                 if (newTitle.length == 0) {
                     newTitle = @"ERROR";
                     shot.title = newTitle;
                 }
                 else
                 {
                     shot.title = newTitle;
                     shot.sampleDataDownloaded = YES;
                 }
                 
                 
                 NSString *namingTemplate = [[QBDBInfo info].configurationData objectForKey:[@"defaultSampleModeNamingConvention" uppercaseString]];
                 NSString *newNamingConvention = [shot regenerateStringUsingTemplate:namingTemplate];
                 
                 
                 if (newNamingConvention.length == 0) {
                     
                     shot.naming_convention = @"";
                 }
                 else
                 {
                     shot.naming_convention = newNamingConvention;
                 }
                 
                 
                 
                 // *** Link to Grouping, generating if necessary
                
                 [[QBDataImporter sharedImporter] importGroupingForPDRstring:pdrString onComplete:^(NSNumber *groupingRID) {
                     [mainMOC performBlock:^{
                         Shot *mainMOCShot = [Shot existingWithObjectID:shot.objectID inContext:mainMOC];
                         [mainMOCShot setParam:groupingRID forKey:[mainMOCShot fieldNameForMastagTable:@"Groupings"]];
                         [mainMOC save:nil];
                         [[QBDataExporter sharedExporter] postToServer:mainMOCShot];
                         if (mainMOCShot.productdatarecords.count > 0)
                         {
                             [[QBDataExporter sharedExporter] createLinkedGroupingRID:groupingRID withPDRIDs:[mainMOCShot.productdatarecords valueForKeyPath:@"objectID"] inMOC:mainMOC];
                         }
                     }];
                 }];
                 
                 
                 // *** Save and update the record
                 
                 [sampleModeContext save:nil];
                 [mainMOC performBlock:^{
                     [mainMOC save:nil];
                     
                     if (onComplete)
                     {
                         onComplete();
                     }
                 }];
             }];
        }];
    });
}

- (NSString *) regenerateStringUsingTemplate:(NSString*)stringTemplate {
    NSString *returnString = nil;
    returnString = [self substituteDynamicDates:stringTemplate];
    
    NSError *error = nil;

    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(\\[.*?\\])"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    NSArray *titleComponents = [regex matchesInString:returnString
                                              options:0
                                                range:NSMakeRange(0, [returnString length])];
    
    NSMutableDictionary *substitutionComponents = [NSMutableDictionary dictionary];
    
    for (NSTextCheckingResult *match in titleComponents)
    {
        NSRange matchRange = [match range];
        NSString *matchString = [returnString substringWithRange:matchRange];
        NSString *titleComponent = [matchString substringWithRange:NSMakeRange(1, matchRange.length-2)];
        
        NSArray *pathComponents = [titleComponent componentsSeparatedByString:@"."];
        NSString *tableName = [pathComponents firstObject];
        NSString *fieldName = [pathComponents lastObject];
        
        
        QBTable *targetTable = [QBTable tableNamed:tableName inMOC:self.managedObjectContext];
        if (targetTable == nil)
        {
            DDLogError(@"Error getting Substitution component information. Target table '%@' does not exist.", tableName);
            continue;
        }
        else
        {
            NSMutableArray *subValues = [NSMutableArray array];
            
            if ([tableName containsString:@"Product Data"])
            {
                for (ProductDataRecord *pdr in self.orderedPDRs) {
                    if ([pdr.params objectForKey:fieldName])
                    {
                        if (![subValues containsObject:[pdr.params valueForKey:fieldName]]) [subValues addObject:[pdr.params valueForKey:fieldName]];
                    }
                }
            }
            else if ([tableName containsString:@"Sample"])
            {
                for (Sample *sample in self.samples) {
                    if ([sample.params objectForKey:fieldName])
                    {
                        if (![subValues containsObject:[sample.params valueForKey:fieldName]]) [subValues addObject:[sample.params valueForKey:fieldName]];
                    }
                }
            }
            else if ([tableName containsString:@"Shot"])
            {
                if ([self.params objectForKey:fieldName])
                {
                    if (![subValues containsObject:[self.params valueForKey:fieldName]]) [subValues addObject:[self.params valueForKey:fieldName]];
                }
            }
            else
            {
                DDLogError(@"Incompatible FolderArgument Table '%@'", tableName);
            }
            
            // add combined values to subVariables Dictionary
            [substitutionComponents setObject:[subValues componentsJoinedByString:[QBDBInfo info].defaultSeparator] forKey:matchString];
        }
    }
    
    
    if (substitutionComponents.count == 0) {
        DDLogError(@"Error substituting components");
        return stringTemplate;
    }
    else
    {
        for (id substitution in substitutionComponents) {
            NSString *value = [substitutionComponents valueForKey:substitution];
            if (value.length != 0)
            {
                returnString = [returnString stringByReplacingOccurrencesOfString:substitution withString:value];
            }
        }
        
        return returnString;
    }
}

#pragma  mark - inherited functions


- (void) updateWithRecordData:(RXMLElement *)record {
    [super updateWithRecordData:record];
    
    
    self.title = [self.params valueForKey:@"Title"];
    
    NSString *substitutedTitleString = [self substituteDynamicDates:[self.params valueForKey:@"Title"]];
    
    if (![self.title isEqualToString:substitutedTitleString]) {
        self.title = substitutedTitleString;
        //[self setParam:self.title forKey:@"Title"];
    }
    
    if (![[self.params valueForKey:@"Naming Convention"] isEqualToString:@""])
    {
        NSString *substitutedNCString = [self substituteDynamicDates:[self.params valueForKey:@"Naming Convention"]];
        self.naming_convention = [self.params valueForKey:@"Naming Convention"];
        
        if (![self.naming_convention isEqualToString:substitutedNCString])
        {
            self.naming_convention = substitutedNCString;
        }
 
    }
    
    self.needsAttention = [[self.params valueForKey:@"Needs Attention"]boolValue];
    self.status = [self.params valueForKey:@"Status"];
    
    
    if ([[[self params]valueForKey:@"# of Captures"] isEqualToString:@""])
    {
        self.numberOfCaptures = 0;
    }
    else
    {
        self.numberOfCaptures = [[self.params valueForKey:@"# of Captures"]intValue];
    }
    
    
    if ([self.params objectForKey:@"Date Shot"])
    {
        self.dateCompleted = [self convertDateFromImport:[self.params valueForKey:@"Date Shot"]];
    }
    
    
    
    if ([self.status isEqualToString:@"Completed"])
    {
        self.isComplete = YES;
        [self setParam:@"1" forKey:@"isComplete"];
    }
    else
    {
        self.isComplete = NO;
        [self setParam:@"0" forKey:@"isComplete"];
    }
    
    
    //self.parent = nil;
    if ([self.params objectForKey:@"Shot Duration"])
    {
        NSString *value = [self.params valueForKey:@"Shot Duration"];
        
        double incomingDuration = [value doubleValue] / 1000;
        self.shotDuration = [NSNumber numberWithDouble:incomingDuration];
    }
    else
    {
        self.shotDuration = [NSNumber numberWithDouble:0];
    }
    
    
    if ([self.title hasPrefix:@"**"])
    {
        self.sampleDataDownloaded = NO;
    }
    else
    {
        self.sampleDataDownloaded = YES;
    }
    
    [self setupLinks];
    
}

- (NSDictionary*) listPropertiesToPOST {
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    
    if (self.online) {
        [postParams setValue:self.record_id forKey:@"rid"];
    }
    
    
    [postParams addEntriesFromDictionary:[self listChangedParamsForPOST]];
    
    return postParams;
}

- (NSDictionary*) listChangedParamsForPOST {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:[super listChangedParamsForPOST]];
    
    if (![self.status isEqualToString:@"Completed"])
    {
        [dictionary setValue:@"" forKey:[NSString stringWithFormat:@"_fid_%@", [self fieldIDForField:@"Date Shot"]]];
    }
    
    // *** update shot duration to format on Cloud *** //
    [dictionary setValue:[NSString stringWithFormat:@":%@", [self durationString]] forKey:[NSString stringWithFormat:@"_fid_%@", [self fieldIDForField:@"Shot Duration"]]];
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}


#pragma mark - Shot Duration
+ (NSSet*) keyPathsForValuesAffectingDurationString {
    return [NSSet setWithObject:@"shotDuration"];
}


- (NSString*) durationString {
    return [self durationStringFromInterval:[self.shotDuration doubleValue]];
}

- (NSString*) durationStringFromInterval:(NSTimeInterval)timeInterval {
    NSInteger ti = (NSInteger) timeInterval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    
    NSString *returnString = [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
    if ([returnString isEqualToString:@"00:00"]) returnString = @"";
    return returnString;
}

#pragma mark - Sub Shots

- (NSArray*) orderedSubShots {
    return [self.subShots sortedArrayUsingDescriptors:@[[AppDelegate sharedInstance].subShotSorting, [AppDelegate sharedInstance].sortByRecordID]];
}

- (NSArray*) orderedParentedSubShots {
    return [self.orderedSubShots filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parent == %@", self]];
}

- (void) setupLinks {
    // **** Set Lists **** //
    NSString *relatedSetListKey = [self fieldNameForMastagTable:@"Set Lists"];
    
    if ([self.params objectForKey:relatedSetListKey])
    {
        if (![[self.params valueForKey:relatedSetListKey] isEqualToString:@"0"]) {
        
            @try
            {
                
                SetList *setlist = [SetList existingWithPrimaryKeyValue:[self convertedParamValueForFieldNamed:relatedSetListKey] inContext:self.managedObjectContext];
                self.setlist = setlist;
            }
            @catch (NSException *exception)
            {
                DDLogVerbose(@"Exception assigning SetList to Shot: %@", exception);
            }
        }
        else
        {
            DDLogError(@"%@ has no related set list!", self.description);
        }
    }
    
    // **** Associated Shots **** //
    NSString *associatedShotKey = [self fieldNameForMastagTable:@"Shots"];
    
    if (([self.params objectForKey:associatedShotKey]) &&
        ([[self.params valueForKey:associatedShotKey] length] > 0)) {
        id test = [self convertedParamValueForFieldNamed:associatedShotKey];
        self.linkedShotRecordID = [self convertedParamValueForFieldNamed:associatedShotKey];
    }
    else
    {
        self.linkedShotRecordID = [NSNumber numberWithInt:0];
    }
    
    
    //[self linkAssociatedShots];
}


#pragma mark - Associated Shots -

-(NSArray*) associatedShots {
    if (!self.isLinked) return nil;
    NSMutableArray *returnArray = [NSMutableArray array];
    
    if (self.topShot != nil) {
        // is a subShot
        [returnArray addObject:self.topShot];
        [returnArray addObjectsFromArray:[self.topShot orderedParentedLinkedShots]];
    }
    else
    {
        [returnArray addObject:self];
        [returnArray addObjectsFromArray:[self orderedParentedLinkedShots]];
    }
    
    return returnArray;
}

- (NSSet*) subShots {
    NSMutableSet *returnSet = [self primitiveValueForKey:@"subShots"];
    
    if ([returnSet containsObject:self])
    {
        [returnSet removeObject:self];
    }
    
    return [NSSet setWithSet:returnSet];
}

- (void) linkAssociatedShots {
    
    
    
    if ((![self.linkedShotRecordID isEqualToNumber:[NSNumber numberWithInteger:0]]) ||
        (self.topShot != nil) ||
        (self.subShots.count > 0) ||
        ([[self.params valueForKey:@"isAssociated"] boolValue])
        )
    {
        self.isLinked = YES;
    }
    else
    {
        self.isLinked = NO;
        self.linkedShotRecordID = [NSNumber numberWithInt:0];
        if (self.topShot != nil) {
            Shot *top = self.topShot;
            self.topShot = nil;
            if (top.subShots.count == 0) top.isLinked = NO;
        }
        self.parent = self.setlist;
        return;
    }

    
    if (self.topShot != nil && ![self.topShot isEqualTo:self]) self.linkedShotRecordID = self.topShot.record_id;
    
    Shot *topShot = nil;
    
    if (self.topShot == nil)
    {
        topShot = [self associateUp:self.record_id];
        self.topShot = topShot;
        self.parent = [self isEqualTo:topShot]? self.setlist : topShot;
    }
    else
    {
        topShot = self.topShot;
    }
    
    if (!topShot.isLinked) topShot.isLinked = YES;
    
    ascShots = [NSMutableSet set];

    [self associateDown: topShot.record_id];
    
        [ascShots minusSet:topShot.subShots];
    
    if (ascShots.count > 0) {
 
        [topShot addSubShots:ascShots];
        
        // if (self.topShot != nil) [ascShots addObject:self.topShot];
        
        for (Shot *subShot in ascShots) {
            subShot.parent = topShot;
            subShot.isLinked = YES;
            subShot.linkedShotRecordID = topShot.record_id;
            
        }
    }
    else if ((ascShots.count == 1) && ([[ascShots anyObject] isEqualTo:self])) {
//        NSLog(@"self-assigning linked shot!");
    }
    
//    NSLog(@"\n\n");
}


- (Shot*) associateUp:(NSNumber*)recordID {
    
    
    NSError *findError = nil;
    Shot *returnShot = nil;
    
    Shot *childShot = nil;
    Shot *parentShot = nil;
    
    childShot = [Shot existingWithRecordID:recordID inContext:self.managedObjectContext];
    
    
    if (findError != nil)
    {
        DDLogError(@"AssociateUp: Error Finding Child Shot: %@", findError);
        [[QBDBInfo info] alertMe:findError];
        returnShot = self;
        return returnShot;
    }
    
    
    if (![childShot.linkedShotRecordID isEqualToNumber:[NSNumber numberWithInt:0]])
    {
        parentShot = [Shot existingWithRecordID:childShot.linkedShotRecordID inContext:self.managedObjectContext];
    }
    
    if (findError != nil)
    {
        DDLogError(@"AssociateUp: Error Finding Parent Shot: %@", findError);
        [[QBDBInfo info] alertMe:findError];
        returnShot = childShot;
        return returnShot;
    }
    
    if (parentShot != nil)
    {
        returnShot = parentShot;
        // if there is another linkedShotRecordID
        if (![parentShot.linkedShotRecordID isEqualToNumber:[NSNumber numberWithInt:0]] && [childShot isNotEqualTo:parentShot])
        {
            returnShot = [self associateUp:parentShot.record_id];
        }
        else
        {
            returnShot = parentShot;
        }
    }
    else {
        returnShot = childShot;
    }
    
    return returnShot;
    
}

- (void) associateDown:(NSNumber *) recordID {
    
    NSPredicate *linkedShotPredicate = [NSPredicate predicateWithFormat:@"linkedShotRecordID == %@", recordID];
    NSSet *filteredShots = [self.setlist.shots filteredSetUsingPredicate:linkedShotPredicate];
    
    if (filteredShots.count > 0) {
        
            [ascShots addObjectsFromArray:[filteredShots sortedArrayUsingDescriptors:@[[AppDelegate sharedInstance].subShotSorting]]];
            for (Shot* shot in filteredShots) {
                [self associateDown:shot.record_id];
            }
        
    }
    else
    {
        return;
    }
}



-(NSString*) listAssociatedShots {
    NSString *returnString = [[self.topShot subShots] valueForKeyPath:@"name"];
    DDLogVerbose(@"%@", returnString);
    return returnString;
}

-(NSString*) associatedGroupString {
    
    if (self.subShots.count > 0) {
        NSMutableArray *recordIDs = [NSMutableArray arrayWithObject:self.record_id];
        for (Shot* subShot in self.subShots) {
            if (![recordIDs containsObject:subShot.record_id]){
                [recordIDs addObject:subShot.record_id];
            }
        }
        
        NSArray *sortedArray = [[recordIDs valueForKeyPath:@"@distinctUnionOfObjects.self"] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES]]];
        
        NSString *returnString = [sortedArray componentsJoinedByString:@"."];
        return returnString;
    }
    else if (self.topShot != nil){
        return [self.topShot associatedGroupString];
    }
    else {
        return @"No Associated Shots";
    }
}

#pragma mark - Data for XMP

-(NSDictionary*) dataForXMP {
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    
    
    /**********************************
     XMP from Shots API_XMP_View
     **********************************/
    
    QBTable *shotsTable = [QBTable tableNamed:@"Shots" inMOC:self.managedObjectContext];
    NSArray *shotsFields = [[shotsTable fields_xmp] valueForKeyPath:@"name"];
    
    for (id key in shotsFields) {
        
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [key stringByReplacingOccurrencesOfString:@" " withString:@""];
        //safeKey = [safeKey stringByReplacingOccurrencesOfString:@"-" withString:@""];
        safeKey = [safeKey stringByReplacingOccurrencesOfString:@"#" withString:@"Number"];
        safeKey = [safeKey stringByReplacingOccurrencesOfString:@"/" withString:@""];
        safeKey = [safeKey stringByReplacingOccurrencesOfString:@"'" withString:@""];
        
        
        
        if ([key containsString:@"Date"]){
            [data setValue:[self xmpDate:[[self params] valueForKey:key]] forKey:safeKey];
        } else {
            NSString *keyValue = [[[self params] valueForKey:key] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *safeKeyValue = [self xmlSafeValue:keyValue];
            if (safeKeyValue.length > 0 && ![safeKeyValue isEqualToString:keyValue])
            {
                DDLogVerbose(@"Encode fix for XML: '%@' -> '%@'", keyValue, safeKeyValue);
                keyValue = safeKeyValue;
            }
            
            [data setValue:keyValue forKey:safeKey];
        }
    }
    
    
    
    /**********************************
     XMP from Product Data API_XMP_View
     **********************************/
    
    if (self.productdatarecords.count == 0) {
        // no product data records
       DDLogVerbose(@"No associated Product Data Records");
        
    }
    else
    {
        QBTable *pdrTable = [QBTable tableNamed:@"Product Data" inMOC:self.managedObjectContext];
        NSArray *pdrFields = [[pdrTable fields_xmp] valueForKeyPath:@"name"];
        
        //NSLog(@"pdrFields: %@", pdrFields);
        
        for (id key in pdrFields) {
            // DDLogVerbose(@"Key: %@", key);
            
            if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"])){
                continue;
            }
            
            NSArray *orderedRecords = [self sortedChildren:@"productdatarecords" ascending:YES];
            NSMutableArray *allValues = [NSMutableArray array];
            
            for (ProductDataRecord *pdr in orderedRecords) {
                if ([key containsString:@"Date"]) {
                    id val = [[pdr params]valueForKey:key];
                    if ([val length])[allValues addObject:[pdr xmpDate:[[pdr params]valueForKey:key]]];
                }
                else
                {
                    @try {
                        if ([[[pdr params]valueForKey:key] isKindOfClass:[NSString class]]) {
                            NSString* val = [[[pdr params]valueForKey:key] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            
                            NSString *safeKeyValue = [self xmlSafeValue:val];
                            if (safeKeyValue.length > 0 && ![safeKeyValue isEqualToString:val])
                            {
                                DDLogVerbose(@"Encode fix for XML: '%@' -> '%@'", val, safeKeyValue);
                                val = safeKeyValue;
                            }
                            
                            if (val.length > 0) [allValues addObject:val];
                        }
                        else {
                            NSString *val = [[[pdr params] valueForKey:key] stringValue];
                            NSString *safeKeyValue = [self xmlSafeValue:val];
                            if (safeKeyValue.length > 0 && ![safeKeyValue isEqualToString:val])
                            {
                                DDLogVerbose(@"Encode fix for XML: '%@' -> '%@'", val, safeKeyValue);
                                val = safeKeyValue;
                            }
                            
                            [allValues addObject:val];
                        }
                        
                    }
                    @catch (NSException *exception) {
                        DDLogVerbose(@"\n\n\tException:%@\n\tPDR RID: %@\n\tKey: %@\n", exception, pdr.record_id, key);
                    }
                }
            }
            
            NSString *value;
            if (allValues.count > 1) {
                NSMutableArray *existingValues = [NSMutableArray array];
                for (id val in allValues) {
                    if ([existingValues containsObject:val]) continue;
                    [existingValues addObject:val];
                }
                value = [existingValues componentsJoinedByString:@"; "];
            }
            else {
                value = [allValues firstObject];
            }
            
            NSString *safeKey = [key stringByReplacingOccurrencesOfString:@" " withString:@""];
//            safeKey = [safeKey stringByReplacingOccurrencesOfString:@"-" withString:@""];
            safeKey = [safeKey stringByReplacingOccurrencesOfString:@"#" withString:@"Number"];
            safeKey = [safeKey stringByReplacingOccurrencesOfString:@"/" withString:@""];
            safeKey = [safeKey stringByReplacingOccurrencesOfString:@"'" withString:@""];
            
            
            //            DDLogVerbose(@"adding: %@ - %@", safeKey, value);
            [data setValue:value forKey:safeKey];
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:data];
}

- (NSString*) xmlSafeValue:(NSString*)testValue
{
    if (testValue && testValue.length > 0)
    {
        __block NSString *returnString = [NSString stringWithString:testValue];
        NSDictionary *unsafeCharacters = @{
                                           @"&" : @"&amp;",
                                           @"\"" : @"&quot;",
                                           @"'" : @"&#39;",
                                           @">" : @"&#62;",
                                           @"<" : @"&#60;",
                                           @"©" : @"&#169;",
                                           @"™" : @"&#8482;",
                                           @"®" : @"&#174;"
                                           };
        
        
        [unsafeCharacters enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull val, BOOL * _Nonnull stop) {
            if (returnString && returnString.length > 0 && [returnString containsString:key]){
                returnString = [returnString stringByReplacingOccurrencesOfString:key withString:val];
                DDLogVerbose(@"xmlSafeValue (%@): %@", key, returnString);
            }
        }];
        
        return returnString;
    }
    else {
        return @"";
    }
}


- (NSString*) substituteDynamicDates:(NSString*) testString {
    
    if ([testString containsString:@"[Date("])
    {
        NSError *error = nil;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(?<=\\[Date\\()(.+)(?=\\)\\])"
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:&error];
        NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:testString
                                                             options:0
                                                               range:NSMakeRange(0, [testString length])];
        
        NSString *dateFormatString = [[testString substringWithRange:rangeOfFirstMatch] uppercaseString];
        dateFormatString = [dateFormatString stringByReplacingOccurrencesOfString:@"D" withString:@"d"];
        DDLogVerbose(@"Date substring: %@", dateFormatString);
        
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        NSDate *today = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = dateFormatString;
        NSString *replacementDate = [dateFormatter stringFromDate:today];
        DDLogVerbose(@"Date: %@", replacementDate);
        
        NSRegularExpression *fullReplacement = [NSRegularExpression regularExpressionWithPattern:@"(\\[Date\\(.*\\)\\])"
                                                                                         options:NSRegularExpressionCaseInsensitive
                                                                                           error:&error];
        
        NSString *newString = [fullReplacement stringByReplacingMatchesInString:testString options:0 range:NSMakeRange(0, [testString length]) withTemplate:replacementDate];
        DDLogVerbose(@"New DateString: %@", newString);
        return newString;
        
    }
    else
    {
        return  testString;
    }
}


@end
