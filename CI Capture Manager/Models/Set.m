//
//  Set.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/15/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "Set.h"
#import "AppDelegate.h"
#import "SetList.h"
#import "Station+CoreDataClass.h"
#import "QBDBInfo.h"

static NSString * _SetContext = @"_SetContext";

@implementation Set

@dynamic set_name;
@dynamic setlists;
@dynamic station;
@dynamic activeUser;
@dynamic isAvailable;

- (NSString *) tableName {
    return @"Sets";
}

+ (NSString *)entityName {
    return @"Set";
}

- (NSString*) debugDescription {
    /***** Prettify Params *****/
    NSMutableString *prettyParams = [NSMutableString string];
    for (id key in [[self.params allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyParams appendFormat:@"\t%@: %@\n", key, [self.params valueForKey:key]];
    }
    
    NSMutableString *prettyPreviousParams = [NSMutableString string];
    for (id key in [[self.previousParams allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyPreviousParams appendFormat:@"\t%@: %@\n", key, [self.previousParams valueForKey:key]];
    }
    
    NSMutableString *desc = [NSMutableString stringWithFormat:
@"\nRecord ID: %@\n\
Name: %@\n\
Status: %@\n\
Active User: %@\n\
SetLists:\n\t%@\n",
                             self.record_id,
                             self.name,
                             self.status,
                             self.activeUser,
                             [[[self orderedSetLists] valueForKeyPath:@"description"] componentsJoinedByString:@"\n\t"]];
    
    
    [desc appendFormat:@"Params: \n%@\n", prettyParams];
    
    return desc;
}

//- (NSString*) description {
//    return [NSString stringWithFormat:@"%@: %@ user: '%@' | '%@'", self.record_id, self.name, self.activeUser, [self.params valueForKey:@"Active User"]];
//}

+ (NSSet*) keyPathsForValuesAffectingName {
    return [NSSet setWithObject:@"set_name"];
}

- (NSString*) name {
    return self.set_name;
    
}


- (NSArray*) orderedSetLists {
    NSArray *returnArray = [self sortedChildren:@"setlists" sortedBy:@"priorityNum, record_id" ascending:YES];
    return returnArray;
}

+ (NSSet*) keyPathsForValuesAffectingIsAvailable {
    return [NSSet setWithObject:@"activeUser"];
}

- (BOOL) isAvailable {
    NSString *userInfo = self.activeUser;
    
    if ([userInfo isEqualToString:[[QBDBInfo info] userID]] ||
        userInfo == nil ||
        [userInfo isEqualToString:@""] ||
        [userInfo isEqualToString:@"<null>"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

- (void) updateWithRecordData:(RXMLElement *)record {

    [super updateWithRecordData:record];
    
    self.set_name = [self.params objectForKey:@"Name"]? [self.params valueForKey:@"Name"] : [self.params valueForKey:@"Set Name"];
    
    
    self.activeUser = [self.params valueForKey:@"Active User"];
   }



- (NSDictionary*) listPropertiesToPOST {
    NSDictionary *postParams = @{
                                 @"rid" : self.record_id,
                                 [NSString stringWithFormat:@"_fid_%@", [self fieldIDForField:@"Active User"]] : self.activeUser
                             };
    
    return postParams;
}

@end
