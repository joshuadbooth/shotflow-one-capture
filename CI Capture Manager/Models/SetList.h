//
//  SetList.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QBManagedObject.h"

@class DummyObject;

@class Set, Shot;

static NSString * _SetListContext = @"_SetListContext";

@interface SetList : QBManagedObject

@property (nonatomic) BOOL canComplete;
@property (nonatomic) BOOL isComplete;
@property (nonatomic) BOOL  sampleMode;
@property (nonatomic, retain) NSString * set_list_name;

@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSNumber * priorityNum;

@property (nonatomic, retain) NSNumber * related_set;
@property (nonatomic, retain) NSString * set_name;

@property (nonatomic, retain) NSString * type;

@property (nonatomic, retain) NSString * job_id;

@property (nonatomic, retain) NSSet *shots;
@property (nonatomic, retain) Set *set;

@property (nonatomic, readonly) id parent;
@property (nonatomic, readonly) NSArray* children; // necessary for tree controller
@property (nonatomic, readonly) NSArray* filteredChildren; // necessary for tree controller

@property (nonatomic, readonly) NSNumber * totalNumberOfCaptures;
@property (nonatomic, readonly) NSNumber * averageNumberOfCaptures;


@property (nonatomic, readonly) NSArray *shotsRecordIDs;

@property (nonatomic, readonly) NSArray *orderedShots;
@property (nonatomic, readonly) NSArray *orderedParentedShots;

- (BOOL) checkAllShotsCompleted;
- (NSNumber *) totalNumberOfCaptures;
- (BOOL) shouldHide;
@end

@interface SetList (CoreDataGeneratedAccessors)

- (void)addShotsObject:(Shot *)value;
- (void)removeShotsObject:(Shot *)value;
- (void)addShots:(NSSet *)values;
- (void)removeShots:(NSSet *)values;

@end
