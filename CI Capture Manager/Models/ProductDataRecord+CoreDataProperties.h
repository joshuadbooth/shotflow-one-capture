//
//  ProductDataRecord+CoreDataProperties.h
//  CI Capture Manager
//
//  Created by Josh Booth on 10/27/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProductDataRecord.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductDataRecord (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *relatedPDR;
@property (nullable, nonatomic, retain) NSNumber *relatedShot;
@property (nullable, nonatomic, retain) NSSet<Sample *> *samples;
@property (nullable, nonatomic, retain) NSSet<Shot *> *shots;

@end

@interface ProductDataRecord (CoreDataGeneratedAccessors)

- (void)addSamplesObject:(Sample *)value;
- (void)removeSamplesObject:(Sample *)value;
- (void)addSamples:(NSSet<Sample *> *)values;
- (void)removeSamples:(NSSet<Sample *> *)values;

- (void)addShotsObject:(Shot *)value;
- (void)removeShotsObject:(Shot *)value;
- (void)addShots:(NSSet<Shot *> *)values;
- (void)removeShots:(NSSet<Shot *> *)values;

@end

NS_ASSUME_NONNULL_END
