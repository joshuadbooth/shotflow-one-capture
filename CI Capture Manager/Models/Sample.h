//
//  Sample.h
//  CI Capture Manager
//
//  Created by Josh Booth on 10/27/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QBManagedObject.h"

@class ProductDataRecord, Shot;

NS_ASSUME_NONNULL_BEGIN

@interface Sample : QBManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Sample+CoreDataProperties.h"
