//
//  QBManagedObject+CoreDataProperties.h
//  CI Capture Manager
//
//  Created by Josh Booth on 3/29/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QBManagedObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface QBManagedObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date_created;
@property (nullable, nonatomic, retain) NSDate *date_modified;
@property (nullable, nonatomic, readonly, retain) id fieldIDs;
@property (nullable, nonatomic, retain) NSString *name;
@property (nonatomic) BOOL online;

@property (nullable, nonatomic, retain) id params;
@property (nullable, nonatomic, retain) id previousParams;
@property (nullable, nonatomic, retain) NSNumber *record_id;
@property (nullable, nonatomic, retain) NSString *status;
@property (nullable, nonatomic, readonly) NSString *tableName;
@property (nullable, nonatomic, retain) NSString *update_id;
@property (nullable, nonatomic, retain) id primaryKeyValue;
@property (nullable, nonatomic, retain) QBField *primaryKey;
@property (nullable, nonatomic, retain) QBTable *parentTable;

@end

NS_ASSUME_NONNULL_END
