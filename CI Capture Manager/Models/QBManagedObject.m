//
//  QBManagedObject.m
//  CI Capture Manager
//
//  Created by Josh Booth on 3/29/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "QBManagedObject.h"
#import "QBField.h"
#import "QBTable.h"
#import "QBDBInfo.h"

@implementation QBManagedObject


#pragma mark - Custom Setters
- (void) setRecord_id:(NSNumber *)record_id
{
    [self willChangeValueForKey:@"record_id"];
    [self setPrimitiveValue:record_id forKey:@"record_id"];
    [self setParam:record_id.stringValue forKey:@"Record ID#"];
    [self didChangeValueForKey:@"record_id"];
}

-(void) setStatus:(NSString *)status
{
    [self willChangeValueForKey:@"status"];
    [self setPrimitiveValue:status forKey:@"status"];
    [self setParam:status forKey:@"Status"];
    [self didChangeValueForKey:@"status"];
}

#pragma mark - Easy Creation -
+ (NSString *) tableName
{
    return @"QBMO";
}

+ (NSString *)entityName
{
    return @"QBMO";
}

#pragma mark - Insertion & Retrieval Functions
+ (instancetype) insertNewObjectIntoContext:(NSManagedObjectContext *)moc
{
    id obj = [NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                           inManagedObjectContext:moc];
    NSError *permIDError = nil;
    [moc obtainPermanentIDsForObjects:@[obj] error:&permIDError];
    [obj assignParentTableAndPrimaryKey];
    return obj;
}

+ (instancetype) insertWithXMLData:(SF1XMLRecord*)xmlData intoContext:(NSManagedObjectContext *)moc
{
    id obj = [NSEntityDescription insertNewObjectForEntityForName:[self entityName]
                                           inManagedObjectContext:moc];
    
    NSError *permIDError = nil;
    [moc obtainPermanentIDsForObjects:@[obj] error:&permIDError];

    [obj assignParentTableAndPrimaryKey];
    [obj setOnline: YES];
    [obj populateParams:xmlData];
    [obj setPreviousParams: [obj params]];
    
    return obj;
}

+ (instancetype) existingWithObjectID:(NSManagedObjectID *)objID inContext:(NSManagedObjectContext *)moc
{
    NSError *findError = nil;
    id returnObj = [moc existingObjectWithID:objID error:&findError];
    if (findError) DDLogError(@"existingWithObjectID error() %@", findError);
    return returnObj;
}

+ (instancetype) existingWithRecordID:(NSNumber *)recordID inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"record_id == %@", recordID];
    fetchRequest.predicate = predicate;
    fetchRequest.fetchLimit = 1;
    
    NSError *fetchError = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&fetchError];
    //NSArray *results = [[moc executeFetchRequest:fetchRequest error:&fetchError] filteredArrayUsingPredicate:predicate];
        
    if (fetchError == nil)
    {
        if ([results count] == 0) return nil;
        id fetchedObject = [results firstObject];
        return fetchedObject;
    }
    else {
        DDLogError(@"existingWithObjectID error() %@", fetchError);
        return nil;
    }
}

+ (instancetype) existingWithPrimaryKeyValue:(id)pkv inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"primaryKeyValue == %@", pkv];
    //fetchRequest.predicate = predicate;
    //fetchRequest.fetchLimit = 1;
    
    NSError *fetchError = nil;
    NSArray *results = [[moc executeFetchRequest:fetchRequest error:&fetchError] filteredArrayUsingPredicate:predicate];
    //NSArray *results = [moc executeFetchRequest:fetchRequest error:&fetchError];
    
    if (fetchError == nil)
    {
        if ([results count] == 0) return nil;
        id fetchedObject = [results firstObject];
        return fetchedObject;
    }
    else {
        DDLogError(@"existingWithObjectID error() %@", fetchError);
        return nil;
    }
    
}

- (instancetype) findInManagedObjectContext:(NSManagedObjectContext *)context error:(NSError**)error
{
    NSError *findError = nil;
    id obj = [context existingObjectWithID:self.objectID error:&findError];
    if (findError != nil) {
        DDLogVerbose(@"Error! %@", findError);
        if (error) *error = findError;
        return nil;
    }
    else {
        return obj;
    }
}

#pragma mark - Key Data Functions

- (void) populateParams:(SF1XMLRecord *)record
{
    
    if (self.parentTable == nil) [self assignParentTableAndPrimaryKey];
    
    self.previousParams = [NSDictionary dictionaryWithDictionary:self.params];
    
    if (self.params == nil)
    {
        [self setParams:[NSMutableDictionary dictionary]];
    }
    
    [record iterate:@"f" usingBlock:^(RXMLElement *param) {
        NSString* paramKey = [self.fieldIDs valueForKey:[param attribute:@"id"]];
        RXMLElement* paramValue = param;
        [self setParam:paramValue.text forKey:paramKey];
    }];
    
    [self updateWithRecordData:record];
}

- (void) updateWithRecordData:(SF1XMLRecord *)record
{
    self.record_id = [self recordAsNSNumber:[[self.params valueForKey:@"Record ID#"] intValue]];
    self.date_created = [self convertDateFromImport:[self.params valueForKey:@"Date Created"]];
    self.date_modified = [self convertDateFromImport:[self.params valueForKey:@"Date Modified"]];
    
}

- (void) setParam:(id)param forKey:(NSString *)key
{
    
    NSMutableDictionary *tempParams = [NSMutableDictionary dictionaryWithDictionary:self.params];
    
    [tempParams setValue:[param isKindOfClass:[NSString class]]? param : [param stringValue]
                  forKey:key];
    
    [self willChangeValueForKey:@"params"];
    self.params = [NSMutableDictionary dictionaryWithDictionary:tempParams];
    [self didChangeValueForKey:@"params"];
    tempParams = nil;
    
}

- (void) updateDateModified
{
    [self setDate_modified:[NSDate date]];
}

- (void) setDate_modified:(NSDate *)date_modified
{
    if (date_modified == nil)
    {
        date_modified = [NSDate date];
    }
    
    [self willChangeValueForKey:@"date_modified"];
    [self setPrimitiveValue:date_modified forKey:@"date_modified"];
    [self setParam:[self convertDateForPOST:date_modified] forKey:@"Date Modified"];
    [self didChangeValueForKey:@"date_modified"];
}

#pragma mark - Helper Functions

- (NSString *) description
{
    return [NSString stringWithFormat:@"(%@) %@: %@", [self class], self.record_id, self.name];
}

- (NSString *) debugDescription
{
    NSMutableString *returnString = [NSMutableString stringWithFormat:@"Class: %@\nRecord ID: %@ \nName: %@\n\n", [self class], self.record_id, self.name];
    [returnString appendString:@"Params:\n"];
    [returnString appendString:[self prettyParams]];
    
    return returnString;
}

- (NSString *) prettyParams {

    NSMutableString *prettyParams = [NSMutableString string];
    for (id key in [[self.params allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyParams appendFormat:@"\t%@: %@\n", key, [self.params valueForKey:key]];
    }
    return prettyParams;
}

- (NSString *) prettyPreviousParams {
    /***** Prettify Params *****/
    NSMutableString *prettyParams = [NSMutableString string];
    for (id key in [[self.previousParams allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyParams appendFormat:@"\t%@: %@\n", key, [self.params valueForKey:key]];
    }
    return prettyParams;
}


- (NSNumber *) recordAsNSNumber: (NSInteger)num
{
    return [NSNumber numberWithInteger:num];
}
#pragma mark Dates
- (NSDate *) convertDateFromImport:(NSString*)dateString
{
    
    NSDate *returnDate;
    
    if ([dateString isEqualToString:@""]) {
        returnDate = nil;
    }
    else
    {
        double seconds = dateString.doubleValue / 1000;
        NSDate *dateUTC = [NSDate dateWithTimeIntervalSince1970:seconds];
        
        returnDate = dateUTC;
    }
    
    return returnDate;
}

- (NSString *) convertDateForPOST:(NSDate *)date
{
    
    //    DDLogVerbose(@"Date To Convert: %@", date);
    NSString *dateString = nil;
    long newInt = [date timeIntervalSince1970] * 1000;
    dateString = [NSString stringWithFormat:@"%ld", newInt];
    //    DDLogVerbose(@"Converted String: %@", dateString);
    
    return dateString;
}

- (NSString *) prettyDate:(NSDate*) date
{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    [format setTimeZone:[NSTimeZone systemTimeZone]];
    [format setDateStyle: NSDateFormatterMediumStyle];
    [format setTimeStyle: NSDateFormatterMediumStyle];
    [format setCalendar:calendar];
    
    return [format stringFromDate:date];
}

- (NSString *) xmpDate:(NSString*)dateString
{
    if (dateString.length > 0) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        double seconds = dateString.doubleValue;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:(seconds/1000)];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        
        [format setTimeZone:[NSTimeZone systemTimeZone]];
        [format setDateStyle: NSDateFormatterShortStyle];
        [format setTimeStyle: NSDateFormatterNoStyle];
        [format setCalendar:calendar];
        [format setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MM/dd/YYYY" options:0 locale:nil]];
        // DDLogVerbose(@"XMPDate: %@", [format stringFromDate:date]);
        return [format stringFromDate:date];
    }
    else
    {
        return @"";
    }
}

- (NSString*) localDateStringForDate:(NSDate* _Nonnull)date dateFormat:(NSString * _Nullable )dateFormat field:(QBField* _Nullable)field
{
    NSString *returnString;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    [df setCalendar:[NSCalendar currentCalendar]];
    
    
    if (dateFormat != nil)
    {
        [df setDateFormat:dateFormat];
    }
    else
    {
        if (field != nil && [[field field_type] isEqualToString:@"timestamp"])
        {
            [df setDateFormat:@"MM-dd-yyyy hh:mm:ss a zz"];
        }
        else
        {
            [df setDateFormat:@"MM-dd-yyyy"];
        }
    }
    
    returnString = [df stringFromDate:date];
    if (returnString == nil || returnString.length == 0) returnString = @"";
    return returnString;
}

- (NSDictionary *) dictionaryValues
{
    NSArray *keys = [[[self entity] attributesByName] allKeys];
    return [self dictionaryWithValuesForKeys:keys];
}

#pragma mark - Parameter Comparison Functions

- (NSArray *) updatedParams
{
    __block NSMutableDictionary *differences = [NSMutableDictionary dictionary];
    
    self.previousParams = nil;

    if ([self.params isEqualToDictionary:self.previousParams]) {
        
        return @[];
    }
    else if (self.previousParams == nil){
        
        [self.params enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
            [differences setValue:[self.params valueForKey:key] forKey:key];
        }];
    }
    else {
        
        [self.params enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
            
            // Value for Current Param AND No value for Previous Params
            if (([self.params objectForKey:key]) && (![self.previousParams objectForKey:key]))
            {
                [differences setValue:@{@"Previous" : [NSNull null],
                                        @"Current" : [self.params valueForKey:key]
                                        }
                               forKey:key];
            }
            // No value for Current Params but exists for Previous Params
            else if (([self.previousParams objectForKey:key]) && (![self.params objectForKey:key]))
            {
                [differences setValue:@{@"Previous" : [self.previousParams valueForKey:key],
                                        @"Current" : [NSNull null]
                                        }
                               forKey:key];
            }
            // If the values are equal, do nothing.
            else if ([[self.params valueForKey:key] isEqualTo:[self.previousParams valueForKey:key]])
            {
            }
            // The values aren't equal, so mark the change.
            else
            {
                [differences setValue:@{@"Previous": [self.previousParams valueForKey:key],
                                        @"Current": [self.params valueForKey:key]}
                               forKey:key];
            }
            
        }];
    }
    
    // Clean up
    NSMutableArray *changedParams = [NSMutableArray arrayWithArray:[differences allKeys]];
    
    QBTable *table = [QBTable tableNamed:self.tableName inMOC:self.managedObjectContext];
    
    NSArray *illegalFields = [table.fields_illegal valueForKeyPath:@"@distinctUnionOfObjects.name"];
    
    NSArray *checkParams = [NSArray arrayWithArray:changedParams];
    
    for (NSString* param in checkParams) {
        if ([illegalFields containsObject:param]) {
            [changedParams removeObject:param];
        }
    }
    
    
    NSDictionary *returnDict = [differences dictionaryWithValuesForKeys:changedParams];
    DDLogVerbose(@"total differences: %@", returnDict);
    return changedParams;
}

- (NSDictionary *) listPropertiesToPOST
{
    return [self listChangedParamsForPOST];
}

- (NSDictionary *) listChangedParamsForPOST
{
    NSMutableDictionary *postParams = [NSMutableDictionary dictionary];
    
    NSMutableArray *changedParams = [NSMutableArray arrayWithArray:[self updatedParams]];
    NSArray *illegalFields = [self.parentTable.fields_illegal valueForKeyPath:@"@distinctUnionOfObjects.name"];
    
    [changedParams enumerateObjectsUsingBlock:^(id  _Nonnull key, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([illegalFields containsObject:key]) return;
        NSString *keyFID = [self fieldIDForField:key];
        
        if (keyFID == nil) {
            DDLogError(@"Error: null fieldID for key: %@", key);
            return;
        }
        NSString *value;
        
        if ([self.params objectForKey:key]) {
            if (![[self.params objectForKey:key] isEqualTo:@""])
            {
                value = [self.params valueForKey:key];
            }
            else
            {
                value = @"";
            }
        }
        else // No value
        {
            value = @"";
        }
        
        [postParams setValue:value forKey:[NSString stringWithFormat:@"_fid_%@", keyFID]];
        //[postParams setValue:value forKey:[NSString stringWithFormat:@"field fid=\"%@\"", keyFID]];
    }];
    
    return postParams;
    
}

#pragma mark - Sorted Children Helper Functions
- (NSArray *) sortedChildren:(NSString *)childName ascending:(BOOL)asc
{
    return [self sortedChildren:childName sortedBy:@"record_id" ascending:asc];
}

- (NSArray *) sortedChildren:(NSString *)childName sortedBy:(NSString *)sortKey ascending:(BOOL)asc
{
    //    NSLog(@"Children for %@", [self name]);
    
    SEL childrenSelector = NSSelectorFromString(childName);
    
    NSArray *sortingKeys = [sortKey componentsSeparatedByString:@", "];
    NSMutableArray *validSortDescriptors = [NSMutableArray array];
    
    
    if ([self respondsToSelector:childrenSelector]) {
        id child = [[self valueForKey:childName] anyObject];
        if (child == nil) return @[];
        
        
        for (NSString* sortKey in sortingKeys) {
            SEL sortingSelector = NSSelectorFromString(sortKey);
            
            if ([child respondsToSelector:sortingSelector])
            {
                if ([sortKey isEqualToString:@"priorityNum"]) {
                    [validSortDescriptors addObject:[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:NO]];
                } else
                {
                    [validSortDescriptors addObject:[NSSortDescriptor sortDescriptorWithKey:sortKey ascending:asc]];
                }
            }
            else {
                DDLogVerbose(@"Error: %@'s children '%@' do not respond to the sortKey: '%@'", [self class], childName, sortKey);
            }
        }
        
        NSArray *returnArray = [[self valueForKey:childName] sortedArrayUsingDescriptors:validSortDescriptors];
        return returnArray;
    }
    else
    {
        DDLogVerbose(@"Error: %@ does not have any children '%@'.", [self class], childName);
        return @[];
    }
}

#pragma mark - Record Comparison Functions
- (NSMutableDictionary*) listCompareParams:(SF1XMLRecord*) record
{
    NSMutableDictionary *compareParams = [NSMutableDictionary dictionary];
    
    for (RXMLElement* param in [record children:@"f"]) {
        if (self.parentTable == nil) [self assignParentTableAndPrimaryKey];
        
        NSString* paramKey = [self.fieldIDs valueForKey:[param attribute:@"id"]];
        
        RXMLElement* paramValue = param;
        [compareParams setValue:paramValue.text forKey:paramKey];
    }
    
    return compareParams;
}

-(NSMutableDictionary *) compareToRecord:(SF1XMLRecord *)record
{
    NSMutableDictionary *compareParams = [self listCompareParams:record];
    __block NSMutableDictionary *differences = [NSMutableDictionary dictionary];
    
    if ([self.params isEqualToDictionary:compareParams])
    {
        return differences;
    }
    else
    {
        DDLogVerbose(@"Comparing records for %@ with RID: %@", [self class], self.record_id);
        [compareParams enumerateKeysAndObjectsUsingBlock: ^(id key, id obj, BOOL *stop) {
            // do something with key and obj
            if (![[self.params valueForKey:key] isEqualTo:[compareParams valueForKey:key]])
            {
                [differences setValue:[compareParams valueForKey:key] forKey:key];
            }
        }];
        return differences;
    }
}

- (NSComparisonResult) compareModDateToRecord:(SF1XMLRecord*) record
{
    NSDate *myDate = [self date_modified];
    NSDate *compareDate = [self convertDateFromImport:[[self listCompareParams:record] valueForKey:@"Date Modified"]];
    NSComparisonResult result = [myDate compare:compareDate];
    return result;
    
}


#pragma mark - Table & Field Helper Functions
- (id) primaryKeyValue
{
    if ([self primitiveValueForKey:@"primaryKeyValue"] == nil)
    {
        [self setPrimitiveValue:[self convertedPKValue] forKey:@"primaryKeyValue"];
    }
    
    id returnValue = [self primitiveValueForKey:@"primaryKeyValue"];
    
    return returnValue;
}

- (QBTable*) assignParentTableAndPrimaryKey
{
    self.parentTable = [QBTable tableNamed:[self tableName] inMOC:self.managedObjectContext];
    self.primaryKey = self.parentTable.primaryKey;
    return self.parentTable;
}

- (QBField *) fieldNamed:(NSString*)fieldName
{
    return [self.parentTable fieldNamed:fieldName];
}

- (NSString*) fieldNameForMastagTable:(NSString*)tableName
{
    return [self.parentTable fieldNameForMastagTableNamed:tableName];
}

- (NSString *) fieldIDForField:(NSString*)fieldName
{
    return [[self.parentTable fieldNamed:fieldName] fid];
}

- (id) convertedPKValue
{
    id returnValue = nil;
    if ([self.primaryKey.base_type isEqualToString:@"text"]) {
        returnValue = [self.params valueForKey:self.primaryKey.name];
    }
    else if ([self.primaryKey.base_type isEqualToString:@"float"]) {
        returnValue = [NSNumber numberWithFloat:[[self.params valueForKey:self.primaryKey.name] floatValue]];
    }
    else if ([self.primaryKey.base_type containsString:@"int"]) {
        returnValue = [NSNumber numberWithInteger:[[self.params valueForKey:self.primaryKey.name] integerValue]];
    }
    else
    {
        @throw [NSException exceptionWithName:@"UndeterminedPKBaseTypeException"
                                       reason:@"PrimaryKey BaseType is not text, float, or int." userInfo:@{@"object" : self.description}];
        returnValue = nil;
    }
    
    return returnValue;
}


- (id) convertedParamValueForFieldNamed:(NSString*)fieldName
{
    QBField *targetedField = [self.parentTable fieldNamed:fieldName];
    
    if (targetedField == nil) {
        DDLogVerbose(@"ERROR: field named '%@' not Found", fieldName);
        return nil;
    }
    
    if ([targetedField.base_type isEqualToString:@"text"]) {
        return [self.params valueForKey:fieldName];
    }
    else if ([targetedField.base_type isEqualToString:@"float"]) {
        return [NSNumber numberWithFloat:[[self.params valueForKey:fieldName] floatValue]];
    }
    else if ([targetedField.base_type rangeOfString:@"int"].location != NSNotFound) {
        return [NSNumber numberWithInteger:[[self.params valueForKey:fieldName] integerValue]];
    }
    else if ([targetedField.base_type isEqualToString:@"bool"]) {
        @try {
            return [NSNumber numberWithBool:[[self.params valueForKey:fieldName] boolValue]];
        } @catch (NSException *exception) {
            DDLogVerbose(@"targetedField Base Type Exception: %@", exception);
            return [NSNumber numberWithInteger:[[self.params valueForKey:fieldName] integerValue]];
        }
    }
    else {
        return nil;
    }
    
}

- (id) convertedValueForMastagTable:(NSString*)mtable
{
    NSString *mastagFieldName = [self.parentTable fieldNameForMastagTableNamed:mtable];
    return [self convertedParamValueForFieldNamed:mastagFieldName];
}

- (NSMutableDictionary *) fieldIDs // TODO: eventually remove
{
    NSMutableDictionary *returnDict = [NSMutableDictionary dictionary];
    QBTable *table = self.parentTable;
    if (table == nil) table = [self assignParentTableAndPrimaryKey];
    for (QBField *field in table.fields) {
        [returnDict setValue:field.name forKey:field.fid];
    }
    
    return returnDict;
}


#pragma mark - Misc Functions

- (void) awakeFromInsert
{
    [super awakeFromInsert];
    
    if (self.params == nil) {
        self.params = [NSMutableDictionary dictionary];
    }
}

- (void) awakeFromFetch
{
    [super awakeFromFetch];
    
    if (self.params == nil) {
        self.params = [NSMutableDictionary dictionary];
    }
}

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)theKey
{
    BOOL automatic = NO;
    if (([theKey isEqualToString:@"params"]) || ([theKey isEqualToString:@"changedParams"])){
        automatic = NO;
    }
    else {
        automatic = YES;
    }
    return automatic;
}


@end
