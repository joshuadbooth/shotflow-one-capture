//
//  Station+CoreDataClass.h
//  CI Capture Manager
//
//  Created by Josh Booth on 8/10/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QBManagedObject.h"

@class Set;

NS_ASSUME_NONNULL_BEGIN

@interface Station : QBManagedObject

+ (instancetype) existingWithHardwareUUID:(NSString *)uuid inContext:(NSManagedObjectContext *)moc;

@end

NS_ASSUME_NONNULL_END

#import "Station+CoreDataProperties.h"
