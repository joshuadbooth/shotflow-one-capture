//
//  SetList.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "SetList.h"
#import "Shot.h"
#import "Set.h"
#import "AppDelegate.h"

#import "DummyObject.h"


@implementation SetList {
    DummyObject *dummy;
    id lookupParent;
}

@dynamic canComplete;
@dynamic isComplete;
@dynamic sampleMode;

@dynamic priority;
@dynamic priorityNum;
@dynamic related_set;
@dynamic set_list_name;

@dynamic type;
@dynamic job_id;
@dynamic shots;
@dynamic set;
@dynamic shotsRecordIDs;
@dynamic set_name;

@dynamic totalNumberOfCaptures;
@dynamic averageNumberOfCaptures;


@synthesize orderedShots;
@synthesize orderedParentedShots;

enum
{
    kSetListPriorityLOW = 1,
    kSetListPriorityMEDIUM = 2,
    kSetListPriorityHIGH = 3,
    kSetListPriorityIMMEDIATE = 4
};

typedef NSInteger SetListPriority;


- (NSString *) tableName {
    return @"Set Lists";
}


+ (NSString *)entityName {
    return @"SetList";
}

+ (NSSet*) keyPathsForValuesAffectingName {
    return [NSSet setWithObject:@"set_list_name"];
}

- (NSString*) name {
    return self.set_list_name;
}

- (NSString*) set_name {
    return self.set.set_name;
}

- (NSArray*) orderedShots {
    
    NSArray *ordered;
    if (!self.sampleMode) {
        ordered = [[self shots] sortedArrayUsingDescriptors:@[[AppDelegate sharedInstance].shotSorting, [AppDelegate sharedInstance].sortByRecordID]];
    }
    else
    {
        ordered = [[self shots] sortedArrayUsingDescriptors:@[[AppDelegate sharedInstance].sortByRecordID]];
    }
    
    return ordered;
}



- (NSArray*) orderedParentedShots {
    NSArray *ordered = [self orderedShots];
    return  [ordered filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"parent == %@", self]];
}



- (NSString*) debugDescription {
    /***** Prettify Params *****/
    NSMutableString *prettyParams = [NSMutableString string];
    for (id key in [[self.params allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyParams appendFormat:@"\t%@: %@\n", key, [self.params valueForKey:key]];
    }
    
    NSMutableString *prettyPreviousParams = [NSMutableString string];
    for (id key in [[self.previousParams allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyPreviousParams appendFormat:@"\t%@: %@\n", key, [self.previousParams valueForKey:key]];
    }
    
    NSMutableString *desc = [NSMutableString stringWithFormat:
@"\nRecord ID: %@\n\
Name: %@\n\
Set: %@\n\
Status: %@\n\
SampleMode: %d\n\
Shots:\n\t%@\n",
                             self.record_id,
                             self.name,
                             self.set.description,
                             self.status,
                             self.sampleMode,
                             [[[self orderedParentedShots] valueForKeyPath:@"description"] componentsJoinedByString:@"\n\t"]];
    
    
    [desc appendFormat:@"Params: \n%@\n", prettyParams];
    
    return desc;
}


- (void) changes:(NSNotification*)notification {
    NSSet *objects = nil;
    NSMutableSet *combinedSet = nil;
    NSPredicate *predicate = nil;
    
    // combine all changes into one set to easily filter
    NSDictionary *userInfo = [notification userInfo];
    objects = [userInfo valueForKey:NSInsertedObjectsKey];
    combinedSet = [NSMutableSet setWithSet:objects];
    objects = [[notification userInfo] valueForKey:NSUpdatedObjectsKey];
    [combinedSet unionSet:objects];
    objects = [[notification userInfo] valueForKey:NSDeletedObjectsKey];
    [combinedSet unionSet:objects];
    
    [combinedSet filterUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [Shot class]]];
    // create a predicate to filter based on if it's a shot and it's setlist is this current setlist (me)
    predicate = [NSPredicate predicateWithFormat:@"setlist.record_id == %@", self.record_id];
    [combinedSet filterUsingPredicate:predicate];
    
    if ([combinedSet count] == 0) {
        return;
    }
    else {
        [self checkAllShotsCompleted];
    }
  
}


- (NSArray*) children
{
    NSCompoundPredicate *filterPredicate = [NSCompoundPredicate
                                            andPredicateWithSubpredicates:@[[AppDelegate sharedInstance].filteredChildrenPredicate]];
    
    NSMutableArray *shotsPlusAdd = [NSMutableArray arrayWithArray: [self.orderedParentedShots filteredArrayUsingPredicate:filterPredicate]];
    
    if (dummy == nil){
        dummy = [DummyObject dummyWithParent:self andType:kDummyTypeAddShot];
    }
    
    [shotsPlusAdd addObject: dummy];
    
    return shotsPlusAdd;
}

- (NSArray*) filteredChildren {
    return [self.children filteredArrayUsingPredicate:[AppDelegate sharedInstance].filteredChildrenPredicate];
}

+ (NSSet*) keyPathsForValuesAffectingNumberOfChildren {
    return [NSSet setWithObject:@"children"];
}

+ (NSSet*) keyPathsForValuesAffectingChildren {
//    DDLogVerbose(@"SetList keyPathsForValuesAffectingChildren");
    return [NSSet setWithObject:@"shots"];
}


+ (NSSet*) keyPathsForValuesAffectingParent {
    return [NSSet setWithObject:@"set"];
}

- (id) parent {
    return self.set;
}

- (BOOL) shouldHide {
    BOOL faceValue = [[AppDelegate sharedInstance].uiStatusPredicate evaluateWithObject:self];
//    BOOL filteredValue = [[AppDelegate sharedInstance].filteredChildrenPredicate evaluateWithObject:self];
//    
//    BOOL returnValue = (faceValue == YES && filteredValue == NO);
    return faceValue;
}

//- (NSNumber *) related_set {
//    return self.set.record_id;
//}

- (NSArray*) shotsRecordIDs {
    
    if (self.shots.count !=0)
    {
        NSArray *recordIDs = [[self.shots sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"record_id" ascending:YES]]] valueForKeyPath:@"record_id"];
        return recordIDs;
    } else {
        return nil;
    }
}

- (BOOL) canComplete {
    

    
    if (self.shots.count == 0)
    {
        if (self.sampleMode)
        {
         return YES;
        }
        else
        {
            if (self.isComplete)
            {
                self.isComplete = NO;
                [self setParam:@"0" forKey:@"isComplete"];
            }
            
            return NO;
        }
    }
    
    
    
    NSMutableArray *availableShots = [NSMutableArray arrayWithArray:[self.orderedShots filteredArrayUsingPredicate:[AppDelegate sharedInstance].hiddenStatusPredicate]];
    
    if (availableShots.count == 0) return NO;
    
    [availableShots filterUsingPredicate:[NSPredicate predicateWithFormat:@"NOT(status == %@)", @"Completed"]];
    
    BOOL returnValue = (availableShots.count == 0);
    
    if (!returnValue && [self.status isEqualToString:@"Completed"]) {
        self.isComplete = NO;
        [self setStatus:@"In Progress"];
    }
    
    return returnValue;
}

- (BOOL) checkAllShotsCompleted {
    BOOL returnValue = [self canComplete];
    
        if ([[NSPredicate predicateWithFormat:@"ANY shots.numberOfCaptures >= 1"] evaluateWithObject:self])
        {
             self.status = @"In Progress";
            //[self setParam:@"In Progress" forKey:@"Status"];
        }
        else
        {
            self.status = @"Ready";
            //[self setParam:@"Ready" forKey:@"Status"];
        }
    
    return returnValue;
}



- (void) updateWithRecordData:(RXMLElement *)record {
    [super updateWithRecordData:record];

    
    self.job_id = [self.params valueForKey:@"Job ID"];
    self.set_list_name = [self.params objectForKey:@"Name"]? [self.params valueForKey:@"Name"] : [self.params valueForKey:@"Set List Name"];
    
    self.priority = [self.params valueForKey:@"Priority"];
    self.priorityNum = [NSNumber numberWithInteger:[[self.params valueForKey:@"priorityNum"] integerValue]];
    
    self.status = [self.params valueForKey:@"Status"];
    self.type = [self.params valueForKey:@"Type"];
    
    self.sampleMode = [[self.params valueForKey:@"Sample Mode"] boolValue];
    
    self.isComplete = [self.status isEqualToString:@"Completed"];
    
    self.related_set = [NSNumber numberWithInteger:[[self.params valueForKey:@"Related Set"] integerValue]];
    
}


- (NSDictionary*) listPropertiesToPOST
{
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    
    if (self.online) {
        [postParams setValue:self.record_id forKey:@"rid"];
    }
    
    [postParams addEntriesFromDictionary:[self listChangedParamsForPOST]];
    return postParams;
}

- (NSNumber*) returnPriorityNum {
    NSNumber *number = nil;
    if (self.priority !=nil) {
        if ([self.priority isEqualToString:@""]) {
            number = [NSNumber numberWithInt:0];
        } else if ([self.priority isEqualToString:@"Low"]) {
            number = [NSNumber numberWithInt:1];
        } else if ([self.priority isEqualToString:@"Medium"]) {
            number = [NSNumber numberWithInt:2];
        }else if ([self.priority isEqualToString:@"High"]) {
            number = [NSNumber numberWithInt:3];
        }else if ([self.priority isEqualToString:@"Immediate"]) {
            number = [NSNumber numberWithInt:4];
        } else {
            number = [NSNumber numberWithInt:0];
        }
    }
    return number;
}
- (void) willSave {
    if ((self.priorityNum == nil) && (self.priority !=nil)) self.priorityNum = [self returnPriorityNum];
}

- (NSNumber*) totalNumberOfCaptures {
    return [[self shots] valueForKeyPath:@"@sum.numberOfCaptures"];
}

- (NSNumber*) averageNumberOfCaptures {
    return [[self shots] valueForKeyPath:@"@avg.numberOfCaptures"];
}


@end
