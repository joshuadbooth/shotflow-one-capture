//
//  Shot+CoreDataProperties.h
//  CI Capture Manager
//
//  Created by Josh Booth on 8/16/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "Shot.h"


NS_ASSUME_NONNULL_BEGIN

@interface Shot (CoreDataProperties)

//+ (NSFetchRequest<Shot *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateCompleted;
@property (nonatomic, nonatomic, strong) NSNumber* shotDuration;

@property (nullable, nonatomic, retain) NSObject *dummy;
@property (nonatomic) BOOL isComplete;
@property (nonatomic) BOOL isLinked;
@property (nullable, nonatomic, copy) NSNumber *linkedShotRecordID;
@property (nullable, nonatomic, copy) NSString *naming_convention;
@property (nonatomic) BOOL needsAttention;
@property (nullable, nonatomic, copy) NSNumber *numberOfCaptures;
@property (nullable, nonatomic, copy) NSNumber *related_setlist;
@property (nullable, nonatomic, copy) NSNumber *sampleDataDownloaded;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *orderedPDRString;
@property (nullable, nonatomic, retain) NSSet<ProductDataRecord *> *productdatarecords;
@property (nullable, nonatomic, retain) NSSet<Sample *> *samples;
@property (nullable, nonatomic, retain) SetList *setlist;
@property (nullable, nonatomic, retain) NSSet<Shot *> *subShots;
@property (nullable, nonatomic, retain) Shot *topShot;

@end

@interface Shot (CoreDataGeneratedAccessors)

- (void)addProductdatarecordsObject:(ProductDataRecord *)value;
- (void)removeProductdatarecordsObject:(ProductDataRecord *)value;
- (void)addProductdatarecords:(NSSet<ProductDataRecord *> *)values;
- (void)removeProductdatarecords:(NSSet<ProductDataRecord *> *)values;

- (void)addSamplesObject:(Sample *)value;
- (void)removeSamplesObject:(Sample *)value;
- (void)addSamples:(NSSet<Sample *> *)values;
- (void)removeSamples:(NSSet<Sample *> *)values;

- (void)addSubShotsObject:(Shot *)value;
- (void)removeSubShotsObject:(Shot *)value;
- (void)addSubShots:(NSSet<Shot *> *)values;
- (void)removeSubShots:(NSSet<Shot *> *)values;

@end

NS_ASSUME_NONNULL_END
