//
//  Shot+CoreDataProperties.m
//  CI Capture Manager
//
//  Created by Josh Booth on 8/16/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "Shot+CoreDataProperties.h"

@implementation Shot (CoreDataProperties)

//+ (NSFetchRequest<Shot *> *)fetchRequest {
//	return [[NSFetchRequest alloc] initWithEntityName:@"Shot"];
//}

@dynamic orderedPDRString;
@dynamic dateCompleted;
//@dynamic shotDuration;
@dynamic dummy;
@dynamic isComplete;
@dynamic isLinked;
@dynamic linkedShotRecordID;
@dynamic naming_convention;
@dynamic needsAttention;
@dynamic numberOfCaptures;
@dynamic related_setlist;
@dynamic sampleDataDownloaded;
@dynamic title;
@dynamic productdatarecords;
@dynamic samples;
@dynamic setlist;
@dynamic subShots;
@dynamic topShot;

@end
