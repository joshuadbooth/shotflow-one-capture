//
//  Sample.m
//  CI Capture Manager
//
//  Created by Josh Booth on 10/27/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "Sample.h"
#import "ProductDataRecord.h"
#import "Shot.h"
#import "AppDelegate.h"
#import "QBTable.h"
#import "QBField.h"

@implementation Sample

- (NSString *) tableName {
    return @"Samples";
}

+ (NSString *)entityName {
    return @"Sample";
}

- (NSString*) description {
    return [NSString stringWithFormat:@"%@: %@", self.record_id, self.name];
}

// Insert code here to add functionality to your managed object subclass
- (NSArray*) relatedShots {
    if (self.shots.count == 0) {
        return @[];
    } else {
        return [[self.shots sortedArrayUsingDescriptors:@[[AppDelegate sharedInstance].sortByRecordID]] valueForKeyPath:@"@distinctUnionOfObjects.record_id"];
    }
}

- (NSString*) debugDescription {
    __block    NSMutableString *prettyParams = [NSMutableString string];
    
    for (id key in [[self.params allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyParams appendFormat:@"\t%@: %@\n", key, [self.params valueForKey:key]];
    }
    
    NSMutableString *prettyPreviousParams = [NSMutableString string];
    
    for (id key in [[self.previousParams allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        [prettyPreviousParams appendFormat:@"\t%@: %@\n", key, [self.previousParams valueForKey:key]];
    }
    
    NSMutableString *desc = [NSMutableString stringWithFormat:
@"\nRecord ID: %@\n\
Name: %@\n\
BarCodeValue: %@\n\
RelatedPDR: %@\n\
PDR (actual): %@\n\
Linked Shots:\n\t%@\n",
                             self.record_id,
                             self.name,
                             self.barCodeValue,
                             self.relatedPDR,
                             self.productdatarecord.description,
                             [[[self sortedChildren:@"shots" ascending:YES] valueForKeyPath:@"description"] componentsJoinedByString:@"\n\t"]];
    
    [desc appendFormat:@"Params: \n%@\n", prettyParams];
    
    return desc;
}

- (NSString*) name {
    return [NSString stringWithFormat:@"Sample: %@",self.barCodeValue];
}

- (void) updateWithRecordData:(RXMLElement *)record {
    [super updateWithRecordData:record];
    
    self.status = [self.params valueForKey:@"Status"];
    self.barCodeValue = [self.params valueForKey:@"API_BarCodeValue"];
    
    QBTable *samplesTable = [QBTable tableNamed:@"Samples" inMOC:self.managedObjectContext];
    QBField *relatedPDRfield = [samplesTable fieldForMastagTableNamed:@"Product Data"];
    
    self.relatedPDR = [self.params valueForKey:relatedPDRfield.name];
}

@end
