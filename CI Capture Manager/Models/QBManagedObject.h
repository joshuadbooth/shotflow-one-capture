//
//  QBManagedObject.h
//  CI Capture Manager
//
//  Created by Josh Booth on 3/29/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "RXMLElement.h"
#import "SF1XMLRecord.h"

@class QBManagedObject, RXMLElement, QBField, QBTable;

NS_ASSUME_NONNULL_BEGIN

typedef QBManagedObject QBMO;

@interface QBManagedObject : NSManagedObject




// Insert code here to declare functionality of your managed object subclass
#pragma mark - Easy Creation
+ (NSString *) tableName;
+ (NSString *) entityName;

#pragma mark - Insertion & Retrieval Functions
+ (instancetype) insertNewObjectIntoContext:(NSManagedObjectContext *)moc;
+ (instancetype) insertWithXMLData:(SF1XMLRecord*)xmlData intoContext:(NSManagedObjectContext *)moc;

+ (instancetype) existingWithObjectID:(NSManagedObjectID*)objID inContext:(NSManagedObjectContext *)moc;
+ (instancetype) existingWithRecordID:(NSNumber *)recordID inContext:(NSManagedObjectContext *)moc;
+ (instancetype) existingWithPrimaryKeyValue:(id)pkv inContext:(NSManagedObjectContext *)moc;

- (instancetype) findInManagedObjectContext:(NSManagedObjectContext *)context error:(NSError**)error;


#pragma mark - Key Data Functions
- (void) populateParams: (SF1XMLRecord*)record;
- (void) updateWithRecordData: (SF1XMLRecord*)record;
- (void) setParam:(id)param forKey:(NSString*)key;
- (void) updateDateModified;

#pragma mark - Helper Functions
// (description)
- (NSNumber *) recordAsNSNumber: (NSInteger)num;
- (NSDate *) convertDateFromImport:(NSString*)dateString;

- (NSString *) convertDateForPOST:(NSDate *)date;
- (NSString *) prettyDate: (NSDate*) date;
- (NSString *) xmpDate: (NSDate*) date;
- (NSString*) localDateStringForDate:(NSDate* _Nonnull)date dateFormat:(NSString * _Nullable )dateFormat field:(QBField* _Nullable)field;


#pragma mark - Parameter Comparison Functions
- (NSArray *) updatedParams;
- (NSDictionary *) listPropertiesToPOST;
- (NSDictionary *) listChangedParamsForPOST;

#pragma mark - Sorted Children Helper functions
- (NSArray *) sortedChildren:(NSString *)childName ascending:(BOOL)asc;
- (NSArray *) sortedChildren:(NSString *)childName sortedBy:(NSString *)sortKey ascending:(BOOL)asc;


#pragma mark - Record Comparison Functions
- (NSMutableDictionary *) listCompareParams:(SF1XMLRecord*) record;
- (NSMutableDictionary *) compareToRecord:(SF1XMLRecord*)record;
- (NSComparisonResult) compareModDateToRecord:(SF1XMLRecord*) record;

#pragma mark - Table & Field Helper Functions
// (primaryKeyValue)
- (QBTable *) assignParentTableAndPrimaryKey;
- (QBField *) fieldNamed:(NSString*)fieldName;
- (NSString *) fieldNameForMastagTable:(NSString*)tableName;
- (NSString *) fieldIDForField:(NSString*)fieldName;

- (id) convertedPKValue;
- (id) convertedParamValueForFieldNamed:(NSString*)fieldName;
- (id) convertedValueForMastagTable:(NSString*)mtable;



- (NSString *) prettyParams;
- (NSString *) prettyPreviousParams;

@end

NS_ASSUME_NONNULL_END

#import "QBManagedObject+CoreDataProperties.h"
