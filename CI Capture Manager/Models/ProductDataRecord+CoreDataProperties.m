//
//  ProductDataRecord+CoreDataProperties.m
//  CI Capture Manager
//
//  Created by Josh Booth on 10/27/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProductDataRecord+CoreDataProperties.h"

@implementation ProductDataRecord (CoreDataProperties)

@dynamic relatedPDR;
@dynamic relatedShot;
@dynamic samples;
@dynamic shots;

@end
