//
//  Set.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/15/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QBManagedObject.h"

@class SetList, Station;

@interface Set : QBManagedObject

@property (nonatomic, retain) NSString * set_name;
@property (nonatomic, retain) NSSet *setlists;
@property (nonatomic, retain) Station *station;
@property (nonatomic, retain) NSString *activeUser;
@property (nonatomic) BOOL isAvailable;
@property (nonatomic, readonly) NSArray *orderedSetLists;
@end



@interface Set (CoreDataGeneratedAccessors)

- (void)addSetlistsObject:(SetList *)value;
- (void)removeSetlistsObject:(SetList *)value;
- (void)addSetlists:(NSSet *)values;
- (void)removeSetlists:(NSSet *)values;

@end
