//
//  Station+CoreDataProperties.m
//  CI Capture Manager
//
//  Created by Josh Booth on 8/10/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "Station+CoreDataProperties.h"

@implementation Station (CoreDataProperties)

//+ (NSFetchRequest<Station *> *)fetchRequest {
//	return [[NSFetchRequest alloc] initWithEntityName:@"Station"];
//}

@dynamic isActive;
@dynamic hardwareUUID;
@dynamic license_code;
@dynamic serialNumber;
@dynamic setName;
@dynamic studioName;

@dynamic relatedSet;
@dynamic set;

@end
