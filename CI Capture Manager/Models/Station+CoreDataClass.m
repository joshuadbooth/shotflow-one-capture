//
//  Station+CoreDataClass.m
//  CI Capture Manager
//
//  Created by Josh Booth on 8/10/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "Station+CoreDataClass.h"
#import "Set.h"

@implementation Station
- (NSString *) tableName {
    return @"Stations";
}

+ (NSString *)entityName {
    return @"Station";
}


+ (instancetype) existingWithHardwareUUID:(NSString *)uuid inContext:(NSManagedObjectContext *)moc
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"hardwareUUID == %@", uuid];
    fetchRequest.predicate = predicate;
    
    
    NSError *fetchError = nil;
    NSArray *results = [moc executeFetchRequest:fetchRequest error:&fetchError];
    //NSArray *results = [[moc executeFetchRequest:fetchRequest error:&fetchError] filteredArrayUsingPredicate:predicate];
    
    if (fetchError == nil)
    {
        if ([results count] == 0) return nil;
        id fetchedObject = [results firstObject];
        return fetchedObject;
    }
    else {
        DDLogError(@"existingWithObjectID error() %@", fetchError);
        return nil;
    }
}



- (void) updateWithRecordData:(RXMLElement *)record
{
    [super updateWithRecordData:record];
    
    self.name = [self.params valueForKey:@"Name"];
    self.hardwareUUID = [self.params valueForKey:@"Hardware UUID"];
    self.isActive = [[self.params valueForKey:@"isActive"] boolValue];
    
    self.setName = [self.params valueForKey:@"Set Name"];
    self.studioName = [self.params valueForKey:@"Studio Name"];
    self.license_code = [self.params valueForKey:@"License Code"];
    
    self.relatedSet = [NSNumber numberWithInt:[[self.params valueForKey:[self fieldNameForMastagTable:@"Set"]] intValue]];
    
    
    
    // **** Link to Set **** //
    Set *rSet = [Set existingWithRecordID:self.relatedSet inContext:self.managedObjectContext];
    if (rSet != nil && [self.set isNotEqualTo:rSet])
    {
        self.set = rSet;
    }
}



- (NSDictionary*) listPropertiesToPOST {
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    
    if (self.online) {
        [postParams setValue:self.record_id forKey:@"rid"];
    }
    
    [postParams addEntriesFromDictionary:[self listChangedParamsForPOST]];

    
    return postParams;
}
@end
