//
//  ProductDataRecord.m
//  CI Capture Manager
//
//  Created by Josh Booth on 10/13/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "ProductDataRecord.h"
#import "Sample.h"
#import "Shot.h"
static NSString * _ProductRecordContext = @"_ProductRecordContext";

@implementation ProductDataRecord

- (NSString *) tableName {
    return @"Product Data";
}

+ (NSString *)entityName {
    return @"ProductDataRecord";
}

- (NSString*) description {
    return [NSString stringWithFormat:@"%@: %@", self.record_id, self.name];
}

// Insert code here to add functionality to your managed object subclass

- (NSString*) name {
    return [NSString stringWithFormat:@"PDR: %@", self.primaryKeyValue];
}


- (void) updateWithRecordData:(RXMLElement *)record {
    [super updateWithRecordData:record];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterNoStyle;
    
   // self.relatedPDR = [f numberFromString:[self.params valueForKey:@"Related Product Data Record"]];
   // self.relatedShot = [f numberFromString:[self.params valueForKey:@"Related Shot"]];
    
}
@end
