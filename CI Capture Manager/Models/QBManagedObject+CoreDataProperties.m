//
//  QBManagedObject+CoreDataProperties.m
//  CI Capture Manager
//
//  Created by Josh Booth on 3/29/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "QBManagedObject+CoreDataProperties.h"

@implementation QBManagedObject (CoreDataProperties)

@dynamic date_created;
@dynamic date_modified;
@dynamic fieldIDs;
@dynamic name;
@dynamic online;

@dynamic params;
@dynamic previousParams;
@dynamic record_id;
@dynamic status;
@dynamic tableName;
@dynamic update_id;
@dynamic primaryKeyValue;
@dynamic primaryKey;
@dynamic parentTable;

@end
