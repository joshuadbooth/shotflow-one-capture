//
//  Sample+CoreDataProperties.h
//  CI Capture Manager
//
//  Created by Josh Booth on 7/6/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Sample.h"

NS_ASSUME_NONNULL_BEGIN

@interface Sample (CoreDataProperties)

@property (nullable, nonatomic, retain) id relatedPDR;
@property (nullable, nonatomic, retain) id relatedShots;
@property (nullable, nonatomic, retain) NSString *barCodeValue;
@property (nullable, nonatomic, retain) ProductDataRecord *productdatarecord;
@property (nullable, nonatomic, retain) NSSet<Shot *> *shots;

@end

@interface Sample (CoreDataGeneratedAccessors)

- (void)addShotsObject:(Shot *)value;
- (void)removeShotsObject:(Shot *)value;
- (void)addShots:(NSSet<Shot *> *)values;
- (void)removeShots:(NSSet<Shot *> *)values;

@end

NS_ASSUME_NONNULL_END
