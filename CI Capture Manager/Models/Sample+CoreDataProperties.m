//
//  Sample+CoreDataProperties.m
//  CI Capture Manager
//
//  Created by Josh Booth on 7/6/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Sample+CoreDataProperties.h"

@implementation Sample (CoreDataProperties)

@dynamic relatedPDR;
@dynamic relatedShots;
@dynamic barCodeValue;
@dynamic productdatarecord;
@dynamic shots;

@end
