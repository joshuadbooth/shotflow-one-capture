//
//  Station+CoreDataProperties.h
//  CI Capture Manager
//
//  Created by Josh Booth on 8/10/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "Station+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Station (CoreDataProperties)

//+ (NSFetchRequest<Station *> *)fetchRequest;

@property (nonatomic) BOOL isActive;
@property (nullable, nonatomic, copy) NSString *hardwareUUID;
@property (nullable, nonatomic, copy) NSString *license_code;
@property (nullable, nonatomic, copy) NSString *serialNumber;
@property (nullable, nonatomic, copy) NSString *setName;
@property (nullable, nonatomic, copy) NSString *studioName;

@property (nullable, nonatomic, copy) NSNumber *relatedSet;
@property (nullable, nonatomic, retain) Set *set;

@end

NS_ASSUME_NONNULL_END
