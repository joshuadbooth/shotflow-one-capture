//
//  ShotTableCellView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/3/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "ShotTableCellView.h"



@interface ShotTableCellView ()
@property NSString* shotStatus;
@property BOOL allows0Captures;
@end

@implementation ShotTableCellView {
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    QBDataExporter *exporter;
    C1ProController *C1PC;
    
}
@synthesize shot;
@synthesize iconStackView;
@synthesize shotStatus;



- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}



- (void) viewDidMoveToSuperview {
    [super viewDidMoveToSuperview];
    [self setupCellView];
    [self setNeedsDisplay:YES];
    
}

- (void) assignShot:(Shot*)s {
    self.shot = s;
    [self setupCellView];
}

- (void) setupCellView {
    
    app = [AppDelegate sharedInstance];
    mainMOC = app.mainMOC;
    C1PC = [C1ProController sharedInstance];
    exporter = [QBDataExporter sharedExporter];
    
    
 
    self.allows0Captures = [[QBDBInfo info].configurationData objectForKey:@"ALLOWS0CAPTURES"]? [[[QBDBInfo info].configurationData valueForKey:@"ALLOWS0CAPTURES"] boolValue] : NO;
    
    [self.textField bind:@"stringValue" toObject:shot withKeyPath:@"name" options:nil];
    
    [self.alertButton bind:@"hidden"
                  toObject:shot
               withKeyPath:@"needsAttention"
                   options:@{NSValueTransformerNameBindingOption : NSNegateBooleanTransformerName}];
    
    [self.completeButton bind:@"state" toObject:shot withKeyPath:@"isComplete" options:nil];
    
    if (!self.shot.needsAttention) {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityNotVisible forView:self.alertButton];
    } else {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityMustHold forView:self.alertButton];
    }
    
    if (![self.shot isLinked]) {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityNotVisible forView:self.linkedButton];
    } else {
        [iconStackView setVisibilityPriority:NSStackViewVisibilityPriorityMustHold forView:self.linkedButton];
    }
    
    
    [self.textField bind:NSTextColorBinding toObject:self withKeyPath:@"statusLabelColor" options:nil];
    
    
}

- (NSColor*) statusLabelColor {
    if (app.hideReady || app.hideCompleted || app.hideInProgress) {
        if ([app.uiStatusPredicate evaluateWithObject:self.shot]) {
            return [NSColor disabledControlTextColor];
            
        }
        else
        {
            return [NSColor controlTextColor];
        }
    }
    else
    {
        return [NSColor controlTextColor];
    }
}

- (IBAction)showInfo:(id)sender {
    
    [[AppDelegate sharedInstance].detailView showWindow:self];
    
}
- (IBAction)markCompleted:(id)sender {
    
    [mainMOC performBlock:^{
        
        [mainMOC save:nil];
        
        BOOL isCurrentlyCompleted = shot.isComplete;
        BOOL isSetListCompleted = shot.setlist.isComplete;
        
        
        
        if (shot.numberOfCaptures == 0)
        {
            if (!self.allows0Captures) {
                
                NSError *noCapturesError = [NSError errorWithDomain:@"com.shotflow1.C1Controller" code:2000
                                                           userInfo:@{NSLocalizedDescriptionKey : @"No Recorded Captures", NSLocalizedFailureReasonErrorKey : @"You tried to mark a shot as complete, but haven't recorded any captures in Capture One.\n\nYou must have at least one recorded capture to mark a shot as complete."}];
                [[QBDBInfo info] alertMe:noCapturesError];
                
                //[_completeButton setState:0];
                shot.isComplete = NO;
            }
            else
            {
                if (!isCurrentlyCompleted)
                {
                    
                    NSAlert *noCapturesAlert = [[NSAlert alloc] init];
                    [noCapturesAlert setMessageText:@"No Recorded Captures"];
                    [noCapturesAlert setInformativeText:@"No captures have been recorded for this shot. Are you sure you want to mark this as complete?"];
                    [noCapturesAlert addButtonWithTitle:@"No"];
                    [noCapturesAlert addButtonWithTitle:@"Yes"];
                    
                    [noCapturesAlert setAlertStyle:NSWarningAlertStyle];
                    
                    
                    NSModalResponse response = [noCapturesAlert runModal];
                    
                    if (response == NSAlertFirstButtonReturn)
                    {
                        // do nothing.
                        shot.isComplete = NO;
                    }
                    else if (response == NSAlertSecondButtonReturn)
                    {
                        shot.isComplete = YES;
                        shot.status = @"Completed";
                        [shot setDateCompleted:[NSDate date]];
                        
                        DDLogInfo(@"Shot '%@' didEnd at %@", shot.name, [shot prettyDate:[NSDate date]]);
                    }
                }
                else
                {
                    shot.isComplete = NO;
                    if (shot.numberOfCaptures > 0)
                    {
                        shot.status = @"In Progress";
                        DDLogInfo(@"Shot '%@' didResume at %@", shot.name, [shot prettyDate:[NSDate date]]);
                    }
                    else
                    {
                        shot.status = @"Ready";
                    }
                }
            }
        }
        else
        {
            if (isCurrentlyCompleted)
            {
                shot.isComplete = NO;
                if (shot.numberOfCaptures > 0) {
                    shot.status = @"In Progress";
                    DDLogInfo(@"Shot '%@' didResume at %@", shot.name, [shot prettyDate:[NSDate date]]);
                }
                else {
                    shot.status = @"Ready";
                }
                
            }
            else
            {
                shot.isComplete = YES;
                shot.status = @"Completed";
                [shot setDateCompleted:[NSDate date]];
                
                DDLogInfo(@"Shot '%@' didEnd at %@", shot.name, [shot prettyDate:[NSDate date]]);
            }
        }
        
        [shot.setlist checkAllShotsCompleted];
        
        NSError *saveError = nil;
        
        if (shot.isComplete != isCurrentlyCompleted) {
            if (!shot.isComplete) [shot setDateCompleted:nil];
            
            [shot updateDateModified];
            [mainMOC save:&saveError];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [C1PC initiateSave:^{
                    if ([[C1PC activeShot] isEqualTo:[self shot]] && [C1PC isTiming])
                    {
                        [C1PC changeToShot:nil];
                    }
                    
                    [exporter postToServer:shot onComplete:nil];
                }];
            });
        }
        
        if (shot.setlist.isComplete != isSetListCompleted) {
            
            [shot.setlist updateDateModified];
            [mainMOC save:&saveError];
            dispatch_async(dispatch_get_main_queue(), ^{
                [exporter postToServer:shot.setlist onComplete:nil];
            });
        }
        
    }];
}



@end
