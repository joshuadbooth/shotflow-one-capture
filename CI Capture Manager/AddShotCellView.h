//
//  AddShotCellView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DummyObject.h"
#import "M1TableViewCell.h"

@interface AddShotCellView : M1TableViewCell
@property (weak) IBOutlet NSButton *addButton;
extern NSString *kAddShotNotification;

@property (nonatomic, assign) DummyObject *dummyobj;

- (IBAction) addShot:sender;
@end
