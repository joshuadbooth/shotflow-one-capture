//
//  DummyObject.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SetList;

@interface DummyObject : NSObject

enum
{
    kDummyTypeAddShot = 0,
    kDummyTypeFolder = 1,
};
typedef NSInteger DummyObjectType;


@property (nonatomic, strong) id parent;
@property (nonatomic, readonly) NSArray* children; // necessary for tree controller
@property (nonatomic, readonly) NSArray* filteredChildren; // necessary for tree controller

//@property (readonly) NSNumber *sortID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, readonly) NSNumber *record_id;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) id objectID;

+ (instancetype) dummyWithParent:(id)p andType:(DummyObjectType)t;
@end
