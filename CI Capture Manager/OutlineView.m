//
//  OutlineView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 8/6/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "OutlineView.h"
#import "AppDelegate.h"

#import "SetsArrayController.h"
#import "SetListsArrayController.h"
#import "M1HeaderCell.h"
#import "MainTableCellView.h"

#import "Set.h"
#import "SetList.h"
#import "Shot.h"



@implementation OutlineView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

+ (instancetype) sharedInstance {
    __strong static id _sharedInstance  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self allocForReal] initForReal];
    });
    return _sharedInstance;
}

+ (id) allocForReal { return [super allocWithZone:NULL]; }
+ (id) alloc { return [self sharedInstance]; }
+ (id) allocWithZone:(NSZone *)zone { return [self sharedInstance]; }
- (id) init {  return self; }



- (id) initForReal {
    DDLogVerbose(@"OutlineView InitForReal");
    self = [super init];
    
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
}

- (void) viewDidMoveToSuperview {
    [super viewDidMoveToSuperview];
    NSTableColumn *column = [[self tableColumns]firstObject];

    NSTableHeaderView *headerView = [[NSTableHeaderView alloc] initWithFrame:NSMakeRect(0, 0, 250, 25)];
    M1HeaderCell *headerCell = [[M1HeaderCell alloc] initTextCell:[NSString stringWithFormat:@"Set: %@", [AppDelegate sharedInstance].selectedSet.name]];
    //    headerCell.font = [NSFont systemFontOfSize:10];
    
    [headerCell drawWithFrame:NSMakeRect(0, 0, headerView.frame.size.width, headerView.frame.size.height) inView:headerView];
    headerCell.alignment = NSTextAlignmentCenter;
    
    [self setHeaderView:headerView];
    [column setHeaderCell:headerCell];
    
}

- (id)makeViewWithIdentifier:(NSString *)identifier owner:(id)owner {
    id view = [super makeViewWithIdentifier:identifier owner:owner];
    
    if ([identifier isEqualToString:NSOutlineViewDisclosureButtonKey]) {
        // Do your customization
        
        [(NSButton *)view setImage:[NSImage imageNamed:@"disclosure-right"]];
        [(NSButton *)view setAlternateImage:[NSImage imageNamed:@"disclosure-down"]];
    }
    
    return view;
}

- (void) selectRowIndex:(NSUInteger) row {
    [self selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
}

@synthesize selectedObject = _selectedObject;


- (id) selectedObject
{
    id anObject;
    NSInteger selectedRow = [self selectedRow];
    if (selectedRow == -1) return nil;
    anObject = [self itemAtRow:selectedRow];
    return anObject;
}

- (void) setSelectedObject:(id)anObject {
    if (anObject) {
        [self expandItemAndParentItems:anObject];
        NSInteger objectRow = [self rowForItem:anObject];
        if (objectRow != -1) {
            [self selectRowIndex:objectRow];
            _selectedObject = anObject;
            [self scrollRowToVisible:objectRow];
        }
    }
    else{
        _selectedObject = nil;
    }
}
- (void) deselectAllRows {
    self.selectedObject = nil;
    [self selectRowIndexes:[NSIndexSet indexSet] byExtendingSelection:NO];
}

- (void) selectObject:(id)anObject {
    [self setSelectedObject:anObject];
}


- (void) expandItemAndParentItems:(nullable id)item {
    if ([item isKindOfClass:[SetList class]]){
        [self expandItem:item];
    }
    else if ([item isKindOfClass:[Shot class]]){
        id setListItem = [item setlist];
        [self expandItem:setListItem];
        if ([[item parent] isNotEqualTo:setListItem]) {
            [self expandItem:[item parent]];
        }
    }
}

- (void) reloadData {
    
    if (!_isReloadingData){
        _isReloadingData = YES;
        id selectedObject = [self selectedObject];
        [super reloadData];
        if (selectedObject) [self selectObject:selectedObject];
        _isReloadingData = NO;
    }
}


- (id) itemForView:(NSView*)view {
    id returnItem = nil;
    
    NSUInteger row = [self rowForView:view];
    if (row == NSNotFound) return nil;
    
    returnItem = [self itemAtRow:row];
    return returnItem;
}

- (void) insertItemAtIndex:(NSUInteger)index inParent:(id)parent withAnimation:(NSTableViewAnimationOptions)animationOptions {
    [self insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:index] inParent:parent withAnimation:animationOptions];
}

-(NSMenu*)menuForEvent:(NSEvent*)evt
{
    NSLog(@"menuForEvent %@ %@",self, [self delegate]);
    NSPoint pt = [self convertPoint:[evt locationInWindow] fromView:nil];
    int row=[self rowAtPoint:pt];
    id obj = [self itemAtRow:row];
    // create menu ..
    NSMenu *menu = nil;
    return menu;
}


@end
