//
//  RowSelectionButton.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/10/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RowSelectionButton : NSButton

@end
