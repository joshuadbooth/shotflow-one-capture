//
//  OutlineViewArrayController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 5/31/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "OutlineViewArrayController.h"


@implementation OutlineViewArrayController

@synthesize shouldStripDummyObjects;

- (NSString*) debugDescription {
    NSMutableString *returnString = [NSMutableString string];
    
    [returnString appendFormat:@"\n\tTotal # of Objects: %lu", (unsigned long)[self.arrangedObjects count]];
    [returnString appendFormat:@"\n\tIndex of Selected Object: %ld", (long)[self indexOfSelectedObject]];
    [returnString appendFormat:@"\n\tSelected Object: %@", [self.selectedObject name]];
    [returnString appendFormat:@"\n\tArranged Objects: "];
    
    for (id obj in self.arrangedObjects) {
        [returnString appendFormat:@"\n\t\t%lu: %@", (unsigned long)[self.arrangedObjects indexOfObject:obj], [obj name]];
    }
    
    [returnString appendString:@"\n"];
    
    return returnString;
    
    
}

//- (id) normalArrangedObjects {
//    id returnObjects = [super arrangedObjects];
//    return returnObjects;
//}

//- (id) arrangedObjects {
//    NSMutableArray *returnArray = [NSMutableArray arrayWithArray:[self normalArrangedObjects]];
//    return returnArray;
//}



@end
