//
//  AssociatedShotTitleCellView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 6/3/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "ShotTableCellView.h"

@interface AssociatedShotTitleCellView : ShotTableCellView

@property (nonatomic, weak)  Shot* topShot;

@end
