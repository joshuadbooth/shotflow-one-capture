//
//  ListViewController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "InfoViewController.h"
#import "M1TableView.h"
#import "OutlineView.h"
#import "C1ProController.h"

@interface ListViewController : NSWindowController

@property (strong) IBOutlet NSStackView *listViewStack;

@property (strong) IBOutlet NSView *headerView;
@property (strong) IBOutlet InfoViewController *infoView;
@property (strong) IBOutlet NSView *outlineViewContainer;
@property (strong) IBOutlet OutlineView *outlineView;
@property (strong) IBOutlet NSViewController *OutlineViewController;
@property (strong) IBOutlet NSView *searchView;



+ (instancetype) sharedInstance;


- (IBAction)selectPrevious:(id)sender;
- (IBAction)addShot:(id)sender;
- (IBAction)selectNext:(id)sender;

@property (weak) IBOutlet NSButton *previousBtn;
@property (weak) IBOutlet NSButton *nextBtn;

@end
