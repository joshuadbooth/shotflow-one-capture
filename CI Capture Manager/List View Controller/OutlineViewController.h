//
//  OutlineViewController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 5/2/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OutlineView.h"
#import "M1ArrayController.h"
#import "OutlineViewArrayController.h"

@class OutlineViewController, QBManagedObject;

@interface OutlineViewController : NSViewController <NSOutlineViewDelegate, NSOutlineViewDataSource, NSSearchFieldDelegate, NSTextFieldDelegate>

@property (nonatomic, strong) IBOutlet OutlineView *outlineView;
@property (strong) IBOutlet NSSearchField *searchField;
@property (nonatomic, readonly) NSString *searchString;
@property (strong) IBOutlet NSButton *filteredStatusButton;
- (IBAction)toggleStatusFilter:(id)sender;



@end
