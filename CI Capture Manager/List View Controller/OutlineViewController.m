//
//  OutlineViewController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 5/2/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "OutlineViewController.h"

#import "AppDelegate.h"
#import "QBDBInfo.h"
#import "QBSyncManager.h"
#import "QBQuery.h"
#import "QBDataExporter.h"

#import <CoreData/CoreData.h>

#import "SetsArrayController.h"
#import "SetListsArrayController.h"
#import "ShotsArrayController.h"

#import "Set.h"
#import "SetList.h"
#import "Shot.h"
#import "DummyObject.h"

#import "OutlineView.h"
#import "M1TableRowView.h"
#import "SetListTableCellView.h"
#import "ShotTableCellView.h"
#import "AddShotCellView.h"


#import "QBTable.h"
#import "QBField.h"

#import "C1ProController.h"


#pragma mark -


@interface OutlineViewController () <NSTextDelegate>

@end

typedef void (^completionBlock) (void);


@implementation OutlineViewController  {
    
    AppDelegate *app;
    NSManagedObjectContext *mainMOC;
    NSManagedObjectContext *bgMOC;
    
    QBDataExporter *exporter;
    C1ProController *C1PC;
    QBDBInfo *QBinfo;
    
    M1ArrayController *setListsArrayController;
    M1ArrayController *topShotItemsArrayController;
    M1ArrayController *subShotItemsArrayController;
    
   
    
    NSPredicate *searchFieldPredicate;
    
    BOOL runOnce;
    BOOL isAwake;
    BOOL didRegisterObservations;
    
    __block BOOL isAddingShot;
    __block BOOL isPopulating;
    __block BOOL isBuilding;
    __block BOOL isUpdating;
    NSMutableDictionary *statusFilters;
}


@synthesize outlineView;


static void * OutlineViewContext = @"OutlineViewContext";

- (void) awakeFromNib {
    [super awakeFromNib];
    if (!isAwake)
    {
        isAwake = YES;
        isAddingShot = NO;
        
        app = [AppDelegate sharedInstance];
        mainMOC = app.mainMOC;
        C1PC = [C1ProController sharedInstance];
        exporter = [QBDataExporter sharedExporter];
        
        QBinfo = [QBDBInfo info];
        
        [[SetListsArrayController sharedController] addObserver:self
                                                     forKeyPath:@"arrangedObjects"
                                                        options:NSKeyValueObservingOptionNew
                                                        context:OutlineViewContext];
        
        //setListsArrayController = [SetListsArrayController sharedController];
        
        setListsArrayController = [[M1ArrayController alloc] init];
        [setListsArrayController setManagedObjectContext:mainMOC];
        [setListsArrayController setObjectClass:[SetList class]];
        [setListsArrayController bind:@"contentSet" toObject:app.selectedSet withKeyPath:@"setlists" options:@{NSAllowsNullArgumentBindingOption : [NSNumber numberWithBool:YES]}];
        setListsArrayController.sortDescriptors = @[app.sortByPriority, app.sortByName];
        //[setListsArrayController bind:@"filterPredicate" toObject:app withKeyPath:@"filteredChildrenPredicate" options:nil];
        //[setListsArrayController setAutomaticallyPreparesContent:YES];
        [setListsArrayController setAvoidsEmptySelection:NO];
        [setListsArrayController setSelectsInsertedObjects:NO];
        
        
        
        topShotItemsArrayController = [[M1ArrayController alloc] init];
        [topShotItemsArrayController setManagedObjectContext:mainMOC];
        [topShotItemsArrayController setObjectClass:[Shot class]];
        
        topShotItemsArrayController.sortDescriptors = @[app.sortByRecordID];
        [topShotItemsArrayController bind:@"filterPredicate" toObject:app withKeyPath:@"filteredChildrenPredicate" options:nil];
        [topShotItemsArrayController setAutomaticallyPreparesContent:YES];
        [topShotItemsArrayController setAvoidsEmptySelection:NO];
        [topShotItemsArrayController setSelectsInsertedObjects:NO];
        
        
        
        subShotItemsArrayController = [[M1ArrayController alloc] init];
        [subShotItemsArrayController setManagedObjectContext:mainMOC];
        [subShotItemsArrayController setObjectClass:[Shot class]];
        
        subShotItemsArrayController.sortDescriptors = @[app.sortByRecordID];
        [subShotItemsArrayController bind:@"filterPredicate" toObject:app withKeyPath:@"searchFieldPredicate" options:nil];
        [subShotItemsArrayController setAutomaticallyPreparesContent:YES];
        [subShotItemsArrayController setAvoidsEmptySelection:NO];
        [subShotItemsArrayController setSelectsInsertedObjects:NO];
        
        
        
        statusFilters = [NSMutableDictionary dictionary];
        [statusFilters setObject:[NSNumber numberWithBool:app.hideCompleted] forKey:@"hideCompleted"];
        [statusFilters setObject:[NSNumber numberWithBool:app.hideInProgress] forKey:@"hideInProgress"];
        [statusFilters setObject:[NSNumber numberWithBool:app.hideReady] forKey:@"hideReady"];
        
        
        if (app.hideCompleted || app.hideInProgress || app.hideReady) {
            [_filteredStatusButton setState:1];
        }
        else
        {
            [_filteredStatusButton setState:0];
        }
        
        self.searchField.delegate = self;
        
        //        [outlineView reloadData];
        [self registerObservations:nil];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(registerObservations:)
                                                 name:kFinishedSyncNotification
                                               object:nil];
    //    [self registerObservations:nil];
    
    if (searchFieldPredicate == nil) searchFieldPredicate = [NSPredicate predicateWithValue:YES];
}

- (void)viewWillAppear
{
    [super viewWillAppear];
    
}


- (void) registerObservations:(NSNotification*) notification {
    
    if (!didRegisterObservations) {
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kFinishedSyncNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(processUpdates:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:mainMOC];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(processSelection:)
                                                     name:kSelectionDidChangeNotification
                                                   object:nil];
        
        // notification to add a shot
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(addShot:)
                                                     name:kAddShotNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFilterButtonState:) name:kStatusFilterChangedNotification object:app];
        
        didRegisterObservations = YES;
    }
}

- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFinishedSyncNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAddShotNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:mainMOC];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextObjectsDidChangeNotification object:mainMOC];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kStatusFilterChangedNotification object:nil];
}


#pragma mark - Handling Updates

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if (context == OutlineViewContext) {
        //[setListsArrayController rearrangeObjects];
        [outlineView reloadData];
    }
}


- (void) processUpdates:(NSNotification *)note {
    if (![NSThread isMainThread]) return;
    if (isAddingShot) return;
    [outlineView reloadData];
    return;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSSet* insertedObjects = [[note.userInfo objectForKey:NSInsertedObjectsKey]
                                  filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"(class == %@) OR (class == %@)", [SetList class], [Shot class]]];
        NSSet* updatedObjects = [[note.userInfo objectForKey:NSUpdatedObjectsKey]
                                 filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"(class == %@) OR (class == %@)", [SetList class], [Shot class]]];
        NSSet* deletedObjects = [[note.userInfo objectForKey:NSDeletedObjectsKey]
                                 filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"(class == %@) OR (class == %@)", [SetList class], [Shot class]]];
        
        
        
        
        NSArray *setlistsToInsert = [[[insertedObjects.allObjects
                                      filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [SetList class]]]
                                     sortedArrayUsingDescriptors:@[app.sortByPriority, app.sortByName]] valueForKeyPath:@"objectID"];
        
        NSArray *setlistsToUpdate = [[[updatedObjects.allObjects
                                      filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [SetList class]]]
                                     sortedArrayUsingDescriptors:@[app.sortByPriority, app.sortByName]] valueForKeyPath:@"objectID"];
        
        NSArray *setlistsToRemove = [[[deletedObjects.allObjects
                                      filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [SetList class]]]
                                     sortedArrayUsingDescriptors:@[app.sortByPriority, app.sortByName]] valueForKeyPath:@"objectID"];
        
        
        
        
        NSArray *shotsToInsert = [[[insertedObjects.allObjects
                                   filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [Shot class]]]
                                  sortedArrayUsingDescriptors:@[app.sortByRecordID]] valueForKeyPath:@"objectID"];
        
        NSArray *shotsToUpdate = [[[updatedObjects.allObjects
                                   filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [Shot class]]]
                                  sortedArrayUsingDescriptors:@[app.sortByRecordID]] valueForKeyPath:@"objectID"];
        
        NSArray *shotsToRemove = [[[deletedObjects.allObjects
                                   filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"class == %@", [Shot class]]]
                                  sortedArrayUsingDescriptors:@[app.sortByRecordID]] valueForKeyPath:@"objectID"];
        
        
        // **** Perform Updates **** //
        
        
        [outlineView beginUpdates];
        
        // **********************************************
        // Set Lists
        // **********************************************
        
        
        // SetList Removal
        for (SetList *setList in setlistsToRemove) {
            NSMutableArray *setlists = [NSMutableArray arrayWithArray:[app.selectedSet.setlists sortedArrayUsingDescriptors:@[app.sortByPriority, app.sortByName]]];
            [setlists addObject:setList];
            [setlists sortUsingDescriptors:@[app.sortByPriority, app.sortByName]];
            
            NSInteger removalIndex = [setlists indexOfObject:setList];
            [outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:removalIndex] inParent:nil withAnimation:NSTableViewAnimationEffectGap];
            setlists = nil;
        }
        
        // SetList Insertions
        for (SetList *setList in setlistsToInsert)
        {
            if (setList == nil) continue;
            
            NSArray *setlists = [app.selectedSet.setlists sortedArrayUsingDescriptors:@[app.sortByPriority, app.sortByName]];
            NSInteger insertionIndex = [setlists indexOfObject:setList];
            [outlineView insertItemAtIndex:insertionIndex inParent:nil withAnimation:NSTableViewAnimationSlideLeft];
        }
        
        // SetList Updates | removal, insertion, & moving (based on status)
        for (SetList *setList in setlistsToUpdate)
        {
            if (setList == nil) continue;
            NSArray *setlists = [setListsArrayController arrangedObjects];
            
            if (![app.filteredChildrenPredicate evaluateWithObject:setList]) {
                // if doesn't meet filteredChildrenPredicate, remove
                //NSInteger removalIndex = [setlists indexOfObject:setList];
                NSInteger removalIndex = [self currentIndexForItem:setList];
                
                [outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:removalIndex] inParent:nil withAnimation:NSTableViewAnimationEffectGap];
            }
            else
            {
                // check to see if needs to be re-added
                if ([outlineView rowForItem:setList] == -1)
                {
                    NSInteger insertionIndex = [setlists indexOfObject:setList];
                    if (insertionIndex == NSNotFound) {
                        insertionIndex = [setlists indexOfObject:setList];
                    }
                    [outlineView insertItemAtIndex:insertionIndex inParent:nil withAnimation:NSTableViewAnimationSlideLeft];
                }
                else
                {
                    // check if needs to be moved
                    NSInteger currentIndex = [self currentIndexForItem:setList];
                    NSInteger proposedIndex = [self proposedIndexForItem:setList];
                    
                    if (currentIndex != proposedIndex) {
                        [outlineView moveItemAtIndex:currentIndex inParent:nil toIndex:proposedIndex inParent:nil];
                    }
                }
                
                
            }
        }
        
        // general reordering of SetLists
        for (SetList *setList in [setListsArrayController.arrangedObjects filteredArrayUsingPredicate:app.filteredChildrenPredicate])
        {
            // check if needs to be moved
            NSInteger currentIndex = [self currentIndexForItem:setList];
            NSInteger proposedIndex = [setListsArrayController.arrangedObjects indexOfObject:setList];
            
            if (currentIndex != proposedIndex) {
                [outlineView moveItemAtIndex:currentIndex inParent:nil toIndex:proposedIndex inParent:nil];
            }
            [outlineView reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:[outlineView rowForItem:setList]] columnIndexes:[NSIndexSet indexSetWithIndex:0]];
        }
        
        
        
        
        // **********************************************
        // Shots
        // **********************************************
        
        
        // Shot Removal
        for (Shot *shot in shotsToRemove) {
            if ([setlistsToRemove containsObject:shot.setlist]) continue;
            if ([outlineView rowForItem:shot]){
                SetList *setlist = shot.setlist;
                Shot *topShot = shot.topShot;
                id parentItem = shot.parent;
                
                NSMutableArray *siblings = [NSMutableArray array];
                if ([parentItem isKindOfClass:[SetList class]])
                {
                    [siblings addObjectsFromArray:[parentItem orderedParentedShots]];
                }
                else if ([parentItem isKindOfClass:[Shot class]])
                {
                    [siblings addObjectsFromArray:[parentItem orderedParentedLinkedShots]];
                }
                
                if (![siblings containsObject:shot]) [siblings addObject:shot];
                
                [siblings sortUsingDescriptors:@[app.sortByRecordID]];
                
                NSInteger removalIndex = [siblings indexOfObject:shot];
                [outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:removalIndex] inParent:[shot parent] withAnimation:NSTableViewAnimationSlideDown];
            }
        }
        
        // Shot Insertions
        for (Shot *shot in shotsToInsert)
        {
            
            SetList *setlist = shot.setlist;
            id parent = shot.parent;
            Shot *topShot = shot.topShot;
            
            if (![outlineView isItemExpanded:setlist]) continue;
            
            M1ArrayController *availableShots;
            
            
            if ([parent isKindOfClass:[SetList class]]) {
                //setlist = shot.setlist;
                availableShots = topShotItemsArrayController;
                [availableShots setContent:nil];
                
                [availableShots addObjects:setlist.orderedParentedShots];
            }
            else {
                //setlist = [shot.parent setlist];
                availableShots = subShotItemsArrayController;
                [availableShots setContent:nil];
                [availableShots addObjects:[parent orderedParentedShots]];
            }
            
            if (![availableShots.arrangedObjects containsObject:shot]) [availableShots addObject:shot];
            
            [availableShots rearrangeObjects];
            
            NSInteger insertionIndex = [availableShots indexOfObject:shot];
            [outlineView insertItemAtIndex:insertionIndex inParent:parent withAnimation:NSTableViewAnimationSlideLeft];
        }
        
        // Shot Updates | removal, insertion, & moving (based on status)
        for (Shot *shot in shotsToUpdate)
        {
            // Getting SetList & Parent variables
            M1ArrayController *availableShots;
            SetList *setlist = shot.setlist;
            Shot *topShot = shot.topShot;
            id parent = shot.parent;
            
            id ovParentForItem = [outlineView parentForItem:shot];
            id ovParentForParentItem = [outlineView parentForItem:parent];
            
            //if ([parent isKindOfClass:[SetList class]])
            if (topShot == nil)
            {
                availableShots = topShotItemsArrayController;
                [availableShots setContent:nil];
                [availableShots setContent:[NSArray arrayWithArray:setlist.orderedParentedShots]];
            }
            else
            {
                availableShots = subShotItemsArrayController;
                [availableShots setContent:nil];
                [availableShots setContent:[NSArray arrayWithArray:[topShot orderedParentedLinkedShots]]];
            }
            
            if (![availableShots containsObject:shot])
            {
                [availableShots addObject:shot];
            }
            
            // **** Quick Checks **** //
            
            // if setlist was removed, nothing needs to happen.
            if ([setlistsToRemove containsObject:setlist]) continue;
            
            // if parent is collapsed, nothing needs to happen.
            //if (![outlineView isItemExpanded:parent]) continue;
            
            
            // **** Further Evaluation of Shot to determine actions **** //
            
            if (![app.filteredChildrenPredicate evaluateWithObject:shot]) {
                // if doesn't meet filteredChildrenPredicate, remove
                NSInteger removalIndex = [outlineView childIndexForItem:shot];
                if ([outlineView childIndexForItem:parent] && removalIndex != NSNotFound)
                {
                    // if parent exists & removalIndex is available...
                    [outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:removalIndex] inParent:parent withAnimation:NSTableViewAnimationEffectGap];
                }
            }
            else
            {
                // Shot meets filtering conditions.
                // check to see if needs to be re-inserted if parent is expanded
                if ([outlineView rowForItem:shot] == -1)
                {
                    if ([outlineView rowForItem:parent] != -1 && [outlineView isItemExpanded:parent])
                    {
                        if (![availableShots containsObject:shot]) [availableShots addObject:shot];
                        NSInteger insertionIndex = [availableShots indexOfObject:shot];
                        [outlineView insertItemAtIndex:insertionIndex inParent:parent withAnimation:NSTableViewAnimationSlideLeft];
                    }
                }
                else
                {
                    //TODO: FIX THIS: MOC weirdness
                    // check if needs to be moved
                    
                    if ([shot.setlist isNotEqualTo:parent])
                    {
                        // availableShots = [shot.setlist orderedParentedShots];
                        NSUInteger currentIndex = [outlineView childIndexForItem:shot];
                        NSUInteger proposedIndex = [availableShots indexOfObject:shot];
                        
                        if (proposedIndex != NSNotFound)
                            [outlineView moveItemAtIndex:currentIndex
                                                inParent:parent
                                                 toIndex:proposedIndex
                                                inParent:[[availableShots.arrangedObjects firstObject] parent]];
                    }
                    
                    //[outlineView reloadItem:shot];
                }
            }
        }
        
        
        
        
        
        
        [outlineView endUpdates];
        
        
        
        //});
        //}];
        
    });
    
    
}



- (void) mainMOCDidSave:(NSNotification*)note {
    
    
}


#pragma mark - Adding Shots

- (void) addShot:(NSNotification *)notif
{
    if (isAddingShot) return;
    isAddingShot = YES;
    DDLogVerbose(@"addShotNotification: %@", notif.object);
    
    NSUInteger senderRow = [outlineView rowForView:notif.object];
    DummyObject *dummyObj = [outlineView itemAtRow:senderRow];
    SetList *dummySetList = ([dummyObj.parent isKindOfClass:[SetList class]])? dummyObj.parent : [dummyObj.parent setlist];
    
    [self setContext];
    [bgMOC performBlock:^{
        
        id parent = [dummyObj.parent findInManagedObjectContext:bgMOC error:nil];
        
        SetList *parentSetList = [SetList existingWithObjectID:dummySetList.objectID inContext:bgMOC];
        
        Shot *newShot;
        
        BOOL sampleMode = parentSetList.sampleMode;
        BOOL isAssociated = [parent isKindOfClass:[Shot class]] || [parent isKindOfClass:[DummyObject class]];
        
        /***** Add Shot to MOC *****/
        newShot = [Shot insertNewObjectIntoContext:bgMOC];
        
        newShot.title = @"New Shot";
        
        newShot.date_created = [NSDate date];
        newShot.status = @"Ready";
        
        if (sampleMode) newShot.title = @"Scan Bar Code";
        if (sampleMode) newShot.sampleDataDownloaded = NO;
        if (isAssociated) newShot.title = [[parent name] stringByAppendingString:QBinfo.defaultSeparator];
        
        newShot.record_id = [[QBDBInfo info] getLatestRecordIDforTable:@"Shots" orEntity:nil inMOC:bgMOC isTemporary:YES];
        [newShot setParam:newShot.record_id forKey:@"Record ID#"];
        newShot.numberOfCaptures = 0;
        [newShot setParam:@"Ready" forKey:@"Status"];
        newShot.isComplete = 0;
        newShot.online = NO;
        
        if (isAssociated)
        {
            
            id topShotItem = [parent isKindOfClass:[DummyObject class]]? [outlineView child:0 ofItem:parent] : parent;
            Shot *parentShot = [Shot existingWithObjectID:[topShotItem objectID] inContext:bgMOC];
            
            newShot.topShot = parentShot;
            newShot.linkedShotRecordID = parentShot.record_id;
            [newShot setParam:parentShot.record_id forKey:@"Linked Shot"];
            
            if (parentShot.naming_convention.length > 0){
                newShot.naming_convention = [parentShot.naming_convention stringByAppendingString:QBinfo.defaultSeparator];
            }
            
            [newShot linkAssociatedShots];
            [newShot addProductdatarecords:parentShot.productdatarecords];
            [newShot addSamples:parentShot.samples];
            [newShot setParam:[[parentShot params]valueForKey:@"Shot Type"] forKey:@"Shot Type"];
        }
        
        newShot.setlist = parentSetList;
        
        [newShot setParam:newShot.setlist.primaryKeyValue forKey:[newShot.parentTable fieldNameForMastagTableNamed:@"Set Lists"]];
        newShot.dateCompleted = nil;
        
        /***** Saving & Exporting *****/
        [bgMOC save:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            id uiParent;
            if (![parent isKindOfClass:[DummyObject class]]) {
                uiParent = [parent findInManagedObjectContext:mainMOC error:nil];
            }
            else {
                uiParent = parent;
            }
            
            Shot *uiShot = [Shot existingWithObjectID:newShot.objectID inContext:mainMOC];
            //TODO fix this
            NSInteger insertionIndex = [[uiParent filteredChildren] indexOfObject:uiShot];
            
            [outlineView insertItemAtIndex:insertionIndex
                                  inParent:uiParent
                             withAnimation:NSTableViewAnimationSlideLeft];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                app.selectedShot = uiShot;
                [[NSNotificationCenter defaultCenter]postNotificationName:kSelectionDidChangeNotification
                                                                   object:uiShot.objectID
                                                                 userInfo:@{@"sender" : @"none"}];
                
                [outlineView editColumn:0
                                    row:[outlineView rowForItem:uiShot]
                              withEvent:nil
                                 select:YES];
                isAddingShot = NO;
            });
            
        });
    }];
}


- (NSString*) associatedGroupName:(Shot*)shot {
    Shot *topShot = [shot topShot];
    NSArray *array = [topShot orderedLinkedShots];
    
    NSArray *titlesArray = [array valueForKeyPath:@"title"];
    NSString *longestString = [NSString string];
    NSString *shortestString = [titlesArray firstObject];
    
    
    for (int i=1; i <= shortestString.length; i++) {
        NSString *testString = [shortestString substringToIndex:i];
        BOOL shouldBreak = false;
        for (NSString *title in titlesArray) {
            if ([[title lowercaseString] rangeOfString:[testString lowercaseString]].location == NSNotFound) {
                shouldBreak = true;
                break;
            }
        }
        if (shouldBreak) break;
        NSMutableCharacterSet *characterSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
        [characterSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
        longestString = [testString stringByTrimmingCharactersInSet:characterSet];
    }
    if (longestString.length <= 3) longestString = [topShot name];
    return longestString;
}

#pragma mark - Editing

- (void) controlTextDidChange:(NSNotification *)note {
    
    if  ([note.object isEqualTo:self.searchField]) {
        [self updateFilterPredicate];
        //[self filterOutline];
    }
    
}

- (void) controlTextDidEndEditing:(NSNotification *)obj {
    
    id sender = obj.object;
    if ([sender isEqualTo:self.searchField]) {
        return;
    }
    
    
    NSInteger senderRow = [outlineView rowForView:sender];
    if (senderRow == NSNotFound || senderRow == -1) return;
    NSView *view = [outlineView viewAtColumn:0 row:senderRow makeIfNecessary:NO];
    Shot *uiShot = [outlineView itemAtRow:senderRow];
    
    NSString *newValue = [[[view subviews]firstObject]stringValue];
    
    __block NSMutableArray *lookupValues = [NSMutableArray array];
    [self setContext];
    [bgMOC performBlock:^{
        
        Shot *shot = [Shot existingWithObjectID:uiShot.objectID inContext:bgMOC];
        
        /***** Sample Mode? *****/
        BOOL sampleMode = shot.setlist.sampleMode;
        if (!sampleMode || shot.sampleDataDownloaded){
            shot.title = newValue;
        }
        else
        {
            NSArray *barcodeValues = [newValue componentsSeparatedByString:@","];
            
            [barcodeValues enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString *val = [obj stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if (val.length > 0 && ![lookupValues containsObject:val]) [lookupValues addObject:val];
            }];
            
            
            //[lookupValues sortUsingSelector:@selector(compare:)];
            shot.title = [NSString stringWithFormat:@"**%@", [lookupValues componentsJoinedByString:QBinfo.defaultSeparator]];
        }
        
        [shot setDate_modified:[NSDate date]];
        
        if ([bgMOC save:nil]){
            [mainMOC performBlock:^{
                [mainMOC save:nil];
                Shot *mainMOCShot = [shot findInManagedObjectContext:mainMOC error:nil];
                [C1PC changeToShot:mainMOCShot];
                [exporter postToServer:mainMOCShot onComplete:^{
                    if (sampleMode && !mainMOCShot.sampleDataDownloaded) {
                        
                        [mainMOCShot processSampleMode:^{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                Shot *postShot = [Shot existingWithObjectID:mainMOCShot.objectID inContext:mainMOC];
                                C1PC.activeShot = nil;
                                [C1PC changeToShot:postShot];
                                [exporter postToServer:postShot  onComplete:^{
                                    //[C1PC changeToShot:postShot];
                                }];
                                app.selectedShot = postShot;
                                [[NSNotificationCenter defaultCenter]postNotificationName:kSelectionDidChangeNotification object:postShot.objectID userInfo:@{@"sender" : self}];
                                [[NSNotificationCenter defaultCenter]postNotificationName:kInfoDownloadedForShotNotification object:postShot];
                            });
                        }];
                    }
                }];
            }];
        };
    }];
    
}

#pragma mark - Handling Selection Notifications
- (void) processSelection:(NSNotification*) note {
    
    id sender = [note.userInfo valueForKey:@"sender"];
    if ([sender isEqualTo:self]) return;
    
    id targetObjectID = note.object;
    id targetObject;
    
    
    if ([targetObjectID isKindOfClass:[DummyObject class]])
    {
        targetObject = [targetObjectID parent];
    }
    else
    {
        targetObject = [mainMOC existingObjectWithID:targetObjectID error:nil];
    }
    
    
    if ([targetObject isKindOfClass:[SetList class]]) {
        if ([outlineView rowForItem:targetObject])
        {
            [outlineView selectObject:targetObject];
            [outlineView expandItem:targetObject];
        }
    }
    else
    {
        [outlineView selectObject:targetObject];
    }
    [outlineView scrollRowToVisible:[outlineView selectedRow]];
}

#pragma mark - NSOutlineView Datasource Methods

- (NSInteger) outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
    NSInteger itemCount;
    NSArray *children;
    
    if (!item)
    {
        children = setListsArrayController.arrangedObjects;
    }
    else
    {
        children = [item children];
        
    }
    
    itemCount = [[children filteredArrayUsingPredicate:app.filteredChildrenPredicate] count];
    return itemCount;
}

- (id) outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    
    id returnChild;
    NSArray* children;
    
    if (item == nil)
    {
        children =  setListsArrayController.arrangedObjects;
    }
    else
    {
        children = [item children];
    }
    
    returnChild = [[children filteredArrayUsingPredicate:app.filteredChildrenPredicate] objectAtIndex:index];
    return returnChild;
}

- (id) outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
    return item;
}

- (BOOL) outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    NSArray *children = [[item children] filteredArrayUsingPredicate:app.filteredChildrenPredicate];
    return (!item) ?  YES : (children.count > 0);
}


- (NSInteger) proposedIndexForItem:(id)sourceItem {
    // most recent
    
    id parentItem = [sourceItem parent];
    
    NSMutableSet *siblingItems = nil;
    NSArray *sortedSiblings;
    
    if ([sourceItem isKindOfClass:[SetList class]])
    {
        siblingItems = [NSMutableSet setWithArray: setListsArrayController.arrangedObjects];
    }
    else
    {
        siblingItems = ([parentItem isKindOfClass:[SetList class]])? [NSMutableSet setWithArray:[parentItem orderedParentedShots]] : [NSMutableSet setWithArray:[parentItem orderedParentedSubShots]];
    }
    
    
    [siblingItems  addObject:sourceItem];
    
    if ([sourceItem isKindOfClass:[SetList class]])
    {
        sortedSiblings = [siblingItems sortedArrayUsingDescriptors: @[app.sortByPriority, app.sortByName]];
    }
    else if ([sourceItem isKindOfClass:[Shot class]])
    {
        if (![sourceItem isLinked] || [sourceItem isEqualTo:[sourceItem topShot]])
        {
            sortedSiblings = [siblingItems sortedArrayUsingDescriptors:@[app.shotSorting]];
        }
        else
        {
            sortedSiblings = [siblingItems sortedArrayUsingDescriptors:@[app.subShotSorting]];
        }
    }
    
    
    NSInteger childIndex = [sortedSiblings indexOfObject:sourceItem];
    
    return childIndex;
}

- (NSInteger) currentIndexForItem:(id)sourceItem {
    NSInteger returnValue;
    id parentItem = [outlineView parentForItem:sourceItem];
    
    NSInteger sourceItemRow = [outlineView rowForItem:sourceItem];
    if (sourceItemRow == -1) return -1;
    
    
    
    NSInteger numberOfChildren = [outlineView numberOfChildrenOfItem:parentItem];
    NSMutableArray *rowsArray = [NSMutableArray array];
    
    for (NSInteger i=0; i<numberOfChildren; i++) {
        id childItem = [outlineView child:i ofItem:parentItem];
        NSInteger childRow = [outlineView rowForItem:childItem];
        if (childRow != -1){
            [rowsArray addObject:[NSNumber numberWithInteger:childRow]];
        }
    }
    [rowsArray sortUsingSelector:@selector(compare:)];
    // sort?
    returnValue = [rowsArray indexOfObjectPassingTest:^BOOL(NSNumber*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return obj.integerValue == sourceItemRow;
    }];
    
    if (returnValue == NSNotFound) {
        rowsArray = (NSMutableArray*)[app.selectedSet.setlists sortedArrayUsingDescriptors:@[app.sortByPriority, app.sortByName]];
        returnValue = [rowsArray indexOfObject:sourceItem];
    }
    
    return returnValue;
}

#pragma mark - NSOutlineView Delegate Methods
// -------------------------------------------------------------------------------
//	viewForTableColumn:tableColumn:item
// -------------------------------------------------------------------------------
- (BOOL) outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    
    if (![item isKindOfClass:[Shot class]])
    {
        return NO;
    }
    else
    {
        return [(Shot*)item sampleDataDownloaded]? NO : YES;
    }
}

- (BOOL) textShouldBeginEditing:(NSText *)textObject {
    
    return YES;
}

- (NSView *)outlineView:(NSOutlineView *)outline viewForTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    
    
    if ([item isKindOfClass:[DummyObject class]]) {
        AddShotCellView *cellView = [outline makeViewWithIdentifier:@"AddShotCellView" owner:outline];
        
        cellView.dummyobj = (DummyObject*) item;
        [cellView setNeedsDisplay:YES];
        return cellView;
    }
    else if ([item isKindOfClass:[SetList class]]) {
        
        //SetList *setList = (SetList*) item;
        SetListTableCellView *cellView = [outline makeViewWithIdentifier:@"SetListCellView" owner:outline];
        
        [cellView bind:@"setlist" toObject:(SetList*)item withKeyPath:@"self" options:nil];
       
        [cellView setupCellView];
        
        //[cellView assignSetList: setList];
        cellView.textField.editable = NO;
        cellView.textField.delegate = self;
        [cellView setNeedsDisplay:YES];
        return cellView;
    }
    else if ([item isKindOfClass:[Shot class]]) {
        
        
        ShotTableCellView *cellView = [outline makeViewWithIdentifier:@"ShotCellView" owner:outline];
        
        [cellView bind:@"shot" toObject:(Shot*)item withKeyPath:@"self" options:nil];
        //[cellView.textField bind: NSValueBinding toObject:cellView withKeyPath: @"objectValue.params.Title" options: nil];
        cellView.textField.delegate = self;
        [cellView setupCellView];
        
        // cellView.textField.stringValue = [shot title];
        //cellView.shot = shot;
        //cellView.setlist = parentSetList;
        cellView.textField.editable = YES;
        [cellView setNeedsDisplay:YES];
        return cellView;
    }
    
    else {
        NSTableCellView *cellView = [outline makeViewWithIdentifier:@"BasicTableCellView" owner:outline];
        cellView.textField.stringValue = @"";
        cellView.textField.editable = NO;
        return cellView;
    }
}



- (void)outlineViewSelectionDidChange:(NSNotification *)note {
    id selectedObject = outlineView.selectedObject;
    [outlineView expandItem:selectedObject];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)selectedObj
{
    if ([selectedObj isKindOfClass:[DummyObject class]])  {
        return NO;
    }
    else {
        
        if ([selectedObj isKindOfClass:[SetList class]])
        {
            app.selectedShot = nil;
            app.selectedSetList = (SetList*) selectedObj;
            setListsArrayController.selectedObject = selectedObj;
            
            [C1PC changeToShot:nil];
        }
        else if ([selectedObj isKindOfClass:[Shot class]])
        {
            app.selectedShot = selectedObj;
            app.selectedSetList = [selectedObj setlist];
            setListsArrayController.selectedObject = app.selectedSetList;
            
            [C1PC changeToShot:selectedObj];
        }
        else if (selectedObj == nil) {
            DDLogVerbose(@"No item selected");
            app.selectedShot = nil;
            app.selectedSetList = nil;
            [C1PC changeToShot:nil];
        }
        else
        {
            DDLogVerbose(@"SelectedItem is not SetList or Shot");
            app.selectedShot = nil;
            app.selectedSetList = nil;
            
            [C1PC changeToShot:nil];
        }
        
        // post selection notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kSelectionDidChangeNotification
                                                            object:(!selectedObj)? nil : [selectedObj objectID]
                                                          userInfo:@{@"sender" : self}];
        
        return YES;
    }
}

-(CGFloat) outlineView:(NSOutlineView *)outlineView heightOfRowByItem:(id)item {
    return 26.0;
}

- (NSTableRowView*) outlineView:(NSOutlineView *)outlineView rowViewForItem:(id)obj {
    
    
    
    M1TableRowView *rowView;
    
    
    if ([obj isKindOfClass:[DummyObject class]] && [[obj parent] isKindOfClass:[Shot class]])
    {
        rowView = [[M1TableRowView alloc] init];
        [rowView resetDefaults];
        rowView.isAssociated = YES;
        rowView.fillAlpha = .05;
        
    }
    else if ([obj isKindOfClass:[Shot class]] && [obj isLinked]) {
        
        rowView = [[M1TableRowView alloc] init];
        [rowView resetDefaults];
        rowView.isAssociated = YES;
        rowView.fillAlpha = .05;
        
    }
    else
    {
        rowView = [[M1TableRowView alloc] init];
        [rowView resetDefaults];
    }
    //NSLog(@"rowView for %@ returned.", [obj name]);
    return rowView;
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
    // don't allow empty node names
    //return (fieldEditor.string.length == 0 ? NO : YES);
    return YES;
}


#pragma mark - Searching / Filtering
- (void) updateFilterButtonState:(NSNotification*) note {
    if (app.hideCompleted || app.hideInProgress || app.hideReady) {
        [_filteredStatusButton setState:1];
    }
    else
    {
        [_filteredStatusButton setState:0];
    }
}

- (IBAction)toggleStatusFilter:(NSButton*)sender {
    NSInteger buttonState = [sender state];
    
    if (buttonState == 0)
    {
        [statusFilters setObject:[NSNumber numberWithBool:app.hideCompleted] forKey:@"hideCompleted"];
        [statusFilters setObject:[NSNumber numberWithBool:app.hideInProgress] forKey:@"hideInProgress"];
        [statusFilters setObject:[NSNumber numberWithBool:app.hideReady] forKey:@"hideReady"];
        
        app.hideCompleted = NO;
        app.hideInProgress = NO;
        app.hideReady = NO;  
    }
    else
    {
        app.hideCompleted = [[statusFilters objectForKey:@"hideCompleted"] boolValue];
        app.hideInProgress = [[statusFilters objectForKey:@"hideInProgress"] boolValue];
        app.hideReady = [[statusFilters objectForKey:@"hideReady"] boolValue];
    }
    
    [app updateToggleMenuTitles];    
}


- (NSString*) searchString {
    return [_searchField stringValue];
}

- (void) updateFilterPredicate {
    
    if (self.searchString.length == 0)
    {
        searchFieldPredicate = [NSPredicate predicateWithValue:YES];
    }
    else
    {
        NSMutableArray *spaceSeparatedValues = [NSMutableArray array];
        NSMutableArray *allSearchStringValues = [NSMutableArray array];
        
        NSArray *commaSeparatedValues = [[self searchString] componentsSeparatedByString:@","];
        
        for (NSString *csv in commaSeparatedValues) {
            NSString *trimmedValue = [csv stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSArray *spaceSeparatedValues = [trimmedValue componentsSeparatedByString:@" "];
            for (NSString *val in spaceSeparatedValues) {
                if (val.length > 0) {
                    [allSearchStringValues addObject:val];
                }
            }
        }
        
        NSString *searchExpression = [NSString stringWithFormat:@"(?i).*(%@).*", [allSearchStringValues componentsJoinedByString:@"|"]];
 
        searchFieldPredicate = [NSPredicate predicateWithFormat:@"\
                                name MATCHES[c] %@ OR \
                                SUBQUERY(children, $child, $child.name MATCHES[c] %@ OR \
                                SUBQUERY($child.children, $subChild, $subChild.name MATCHES[c] %@).@count > 0\
                                ).@count > 0",
                                searchExpression, searchExpression, searchExpression];
        
    }
    
    app.searchFieldPredicate = searchFieldPredicate;
    
    [self filterOutline];
}




- (void) filterOutline {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [outlineView reloadData];
        if (self.searchString.length > 0) [outlineView expandItem:nil expandChildren:YES];
    });
    
    return;
    
    
    if (self.searchString.length > 0) [outlineView expandItem:nil expandChildren:YES];
    
    
    [outlineView unhideRowsAtIndexes:outlineView.hiddenRowIndexes withAnimation:NSTableViewAnimationSlideUp];
    
    NSMutableIndexSet *hideSet = [NSMutableIndexSet indexSet];
    
    [outlineView enumerateAvailableRowViewsUsingBlock:^(__kindof NSTableRowView * _Nonnull rowView, NSInteger row) {
        id rowObject = [outlineView itemAtRow:row];
        BOOL shouldShow = [app.filteredChildrenPredicate evaluateWithObject:rowObject];
        if ([rowObject isKindOfClass:[DummyObject class]]) shouldShow = YES;
        
        if (!shouldShow)
        {
            [hideSet addIndex:row];
            if (row == outlineView.selectedRow) [outlineView deselectRow:row];
        }
    }];
    [outlineView hideRowsAtIndexes:hideSet withAnimation:NSTableViewAnimationSlideDown];
    
    //[outlineView endUpdates];
}


#pragma mark - Core Data Methods
- (void) setContext {
    bgMOC = nil;
    
    bgMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [bgMOC setParentContext:mainMOC];
}

- (void) initiateSave:(completionBlock)completionBlock {
    // Save the MOC
    DDLogVerbose(@"***OV MOC Save");
    
    __block NSError *error = nil;
    if (bgMOC.hasChanges){
        if ([bgMOC save:&error]) {
            if (error != nil) {
                [[QBDBInfo info] alertMe:error];
            }
            else {
                
                [mainMOC save:nil];
                
                if (completionBlock != nil){
                    completionBlock();
                }
            }
            
            
        } else {
            DDLogError(@"TC: Error Saving mainMOC: %@", error);
        }
    }
    else
    {
        if (completionBlock != nil){
            completionBlock();
        }
        
    }
    
}


@end
