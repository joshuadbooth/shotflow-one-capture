//
//  ListViewController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "ListViewController.h"
#import "SetListsArrayController.h"
#import "ShotsArrayController.h"

@interface ListViewController ()

@end

@implementation ListViewController
@synthesize listViewStack;
@synthesize headerView;
@synthesize infoView;
@synthesize outlineViewContainer;
@synthesize outlineView;
@synthesize searchView;



#pragma mark - Singleton setup
+ (instancetype) sharedInstance {
    __strong static id _sharedInstance  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self allocForReal] initForReal];
    });
    return _sharedInstance;
}



+ (id) allocForReal {
    
    return [super allocWithZone:NULL];
}

+ (id) alloc {
    return [self sharedInstance];
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedInstance];
}

- (id) init {
    return self;
}


- (id) initForReal {
    self = [super initWithWindowNibName:@"ListViewController"];
    return self;
}

- (id) initWithWindowNibName:(NSString *)windowNibName {
    self = [super initWithWindowNibName:windowNibName];
    return self;
}

- (id) initForRealWithWindowNibName:(NSString *)windowNibName {
    self = [super initWithWindowNibName:windowNibName];
    return self;
}

- (id) initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    return self;
}

#pragma mark - windowDidLoad
- (void) awakeFromNib  {
    [super awakeFromNib];
    self.outlineView = [OutlineView sharedInstance];

    
}

- (void)windowDidLoad {
    [super windowDidLoad];
    
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    [self.window setBackgroundColor:[NSColor whiteColor]];

    
//    [listViewStack setViews:@[headerView, infoView.view, searchView, outlineViewContainer] inGravity:NSStackViewGravityBottom];
//    // we want our views arranged from top to bottom
//    listViewStack.orientation = NSUserInterfaceLayoutOrientationVertical;
//    
//    // the internal views should be aligned with their centers
//    // (although since they'll all be the same width, it won't end up mattering)
//    //
//    listViewStack.alignment = NSLayoutAttributeCenterX;
//    
//    listViewStack.spacing = 0; // No spacing between the disclosure views
//    
//    // have the stackView strongly hug the sides of the views it contains
//    [listViewStack setHuggingPriority:NSLayoutPriorityDefaultHigh forOrientation:NSLayoutConstraintOrientationHorizontal];
//    
//    // have the stackView grow and shrink as its internal views grow, are added, or are removed
//    [listViewStack setHuggingPriority:NSLayoutPriorityDefaultHigh forOrientation:NSLayoutConstraintOrientationVertical];
    
    listViewStack.spacing = 0; // No spacing between the disclosure views
    
    //self.window.contentView = listViewStack;

    
    [self.window setFrame:NSMakeRect(0.f, 0.f, 250.f, [[NSScreen mainScreen]frame].size.height) display:YES animate:YES];
    
    [self.window makeFirstResponder:outlineView];
      
}

#pragma mark - main functions

//- (void) keyDown:(NSEvent *)theEvent {
//    if (([theEvent modifierFlags] & NSCommandKeyMask) && ([theEvent keyCode] == 40)) {
//        [[C1ProController sharedInstance] takeCapture];
//    }
//}




@end
