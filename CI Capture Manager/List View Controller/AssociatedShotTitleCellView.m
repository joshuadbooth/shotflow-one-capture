//
//  AssociatedShotTitleCellView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 6/3/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "AssociatedShotTitleCellView.h"
#import "AppDelegate.h"
#import "QBDBInfo.h"

@implementation AssociatedShotTitleCellView {
    QBDataExporter *exporter;
    BOOL didRegisterObservations;
    AppDelegate *app;
    QBDBInfo *QBinfo;
    NSError *completionError;
    
}

@synthesize topShot;

- (void) awakeFromNib {
    [super awakeFromNib];
    app = [AppDelegate sharedInstance];
    QBinfo = [QBDBInfo info];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}

- (void) setupCellView {
    if (!didRegisterObservations) {
        exporter = [QBDataExporter sharedExporter];
        
        //[self.completeButton bind:@"state" toObject:shot withKeyPath:@"isComplete" options:nil];
        
        [self.textField setFont:[NSFont systemFontOfSize:12.0]];
        
    }
    
}

- (void) dealloc {
    
}

- (void) markCompleted:(id)sender {
    NSArray *filteredSubShots = [[topShot orderedParentedSubShots] filteredArrayUsingPredicate:app.uiStatusPredicate];
    NSIndexSet *shotsWithNoCaptures = [filteredSubShots indexesOfObjectsPassingTest:^BOOL(Shot*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return (obj.numberOfCaptures == 0);
    }];
    
    if (shotsWithNoCaptures.count != 0) {
        [self.completeButton setState:0];
        
        NSArray *emptyShots = [filteredSubShots objectsAtIndexes:shotsWithNoCaptures];
        NSArray *shotNames = [emptyShots valueForKeyPath:@"title"];
        NSString *shotList = [shotNames componentsJoinedByString:@"\n\t"];
        
        
        NSString *errText = @"Associated Shots without Captures";
        NSString *errDetail = [NSString stringWithFormat:@"\n\nThe shots listed below do not have any recorded captures. Please create captures for each shot and try again.\n\t%@", shotList];
        
        
        NSMutableDictionary *errorDict = [NSMutableDictionary dictionary];
        [errorDict setValue:errText forKey:NSLocalizedDescriptionKey];
        [errorDict setValue:errDetail forKey:NSLocalizedFailureReasonErrorKey];
        
        NSError *noCapturesError = [NSError errorWithDomain:@"com.shotflow1.C1Controller"
                                                       code:2001
                                                   userInfo:@{
                                                              NSLocalizedDescriptionKey : errText,
                                                              NSLocalizedFailureReasonErrorKey : errDetail
                                                              }];
        
        [QBinfo alertMe:noCapturesError];
        
    }
    else
    {
      [filteredSubShots enumerateObjectsUsingBlock:^(Shot*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
          //shot.isComplete =
      }];
    }

}


@end
