//
//  OutlineViewArrayController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 5/31/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "M1ArrayController.h"

@interface OutlineViewArrayController : M1ArrayController

@property (nonatomic, assign) BOOL shouldStripDummyObjects;
@property (nonatomic) id normalArrangedObjects;

@end
