//
//  SF1XMLRecord.m
//  CI Capture Manager
//
//  Created by Josh Booth on 8/15/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "SF1XMLRecord.h"

#import "QBTable.h"
#import "QBField.h"

@implementation SF1XMLRecord

@synthesize table;
@synthesize xmlRecord;
@synthesize recordID;
@synthesize status;
@synthesize primaryKeyValue;

- (NSString*) debugDescription {
    return [NSString stringWithFormat:@"%@ - %@", self.tag, self.text];
}


+ (SF1XMLRecord *) sf1RecordWithXMLRecord:(RXMLElement*)recordXML fromTable:(QBTable*)tbl {
    return [[SF1XMLRecord alloc] initWithXMLRecord:recordXML fromTable:tbl];
}

- (instancetype) initWithXMLRecord:(RXMLElement *)recordXML fromTable:(QBTable*)tbl {
    self = [super init];
    self.table = tbl;
    self.xmlRecord = recordXML;
    return self;
}


- (NSNumber *) recordID {
    return [NSNumber numberWithInteger:[[self attribute:@"rid"] integerValue]];
}

- (NSString *) status {
    return [self childElementForFieldNamed:@"Status"].text;
}

- (id) primaryKeyValue
{
    return [self valueForField:self.table.primaryKey];
}


- (RXMLElement *) childElementForFieldNamed:(NSString *)fnm
{
    QBField *field = [self.table fieldNamed:fnm];
    return [self childWithID:field.fid];
}

#pragma mark Value Lookups

- (id) valueForMastagTable:(NSString*)mtable {
    return [self valueForFieldNamed:[self.table fieldNameForMastagTableNamed:mtable]];
}

- (id) valueForFieldNamed:(NSString*)fnm
{
    id returnValue;
    RXMLElement *valueElement = [self childElementForFieldNamed:fnm];
    NSString *baseType = [[self.table fieldNamed:fnm] base_type];
    if ([baseType isEqualToString:@"float"])
    {
        returnValue = [NSNumber numberWithDouble:valueElement.textAsDouble];
    }
    else if ([baseType isEqualToString:@"int"])
    {
        returnValue = [NSNumber numberWithDouble:valueElement.textAsInt];
    }
    else if ([baseType isEqualToString:@"bool"])
    {
        returnValue = [NSNumber numberWithDouble:valueElement.textAsInt];
    }
    else  // return as text
    {
        returnValue = valueElement.text;
    }
    
    return returnValue;
}

- (id) valueForField:(QBField*)field
{
    return [self valueForFieldNamed:field.name];
}



#pragma mark - Override functions
- (RXMLElement *) childWithID:(NSString *)fid {
    return [xmlRecord childWithID:fid];
}

- (RXMLElement *) child:(NSString *)tag {
    return [xmlRecord child:tag];
}

- (RXMLElement *) child:(NSString *)tag inNamespace:(NSString *)ns {
    return [xmlRecord child:tag inNamespace:ns];
}

- (void) iterate:(NSString *)query usingBlock:(void (^)(RXMLElement *))blk {
    return [xmlRecord iterate:query usingBlock:blk];
}

- (NSString*) text {
    return xmlRecord.text;
}

- (NSInteger) textAsInt {
    return xmlRecord.textAsInt;
}

- (double) textAsDouble {
    return xmlRecord.textAsDouble;
}

- (NSString *)attribute:(NSString *)attributeName {
    return [xmlRecord attribute:attributeName];
}

- (NSString *)attribute:(NSString *)attributeName inNamespace:(NSString *)ns {
    return [xmlRecord attribute:attributeName inNamespace:ns];
}

- (NSString*) tag {
    return xmlRecord.tag;
}

- (NSArray *)children:(NSString *)tag {
    return [xmlRecord children:tag];
}
- (NSArray *)children:(NSString *)tag inNamespace:(NSString *)ns {
    return [xmlRecord children:tag inNamespace:ns];
}

- (NSDictionary*) recordAsDictionary {
    NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionary];
    [self iterate:@"*" usingBlock:^(RXMLElement *child) {
        if ([child.tag isEqualToString:@"f"] && [[child attributeNames]containsObject:@"id"])
        {
            QBField *field = [self.table fieldWithID:[child attribute:@"id"]];
            NSString *fieldName = field.name;
            [returnDictionary setValue:child.text forKey:fieldName];
        }
        else
        {
            [returnDictionary setValue:child.text forKey:child.tag];
        }
    }];
    return returnDictionary;
}

@end
