//
//  SF1XMLRecord.h
//  CI Capture Manager
//
//  Created by Josh Booth on 8/15/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "RXMLElement.h"
#import "RXMLElement+Supplemental.h"

@class QBTable, QBField, SF1XMLRecord;

@interface SF1XMLRecord : RXMLElement


@property (nonatomic, strong) QBTable *table;
@property (nonatomic, strong) RXMLElement *xmlRecord;
@property (nonatomic, readonly) NSNumber * recordID;
@property (nonatomic, readonly) NSString * status;
@property (nonatomic, readonly) id primaryKeyValue;

+ (SF1XMLRecord *) sf1RecordWithXMLRecord:(RXMLElement*)recordXML fromTable:(QBTable*)tbl;
- (instancetype) initWithXMLRecord:(RXMLElement*)recordXML fromTable:(QBTable*)tbl;
- (RXMLElement *) childElementForFieldNamed:(NSString *)fnm;
- (id) valueForFieldNamed:(NSString*)fnm;
- (id) valueForField:(QBField*)field;
- (id) valueForMastagTable:(NSString*)mtable;

@end
