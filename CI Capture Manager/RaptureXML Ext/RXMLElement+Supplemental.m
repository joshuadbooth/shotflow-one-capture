//
//  RXMLElement+Supplemental.m
//  CI Capture Manager
//
//  Created by Josh Booth on 2/10/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "RXMLElement+Supplemental.h"

@implementation RXMLElement (Supplemental)


- (RXMLElement*) childWithID:(NSString *)fid {
    NSArray *props = [self children:@"f"];
    
    NSUInteger elemIndex = [props indexOfObjectPassingTest:^BOOL(RXMLElement  *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSUInteger xid = [obj attributeAsInt:@"id"];
        return (xid == fid.integerValue);
        *stop = YES;
    }];
    
    if (elemIndex == NSNotFound) {
        return nil;
    }
    else {
        RXMLElement* elem = [props objectAtIndex:elemIndex];
        return elem;
    }
    
}

- (NSString*) debugDescription {
    return [NSString stringWithFormat:@"%@ - %@", self.tag, self.text];
}

- (NSDictionary*) recordAsDictionary {
    NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionary];
    [self iterate:@"*" usingBlock:^(RXMLElement *child) {
        [returnDictionary setValue:child.text forKey:child.tag];
    }];
    
    
    return returnDictionary;
}

@end
