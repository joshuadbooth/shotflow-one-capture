//
//  RXMLElement+Supplemental.h
//  CI Capture Manager
//
//  Created by Josh Booth on 2/10/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "RXMLElement.h"

@interface RXMLElement (Supplemental)



- (RXMLElement *) childWithID:(NSString *)fid;

- (NSDictionary*) recordAsDictionary;
@end
