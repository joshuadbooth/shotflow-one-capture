//
//  OutlineView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 8/6/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface OutlineView : NSOutlineView

+ (instancetype) sharedInstance;

@property (nonatomic, weak, nullable) id selectedObject;

- (void) selectRowIndex:(NSUInteger) row;
- (void) selectObject:(nonnull id)anObject;

- (void)insertItemAtIndex:(NSUInteger)index inParent:(nullable id)parent withAnimation:(NSTableViewAnimationOptions)animationOptions;
- (void) expandItemAndParentItems:(nullable id)item;
- (void) deselectAllRows;

- (id) itemForView:(NSView*)view;

@property (nonatomic, assign) BOOL isReloadingData;

@end
