//
//  M1ArrayController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 11/19/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface M1ArrayController : NSArrayController

@property (nonatomic, retain) NSManagedObjectContext *privateMOC;
@property (nonatomic, weak) id selectedObject;

- (NSInteger) indexOfSelectedObject;
- (NSInteger) indexOfObject:(id)object;
- (NSInteger) indexOfLastObject;
- (BOOL) containsObject:(id)anObject;

- (instancetype) initWithContentFromArrayController:(id)controller;
- (void) logNamesOfArrangedObjects;
- (NSArray*) namesOfArrangedObjects;
- (id) objectWithName:(NSString*)itemName;


@end
