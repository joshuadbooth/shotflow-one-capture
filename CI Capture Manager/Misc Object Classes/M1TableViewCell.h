//
//  M1TableViewCell.h
//  CI Capture Manager
//
//  Created by Josh Booth on 5/27/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface M1TableViewCell : NSTableCellView
@property (nonatomic) BOOL needsLeftBorder;
@end
