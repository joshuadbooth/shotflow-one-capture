//
//  M1HeaderCell.h
//  CI Capture Manager
//
//  Created by Josh Booth on 11/11/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface M1HeaderCell : NSTableHeaderCell

@property BOOL needsLeftBorder;
- (void)drawWithFrame:(NSRect)cellFrame
          highlighted:(BOOL)isHighlighted
               inView:(NSView *)view;

@end
