//
//  M1TableRowView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 11/13/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "M1TableRowView.h"
#import "AppDelegate.h"
#import "IconTableCellView.h"
#import "M1TableView.h"
#import "ShotTableCellView.h"
#import "DummyObject.h"

@implementation M1TableRowView {
    id iconView;
    id tableView;
    NSInteger moveCount;
}
@synthesize drawBorder;
@synthesize strokeAlpha;
@synthesize fillAlpha;
@synthesize isAssociated;
@synthesize isLast;

-(id) init {
    self = [super init];
    [self resetDefaults];
    
    return self;
}

- (void) resetDefaults {
    self.isAssociated = NO;
    drawBorder = YES;
    strokeAlpha = 0.25;
    fillAlpha = 0.25;
}

- (void)drawRect:(NSRect)dirtyRect {
    
    [super drawRect:dirtyRect];
    
}

- (NSBackgroundStyle) interiorBackgroundStyle {
    if (self.selected) {
        return NSBackgroundStyleDark;
    }
    else {
        return NSBackgroundStyleLight;
    }
}

-(void) viewDidMoveToSuperview {
    [super viewDidMoveToSuperview];
    
    moveCount++;
    
    tableView = self.superview;
    NSTableColumn *iconColumn;
    NSInteger col;
    
    if ([tableView isKindOfClass:[M1TableView class]]){
        iconColumn = [tableView tableColumnWithIdentifier:@"IconColumn"];
        if (!iconColumn) {
            iconView = nil;
            return;
        }
        col = [[tableView tableColumns] indexOfObject:iconColumn];
        iconView = [self viewAtColumn:col];
    }
}
- (void) dealloc {
    [self resetDefaults];
    
}


-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if (context == @"icons") {
        if ([object isKindOfClass:[DummyObject class]]) return;
    
        if (self.selected && iconView != nil) {
            [[iconView linkedButton] setImage:[NSImage imageNamed:@"linkedShot_alt"]];
        } else {
            [[iconView linkedButton] setImage:[NSImage imageNamed:@"linkedShot_off"]];
        }
    }
}

- (void) drawBackgroundInRect:(NSRect)dirtyRect {
    if (self.isAssociated)
    {
        [super drawBackgroundInRect:dirtyRect];
        
        NSColor *sfBlueColor = [AppDelegate sharedInstance].sfBlueColor;
        [[sfBlueColor colorWithAlphaComponent:fillAlpha] set];
        [[NSBezierPath bezierPathWithRect:dirtyRect] fill];
    }
    else
    {
        [super drawBackgroundInRect:dirtyRect];
    }
}



- (void) drawSelectionInRect:(NSRect)dirtyRect {
    
    if (self.selectionHighlightStyle != NSTableViewSelectionHighlightStyleNone) {
        
        NSColor *sfBlueColor = [AppDelegate sharedInstance].sfBlueColor;
        NSRect selectionRect = NSInsetRect(self.bounds, 2.5, 2.5);
        
        [[sfBlueColor colorWithAlphaComponent: .6] setStroke];
        [[sfBlueColor colorWithAlphaComponent: .6] setFill];
        
//        NSBezierPath *selectionPath = [NSBezierPath bezierPathWithRoundedRect:selectionRect xRadius:0 yRadius:0];
          NSBezierPath *selectionPath = [NSBezierPath bezierPathWithRoundedRect:selectionRect xRadius:4 yRadius:4];
        [selectionPath fill];
        
        if (drawBorder) {
            [selectionPath stroke];
        }
    }
}

@end
