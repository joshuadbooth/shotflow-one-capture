//
//  M1TableRowView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 11/13/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface M1TableRowView : NSTableRowView

@property BOOL drawBorder;
@property (nonatomic) float fillAlpha;
@property (nonatomic) float strokeAlpha;
@property (nonatomic) BOOL isAssociated;
@property (nonatomic) BOOL isLast;

- (void) resetDefaults;
@end
