//
//  DateValueTransformer.h
//  CI Capture Manager
//
//  Created by Josh Booth on 3/30/17.
//  Copyright © 2017 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateValueTransformer : NSValueTransformer

typedef enum
{
    kDateTypeDate,
    kDateTypeTimestamp,
    kDateTypeDuration
} kDateType;

- (id) initWithDateFormat:(NSString*)dateFormat;
- (id) initWithTargetObj:(id)obj fieldName:(NSString*)fnm;
- (id) initWithDateType:(kDateType)dateType;
- (void) dateFormatFromDateType:(kDateType)dt;
@end
