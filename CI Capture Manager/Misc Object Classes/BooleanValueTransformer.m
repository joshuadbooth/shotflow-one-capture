//
//  BooleanValueTransformer.m
//  CI Capture Manager
//
//  Created by Josh Booth on 7/28/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "BooleanValueTransformer.h"

@implementation BooleanValueTransformer
@synthesize trueValue;
@synthesize falseValue;

+ (instancetype) valueTransformerForName:(NSString *)name valueTrue:(id)tValue valueFalse:(id)fValue
{
    if (![[NSValueTransformer valueTransformerNames]containsObject:name])
    {
        // Set the value transformer
        [NSValueTransformer setValueTransformer:[[BooleanValueTransformer alloc] initWithTrueValue:tValue falseValue:fValue] forName:name];
    }
    
    // Get the value transformer
    BooleanValueTransformer *valueTransformer = (BooleanValueTransformer*)[NSValueTransformer valueTransformerForName:name];
    return valueTransformer;
}

- (id) initWithTrueValue:(id)tValue falseValue:(id)fValue
{
    self = [super init];
    self.trueValue = tValue;
    self.falseValue = fValue;
    return self;
}


- (Class) transformedValueClass
{
    return [trueValue class];
}

+ (BOOL)allowsReverseTransformation { return NO; }
- (id)transformedValue:(BOOL)value {
    
    return (value) ? trueValue : falseValue;
}

@end
