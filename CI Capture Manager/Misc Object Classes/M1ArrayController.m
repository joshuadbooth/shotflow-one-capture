//
//  M1ArrayController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 11/19/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "M1ArrayController.h"
#import "QBManagedObject.h"
#import "AppDelegate.h"

@implementation M1ArrayController
{
    AppDelegate *app;
}

@synthesize privateMOC;

@synthesize selectedObject = _selectedObject;



- (instancetype) init {
    if (self = [super init]) {
        app = [AppDelegate sharedInstance];
    }
    
    return self;
}

-(instancetype) initWithContentFromArrayController:(id)controller {
    self = [super init];
    [self addObjects:[controller arrangedObjects]];
    return self;
}


//- (id) arrangedObjects {
//    id returnArray = [[super arrangedObjects] filteredArrayUsingPredicate:app.hiddenStatusPredicate];
//    return returnArray;
//}


- (id) selectedObject {
    return [[self selectedObjects] firstObject];
}
- (void) setSelectedObject:(id)selectedObject {
    [self willChangeValueForKey:@"selectedObject"];
    NSUInteger objIndex = [self.arrangedObjects indexOfObject:selectedObject];
    if (objIndex != NSNotFound) {
        [self setSelectionIndex:objIndex];
    }
    else {
        [self setSelectionIndexes:[NSIndexSet indexSet]];
    }
    [self didChangeValueForKey:@"selectedObject"];
}




- (NSInteger) indexOfSelectedObject {
    id object = self.selectedObject;
    return [self.arrangedObjects indexOfObject:object];
}

- (NSInteger) indexOfObject:(id)object {
    return [self.arrangedObjects indexOfObject:object];
}

- (NSInteger) indexOfLastObject {
    return [[self arrangedObjects] count] - 1;
}


- (BOOL) containsObject:(id)anObject {
    return [self.arrangedObjects containsObject:anObject];
}

- (id) objectWithName:(NSString*)itemName {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", itemName];
    return [[self.arrangedObjects filteredArrayUsingPredicate:predicate] firstObject];
}

- (NSString*) debugDescription {
    
    NSMutableString *returnString = [NSMutableString string];
    
    [returnString appendFormat:@"\n\tTotal # of Objects: %lu", (unsigned long)[self.arrangedObjects count]];
    [returnString appendFormat:@"\n\tIndex of Selected Object: %ld", (long)[self indexOfSelectedObject]];
    [returnString appendFormat:@"\n\tSelected Object: %@", [self.selectedObject name]];
    [returnString appendFormat:@"\n\tArranged Objects: "];
    
    for (id obj in self.arrangedObjects) {
        [returnString appendFormat:@"\n\t\t%d: %@", [self.arrangedObjects indexOfObject:obj], [obj name]];
    }
    
    [returnString appendString:@"\n"];
    
    return returnString;
    
}

- (NSArray*) namesOfArrangedObjects {
    return [self.arrangedObjects valueForKeyPath:@"name"];
}

- (NSString*) namesOfArrangedObjectsString {
    return [[self.arrangedObjects valueForKeyPath:@"name"] componentsJoinedByString:@"\n\t"];
}



- (void) logNamesOfArrangedObjects {
    DDLogVerbose(@"\nArranged Objects: \n\t%@", [[self.arrangedObjects valueForKeyPath:@"name"] componentsJoinedByString:@"\n\t"]);
}


@end
