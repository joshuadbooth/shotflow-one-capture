//
//  StrikethruTableCellView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 11/5/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface StrikethruTableCellView : NSTableCellView
@property BOOL isCrossedOut;

- (void) toggleStrikeThru;
@end
