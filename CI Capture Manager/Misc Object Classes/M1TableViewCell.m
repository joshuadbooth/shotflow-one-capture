//
//  M1TableViewCell.m
//  CI Capture Manager
//
//  Created by Josh Booth on 5/27/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import "M1TableViewCell.h"
#import "AppDelegate.h"

@implementation M1TableViewCell {
    float barValue;
    CGRect leftBorder;
    AppDelegate *app;
}
- (void) awakeFromNib {
    [super awakeFromNib];
    app = [AppDelegate sharedInstance];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
//    [self drawWithFrame:dirtyRect highlighted:NO];
    
}

//- (BOOL) isOpaque {
//    return NO;
//}


- (void)drawWithFrame:(NSRect)dirtyRect
          highlighted:(BOOL)isHighlighted {
    
    CGContextRef myContext = [[NSGraphicsContext currentContext] graphicsPort];
    
    if (myContext == nil) {
        return;
    }
    
    NSRect workingFrame = dirtyRect;
    
    float cellHeight;
    if (self.textField)
    {
        cellHeight = self.textField.frame.size.height;
        NSSize textSize =  [self.textField.stringValue sizeWithAttributes:@{NSFontAttributeName : self.textField.font }];
        cellHeight = textSize.height;
    }
    else
    {
        cellHeight = self.imageView.frame.size.height;
    }
    

    
    
    CGRect contentRect, topPaddingRect, bottomPaddingRect, borderRect, gradientRect;
    
    float borderThickness = 1;
    float paddingThickness = floor((workingFrame.size.height - cellHeight - (2 * borderThickness)) / 2);
    
    
    NSString *leftsideCoordinates = [NSString stringWithFormat:@"{%f,%f,%f,%f}",
                                     (workingFrame.origin.x),
                                     (workingFrame.origin.y + borderThickness),
                                     borderThickness,
                                     (workingFrame.size.height -(2 * borderThickness))];
    
    if (_needsLeftBorder) {
        leftBorder = NSRectFromString(leftsideCoordinates);
    }
    
    borderRect = workingFrame;
    gradientRect = CGRectInset(workingFrame, borderThickness, borderThickness);
    
    if (!_needsLeftBorder) {
        gradientRect = CGRectInset(workingFrame, 0, borderThickness);
        gradientRect.origin.x -=borderThickness;
    } else {
        // needs left border
        gradientRect = CGRectInset(workingFrame, borderThickness, borderThickness);
    }
    
    contentRect = gradientRect;
    
    CGRectDivide(contentRect, &topPaddingRect, &contentRect, paddingThickness, CGRectMinYEdge);       // Top Padding
    CGRectDivide(contentRect, &bottomPaddingRect, &contentRect, paddingThickness, CGRectMaxYEdge);    // Bottom Padding
    
    
    //    NSGradient *gradient = [[NSGradient alloc]
    //                            initWithStartingColor:[NSColor whiteColor]
    //                            endingColor:[NSColor colorWithDeviceWhite:.96 alpha:1.0]];
    
    
    [[NSColor colorWithDeviceWhite:0.8 alpha:1.0] set];
    NSColor *cellColor = [self randomColor];
    [cellColor set];
    
    NSRectFill(borderRect);
    
    
    if (_needsLeftBorder) {
        NSRectFill(leftBorder);
    }
    
//    [[NSColor whiteColor]set];
    [cellColor set];
    
    NSRectFill(gradientRect);
    //[gradient drawInRect:gradientRect angle:90.0];
    
    if(isHighlighted) {
        // If highlighted
        NSColor *sfBlueColor = app.sfBlueColor;
        [[sfBlueColor colorWithAlphaComponent:.1] set];
        NSRectFillUsingOperation(gradientRect, NSCompositeSourceOver);
    }
    //[super drawRect:contentRect];
//    [super drawInteriorWithFrame:contentRect inView:controlView];
    
}

- (NSColor*) randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    NSColor *color = [NSColor colorWithHue:hue saturation:saturation brightness:brightness alpha:.5];
    return color;
}


@end
