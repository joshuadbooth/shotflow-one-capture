//
//  StrikethruTableCellView.m
//  CI Capture Manager
//
//  Created by Josh Booth on 11/5/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "StrikethruTableCellView.h"

@implementation StrikethruTableCellView
@synthesize isCrossedOut;

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}


- (void) toggleStrikeThru {
   NSRect textFieldRect = [self.textField bounds];
    NSMutableAttributedString *as = [[self.textField attributedStringValue] mutableCopy];
    

    if (!isCrossedOut) {
        // Perform Strikethrough
        [as addAttribute:NSStrikethroughStyleAttributeName value:(NSNumber *)kCFBooleanTrue range:NSMakeRange(0, [as length])];
        [as addAttribute:NSForegroundColorAttributeName value:[NSColor disabledControlTextColor] range:NSMakeRange(0, [as length])];
        [self.textField setAttributedStringValue:as];
        
        [self.textField setTextColor:[NSColor disabledControlTextColor]];
        //[self.textField setEditable:NO];
        isCrossedOut = YES;
    } else {
        // Undo Strikethrough
        [as addAttribute:NSStrikethroughStyleAttributeName value:(NSNumber *)kCFBooleanFalse range:NSMakeRange(0, [as length])];
        [as addAttribute:NSForegroundColorAttributeName value:[NSColor controlTextColor] range:NSMakeRange(0, [as length])];
        [self.textField setAttributedStringValue:as];
        [self.textField setTextColor:[NSColor controlTextColor]];
        //[self.textField setEditable:YES];
        isCrossedOut = NO;
    }
    [self.textField drawRect:textFieldRect];
}
@end
