//
//  M1HeaderCell.m
//  CI Capture Manager
//
//  Created by Josh Booth on 11/11/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

//@import CoreGraphics;
#import "M1HeaderCell.h"
#import "AppDelegate.h"


@implementation M1HeaderCell {
    float barValue;
    CGRect leftBorder;
}



- (id)initImageCell:(NSImage *)anImage
// Overriden Designated Initializer calls -initTextCell:
{
    return [self initTextCell:@" "];
}


- (id)initTextCell:(NSString *)aString
// Overriden Designated Initializer
{
    self = [super initTextCell:aString];

    [self setFont:[NSFont systemFontOfSize:11]];
    _needsLeftBorder = NO;
   
    return self;
}


- (BOOL)isOpaque
// Returns NO so that background will show through
{
    return NO;
}



- (void)drawWithFrame:(NSRect)cellFrame
               inView:(NSView *)controlView {
    [self drawWithFrame:(NSRect)cellFrame highlighted:NO inView:controlView];
}

- (void)drawWithFrame:(NSRect)cellFrame
        highlighted:(BOOL)isHighlighted
               inView:(NSView *)controlView {
    
    CGContextRef myContext = [[NSGraphicsContext currentContext] graphicsPort];
    
    if (myContext == nil) {
        return;
    }
    
    NSRect workingFrame = cellFrame;
    float cellHeight;
    
//    NSAttributedString *as = [[NSAttributedString alloc] initWithString:self.stringValue
//                                                             attributes:@{
//                                                                          NSFontAttributeName : self.font
//                                                                          }];

    NSSize textSize =  [self.stringValue sizeWithAttributes:@{NSFontAttributeName : self.font }];
    cellHeight = textSize.height;
    
    
    
    CGRect contentRect, topPaddingRect, bottomPaddingRect, borderRect, gradientRect;
    
    float borderThickness = 1;
    float paddingThickness = floor((workingFrame.size.height - cellHeight - (2 * borderThickness)) / 2);
    
    
    NSString *leftsideCoordinates = [NSString stringWithFormat:@"{%f,%f,%f,%f}",
                                     (workingFrame.origin.x),
                                     (workingFrame.origin.y + borderThickness),
                                     borderThickness,
                                     (workingFrame.size.height -(2 * borderThickness))];
    
    if (_needsLeftBorder) {
        leftBorder = NSRectFromString(leftsideCoordinates);
    }
    
    borderRect = workingFrame;
    gradientRect = CGRectInset(workingFrame, borderThickness, borderThickness);
    
    if (!_needsLeftBorder) {
        gradientRect = CGRectInset(workingFrame, 0, borderThickness);
        gradientRect.origin.x -=borderThickness;
    } else {
        // needs left border
        gradientRect = CGRectInset(workingFrame, borderThickness, borderThickness);
    }
    
    contentRect = gradientRect;
    
    CGRectDivide(contentRect, &topPaddingRect, &contentRect, paddingThickness, CGRectMinYEdge);       // Top Padding
    CGRectDivide(contentRect, &bottomPaddingRect, &contentRect, paddingThickness, CGRectMaxYEdge);    // Bottom Padding
    
    
//    NSGradient *gradient = [[NSGradient alloc]
//                            initWithStartingColor:[NSColor whiteColor]
//                            endingColor:[NSColor colorWithDeviceWhite:.96 alpha:1.0]];
    
    
    [[NSColor colorWithDeviceWhite:0.8 alpha:1.0] set];
    
    NSRectFill(borderRect);
    
    
    if (_needsLeftBorder) {
        NSRectFill(leftBorder);
    }
    [[NSColor whiteColor]set];
    NSRectFill(gradientRect);
    //[gradient drawInRect:gradientRect angle:90.0];
    
    if(isHighlighted) {
     // If highlighted
     NSColor *sfBlueColor = [AppDelegate sharedInstance].sfBlueColor;
     [[sfBlueColor colorWithAlphaComponent:.1] set];
     NSRectFillUsingOperation(gradientRect, NSCompositeSourceOver);
    }

    [super drawInteriorWithFrame:contentRect inView:controlView];
    
}

- (NSColor*) randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    NSColor *color = [NSColor colorWithHue:hue saturation:saturation brightness:brightness alpha:.5];
    return color;
}

- (NSSize)cellSizeForBounds:(NSRect)aRect
// Overridden to return a size large enough for the label and the bar
{
    NSSize   cellSize = [super cellSizeForBounds:aRect];
    return cellSize;
}


@end
