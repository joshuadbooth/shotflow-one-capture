//
//  DateValueTransformer.m
//  CI Capture Manager
//
//  Created by Josh Booth on 3/30/17.
//  Copyright © 2017 Capture Integration, Inc. All rights reserved.
//

#import "DateValueTransformer.h"

#import "QBField.h"
#import "Shot.h"
#import "SetList.h"
#import "ProductDataRecord.h"
#import "Sample.h"

@interface DateValueTransformer()
@property (nonatomic, weak) id targetObj;
@property (nonatomic, strong) NSString* dateFormat;
@property (nonatomic) kDateType dateType;
@property (nonatomic, strong) NSString* fieldName;
@property (nonatomic, strong) NSString* fieldType;
@end


@implementation DateValueTransformer
+ (Class) transformedValueClass{
    return [NSString class];
}

+ (BOOL) allowsReverseTransformation
{
    return NO;
}
- (id) initWithDateFormat:(NSString*)dateFormat
{
    self = [super init];
    [self setDateFormat:dateFormat];
    return self;
}

- (id) initWithDateType:(kDateType)dateType
{
    self = [super init];
    [self dateFormatFromDateType:dateType];
    return self;
}

- (void) dateFormatFromDateType:(kDateType)dt
{
    if (dt == NSNotFound) dt = [self dateType];
    
    switch(dt)
    {
        default:
        case kDateTypeDate:
            [self setDateFormat:@"MM/dd/yyyy"];
            break;
        case kDateTypeTimestamp:
            [self setDateFormat:@"MM/dd/yyyy hh:mm a"];
            break;
        case kDateTypeDuration:
            [self setDateFormat:@"mm:ss"];
            break;
    }
}


- (NSString*) transformedValue:(id)value
{
    NSString* returnValue;
    if (value == nil || [value isEqualToString: @""])
    {
         returnValue = @"";
    }
    else if ([self dateType] == kDateTypeDuration)
    {
        returnValue = [self durationStringFromInterval:[value doubleValue]];
    }
    else
    {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:([value doubleValue]/1000)];
        returnValue = [self localDateStringForDate:date];
    }

    return returnValue;
}

- (NSString*) localDateStringForDate:(NSDate* _Nonnull)date {
    NSString *returnString;
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    [df setLocale:locale];
    [df setCalendar:[NSCalendar currentCalendar]];
    
    [df setDateFormat:[self dateFormat]];
    
    returnString = [df stringFromDate:date];
    if (returnString == nil || returnString.length == 0) returnString = @"";
    return returnString;
}

- (NSString*) durationStringFromInterval:(NSTimeInterval)timeInterval {
    NSInteger ti = (NSInteger) timeInterval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    
    NSString *returnString = [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
    if ([returnString isEqualToString:@"00:00"]) returnString = @"";
    return returnString;
}
@end
