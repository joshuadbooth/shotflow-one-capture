//
//  SetsArrayController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/21/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "M1ArrayController.h"

@interface SetsArrayController : M1ArrayController



+ (instancetype) sharedController;

@end
