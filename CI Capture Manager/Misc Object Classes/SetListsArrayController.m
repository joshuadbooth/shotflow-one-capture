//
//  SetListArrayController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/21/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "SetListsArrayController.h"
#import "SetsArrayController.h"
#import "AppDelegate.h"



@implementation SetListsArrayController

+ (instancetype) sharedController {
    __strong static id _sharedInstance  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self allocForReal] initForReal];
    });
    return _sharedInstance;
}

+ (id) allocForReal { return [super allocWithZone:NULL]; }
+ (id) alloc { return [self sharedController]; }
+ (id) allocWithZone:(NSZone *)zone { return [self sharedController]; }
- (id) init {  return self; }

- (id) initForReal {
//    DDLogVerbose(@"SetListsArrayController InitForReal");
    self = [super init];
    return self;
}



//- (BOOL) setSelectionIndex:(NSUInteger)index {
//    
//    return [super setSelectionIndex:index];
//}
//
@end
