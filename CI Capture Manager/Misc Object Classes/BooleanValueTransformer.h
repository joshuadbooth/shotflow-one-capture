//
//  BooleanValueTransformer.h
//  CI Capture Manager
//
//  Created by Josh Booth on 7/28/16.
//  Copyright © 2016 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BooleanValueTransformer : NSValueTransformer
@property (nonatomic, strong) id trueValue;
@property (nonatomic, strong) id falseValue;

+ (instancetype) valueTransformerForName:(NSString *)name valueTrue:(id)tValue valueFalse:(id)fValue;
- (Class) transformedValueClass;
- (id) initWithTrueValue:(id)tValue falseValue:(id)fValue;
- (id)transformedValue:(BOOL)value;
@end
