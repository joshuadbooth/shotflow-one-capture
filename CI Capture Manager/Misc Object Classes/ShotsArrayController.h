//
//  ShotsArrayController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/21/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Shot.h"
#import "M1ArrayController.h"

@interface ShotsArrayController : M1ArrayController
//@property (readonly) Shot* selectedObject;

+ (instancetype) sharedController;

@end
