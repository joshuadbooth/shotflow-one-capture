//
//  DummyObject.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/9/15.
//  Copyright © 2015 Capture Integration, Inc. All rights reserved.
//

#import "DummyObject.h"
#include "AppDelegate.h"
#import "SetList.h"
#import "Shot.h"


@interface DummyObject ()
    @property (nonatomic) DummyObjectType type;
@end

@implementation DummyObject {
    AppDelegate *app;
}

@synthesize name = _name;
@synthesize parent;


+ (instancetype) dummyWithParent:(id)p andType:(DummyObjectType)t {
    DummyObject *dummy = [[DummyObject alloc] init];
    dummy.parent = p;
    return dummy;
}

- (instancetype) init {
    self = [super init];
    app = [AppDelegate sharedInstance];
    return self;
}

- (NSArray *)children {
    return nil;
}

- (NSArray *) filteredChildren {
    return nil;
}

+ (NSSet*) keyPathsForValuesAffectingName {
    return [NSSet setWithArray:@[@"objectID", @"parent"]];
}

- (NSString*) name {
    
    return (!self.parent)? @"Dummy" : self.objectID;
}
- (NSString*) debugDescription {
    return self.description;
}

- (NSString*) description {
    NSMutableString *returnString = [NSMutableString stringWithFormat:@"%@", self.name];
    //  if (self.parent != nil) [returnString appendFormat:@" - Parent: %@", [self.parent name]];
    return returnString;
}


- (NSNumber*) record_id {
    return [NSNumber numberWithInteger:0];
}

- (NSString*) status {
//    if ([self.parent isKindOfClass:[Shot class]]) {
//        NSArray *subShotStatuses = [self.parent valueForKeyPath:@"orderedParentedSubShots.@distinctUnionOfObjects.status"];
//        NSLog(@"subShotStatuses: %@", [subShotStatuses componentsJoinedByString:@", "]);
//    }
//    else
//    {
//        NSLog(@"%@ status: %@", [self.parent valueForKey:@"description"], [self.parent valueForKey:@"status"]);
//    }
    
    if ([self.parent isKindOfClass:[SetList class]] && [[self.parent valueForKey:@"status"] isEqualToString:@"Completed"])
    {
        return @"Completed";
    }
    else if ([self.parent isKindOfClass:[Shot class]] && [[self.parent valueForKeyPath:@"orderedParentedSubShots.@distinctUnionOfObjects.status"] isEqualTo:@[@"Completed"]])
    {
        return @"Completed";
    }
    else
    {
        return @"Always";
    }
}

- (id) objectID {
    if (self.parent){
        return [NSString stringWithFormat:@"%@.%@.dummy", [self.parent className], [self.parent primaryKeyValue]];
    }
    return nil;
}

-(void) dealloc {
    //    DDLogVerbose(@"dummy dealloc");
}

@end
