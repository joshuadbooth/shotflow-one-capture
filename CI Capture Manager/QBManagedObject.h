//
//  QBManagedObject.h
//  QBApiTest2
//
//  Created by Josh Booth on 7/9/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "RXMLElement.h"

@class RXMLElement, QBTable, QBField;


typedef void (^completionBlock) (void);
typedef void (^errorBlock)(void);

@interface QBManagedObject : NSManagedObject

@property (nonatomic, retain) NSDate * date_created;
@property (nonatomic, retain) NSDate * date_modified;
@property (nonatomic, retain) NSNumber * record_id;
@property (nonatomic, retain) NSString * update_id;
@property (nonatomic, retain) NSString * status;

@property (nonatomic, strong) NSMutableDictionary * params;
@property (nonatomic, strong) NSDictionary *previousParams;


@property (nonatomic, retain) NSString * name;
@property (nonatomic) BOOL online;
@property (nonatomic, readonly) NSDictionary *dictionaryValues;
@property (nonatomic, strong) NSMutableDictionary * fieldIDs;

@property (nonatomic) BOOL shouldUpdate;

@property (nonatomic, readonly) NSString *tableName;
@property (nonatomic, readonly) id primaryKeyValue;


+ (NSString *)entityName;
+ (instancetype)insertNewObjectIntoContext:(NSManagedObjectContext *)context;

- (void) populateParams: (RXMLElement*)record;

- (NSNumber *) recordAsNSNumber: (NSInteger)num;
- (NSDate*) convertDateFromImport:(NSString*)dateString;
- (NSString*) convertDateForPOST:(NSDate *)date;
- (NSString*) prettyDate: (NSDate*) date;
- (NSString*) xmpDate: (NSDate*) date;


- (NSArray*) getUpdatedParams;

- (void) updateWithRecordData: (RXMLElement*)record;
- (void) setParam:(id)param forKey:(NSString*)key;


- (NSDictionary *) getPropertiesToPOST;
- (NSDictionary *) getChangedParamsForPOST;


- (NSMutableDictionary*) getCompareParams:(RXMLElement*) record;
- (NSComparisonResult) compareModDateToRecord:(RXMLElement*) record;
- (NSMutableDictionary*) compareToRecord:(RXMLElement*)record;

- (id) findInManagedObjectContext:(NSManagedObjectContext *)context error:(NSError**)error;

- (NSString *) getFieldIDForField:(NSString*)fieldName;
@end
