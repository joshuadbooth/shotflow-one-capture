//
//  LogInController.m
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

@import AppKit;

#import "LogInController.h"
#import "QBDBInfo.h"

#import "QBSyncManager.h"
#import "QBDataImporter.h"

#import "AppDelegate.h"
#import "SetRegistrationController.h"

#import "Set.h"

@interface LoginController () <NSTextFieldDelegate>


@end

@implementation LoginController {
    SetRegistrationController *setRegistrationView;
    NSArray *grantedDBs;
}


- (id) initWithWindowNibName:(NSString *)windowNibName {
    self = [super initWithWindowNibName:windowNibName];
    return self;
    
}


- (void) awakeFromNib {
    [super awakeFromNib];
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(connectionOffline:) name: @"QBSyncManagerConnectionOffline" object:[QBSyncManager sharedManager]];
    [center addObserver:self selector:@selector(connectionOnline:) name: @"QBSyncManagerConnectionOnline" object:[QBSyncManager sharedManager]];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:@"username"] != nil) {
        [_usernameField setStringValue:[prefs stringForKey:@"username"]];
    }
#ifdef DEBUG
    if ([prefs objectForKey:@"password"] != nil) {
        [_passwordField setStringValue:[prefs stringForKey:@"password"]];
    }
    [_loginButton setTitle:@"Login (BETA)"];
#endif
    
    setRegistrationView = [[SetRegistrationController alloc] initWithWindowNibName:@"SetRegistrationController"];
    [AppDelegate sharedInstance].setRegistrationView = setRegistrationView;
    
    
}

- (void) connectionOffline:(NSNotification*) note {
    
    if ([note.name isEqualToString:@"QBSyncManagerConnectionOffline"]) {
        [self.statusLabel setStringValue:@"Connection Offline: you will be unable to download new data."];
        
    } else {
        [self.statusLabel setStringValue:@""];
        
    }
    [_loginButton setEnabled:NO];
}

- (void) connectionOnline:(NSNotification*) note {
    
    if ([note.name isEqualToString:@"QBSyncManagerConnectionOnline"]) {
        [self.statusLabel setStringValue:@""];
    }
    [self checkRequirements];
}

- (void)windowDidLoad {
    
    [super windowDidLoad];
    
//    _clientLabel.stringValue = [NSString stringWithFormat:@"Sign in."];
    NSDictionary *cmInfo = [[NSBundle mainBundle] infoDictionary];
    [[self clientLabel] setStringValue:[NSString stringWithFormat:@"version %@ build %@", [cmInfo valueForKey:@"CFBundleShortVersionString"], [cmInfo valueForKey:@"CFBundleVersion"]]];
    
    _usernameField.delegate = self;
    _passwordField.delegate = self;
    
    [self checkRequirements];
    
    [[self window] setBackgroundColor:[NSColor whiteColor]];
    [[self window] makeFirstResponder:_usernameField];
}

- (void) controlTextDidChange:(NSNotification *)obj {
    [self checkRequirements];
}

- (void) controlTextDidEndEditing:(NSNotification *)obj {
    [self checkRequirements];
    
}

- (void) checkRequirements {
    if ([[QBSyncManager sharedManager] canSync] &&
        (![_usernameField.stringValue isEqualToString:@""]) &&
        (![_passwordField.stringValue isEqualToString:@""])
        )
    {
        [_loginButton setEnabled:YES];
        [_saveUserInfo setEnabled:YES];
        [_saveUserInfo setState:1];
    }
    else {
        [_loginButton setEnabled:NO];
        if (![_usernameField.stringValue isEqualToString:@""]) {
            [_saveUserInfo setState:1];
        } else {
            [_saveUserInfo setState:0];
        }
    }
}

-(IBAction) showWindow:(id)sender {
    [super showWindow:sender];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[QBSyncManager sharedManager] setIsLoading:NO];
    });
    
}

#pragma mark - Login function

- (IBAction)login:(id)sender {
    
    if ([[QBSyncManager sharedManager] canSync])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                _clientLabel.stringValue = [NSString stringWithFormat:@"Authenticating..."];
            });
            
            
            QBDBInfo *QBinfo = [[QBDBInfo alloc] initWithUsername:_usernameField.stringValue AndPassword:_passwordField.stringValue];
            QBinfo = [QBDBInfo info];
            NSError *loginError;
            
            if ([QBinfo authenticate:&loginError])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _clientLabel.stringValue = [NSString stringWithFormat:@"Logged in."];
                });
                
                if (_saveUserInfo.state) {
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setValue:_usernameField.stringValue forKey:@"username"];
                    #ifdef DEBUG
                    [prefs setValue:_passwordField.stringValue forKey:@"password"];
                    #endif
                    [prefs synchronize];
                }
                
                grantedDBs = [QBinfo grantedDBs];
                
                if (grantedDBs == nil)
                {
                    // generate an error
                    DDLogError(@"ERROR: Could not get grantedDBs");
                }
                else if (grantedDBs.count == 1)
                {
                    NSDictionary *selectedDB = [grantedDBs objectAtIndex:0];
                    [[QBDBInfo info] setDBID:[selectedDB valueForKey:@"dbid"]];
                    [[QBDBInfo info] createAppIDTable];
                    
                    [QBinfo downloadTablesAndFields:^{
                        DDLogVerbose(@"CompletionBlock: beginning Import");
                        // prevents notifications from flipping out during initial loading
                        [[QBDataImporter sharedImporter] setInitialLoad:YES];
                        
                        [[QBDataImporter sharedImporter] importSets];
                    }];
                }
                else
                {
                    [_databasePopupButton removeAllItems];
                    for (NSDictionary *dbinfo in grantedDBs) {
                        [_databasePopupButton addItemsWithTitles:[grantedDBs valueForKeyPath:@"dbname"]];
                    }
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    
                    if ([prefs objectForKey:@"lastUsedDatabase"] && [[grantedDBs valueForKeyPath:@"dbname"] containsObject:[prefs valueForKey:@"lastUsedDatabase"]]) {
                        [_databasePopupButton selectItemWithTitle:[prefs valueForKey:@"lastUsedDatabase"]];
                    }
                    
                    _databasePopupButton.enabled = YES;
                    _databaseSelectButton.enabled = YES;
                    
                    
                    [NSAnimationContext runAnimationGroup:^(NSAnimationContext *context) {
                        context.duration = 1;
                        _standardLoginView.animator.alphaValue = 0;
                    }
                                        completionHandler:^{
                                            
                                            _standardLoginView.alphaValue = 0;
                                            _databaseSelectionView.alphaValue = 0;
                                            
                                            self.window.contentView = _databaseSelectionView;
                                            
                                            [NSAnimationContext runAnimationGroup:^(NSAnimationContext *contextB) {
                                                contextB.duration = 1;
                                                _databaseSelectionView.animator.alphaValue = 1;
                                            }
                                                                completionHandler:^{
                                                                    _standardLoginView.hidden = YES;
                                                                    _databaseSelectionView.hidden = NO;
                                                                    _databaseSelectionView.alphaValue = 1;
                                                                }];
                                        }];
                }
                
            }
            else
            {
                DDLogError(@"Error signing in");
                dispatch_async(dispatch_get_main_queue(), ^{
                    _clientLabel.stringValue = [NSString stringWithFormat:@"Error signing in."];
                });
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setValue:_usernameField.stringValue forKey:@"username"];
                [prefs synchronize];
                
            }
        });
    }
    else
    {
        
        //TODO: update for when offline
        QBDBInfo *QBinfo = [[QBDBInfo alloc] initWithUsername:_usernameField.stringValue AndPassword:_passwordField.stringValue];
        QBinfo = [QBDBInfo info];
        
        setRegistrationView = [[SetRegistrationController alloc] initWithWindowNibName:@"SetRegistrationController"];
        [AppDelegate sharedInstance].setRegistrationView = setRegistrationView;
        //[setRegistrationView showWindow:self];
        //[self.window close];
    }
}

#pragma mark - Select Database Function
- (IBAction)selectDatabase:(id)sender {
    NSInteger selectionIndex = [_databasePopupButton indexOfSelectedItem];
    NSString *selectionTitle = [_databasePopupButton titleOfSelectedItem];
    
    [[NSUserDefaults standardUserDefaults] setValue:selectionTitle forKey:@"lastUsedDatabase"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *selectedDB = [grantedDBs objectAtIndex:selectionIndex];
    
    
    [[QBDBInfo info] setDBID:[selectedDB valueForKey:@"dbid"]];
    [[QBDBInfo info] createAppIDTable];
    
    
    [[QBDBInfo info] downloadTablesAndFields:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            DDLogVerbose(@"CompletionBlock: beginning Import");
            
            // prevents notifications from flipping out during initial loading
            [[QBDataImporter sharedImporter] setInitialLoad:YES];
            [[QBDataImporter sharedImporter] importSets];
        });
    }];
    
    
}
@end
