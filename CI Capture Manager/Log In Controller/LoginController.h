//
//  LogInController.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/4/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LoginController : NSWindowController

// standard login View
@property (strong) IBOutlet NSView *standardLoginView;
@property (strong) IBOutlet NSTextField *usernameField;
@property (strong) IBOutlet NSSecureTextField *passwordField;
@property (weak) IBOutlet NSButton *loginButton;
@property (weak) IBOutlet NSButton *saveUserInfo;
@property (weak) IBOutlet NSTextField *statusLabel;
@property (weak) IBOutlet NSTextField *clientLabel;
- (IBAction)login:(id)sender;

// database selection view
@property (strong) IBOutlet NSView *databaseSelectionView;
@property (strong) IBOutlet NSPopUpButton *databasePopupButton;
@property (strong) IBOutlet NSButton *databaseSelectButton;

- (IBAction)selectDatabase:(id)sender;


@end
