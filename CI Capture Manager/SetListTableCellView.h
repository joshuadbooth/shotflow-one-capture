//
//  JobTableCellView.h
//  CI Capture Manager
//
//  Created by Josh Booth on 9/3/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SetList.h"
#import "M1TableRowView.h"
#import "M1TableViewCell.h"

@interface SetListTableCellView : M1TableViewCell

@property (strong) IBOutlet NSStackView *iconStackView;

@property (strong) IBOutlet NSButton *infoButton;
@property (strong) IBOutlet NSButton *completeButton;
@property (strong) IBOutlet NSButton *sampleModeButton;

@property (nonatomic, weak) SetList* setlist;
@property (nonatomic, strong) NSManagedObjectID *setlistID;

- (IBAction)showInfo:(id)sender;
- (IBAction)markCompleted:(id)sender;

- (void) assignSetList:(SetList*)obj;
- (void) registerForObservations;
- (void) setupCellView;
@end
