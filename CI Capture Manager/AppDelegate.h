//
//  AppDelegate.h
//  CI Capture Manager
//
//  Created by Josh Booth on 7/9/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>

#import "SetsArrayController.h"
#import "SetListsArrayController.h"
#import "ShotsArrayController.h"


@class Set, SetList, Shot, SetsArrayController, SetListsArrayController, ShotsArrayController;
@class LoginController, SetRegistrationController, ListViewController, DetailViewWindowController;


@interface AppDelegate : NSObject <NSApplicationDelegate>
extern NSString* kSelectionDidChangeNotification;
extern NSString* kUpdateShotNotification;
extern NSString* kDV_AddShotNotification;
extern NSString* kStatusFilterChangedNotification;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *mainMOC;


@property (nonatomic, strong) Set *selectedSet;
@property (nonatomic, strong) SetList *selectedSetList;
@property (nonatomic, strong) Shot *selectedShot;
@property (nonatomic) BOOL sampleMode;

@property (nonatomic, strong) NSArray *hiddenStatuses;
@property (nonatomic, strong) NSArray *hiddenStatusesForImport;
@property (nonatomic, strong) NSArray *statusesToHideInUI;

@property (nonatomic, assign) BOOL hideCompleted;
@property (nonatomic, assign) BOOL hideReady;
@property (nonatomic, assign) BOOL hideInProgress;

@property (nonatomic, strong) NSPredicate *searchFieldPredicate;
@property (nonatomic, strong) NSCompoundPredicate *filteredChildrenPredicate;
@property (nonatomic, strong) NSPredicate *uiStatusPredicate;
@property (nonatomic, strong) NSPredicate *hiddenStatusPredicate;
@property (nonatomic, strong) NSPredicate *completedPredicate;
@property (nonatomic, strong) NSPredicate *cancelledPredicate;

@property (nonatomic, strong) NSSortDescriptor *shotSorting;
@property (nonatomic, strong) NSSortDescriptor *subShotSorting;

@property (nonatomic, strong) NSSortDescriptor *sortByRecordID;
@property (nonatomic, strong) NSSortDescriptor *sortByPrimaryKeyValue;
@property (nonatomic, strong) NSSortDescriptor *sortByName;
@property (nonatomic, strong) NSSortDescriptor *sortByTitle;
@property (nonatomic, strong) NSSortDescriptor *sortByPriority;
@property (nonatomic, strong) NSSortDescriptor *sortByCreationDate;
@property (nonatomic, strong) NSSortDescriptor *sortByModDate;

@property (nonatomic, strong) NSColor *sfBlueColor;
@property (nonatomic, strong) NSColor *sfGreyColor;
@property (nonatomic, readonly) NSColor *randomColor;


@property (nonatomic, strong) LoginController *loginView;
@property (nonatomic, strong) SetRegistrationController *setRegistrationView;
@property (nonatomic, strong) ListViewController *listView;
@property (nonatomic, strong) DetailViewWindowController *detailView;

@property (weak) IBOutlet NSMenuItem *adminMenu;
- (IBAction)generatePanels:(id)sender;

@property (weak) IBOutlet NSMenuItem *turnSyncOnBtn;
@property (weak) IBOutlet NSMenuItem *turnSyncOffBtn;
@property (weak) IBOutlet NSMenuItem *forceUpdateMenu;

- (IBAction)turnSyncOn:(id)sender;
- (IBAction)turnSyncOff:(id)sender;
- (IBAction)forceUpdate:(id)sender;

@property (weak) IBOutlet NSMenuItem *manualSyncMenu;
- (IBAction)startManualSync:(id)sender;

- (IBAction)saveAction:(id)sender;

+ (instancetype) sharedInstance;

- (void) resetPersistentStore;

@property (weak) IBOutlet NSMenuItem *toggleCompletedItemsMenuItem;
- (IBAction)toggleCompletedItems:(id)sender;

@property (weak) IBOutlet NSMenuItem *toggleInProgressItemsMenuItem;
- (IBAction)toggleInProgressItems:(id)sender;

@property (weak) IBOutlet NSMenuItem *toggleReadyItemsMenuItem;
- (IBAction)toggleReadyItems:(id)sender;
- (void) updateToggleMenuTitles;

- (NSString *) getSystemSerialNumber;
- (NSString *) getSystemUUID;
@property (weak) IBOutlet NSMenuItem *takeCaptureMenuItem;
@property (weak) IBOutlet NSMenuItem *cleanUpFavoritesMenuItem;

@end

