//
//  C1ProController.m
//
//  Created by Josh Booth on 7/20/15.
//  Copyright (c) 2015 Capture Integration, Inc. All rights reserved.
//

#import "C1ProController.h"

#import "QBDBInfo.h"
#import "QBTable.h"
#import "QBField.h"

#import "QBSyncManager.h"
#import "QBDataExporter.h"
#import "QBQuery.h"
#import "AppDelegate.h"

#import "SetList.h"
#import "Shot.h"
#import "ProductDataRecord.h"



@interface WindowListApplierData : NSObject
{
}

@property (strong, nonatomic) NSMutableArray * outputArray;
@property int order;

@end

@implementation WindowListApplierData

-(instancetype)initWindowListData:(NSMutableArray *)array
{
    self = [super init];
    
    self.outputArray = array;
    self.order = 0;
    
    return self;
}

@end



@interface C1ProController () {
    NSManagedObjectContext *C1PCmoc;
    //NSManagedObjectContext *xmpMOC;
    __block NSInteger captureCounter;
    
    
    NSArrayController *windowsArrayController;
    CGWindowListOption listOptions;
    CGWindowListOption singleWindowListOptions;
    CGWindowImageOption imageOptions;
    CGRect imageBounds;
    
    
}

@property (strong) WindowListApplierData *windowListData;

@end


NSString *kInfoDownloadedForShotNotification = @"kInfoDownloadedForShotNotification";
NSString *kCaptureCountUpdatedNotification = @"kCaptureCountUpdatedNotification";
NSString *kCameraOfflineNotification = @"kCameraOfflineNotification";
NSString *kCameraOnlineNotification = @"kCameraOnlineNotification";

@implementation C1ProController {
    AppDelegate *app;
    QBDataExporter *exporter;
    NSUserDefaults *prefs;
    QBSyncManager *syncManager;
    QBDBInfo *QBinfo;
    NSManagedObjectContext *mainMOC;
    
    NSString *XMP_PREFIX;
    NSString *XMP_NAMESPACE;
    NSString *APPCLIENT;
    
    NSString *captureSubFolderFormat;
    NSString *captureSubFolderName;
    NSString *captureNameFormat;
    BOOL addToFavorites;
    BOOL firstRun;
    
    NSURL *sessionFolder;
    NSURL *baseCaptureFolder;
    
    NSDate *startDate;
    NSDate *endDate;
    NSTimer *durationTimer;
    
    NSTimer *cameraTimer;
}



@synthesize C1Pro;
@synthesize session;
@synthesize camera;
@synthesize imageFormat;


@synthesize watchFolder;
@synthesize isWatching;
@synthesize attempts; // # of attempts at getting session info

@synthesize activeShot;
@synthesize captureName;
@synthesize fileName;
@synthesize captureID;
@synthesize didBegin;


@synthesize c1ControllerQueue;
@synthesize xmpOperationQueue;


#pragma mark - Init -

+ (C1ProController*) sharedInstance {
    static C1ProController *sharedInstance  = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype) init {
    self = [super init];
    
    app = [AppDelegate sharedInstance];
    mainMOC = app.mainMOC;
    exporter = [QBDataExporter sharedExporter];
    firstRun = YES;
    
    QBinfo = [QBDBInfo info];
    
    
    [self retrieveConfigurationData];
    
    
    prefs = [NSUserDefaults standardUserDefaults];
    syncManager = [QBSyncManager sharedManager];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processMainMOCupdates:)
                                                 name:NSManagedObjectContextDidSaveNotification
                                               object:mainMOC];
    
    
    
    //C1PCmoc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    //C1PCmoc.parentContext = mainMOC;
    
    //c1ControllerQueue = dispatch_queue_create("C1ProControllerQueue", DISPATCH_QUEUE_CONCURRENT);
    //    dispatch_async(c1ControllerQueue, ^{
    //        xmpMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    //        xmpMOC.parentContext = mainMOC;
    //    });
    
    //xmpOperationQueue = [[NSOperationQueue alloc] init];
    //xmpOperationQueue.underlyingQueue = c1ControllerQueue;
    
    
    _C1RunningError = [[NSError alloc]
                       initWithDomain:@"com.shotflow.c1controller"
                       code:1001
                       userInfo:@{
                                  NSLocalizedDescriptionKey : @"Capture One is not running.",
                                  NSLocalizedRecoverySuggestionErrorKey : @ "Please open Capture One by pressing the button below, or do so manually then press Continue.",
                                  NSLocalizedFailureReasonErrorKey : @"ShotFlow One: Capture attempted to communicate with Capture One, but it wasn't running.",
                                  NSLocalizedRecoveryOptionsErrorKey : @[@"Open Capture One", @"Continue", @"Quit"],
                                  }];
    
    _C1VersionError = [[NSError alloc]
                       initWithDomain:@"com.shotflow.c1controller"
                       code:1003
                       userInfo:@{
                                  NSLocalizedDescriptionKey : @"Capture One is not the required version.",
                                  NSLocalizedRecoverySuggestionErrorKey : @ "Please update to Capture One version 8.2 or higher.",
                                  //NSLocalizedFailureReasonErrorKey : @"",
                                  NSLocalizedRecoveryOptionsErrorKey : @[@"Update Capture One", @"Quit"],
                                  }];
    
    _C1CatalogError = [[NSError alloc]
                       initWithDomain:@"com.shotflow.c1controller"
                       code:1004
                       userInfo:@{
                                  NSLocalizedDescriptionKey : @"Invalid Document Type.",
                                  NSLocalizedRecoverySuggestionErrorKey : @ "Please create a new Capture One Session (File menu > New Session) then press OK to continue.",
                                  NSLocalizedFailureReasonErrorKey : @"The active document open in Capture One is a Capture One Catalog, but ShotFlow One requires using a Capture One Session.",
                                  }];
    
    
    self.C1Pro = [self identifyC1andCheckIfRunning];
    [self.C1Pro setDelegate:self];
    attempts = 0;
    
    
    [self getSessionInfo];
    
    return self;
}


- (void) initiateSave:(completionBlock)completionBlock {
    DDLogVerbose(@"***C1PC MOC Save");
    
    [mainMOC save:nil];
    
    
    if (completionBlock != nil){
        completionBlock();
    }
}

#pragma mark - Configuration Data

- (void) retrieveConfigurationData {
    APPCLIENT = [QBinfo.configurationData valueForKey:@"APPCLIENT"];
    XMP_PREFIX = [QBinfo.configurationData valueForKey:@"XMP_PREFIX"];
    XMP_NAMESPACE = [QBinfo.configurationData valueForKey:@"XMP_NAMESPACE"];
    
    captureNameFormat = [[QBDBInfo info].configurationData valueForKey:@"CAPTURENAMEFORMAT"];
    captureSubFolderFormat = [[QBDBInfo info].configurationData valueForKey:@"CAPTURESUBFOLDERFORMAT"];
}

#pragma mark - handle mainMOC updates -
- (void) processMainMOCupdates:(NSNotification*) note {
//    DDLogVerbose(@"Updating C1PC MOC");
//    
//    
//    [xmpMOC performBlock:^{
//        [xmpMOC mergeChangesFromContextDidSaveNotification:note];
//    }];
}

- (void) prepareToTerminate {
    
    if ((mainMOC != nil) && (activeShot!=nil)) {
        [mainMOC performBlockAndWait:^{
            activeShot.numberOfCaptures = captureCounter;
            
            [mainMOC save:nil];
//            [mainMOC performBlockAndWait:^{
//                [mainMOC save:nil];
//            }];
            
            [exporter postToServer:activeShot onComplete:^{
                [NSApp replyToApplicationShouldTerminate:YES];
            }];
            
            if (self.isWatching) {
                [self stopWatching];
            }
        }];
    } else {
        [NSApp replyToApplicationShouldTerminate:YES];
    }
}

#pragma mark - C1 Connection & Setup -

- (C1ProApplication *) identifyC1andCheckIfRunning {
    
    
    C1ProApplication *CO8 = [SBApplication applicationWithBundleIdentifier:@"com.phaseone.captureone8"];
    C1ProApplication *CO9 = [SBApplication applicationWithBundleIdentifier:@"com.phaseone.captureone9"];
    C1ProApplication *CO10 = [SBApplication applicationWithBundleIdentifier:@"com.phaseone.captureone10"];
    
    
    NSMutableArray *C1ProApps =[NSMutableArray array];
    
    if (CO10 != nil) {
        [C1ProApps addObject:CO10];
    }
    if (CO9 != nil) {
        [C1ProApps addObject:CO9];
    }
    if (CO8 != nil) {
        [C1ProApps addObject:CO8];
    }
    
    
    id runningC1 = nil;
    
    for (C1ProApplication* C1 in [NSArray arrayWithArray:C1ProApps]) {
        @try {
            if ([C1 isRunning])
            {
                runningC1 = C1;
                break;
            }
        }
        @catch (NSException *exception)
        {
            DDLogVerbose(@"Exception during locating C1Pro: %@", exception);
        }
    }
    
    if (runningC1 == nil)
    {
        // error
    }
    
    return runningC1;
}

- (void) getCameraInfo {
    if (session == nil) [self getSessionInfo];
    camera = [session camera];
    BOOL cameraExists = [camera exists];
    
    // imageFormat = [camera format];
    if (!cameraExists)
    {
        // Post Camera Offline Notification
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter]postNotificationName:kCameraOfflineNotification object:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter]postNotificationName:kCameraOnlineNotification object:nil];
        });
    }
    
    
   
}

- (NSURL*) watchFolder {
    return [[session captures] get];
}

+ (NSSet*) keyPathsForValuesAffectingWatchFolder {
    return [NSSet setWithObject:@"session"];
}

- (void) takeCapture {
    if (C1Pro == nil || ![C1Pro isRunning] || ![camera exists]) {
        return;
    }
    
    if (([C1Pro isRunning]) && (self.isWatching)){
        [C1Pro capture];
    }
}

#pragma mark - Session Info
- (void) getSessionInfo {
    if (self.C1Pro == nil) {
        self.C1Pro = [self identifyC1andCheckIfRunning];
    }
    
    if ([C1Pro isRunning])
    {
        self.session = [[C1Pro documents] firstObject];
        // TODO: Error for multiple documents open
        
        if (![self validateSession:session]) {
            [[QBDBInfo info] alertMe:_C1CatalogError andThen:^{
                [[C1ProController sharedInstance] getSessionInfo];
            }];
        }
        
        
        if (cameraTimer == nil)
        {
            cameraTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(getCameraInfo) userInfo:nil repeats:YES];
            [cameraTimer fire];
        }
        
        if (firstRun) {
            firstRun = NO;
            [self assignCaptureFolder];
            //[self loadActiveShotFromInitialCaptureFolder];
        }
        else
        {
            [self assignCaptureFolder];
        }
        
        
        // ** Better Window Control ** //
//        CFArrayRef windowList = CGWindowListCopyWindowInfo(kCGWindowListOptionAll, kCGNullWindowID);
//        
//        NSMutableArray * prunedWindowList = [NSMutableArray array];
//        self.windowListData = [[WindowListApplierData alloc] initWindowListData:prunedWindowList];
//        
//        CFArrayApplyFunction(windowList, CFRangeMake(0, CFArrayGetCount(windowList)), &WindowListApplierFunction, (__bridge void *)(self.windowListData));
//        CFRelease(windowList);
        
        // Set the new window list
        //[arrayController setContent:prunedWindowList];
        
        
        //[self getCameraInfo];
        
        DDLogVerbose(@"C1 Connection successful");
        attempts = 0;
    }
    else
    {
        if (attempts < 2) {
            attempts++;
            [[QBDBInfo info] alertMe:_C1RunningError andThen:^{[self getSessionInfo];}];
            
//            [[QBDBInfo info]alertMe:_C1RunningError
//                     recoveryBlock1:^{
//                                        [C1Pro activate];
//                                      [self getSessionInfo];
//                                    }
//                     recoveryBlock2:^{[self getSessionInfo];}
//                     recoveryBlock3:^{[NSApp terminate:self];}];
        }
        else
        {
            NSError *C1RunningErrorMaxAttempts = [[NSError alloc]
                               initWithDomain:@"com.shotflow.c1controller"
                               code:1002
                               userInfo:@{
                                          NSLocalizedDescriptionKey : @"ShotFlow One failed to communicate with Capture One after 3 attempts.",
                                          NSLocalizedFailureReasonErrorKey : @"ShotFlow One failed to communicate because a supported version of Capture One is not running. \n\nShotFlow One will now close.",
                                          NSLocalizedRecoveryOptionsErrorKey : @[@"Quit"],
                                          }];
            
            [[QBDBInfo info]alertMe:C1RunningErrorMaxAttempts recoveryBlock1:^{[NSApp terminate:self];} recoveryBlock2:nil recoveryBlock3:nil];
            //[[QBDBInfo info] alertMe:C1RunningErrorMaxAttempts andThen:^{[NSApp terminate:self];}];
        }
    }
}

- (void) assignCaptureFolder {
    sessionFolder = [[[session path] get] URLByAppendingPathComponent:[[session name] stringByDeletingPathExtension]];
    baseCaptureFolder = [sessionFolder URLByAppendingPathComponent:@"Capture" isDirectory:YES];
    
    NSURL *currentFolder = [[session captures] get];
    
    NSURL *newFolder = [self generateCaptureSubFolder];
    
    if ([newFolder isNotEqualTo:baseCaptureFolder]) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        [fileManager createDirectoryAtURL:newFolder
              withIntermediateDirectories:YES
                               attributes:nil
                                    error:&error];
        
        
        self.watchFolder = newFolder;
        if ([currentFolder isNotEqualTo:newFolder]) // prevents setting focus to c1 app unnecessarily
        {
            NSInteger existingFavorite = [session.collections indexOfObjectPassingTest:^BOOL(C1ProCollection * _Nonnull collection, NSUInteger idx, BOOL * _Nonnull stop) {
                return [collection.name isEqualToString:newFolder.lastPathComponent];
            }];
            
            C1ProCollection *newFavorite;
            
            if (existingFavorite == NSNotFound)
            {
                newFavorite = [[[C1Pro classForScriptingClass:@"collection"] alloc] initWithProperties:@{@"name": newFolder.lastPathComponent,
                                                                                                                          @"kind" : @"CCfv",
                                                                                                                          @"file" : newFolder}];
                [session.collections addObject:newFavorite];
            }
            else
            {
                newFavorite = [session.collections objectAtIndex:existingFavorite];
            }
            
            [session setCaptures:newFolder];
            
            if ([currentFolder.lastPathComponent hasPrefix:@"**"] || [currentFolder.lastPathComponent isEqualToString:@"ERROR"] || [currentFolder.lastPathComponent isEqualToString:captureSubFolderFormat] || [currentFolder.lastPathComponent isEqualToString:@"Enter Sample Values"])
            {
                NSArray *oldContents = [fileManager contentsOfDirectoryAtPath:[currentFolder path] error:nil];
                if (oldContents.count == 0)
                {
                    if ([fileManager removeItemAtURL:currentFolder error:nil])
                    {

                        NSString *favoriteName = currentFolder.lastPathComponent;
                        C1ProCollection *oldFavorite = [session.collections objectAtIndex:[session.collections indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            return [[[obj file] lastPathComponent] isEqualToString:currentFolder.lastPathComponent];
                        }]];
                        
                        if (oldFavorite != nil)
                        {
                            [session.collections removeObject:oldFavorite];
                        }
                    }
                }
            }
            
        }
    }
    else
    {
        self.watchFolder = baseCaptureFolder;
    }
}

- (void) cleanUpFavorites
{
    // Clean up bogus favorites
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    SBElementArray *collections = session.collections;
    NSMutableArray *collectionsToRemove = [NSMutableArray array];
    
    [collections enumerateObjectsUsingBlock:^(C1ProCollection* _Nonnull collection, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if (collection.kind == C1ProCollectionTypeFavorite)
        {
            NSString *name = [[collection file] lastPathComponent];
            NSURL *file = [collection file];
            BOOL fileExists = [fileManager fileExistsAtPath:[file path]];
            //NSLog(@"\n\tname: %@\n\tfile: %@\n\texists: %@\n\n", name, [file path], fileExists? @"YES" : @"NO");
            
            
            if (!fileExists || [[file absoluteString] containsString:@"/.Trash/"])
            {
                if (![collectionsToRemove containsObject:collection])
                {
                    [collectionsToRemove addObject:collection.file];
                }
            }
        }
    }];
    
    
    if (collectionsToRemove.count > 0)
    {
        for (int x=0; x < collectionsToRemove.count; x++) {
            NSURL *fileURL = [collectionsToRemove objectAtIndex:x];
            NSInteger collectionIndex = [collections indexOfObjectPassingTest:^BOOL(C1ProCollection*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                return [[obj file] isEqualTo:fileURL];
            }];
            
            [session.collections removeObjectAtIndex:collectionIndex];
        }
    }
    
}

NSString *kAppNameKey = @"applicationName";	// Application Name & PID
NSString *kWindowOriginKey = @"windowOrigin";	// Window Origin as a string
NSString *kWindowSizeKey = @"windowSize";		// Window Size as a string
NSString *kWindowIDKey = @"windowID";			// Window ID
NSString *kWindowLevelKey = @"windowLevel";	// Window Level
NSString *kWindowOrderKey = @"windowOrder";	// The overall front-to-back ordering of the windows as returned by the window server


void WindowListApplierFunction(const void *inputDictionary, void *context);
void WindowListApplierFunction(const void *inputDictionary, void *context)
{
    NSDictionary *entry = (__bridge NSDictionary*)inputDictionary;
    WindowListApplierData *data = (__bridge WindowListApplierData*)context;
    
    // The flags that we pass to CGWindowListCopyWindowInfo will automatically filter out most undesirable windows.
    // However, it is possible that we will get back a window that we cannot read from, so we'll filter those out manually.
    int sharingState = [entry[(id)kCGWindowSharingState] intValue];
    if(sharingState != kCGWindowSharingNone)
    {
        NSMutableDictionary *outputEntry = [NSMutableDictionary dictionary];
        
        // Grab the application name, but since it's optional we need to check before we can use it.
        NSString *applicationName = entry[(id)kCGWindowOwnerName];
        if(applicationName != NULL)
        {
            // PID is required so we assume it's present.
            NSString *nameAndPID = [NSString stringWithFormat:@"%@ (%@)", applicationName, entry[(id)kCGWindowOwnerPID]];
            outputEntry[kAppNameKey] = nameAndPID;
        }
        else
        {
            // The application name was not provided, so we use a fake application name to designate this.
            // PID is required so we assume it's present.
            NSString *nameAndPID = [NSString stringWithFormat:@"((unknown)) (%@)", entry[(id)kCGWindowOwnerPID]];
            outputEntry[kAppNameKey] = nameAndPID;
        }
        
        // Grab the Window Bounds, it's a dictionary in the array, but we want to display it as a string
        CGRect bounds;
        CGRectMakeWithDictionaryRepresentation((CFDictionaryRef)entry[(id)kCGWindowBounds], &bounds);
        NSString *originString = [NSString stringWithFormat:@"%.0f/%.0f", bounds.origin.x, bounds.origin.y];
        outputEntry[kWindowOriginKey] = originString;
        NSString *sizeString = [NSString stringWithFormat:@"%.0f*%.0f", bounds.size.width, bounds.size.height];
        outputEntry[kWindowSizeKey] = sizeString;
        
        // Grab the Window ID & Window Level. Both are required, so just copy from one to the other
        outputEntry[kWindowIDKey] = entry[(id)kCGWindowNumber];
        outputEntry[kWindowLevelKey] = entry[(id)kCGWindowLayer];
        
        // Finally, we are passed the windows in order from front to back by the window server
        // Should the user sort the window list we want to retain that order so that screen shots
        // look correct no matter what selection they make, or what order the items are in. We do this
        // by maintaining a window order key that we'll apply later.
        outputEntry[kWindowOrderKey] = @(data.order);
        data.order++;
        
        [data.outputArray addObject:outputEntry];
    }
}

- (BOOL) validateSession:(C1ProDocument*)document {
    if (document != nil) {
        if ([document kind] == C1ProDocumentTypeSession) {
            return YES;
        } else {
            return NO;
        }
    } else {
        DDLogError(@"*****ERROR: Checking for Session: no such document exists.");
        return NO;
    }
}

#pragma mark - Capture Sub Folder
- (NSURL*) generateCaptureSubFolder
{
    if (activeShot == nil) return baseCaptureFolder;
    
    
    if ([captureSubFolderFormat isEqualToString:@""] || [captureSubFolderFormat isEqualToString:@"none"])
    {
        captureSubFolderName = @"";
        return baseCaptureFolder;
    }
    else
    {
        NSURL *returnURL = nil;
        captureSubFolderName = [activeShot regenerateStringUsingTemplate:captureSubFolderFormat];
        
        if (activeShot.setlist.sampleMode && !activeShot.sampleDataDownloaded) {
            //captureSubFolderName = [@"**" stringByAppendingString:captureSubFolderName];
            captureSubFolderName = activeShot.title;
        }
        returnURL = [baseCaptureFolder URLByAppendingPathComponent:captureSubFolderName];
        
        return returnURL;
    }
}


- (void) loadActiveShotFromInitialCaptureFolder {
    // **** Reverse-locate active shot from session capture folder (in case of crash) **** //
    // 1.) get current capture folder
    // 2.) breakdown products
    // 3.) find shots with those products
    // 4.) get first shot that not complete
    // 4.1) get first In-Progress Shot.. if none, get first Ready Shot
    // 5.) Submit selection notification.
    
    NSNumber *savedObjRID = [prefs objectForKey:@"activeShot"];
    
    
    NSString *folderName = [[[session captures] get] lastPathComponent];
    if ([folderName isEqualToString:@"Capture"] || savedObjRID == nil || [savedObjRID isEqualTo: [NSNull null]])
    {
        [self assignCaptureFolder];
        firstRun = NO;
        return;
    }
    else
    {
        firstRun = NO;
        Shot *shot = [Shot existingWithRecordID:savedObjRID inContext:mainMOC];
        app.selectedShot = shot;
        app.selectedSetList = shot.setlist;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSelectionDidChangeNotification object:shot.objectID userInfo:@{@"sender" : self}];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[C1ProController sharedInstance] changeToShot:shot];
        });
        
        
    }
}

#pragma mark - Folder Watching Setup -
- (BOOL) changeShots {
    return [self changeToShot:nil];
}

- (BOOL) changeToShot:(Shot *)toS {
    
    Shot *toShot = (toS != nil)? [Shot existingWithObjectID:toS.objectID inContext:mainMOC] : nil;
    
    if ([toShot isEqualTo:activeShot]) return YES;
    

    if ((activeShot!=nil) && (didBegin))
    {
        activeShot.numberOfCaptures = captureCounter;
        [self initiateSave:^{
            [exporter postToServer:[Shot existingWithObjectID:activeShot.objectID inContext:mainMOC]];
        }];
        
    }
    
    if (self.isWatching)
    {
        [self stopWatching];
    }
    
    if (toShot == nil)
    {
        if (app.selectedShot != nil) app.selectedShot = nil;
        
        if (activeShot != nil)
        {
            //DDLogVerbose(@"Changing to NO shot");
            
        }
        else {
            //DDLogVerbose(@"No shot to change to!");
        }
        //return YES;
        self.isWatching = NO;
        [prefs setObject:@"" forKey:@"activeShot"];
        activeShot = nil;
        return NO;
    }
    else
    {
        DDLogVerbose(@"Changing shot to %@ at %@", toShot.name, [[QBDBInfo info] prettyDate:[NSDate date]]);
        [prefs setObject:toShot.record_id forKey:@"activeShot"];
    }
    
    [prefs synchronize];
    
    captureCounter = 0;
    didBegin = NO;
    
    
    if ([C1Pro isRunning])
    {
        
        activeShot = toShot;
        captureCounter = activeShot.numberOfCaptures;
        DDLogVerbose(@"activeShot: %@", activeShot.name);
        DDLogVerbose(@"starting at capture: %d", activeShot.numberOfCaptures);
        DDLogVerbose(@"capture count paramsValue: %@", [activeShot.params valueForKey:@"# of Captures"]);
        
        [self getSessionInfo];
        
        [self resetCaptureCounter];
        [self setNextCaptureNaming];
        
        
        
        
        
        
        if ([self startWatching]) {
            
            DDLogVerbose(@"C1 is Ready");
        }
        
    } else {
        
        [self getSessionInfo];
        //[[QBDBInfo info]alertMe:_C1RunningError];
        
        self.isWatching = NO;
        
    }
    // });
    
    
    
    return self.isWatching;
}


-(BOOL) startWatching {
    
    [self stopWatching];
    
    if ([C1Pro isRunning]){
        
        //if (session == nil) {
        [self getSessionInfo];
        //}
        
        self.isWatching = [self setupEventListener];
        
        return self.isWatching;
    }
    else {
        self.isWatching = NO;
        [[QBDBInfo info] alertMe:_C1RunningError];
        return self.isWatching;
    }
    
}

-(BOOL) stopWatching {
    
    if ([events stopWatchingPaths])
    {
        //DDLogVerbose(@"Stream Stopped");
        didBegin = NO;
        self.isWatching = NO;
        [self endTimeTracking];
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL) setupEventListener {
    __block BOOL isReady = YES;
    
        //NSLog(@"EventListenerSetup: isMainThread? %@", [NSThread isMainThread]? @"Yes" : @"NO");
        // DDLogVerbose(@"events: %@", events);
        if (events) {
            [events stopWatchingPaths];
            events = nil;
            // DDLogVerbose(@"killing event.");
        }
        
        events = [[SCEvents alloc] init];
        [events setDelegate:self];
        
        
        NSMutableArray *paths = [NSMutableArray arrayWithObject:[watchFolder relativePath]];
        
        
        NSURL *url1 = [watchFolder URLByAppendingPathComponent:@".DS_Store"];
        NSURL *url2 = [watchFolder URLByAppendingPathComponent:@"CaptureOne" isDirectory:YES];
        
        
        NSMutableArray *excludePaths = [[NSMutableArray alloc] init];
        
        
        // check for CaptureOne & .DS_Store & add to ignored file list
        [excludePaths addObject:[url1 relativePath]];
        [excludePaths addObject:[url2 relativePath]];
        
        [events setExcludedPaths:excludePaths];
        [events setIgnoreEventsFromSubDirs:YES];
        
        isReady = [events startWatchingPaths:paths];
        
        // Display a description of the stream
        DDLogVerbose(@"Stream Started: ActiveShot: %@", app.selectedShot.description);
   
    
    
    return isReady;
}

#pragma mark - pathwatcher delegate

- (void)pathWatcher:(SCEvents *)pathWatcher eventOccurred:(SCEvent *)event  {
    //TODO: Validation: try examining NSURL *lastCapturedFile; from C1Pro...
    
    if ([event eventFlags] & kFSEventStreamEventFlagItemCreated) {
        // DDLogVerbose(@"flag Item Created!");
    } else  if ([event eventFlags] & kFSEventStreamEventFlagItemRemoved){
        //DDLogVerbose(@"Flag Item Removed");
        return;
    } else if ([event eventFlags] & kFSEventStreamEventFlagItemRenamed) {
        //DDLogVerbose(@"Flag Item Renamed");
        
    } else if ([event eventFlags] & kFSEventStreamEventFlagItemModified) {
        //DDLogVerbose(@"Flag Item Modified");
        
    } else if ([event eventFlags] & kFSEventStreamEventFlagItemIsDir) {
        //DDLogVerbose(@"Flag item is Dir");
        return;
    }
    
    
    NSString *filepath = [NSString stringWithFormat:@"%@",[event eventPath] ];
    NSString *filename = [[event eventPath]lastPathComponent];
    NSFileManager *manager = [NSFileManager defaultManager];
    
    /***** verify file is for current shot *****/
    
    NSString *namingConvention = activeShot.naming_convention;
    
    if ((namingConvention == nil)||([namingConvention isEqualToString:@""])){
        namingConvention = [activeShot.name stringByReplacingOccurrencesOfString:@" " withString:QBinfo.defaultSeparator];
    }
    
#ifdef DEBUG
    // ignore filename
#else
    if (![filename containsString:namingConvention]) return;
#endif
    
    if (([[filename pathExtension] isEqualToString:@"xmp"]) ||
        ([filename hasPrefix:@"."]) ||
        ([filename hasPrefix:@"temp"]) ||
        (![manager fileExistsAtPath:[event eventPath]]))
    {
        // ignore... don't do anything
        return;
    }
    else
    {
        NSDate *captureDate = [NSDate date];
        
        if (didBegin == NO)
        {
            didBegin = YES;
            if ([activeShot.status isEqualToString:@"Ready"])
            {
                activeShot.status = @"In Progress";
                if ([activeShot.setlist.status isEqualToString:@"Ready"]) {
                    activeShot.setlist.status = @"In Progress";
                    [[QBDataExporter sharedExporter] postToServer:activeShot];
                    [[QBDataExporter sharedExporter] postToServer:activeShot.setlist];
                    
                }
            }
            [self beginTimeTracking];
        }
        
        captureCounter++;
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setTimeZone:[NSTimeZone systemTimeZone]];
        [df setDateStyle: NSDateFormatterMediumStyle];
        [df setTimeStyle: NSDateFormatterMediumStyle];
        
        DLog(@"shot captured at: %@", [df stringFromDate:captureDate]);
        
        activeShot.numberOfCaptures = captureCounter;
        [activeShot updateDateModified];
        
        [mainMOC save:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kCaptureCountUpdatedNotification object:self userInfo:@{@"shotID" : activeShot.objectID}];
        });
        
        [self writeXMPtoFile:filepath];
    }    
}

#pragma mark - extra functions

- (NSString*) generateCaptureUUID
{
    return [[NSUUID UUID] UUIDString];
}


- (void) writeXMPtoFile:(NSString*)filepath {
    
    DDLogVerbose(@"Writing XMP for File: %@", [filepath lastPathComponent]);
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSManagedObjectContext *xmpMOC = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//        xmpMOC.parentContext = mainMOC;
        
        
        [mainMOC performBlock:^{
            NSString* captureUUID = [[C1ProController sharedInstance] generateCaptureUUID];
            
            Shot *xmpShot = [Shot existingWithObjectID:activeShot.objectID inContext:mainMOC];
            
            NSString *imageDir = [filepath stringByDeletingLastPathComponent];
            // // DDLogVerbose(@"imageDir: %@", imageDir);
            
            fileName = [filepath lastPathComponent];
            // DDLogVerbose(@"FileName: %@", fileName);
            
            NSURL *xmpURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.xmp", imageDir, [fileName stringByDeletingPathExtension]]];
            // DDLogVerbose(@"xmpURL: %@", [xmpURL standardizedURL]);
            
            
            NSString *contentStart = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"Public XMP Toolkit Core 4.0\">\n<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n<rdf:Description rdf:about=\"\"\n xmlns:%@=\"%@\"\n", XMP_PREFIX, XMP_NAMESPACE];
            NSMutableString *contentSequences = [NSMutableString stringWithString:@">\n"];
            NSString *contentEnd = @"</rdf:Description>\n</rdf:RDF>\n</x:xmpmeta>";
            
            
            NSMutableString *metadata = [[NSMutableString alloc] init];
            
            // default XMP data
            [self addMetadata:metadata field:@"ShotRecordID" value:xmpShot.record_id];
            [self addMetadata:metadata field:@"CaptureID" value:[NSString stringWithFormat:@"%@_%@", xmpShot.record_id, @(captureCounter)]];
            [self addMetadata:metadata field:@"CaptureUUID" value:captureUUID];
            [self addMetadata:metadata field:@"ShotName" value:xmpShot.name];
            [self addMetadata:metadata field:@"FileName" value:fileName];
            
            NSDictionary *shotData = [xmpShot dataForXMP];
            
            for (id key in shotData) {
                NSString* dataValue = [shotData valueForKey:key];
                
                if ([key rangeOfString:@"Date"].location == NSNotFound) {
                    [self addMetadata:metadata field:key value:dataValue];
                }
                else { // Dates
                    NSArray *seqValues = [dataValue componentsSeparatedByString:@"; "];
                    [contentSequences appendFormat:@"  <%@:%@>\n    <rdf:Seq>\n", XMP_PREFIX, key];
                    
                    if (![[seqValues lastObject] isEqualToString:@""]){
                        for (NSString* val in seqValues) {
                            [contentSequences appendFormat:@"      <rdf:li>%@</rdf:li>\n", val];
                        }
                    } else {
                        [contentSequences appendFormat:@"      <rdf:li>%@</rdf:li>\n", [[seqValues firstObject] stringByReplacingOccurrencesOfString:@"; " withString:@""]];
                    }
                    [contentSequences appendFormat:@"    </rdf:Seq>\n  </%@:%@>\n", XMP_PREFIX, key];
                }
                
            }
            
            NSString *xmpDataToWrite = [NSString stringWithFormat:@"%@%@%@%@", contentStart, metadata, contentSequences, contentEnd];
            
            
            //dispatch_async(dispatch_get_main_queue(), ^{
                NSError *xmpWriteError;
                if (![xmpDataToWrite writeToURL:xmpURL atomically:YES encoding:NSUTF8StringEncoding error:&xmpWriteError]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSAlert *alert = [[NSAlert alloc] init];
                        [alert setInformativeText:[xmpWriteError localizedDescription]];
                        [alert setMessageText:@"ERROR Writing XMP"];
                        [alert runModal];
                    });
                } else {
                    DDLogInfo(@"XMP file written to %@", [xmpURL relativeString]);
                }
            //});
        }];
    });
}

- (void) addMetadata:(NSMutableString*)metadata field:(NSString*)field value:(id) value {
    //    // DDLogVerbose(@"returning: '%@:%@=\"%@\"'", xmpPrefix, field, value);
    
    return [metadata appendFormat:@" %@:%@=\"%@\"\n", XMP_PREFIX, field, value];
}



#pragma mark - Naming Convention and Capture Counter
- (void) setNextCaptureNaming
{
    if ([C1Pro isRunning])
    {
        NSString *namingConvention = activeShot.naming_convention;
        if ((namingConvention == nil)||([namingConvention isEqualToString:@""]))
        {
            namingConvention = [activeShot.name stringByReplacingOccurrencesOfString:@" " withString:QBinfo.defaultSeparator];
        }
        namingConvention = [namingConvention stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
        
        [session setCaptureName:namingConvention];
        [session setCaptureNameFormat:captureNameFormat];
        
        if ([captureNameFormat containsString:@"Digit Counter"]) {
          [self checkForExistingFilesContainingName:namingConvention];
        }        
    }
    else
    {
        [[QBDBInfo info] alertMe:_C1RunningError];
        self.isWatching = NO;
    }
}


- (void) resetCaptureCounter {
    //NSInteger newValue = captureCounter + 1;
    NSInteger newValue = captureCounter;
    if (newValue == 0) newValue++;
    
    [session setCaptureCounter:newValue];
    
}

-(void) checkForExistingFilesContainingName:(NSString*)name {
    
    // create a dialog if detecting existing files.
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *existingFiles = [NSMutableArray arrayWithArray: [fileManager contentsOfDirectoryAtURL:self.watchFolder
                                        includingPropertiesForKeys:nil
                                                           options:NSDirectoryEnumerationSkipsHiddenFiles
                                                             error:nil]];
    
    
    [existingFiles filterUsingPredicate:[NSPredicate predicateWithFormat:@"(lastPathComponent contains[c] %@) AND NOT(lastPathComponent contains[c] '.xmp')", name]];
    
    if (existingFiles.count == 0) return;
    
    
    NSMutableArray *filenames = [NSMutableArray array];
                                 
    for (NSURL *url in [NSArray arrayWithArray:existingFiles]) {
        
        NSURL *shortURL = [url URLByDeletingPathExtension];
        NSString *filename = [shortURL lastPathComponent];
        //if (![filenames containsObject:filename])[filenames addObject:filename];
        [filenames addObject:filename];
    }
    
    if (filenames.count > 0)
    {
        
        [filenames sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch | NSNumericSearch | NSDiacriticInsensitiveSearch];
        }];
        
        
//        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\d+$"
//                                                                               options:NSRegularExpressionCaseInsensitive
//                                                                                 error:nil];
        NSRegularExpression *regexAlt = [NSRegularExpression regularExpressionWithPattern:@"\\d+ ?\\d+$"
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        
        
        NSRange range = [regexAlt rangeOfFirstMatchInString:filenames.lastObject
                                                        options:0
                                                          range:NSMakeRange(0, [filenames.lastObject length])];
        
        NSString *counterValue = [[[filenames.lastObject substringWithRange:range] componentsSeparatedByString:@" "] firstObject];
        
        DDLogVerbose(@"Discovered existing files in capture directory. Changing capture counter to highest value: %@", counterValue);
        
        //captureCounter = counterValue.integerValue + 1;
        captureCounter = counterValue.integerValue;
        app.selectedShot.numberOfCaptures = captureCounter;
        [self resetCaptureCounter];
    }
    
    
    
}

#pragma mark -


+(NSSet*) keyPathsForValuesAffectingCaptureID {
    return [NSSet setWithObjects:@"activeShot.record_id", @"captureCounterM1", nil];
}

- (id) eventDidFail:(const AppleEvent *)event withError:(NSError *)error {
    DDLogError(@"C1PC AppleScript Error: %@", error);

#ifdef DEBUG
    [[QBDBInfo info] alertString:[NSString stringWithFormat:@"C1PC Error: %@", error]];
#endif
    return error;
}

#pragma mark - Time Tracking

- (void) beginTimeTracking {
//    if (durationTimer != nil) {
//        if (didBegin) {
//            [self endTimeTracking];
//        }
//    }
    startDate = [NSDate date];
    DDLogInfo(@"Shot '%@' didBegin at %@", activeShot.name, [activeShot prettyDate:startDate]);
    
    durationTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateDuration) userInfo:nil repeats:YES];
    
    [durationTimer fire];
    self.isTiming = YES;
    
}

- (void) updateDuration {
    NSDate *currentDate = [NSDate date];
    
    NSTimeInterval addedDuration = [currentDate timeIntervalSinceDate:startDate];
    startDate = currentDate;
    
    NSTimeInterval existingShotDuration = [activeShot.shotDuration doubleValue];
    NSTimeInterval newDuration = existingShotDuration + addedDuration;
    
    [activeShot setShotDuration:[NSNumber numberWithDouble:newDuration]];
    [activeShot updateDateModified];
    
}

- (void) endTimeTracking {
    if (durationTimer != nil) {
        endDate = [NSDate date];
        
        [durationTimer invalidate];
        durationTimer = nil;
        
        [self updateDuration];
       
        [activeShot setParam:activeShot.durationString forKey:@"Shot Duration"];
        [activeShot updateDateModified];
    }
    
    self.isTiming = NO;
}

#pragma mark - Admin functions -

-(void) generatePanelsAdmin:(NSString *)username Password:(NSString *)password {
   QBDBInfo *qb = [QBDBInfo info];
    
    NSString *clientShortName = [self abbreviatedClientName];
   
    QBTable *shotsTable = [qb getTableByName:@"Shots" inMOC:app.mainMOC];
    QBTable *pdrTable = [qb getTableByName:@"Product Data" inMOC:app.mainMOC];
    
    
    
#pragma mark Folder Locations
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSURL *appSupportFolder = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    
    [fileManager createDirectoryAtURL:[appSupportFolder URLByAppendingPathComponent:@"Adobe/XMP/Custom File Info Panels/4.0/custom/"]
          withIntermediateDirectories:YES
                           attributes:nil
                                error:&error];
    
    [fileManager createDirectoryAtURL:[appSupportFolder URLByAppendingPathComponent:@"Adobe/XMP/Custom File Info Panels/4.0/panels/"]
          withIntermediateDirectories:YES
                           attributes:nil
                                error:&error];
    
    
    [fileManager createDirectoryAtURL:[appSupportFolder URLByAppendingPathComponent:@"Adobe/XMP/Metadata Extensions/"]
          withIntermediateDirectories:YES
                           attributes:nil
                                error:&error];
    
    
    NSURL *bridgePanelURL = [appSupportFolder URLByAppendingPathComponent:@"Adobe/XMP/Custom File Info Panels/4.0/custom/"];
    NSURL *bridgeInfoPanelURL = [appSupportFolder URLByAppendingPathComponent:@"Adobe/XMP/Custom File Info Panels/4.0/panels/"];
    NSURL *metadataExtensionsFolderURL = [appSupportFolder URLByAppendingPathComponent:@"Adobe/XMP/Metadata Extensions/"];
    
    
    
    
#pragma mark Bridge
    // Bridge Panel
    NSMutableString *xmlData = [NSMutableString stringWithFormat:@"<?xml version='1.0' encoding='UTF-8'?>\n"];
    [xmlData appendString:@"<xmp_definitions xmlns:ui='http://ns.adobe.com/xmp/fileinfo/ui/'>\n"];
    [xmlData appendFormat:@"<xmp_schema prefix='%@' namespace='%@' label='$$$/ShotFlowOne/%@/Panels/BridgeEmbedded/EmbeddedLabel=ShotFlow One: %@' description='$$$/ShotFlowOne/%@/Panels/BridgeEmbedded/EmbeddedDescription=Custom XMP metadata for %@'>\n",
     XMP_PREFIX, XMP_NAMESPACE, XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"], XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
    
    [xmlData appendFormat:@"<xmp_property name='ShotName' category='internal' type='text' label='$$$/ShotFlowOne/%@/Bridge/Property/ShotName=Shot Name:'/>\n", XMP_PREFIX];
    [xmlData appendFormat:@"<xmp_property name='FileName' category='internal' type='text' label='$$$/ShotFlowOne/%@/Bridge/Property/FileName=File Name:'/>\n", XMP_PREFIX];
   
  
    
    /**********************************
     Shots XMP data
     **********************************/
    for (QBField* field in shotsTable.fields_xmp) {
        NSString* key = field.name;
        
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
        //DDLogVerbose(@"key: %@, safeKey: %@", key, safeKey);
        
        if (([key containsString:@"Notes"]) || ([key containsString:@"Description"])) {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' ui:multiLine='true' label='$$$/ShotFlowOne/%@/Bridge/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else if ([key containsString:@"Date"]) {
        [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='seq' element_type='date' label='$$$/ShotFlowOne/%@/Bridge/Property/%@=%@:'/>\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else {
        [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/Bridge/Property/%@=%@:'/>\n", safeKey, XMP_PREFIX, safeKey, key];
        }
    }

    
    /**********************************
     Product Data XMP data
     **********************************/
    
    for (QBField* field in pdrTable.fields_xmp) {
        NSString* key = field.name;
        
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
        if (([key rangeOfString:@"Notes"].location != NSNotFound) || ([key rangeOfString:@"Description"].location != NSNotFound)) {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' ui:multiLine='true' label='$$$/ShotFlowOne/%@/Bridge/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else if ([key rangeOfString:@"Date"].location != NSNotFound) {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='seq' element_type='date' label='$$$/ShotFlowOne/%@/Bridge/Property/%@=%@:'/>\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/Bridge/Property/%@=%@:'/>\n", safeKey, XMP_PREFIX, safeKey, key];
        }
    }
    
    [xmlData appendString:@"</xmp_schema>\n</xmp_definitions>"];
    
    
    
    //NSURL *bridgePanelURL = [[fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] URLByAppendingPathComponent:@"Adobe/XMP/Custom File Info Panels/4.0/custom/"];
    

    NSString *schemaFileName = [NSString stringWithFormat:@"ShotFlowOne_%@_Bridge.xml", clientShortName];
    
    if (![xmlData writeToURL:[bridgePanelURL URLByAppendingPathComponent:schemaFileName] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
            NSAlert *alert = [[NSAlert alloc] init];
            [alert setInformativeText:[error localizedDescription]];
            [alert setMessageText:@"ERROR Writing XMP"];
            [alert runModal];
    } else
    {
        if ([fileManager fileExistsAtPath:[[bridgePanelURL URLByAppendingPathComponent:schemaFileName] path]]) {
            DDLogVerbose(@"Bridge Embedded Panel created at %@", [bridgePanelURL URLByAppendingPathComponent:schemaFileName]);
        }
    }

#pragma mark Bridge File Info
    //bridgePanelURL = [[fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] URLByAppendingPathComponent:@"Adobe/XMP/Custom File Info Panels/4.0/panels/"];
    
    NSString *bridgeClientFolder = [NSString stringWithFormat:@"ShotFlowOne_%@_Bridge", clientShortName];
    NSString *propertiesFileName = @"properties.xml";
    NSString *manifestFileName = @"manifest.xml";
    
    
    [fileManager createDirectoryAtURL:[bridgeInfoPanelURL URLByAppendingPathComponent:bridgeClientFolder isDirectory:YES] withIntermediateDirectories:YES attributes:nil error:nil];
    NSURL *appBridgeFileInfoFolder = [bridgeInfoPanelURL URLByAppendingPathComponent:bridgeClientFolder isDirectory:YES];
    
    if ([fileManager fileExistsAtPath:[appBridgeFileInfoFolder path]]) {
        DDLogVerbose(@"Bridge File Info Panel folder created at %@", appBridgeFileInfoFolder);
    }
    
    
    
    
    /**********************************
     Bridge File Info
     **********************************/
    xmlData = [NSMutableString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"];
    [xmlData appendFormat:@"<xmp_definitions xmlns:ui=\"http://ns.adobe.com/xmp/fileinfo/ui/\">\n"];
    [xmlData appendFormat:@"<xmp_schema prefix='%@' namespace='%@' label='$$$/ShotFlowOne/%@/BridgeFI/Panels/MainLabel=ShotFlow One: %@' description='$$$/ShotFlowOne/%@/BridgeFI/Panels/MainDescription=Custom XMP metadata for %@'>\n",
     XMP_PREFIX, XMP_NAMESPACE, XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"and"], XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"and"]];
    
    [xmlData appendFormat:@"<xmp_property name='ShotName' category='internal' type='text' label='$$$/ShotFlowOne/%@/BridgeFI/Property/ShotName=Shot Name:' />\n", XMP_PREFIX];
    [xmlData appendFormat:@"<xmp_property name='FileName' category='internal' type='text' label='$$$/ShotFlowOne/%@/BridgeFI/Property/FileName=File Name:' />\n", XMP_PREFIX];
    [xmlData appendString:@"<ui:separator/>\n"];
    
    /**********************************
     Shots XMP data
     **********************************/
    for (QBField* field in shotsTable.fields_xmp) {
        NSString* key = field.name;
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
        if (([key rangeOfString:@"Notes"].location != NSNotFound) || ([key rangeOfString:@"Description"].location != NSNotFound)) {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' ui:multiLine='true' ui:height='75' label='$$$/ShotFlowOne/%@/BridgeFI/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey,key];
        }
        else if ([key rangeOfString:@"Date"].location != NSNotFound) {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='seq' element_type='date' label='$$$/ShotFlowOne/%@/BridgeFI/Property/%@=%@:'/>\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/BridgeFI/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
    }
    [xmlData appendString:@"<ui:separator/>\n"];
    
    /**********************************
     Product Data XMP data
     **********************************/
    for (QBField* field in pdrTable.fields_xmp) {
        NSString* key = field.name;
        
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
       
        if (([key rangeOfString:@"Notes"].location != NSNotFound) || ([key rangeOfString:@"Description"].location != NSNotFound)) {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' ui:multiLine='true' ui:height='75' label='$$$/ShotFlowOne/%@/BridgeFI/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else if ([key rangeOfString:@"Date"].location != NSNotFound) {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='seq' element_type='date' label='$$$/ShotFlowOne/%@/BridgeFI/Property/%@=%@:'/>\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else {
            [xmlData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/BridgeFI/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
    }
    
    [xmlData appendString:@"</xmp_schema>\n</xmp_definitions>"];
    NSCalendar * calendar = [NSCalendar currentCalendar];
    unsigned int unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:[NSDate date]];
    
    
    
    NSString *modifyDate = [NSString stringWithFormat:@"%@-%@-%@", @([comps year]), @([comps month]), @([comps day])];
    
    // Manifest Data
    
    NSMutableString *manifestData = [NSMutableString stringWithString:@"<xfi:fileinfo xmlns:xfi='http://ns.adobe.com/xmp/fileinfo/'>\n<xfi:panels>\n<xfi:panel \n"];
    [manifestData appendFormat:@"name='%@.bridgeFI'\n", clientShortName];
    [manifestData appendFormat:@"label='$$$/ShotFlowOne/%@/BridgeFI/Panels/Label=ShotFlow One: %@'\n", XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
    [manifestData appendFormat:@"description='$$$/ShotFlowOne/%@/BridgeFI/Panels/Description=Display custom XMP metadata for %@ in Bridge File Info.'\n", XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
    [manifestData appendFormat:@"type='generic'\nvisible='true'\n"];
    
    [manifestData appendFormat:@"propertyDescriptionFile='%@'\n",propertiesFileName];
    [manifestData appendFormat:@"version='%@'\n", @"1.0"];
    [manifestData appendFormat:@"modifyDate='%@Z'>\n", modifyDate];
    
    [manifestData appendString:@"</xfi:panel>\n</xfi:panels>\n</xfi:fileinfo>"];
    
    
    error = nil;
    if (
        (![manifestData writeToURL:[appBridgeFileInfoFolder URLByAppendingPathComponent:manifestFileName] atomically:YES encoding:NSUTF8StringEncoding error:&error]) ||
        (![xmlData writeToURL:[appBridgeFileInfoFolder URLByAppendingPathComponent:propertiesFileName] atomically:YES encoding:NSUTF8StringEncoding error:&error])
        ) {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setInformativeText:[error localizedDescription]];
        [alert setMessageText:@"ERROR Writing XMP"];
        [alert runModal];
    } else {
       // [qb alertString:@"Bridge File Info Panels Created" ];
        
    }
#pragma mark Photoshop File Info
    
    /**********************************
     Photoshop File Info
     **********************************/
    NSString *clientFolder = [NSString stringWithFormat:@"ShotFlowOne_%@", clientShortName];
    manifestFileName = @"manifest.xml";
    schemaFileName = @"schema/schema.xml";
    NSString* viewFileName = @"view/view.xml";
    
    
    //bridgePanelURL = [[fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] URLByAppendingPathComponent:[NSString stringWithFormat:@"Adobe/XMP/"]];
    
    [fileManager createDirectoryAtURL:[metadataExtensionsFolderURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@/schema/", clientFolder] isDirectory:YES] withIntermediateDirectories:YES attributes:nil error:nil];
    
    [fileManager createDirectoryAtURL:[metadataExtensionsFolderURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@/view/", clientFolder] isDirectory:YES] withIntermediateDirectories:YES attributes:nil error:nil];

    NSURL *appMetadataExtensionsFolder = [metadataExtensionsFolderURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@/", clientFolder] isDirectory:YES];
    
    
    /**********************************
     Manifest Data
     **********************************/
    manifestData = [NSMutableString stringWithFormat:
                    @"<?xml version='1.0' encoding='UTF-8'?>\n\
<extension xmlns='http://ns.adobe.com/metadata/extension/1.0/'\n\
\tname='com.shotflowone.%@.extension'\n\
\tversion='%@'>\n\
</extension>",
                    clientShortName,
                    @"1.0"];
    
   
    /**********************************
     Schema Data
     **********************************/
    NSMutableString *schemaData = [NSMutableString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"];
    [schemaData appendFormat:@"<xmp_definitions xmlns:ui=\"http://ns.adobe.com/xmp/fileinfo/ui/\">\n"];
    [schemaData appendFormat:@"<xmp_schema prefix='%@' namespace='%@' label='$$$/ShotFlowOne/%@/PS/Panels/PanelLabel=ShotFlow One: %@' description='$$$/ShotFlowOne/%@/PS/Panels/PanelDescription=Custom XMP metadata for %@'>\n",
     XMP_PREFIX, XMP_NAMESPACE, XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"], XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
    
    
    [schemaData appendFormat:@"<xmp_property name='ShotName' category='internal' type='text' label='$$$/ShotFlowOne/%@/PS/Property/ShotName=Shot Name:'/>\n", XMP_PREFIX];
    [schemaData appendFormat:@"<xmp_property name='FileName' category='internal' type='text' label='$$$/ShotFlowOne/%@/PS/Property/FileName=File Name:'/>\n", XMP_PREFIX];
    [schemaData appendString:@"<ui:separator/>\n"];
    
    /**********************************
     --Shots XMP data
     **********************************/
    
    for (QBField* field in shotsTable.fields_xmp) {
        NSString* key = field.name;
        
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
        if (([key rangeOfString:@"Notes"].location != NSNotFound) || ([key rangeOfString:@"Description"].location != NSNotFound)) {
            [schemaData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/PS/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey,key];
        }
        else if ([key rangeOfString:@"Date"].location != NSNotFound) {
            [schemaData appendFormat:@"<xmp_property name='%@' category='external' type='seq' element_type='date' label='$$$/ShotFlowOne/%@/PS/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
        else {
            [schemaData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/PS/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
    }
    [schemaData appendString:@"<ui:separator/>\n"];
    
    /**********************************
     --Product Data XMP data
     **********************************/
    
    for (QBField* field in pdrTable.fields_xmp) {
        NSString* key = field.name;
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
        if (([key rangeOfString:@"Notes"].location != NSNotFound) || ([key rangeOfString:@"Description"].location != NSNotFound)) {
            [schemaData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/PS/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        } else if ([key rangeOfString:@"Date"].location  != NSNotFound) {
            [schemaData appendFormat:@"<xmp_property name='%@' category='external' type='seq' element_type='date' label='$$$/ShotFlowOne/%@/PS/Property/%@=%@:' />\n", safeKey,XMP_PREFIX, safeKey, key];
        } else {
            [schemaData appendFormat:@"<xmp_property name='%@' category='external' type='text' label='$$$/ShotFlowOne/%@/PS/Property/%@=%@:' />\n", safeKey, XMP_PREFIX, safeKey, key];
        }
    }
    [schemaData appendString:@"</xmp_schema>\n</xmp_definitions>"];
    
    
    /**********************************
     View Data
     **********************************/
    NSMutableString *viewData = [NSMutableString stringWithString:@"<?xml version='1.0' encoding='UTF-8'?>\n<views xmlns='http://ns.adobe.com/metadata/ui/1.0/'>\n<view \n"];
    
    [viewData appendFormat:@"xmlns:%@='%@'\n", XMP_PREFIX, XMP_NAMESPACE];
    [viewData appendFormat:@"name='%@.defaultView'\n", clientShortName];
    [viewData appendFormat:@"label='$$$/ShotFlowOne/%@/PS/Panels/ViewLabel=ShotFlow One: %@'\n", XMP_PREFIX, clientShortName];
    [viewData appendFormat:@"description='$$$/ShotFlowOne/%@/PS/Panels/ViewDescription=Custom XMP metadata for %@'>\n", XMP_PREFIX, [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
    
    [viewData appendFormat:@"<section label='ShotFlow One: %@' type='header'>", [APPCLIENT stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"]];
    [viewData appendFormat:@"<property name='%@:ShotName'/>\n", XMP_PREFIX];
    [viewData appendFormat:@"<property name='%@:FileName'/>\n", XMP_PREFIX];
    
    
    /**********************************
     Shots XMP data
     **********************************/
    [viewData appendString:@"<section label='Shot Metadata' type='header'>\n"];
    for (QBField* field in shotsTable.fields_xmp) {
        NSString* key = field.name;
        
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
        
        if (([key rangeOfString:@"Notes"].location != NSNotFound) || ([key rangeOfString:@"Description"].location != NSNotFound)) {
            ([key rangeOfString:@"Description"].location != NSNotFound) ?
            [viewData appendFormat:@"<property name='%@:%@' multiLines='3' readOnly='true'/>\n", XMP_PREFIX, safeKey] :
            [viewData appendFormat:@"<property name='%@:%@' multiLines='3'/>\n", XMP_PREFIX, safeKey];
        } else {
            [viewData appendFormat:@"<property name='%@:%@'/>\n", XMP_PREFIX, safeKey];
        }
    }
    [viewData appendString:@"</section>\n"];
    
    /**********************************
     Product Data XMP data
     **********************************/
    [viewData appendString:@"<section label='Product Metadata' type='header'>\n"];
    for (QBField* field in pdrTable.fields_xmp) {
        NSString* key = field.name;
        
        if (([key isEqualToString:@"Related Shot"]) || ([key isEqualToString:@"Related Product Data Record"]) || ([key isEqualToString:@"Record ID#"])){
            continue;
        }
        
        NSString *safeKey = [self produceSafeKey:key];
        
        if (([key rangeOfString:@"Notes"].location != NSNotFound) || ([key rangeOfString:@"Description"].location != NSNotFound)) {
            ([key rangeOfString:@"Description"].location != NSNotFound) ?
            [viewData appendFormat:@"<property name='%@:%@' multiLines='3' readOnly='true'/>\n", XMP_PREFIX, safeKey] :
            [viewData appendFormat:@"<property name='%@:%@' multiLines='3'/>\n", XMP_PREFIX, safeKey];
        } else {
            [viewData appendFormat:@"<property name='%@:%@'/>\n", XMP_PREFIX, safeKey];
        }
    }
    [viewData appendString:@"</section>\n"]; // end of product metadata section
    [viewData appendString:@"</section>\n</view>\n</views>"];
    
    error = nil;
    if (
        (![manifestData writeToURL:[appMetadataExtensionsFolder URLByAppendingPathComponent:manifestFileName] atomically:YES encoding:NSUTF8StringEncoding error:&error]) ||
        (![schemaData writeToURL:[appMetadataExtensionsFolder URLByAppendingPathComponent:schemaFileName] atomically:YES encoding:NSUTF8StringEncoding error:&error]) ||
         (![viewData writeToURL:[appMetadataExtensionsFolder URLByAppendingPathComponent:viewFileName] atomically:YES encoding:NSUTF8StringEncoding error:&error])
        ) {
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setInformativeText:[error localizedDescription]];
        [alert setMessageText:@"ERROR Writing XMP"];
        [alert runModal];
    } else {
        [qb alertString:@"File Info Panels Created" ];
        
    }
}

#pragma mark - Safe Key

-(NSString*) produceSafeKey:(NSString*) key {
    
    NSString *safeKey = [key stringByReplacingOccurrencesOfString:@" " withString:@""];
    safeKey = [safeKey stringByReplacingOccurrencesOfString:@"#" withString:@"Number"];
    
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"[^a-zA-Z0-9-_]"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:nil];
    
    safeKey = [regex stringByReplacingMatchesInString:safeKey
                                              options:0
                                                range:NSMakeRange(0, safeKey.length)
                                         withTemplate:@""];

    NSString* firstChar = [safeKey substringToIndex:1];
    NSInteger firstCharNum = [firstChar integerValue];
    
    if (firstCharNum != 0)
    {
        safeKey = [@"_" stringByAppendingString:safeKey];
    }
    
    return safeKey;
}

     - (NSString*) abbreviatedClientName {
         if ([QBinfo.configurationData objectForKey:@"APPCLIENTSHORT"]) {
             return [QBinfo.configurationData valueForKey:@"APPCLIENTSHORT"];
         }


         NSRegularExpression *regexp = [NSRegularExpression
                                        regularExpressionWithPattern:@"([a-z])"
                                        options:0
                                        error:NULL];
         NSString *longClientName = [APPCLIENT stringByReplacingOccurrencesOfString:@" " withString:@""];
         NSString *newName = [regexp
                                stringByReplacingMatchesInString:longClientName
                                options:0
                                range:NSMakeRange(0, longClientName.length)
                                withTemplate:@"$2"];
//         DDLogVerbose(@"new APPCLIENT Short name: %@", newName);
         newName = newName.length < 2? longClientName : newName;
         return newName;
     }



@end
